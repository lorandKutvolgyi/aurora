/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class AppLoggerTest {

    private AppLogger underTest;
    private Logger logger;

    @BeforeEach
    void setUp() {
        logger = mock(Logger.class);
        underTest = new AppLogger(logger);
    }

    @Test
    void info_shouldDoNothing_whenInfoIsNotEnabled() {
        when(logger.isInfoEnabled()).thenReturn(false);

        underTest.info("anything", new Object[0]);

        verify(logger, never()).info(anyString());
    }

    @Test
    void info_shouldCallLoggersInfoWithLongStringAndArgs_whenInfoIsEnabled() {
        Object[] args = new Object[]{"param"};
        when(logger.isInfoEnabled()).thenReturn(true);

        underTest.info("anything", args);

        verify(logger).info("anything");
        verify(logger).info("[param]");
    }
}
