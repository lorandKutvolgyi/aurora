/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;

@Aspect
public class AppLoggerAspect {

    private AppLogger appLogger;

    public AppLoggerAspect() {
        appLogger = new AppLogger(LoggerFactory.getLogger("org.bitbucket.lorandkutvolgyi.AppLogger"));
    }

    @Before("@annotation(org.bitbucket.lorandkutvolgyi.Loggable)")
    public void before(JoinPoint joinPoint) {
        appLogger.info(joinPoint.toLongString(), joinPoint.getArgs());
    }

    void setAppLogger(AppLogger appLogger) {
        this.appLogger = appLogger;
    }
}
