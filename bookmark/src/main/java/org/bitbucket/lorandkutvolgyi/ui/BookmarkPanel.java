/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.BookmarkRemoveController;
import org.bitbucket.lorandkutvolgyi.controller.BookmarkTreeController;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.BookmarkPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.*;

public class BookmarkPanel extends SavablePanel {

    private final transient BookmarkPanelPresenter presenter;
    private final transient BookmarkPopupManager bookmarkPopupManager;
    private final transient BookmarkRemoveController bookmarkRemoveController;
    private final transient BookmarkTreeController bookmarkTreeController;
    private final transient Theme selectedTheme;
    private final transient ResourceBundle messagesBundle;
    private final JTree bookmarkTree;

    public BookmarkPanel(BookmarkPanelPresenter presenter,
                         BookmarkPopupManager bookmarkPopupManager,
                         BookmarkRemoveController bookmarkRemoveController,
                         BookmarkTreeController bookmarkTreeController,
                         Theme selectedTheme,
                         ResourceBundle messagesBundle) {
        this.presenter = presenter;
        this.bookmarkPopupManager = bookmarkPopupManager;
        this.bookmarkRemoveController = bookmarkRemoveController;
        this.bookmarkTreeController = bookmarkTreeController;
        this.bookmarkTree = createTree(presenter.getBookmarkLabels());
        this.selectedTheme = selectedTheme;
        this.messagesBundle = messagesBundle;

        setLayout(new BorderLayout());
        add(createToolbar(), BorderLayout.NORTH);
        add(new JScrollPane(bookmarkTree), BorderLayout.CENTER);
        presenter.getModel().addPropertyChangeListener(this::addNewBookmark);
    }

    @Override
    public void saveState() {
        PreferencesService.put(PreferencesService.Key.COLLAPSED_BOOKMARKS, getCollapsedBookmarks());
    }

    private JTree createTree(Map<String, List<String>> bookmarks) {
        JTree newTree = new JTree(buildNodes(bookmarks));
        newTree.setRootVisible(false);
        newTree.setCellRenderer(new BookmarkCellRenderer());
        newTree.addMouseListener(new TreeListener());
        expandTreeIfNeeded(newTree);
        return newTree;
    }

    private JToolBar createToolbar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setBackground(selectedTheme.getToolBarBackgroundColor());
        toolBar.setFloatable(false);
        toolBar.setMargin(new Insets(3, 4, 2, 0));

        toolBar.add(getCreateButton());
        toolBar.addSeparator(new Dimension(2, 1));
        toolBar.add(getRemoveButton());
        toolBar.addSeparator(new Dimension(5, 1));
        toolBar.add(getExpandAllButton());
        toolBar.addSeparator(new Dimension(2, 1));
        toolBar.add(getCollapseAllButton());
        return toolBar;
    }

    private void addNewBookmark(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("newLabel")) {
            addNew((Map<String, String>) event.getNewValue());
        }
    }

    private String getCollapsedBookmarks() {
        StringBuilder collapsedBookmarks = new StringBuilder();
        for (int i = 0; i < bookmarkTree.getRowCount(); i++) {
            if (bookmarkTree.isCollapsed(i)) {
                collapsedBookmarks.append(i).append("_");
            }
        }
        return withoutLastCharacter(collapsedBookmarks).toString();
    }

    private DefaultMutableTreeNode buildNodes(Map<String, List<String>> bookmarks) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        bookmarks.forEach((category, bookmarksPerCategory) -> {
            DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(category);
            bookmarksPerCategory.forEach(bookmark -> categoryNode.add(new DefaultMutableTreeNode(bookmark)));
            root.add(categoryNode);
        });
        return root;
    }

    private void expandTreeIfNeeded(JTree jTree) {
        for (int i = 0; i < jTree.getRowCount(); i++) {
            if (!presenter.isCollapsed(i)) {
                jTree.expandRow(i);
            }
        }
    }

    private JButton getCreateButton() {
        JButton create = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("create.png"))));
        create.setMargin(new Insets(0, 0, 0, 0));
        create.addActionListener(event -> bookmarkPopupManager.showPopup());
        create.setToolTipText(messagesBundle.getString("BOOKMARK_CREATE"));
        return create;
    }

    private JButton getRemoveButton() {
        JButton remove = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("remove.png"))));
        remove.setMargin(new Insets(0, 0, 0, 0));
        remove.addActionListener(event -> removeSelectedAfterConfirmation());
        remove.setToolTipText(messagesBundle.getString("BOOKMARK_REMOVE"));
        return remove;
    }

    private JButton getExpandAllButton() {
        JButton expandAll = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("expand-all.png"))));
        expandAll.setMargin(new Insets(0, 0, 0, 0));
        expandAll.addActionListener(event -> expandTree(bookmarkTree));
        expandAll.setToolTipText(messagesBundle.getString("BOOKMARK_EXPAND_ALL"));
        return expandAll;
    }

    private JButton getCollapseAllButton() {
        JButton collapseAll = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("collapse-all.png"))));
        collapseAll.setMargin(new Insets(0, 0, 0, 0));
        collapseAll.addActionListener(event -> collapseTree(bookmarkTree));
        collapseAll.setToolTipText(messagesBundle.getString("BOOKMARK_COLLAPSE_ALL"));
        return collapseAll;
    }

    private void addNew(Map<String, String> bookmarkLabel) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) bookmarkTree.getModel().getRoot();
        String category = bookmarkLabel.keySet().stream().findAny().orElseThrow();
        String label = bookmarkLabel.get(category);
        boolean found = addIfCategoryIsFound(root, category, label);
        addWithNewCategoryIfNotFound(root, category, label, found);
        bookmarkTree.updateUI();
    }

    private StringBuilder withoutLastCharacter(StringBuilder builder) {
        if (builder.isEmpty()) {
            return builder;
        }
        return builder.delete(builder.length() - 1, builder.length());
    }

    private void removeSelectedAfterConfirmation() {
        TreePath[] selectionPaths = bookmarkTree.getSelectionModel().getSelectionPaths();
        if (selectionPaths.length != 0 && showOptionDialog() == 0) {
            Arrays.stream(selectionPaths).forEach(this::remove);
        }
    }

    private void expandTree(JTree jTree) {
        for (int i = 0; i < jTree.getRowCount(); i++) {
            jTree.expandRow(i);
        }
    }

    private void collapseTree(JTree jTree) {
        for (int i = 0; i < jTree.getRowCount(); i++) {
            jTree.collapseRow(i);
        }
    }

    private boolean addIfCategoryIsFound(DefaultMutableTreeNode root, String category, String label) {
        boolean found = false;
        for (int i = 0; i < bookmarkTree.getModel().getChildCount(root); i++) {
            DefaultMutableTreeNode currentCategory = (DefaultMutableTreeNode) bookmarkTree.getModel().getChild(root, i);
            if (currentCategory.getUserObject().equals(category)) {
                add(label, currentCategory);
                found = true;
            }
        }
        return found;
    }

    private void addWithNewCategoryIfNotFound(DefaultMutableTreeNode root, String category, String label, boolean found) {
        if (!found) {
            addWithNewCategory(root, category, label);
        }
    }

    private int showOptionDialog() {
        return JOptionPane.showOptionDialog(this.getParent(),
                messagesBundle.getString("REMOVING_BOOKMARK"),
                "",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[]{messagesBundle.getString("YES"), messagesBundle.getString("NO")},
                messagesBundle.getString("YES"));
    }

    private void remove(TreePath treePath) {
        DefaultMutableTreeNode categoryNode = (DefaultMutableTreeNode) treePath.getPath()[1];
        String category = (String) categoryNode.getUserObject();
        if (isCategory(treePath)) {
            removeCategoryWithAllBookmarks(categoryNode, category);
        } else {
            removeBookmark(treePath, category);
        }
        bookmarkTree.updateUI();
    }

    private void add(String label, DefaultMutableTreeNode currentCategory) {
        bookmarkTree.expandPath(new TreePath(currentCategory.getPath()));
        currentCategory.insert(new DefaultMutableTreeNode(label), currentCategory.getChildCount());
    }

    private void addWithNewCategory(DefaultMutableTreeNode root, String category, String label) {
        DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(category);
        root.insert(categoryNode, root.getChildCount());
        DefaultMutableTreeNode bookmarkNode = new DefaultMutableTreeNode(label);
        categoryNode.insert(bookmarkNode, categoryNode.getChildCount());
        bookmarkTree.expandPath(new TreePath(categoryNode.getPath()));
    }

    private boolean isCategory(TreePath treePath) {
        return treePath.getPath().length == 2;
    }

    private void removeCategoryWithAllBookmarks(DefaultMutableTreeNode categoryNode, String category) {
        bookmarkRemoveController.remove(category);
        categoryNode.removeAllChildren();
        categoryNode.removeFromParent();
    }

    private void removeBookmark(TreePath treePath, String category) {
        DefaultMutableTreeNode bookmarkTreeNode = (DefaultMutableTreeNode) treePath.getPath()[2];
        bookmarkRemoveController.remove(category, (String) bookmarkTreeNode.getUserObject());
        DefaultMutableTreeNode categoryTreeNode = (DefaultMutableTreeNode) bookmarkTreeNode.getParent();
        bookmarkTreeNode.removeFromParent();
        if (hasNoBookmark(categoryTreeNode)) {
            bookmarkRemoveController.remove(category);
            categoryTreeNode.removeFromParent();
        }
    }

    private boolean hasNoBookmark(DefaultMutableTreeNode categoryTreeNode) {
        return categoryTreeNode.getChildCount() == 0;
    }

    private static class BookmarkCellRenderer extends DefaultTreeCellRenderer {


        @Override
        public Component getTreeCellRendererComponent(JTree jTree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            if (selected) {
                setClosedIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark-category-selected-closed.png"))));
                setOpenIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark-category-selected-open.png"))));
                setLeafIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark-selected.png"))));
            } else {
                setClosedIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark-category-closed.png"))));
                setOpenIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark-category-open.png"))));
                setLeafIcon(new ImageIcon(Objects.requireNonNull(BookmarkPanel.class.getResource("/bookmark.png"))));
            }
            return super.getTreeCellRendererComponent(jTree, value, selected, expanded, leaf, row, hasFocus);
        }
    }

    private class TreeListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getClickCount() == 2) {
                TreePath path = bookmarkTree.getPathForRow(bookmarkTree.getRowForLocation(mouseEvent.getX(), mouseEvent.getY()));
                if (path != null && path.getPath().length == 3) {
                    bookmarkTreeController.open((String) ((DefaultMutableTreeNode) path.getPath()[2]).getUserObject(),
                            (mouseEvent.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) == InputEvent.CTRL_DOWN_MASK);
                }
            }
        }
    }
}
