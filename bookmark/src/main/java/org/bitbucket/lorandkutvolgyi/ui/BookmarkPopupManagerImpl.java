/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.controller.SaveButtonController;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.BookmarkPopupPresenter;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;

import java.util.ResourceBundle;

public class BookmarkPopupManagerImpl implements BookmarkPopupManager {

    private final BookmarkPopupPresenter bookmarkPopupPresenter;
    private final SaveButtonController saveButtonController;
    private final ResourceBundle messagesBundle;
    private final MessageBus messageBus;
    private final Theme selectedTheme;

    public BookmarkPopupManagerImpl(BookmarkPopupPresenter bookmarkPopupPresenter,
                                    SaveButtonController saveButtonController,
                                    ResourceBundle messagesBundle,
                                    MessageBus messageBus,
                                    Theme selectedTheme) {
        this.bookmarkPopupPresenter = bookmarkPopupPresenter;
        this.saveButtonController = saveButtonController;
        this.messagesBundle = messagesBundle;
        this.messageBus = messageBus;
        this.selectedTheme = selectedTheme;
    }

    @Override
    public void setDefaults(String translationName, String bookName, int chapterNumber, int verseNumber) {
        bookmarkPopupPresenter.setDefaults(translationName, bookName, chapterNumber, verseNumber);
    }

    @Override
    public void showPopup() {
        BookmarkPopup bookmarkPopup = new BookmarkPopup(bookmarkPopupPresenter, saveButtonController, messagesBundle, selectedTheme);
        messageBus.subscribe(bookmarkPopup);
        bookmarkPopup.setVisible(true);
    }
}
