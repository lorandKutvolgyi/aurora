/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.SaveButtonController;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.model.TranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.presenter.BookmarkPopupPresenter;
import org.jdesktop.swingx.prompt.PromptSupport;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import static java.awt.GridBagConstraints.*;

public class BookmarkPopup extends JDialog {

    private final transient BookmarkPopupPresenter bookmarkPopupPresenter;
    private final transient SaveButtonController saveButtonController;
    private final transient ResourceBundle messagesBundle;
    private final transient Theme selectedTheme;
    private final String defaultTranslationName;
    private final String defaultBookName;
    private final String defaultChapterNumber;
    private final String defaultVerseNumber;
    private JComboBox<String> categoryComboBox;
    private JComboBox<String> translationComboBox;
    private JComboBox<String> bookComboBox;
    private JComboBox<String> chapterComboBox;
    private RoundedJTextField versesField;

    public BookmarkPopup(BookmarkPopupPresenter bookmarkPopupPresenter, SaveButtonController saveButtonController,
                         ResourceBundle messagesBundle, Theme selectedTheme) {
        this.bookmarkPopupPresenter = bookmarkPopupPresenter;
        this.saveButtonController = saveButtonController;
        this.messagesBundle = messagesBundle;
        this.selectedTheme = selectedTheme;
        this.defaultTranslationName = bookmarkPopupPresenter.getDefaultTranslation();
        this.defaultBookName = bookmarkPopupPresenter.getDefaultBookName();
        this.defaultChapterNumber = bookmarkPopupPresenter.getDefaultChapterNumber();
        this.defaultVerseNumber = bookmarkPopupPresenter.getDefaultVerse();

        setUp();
    }

    @Subscribe
    public void handleTranslationChangedEvent(TranslationChangedEvent event) {
        setBooksIntoComboBox();
    }

    private void setUp() {
        setModal(true);
        setUndecorated(true);
        setResizable(false);
        setSize(450, 210);
        setLocationByScreenSize();
        setContentPanesBackground();

        JPanel mainPanel = createMainPanel();
        JPanel innerPanel = createInnerPanel();

        innerPanel.add(createCategoryComboBox());
        innerPanel.add(createMiddlePanel());
        innerPanel.add(createVersesField());
        innerPanel.add(createButtonPanel());
        innerPanel.setOpaque(false);

        mainPanel.add(innerPanel);
        mainPanel.setOpaque(false);

        add(mainPanel);
    }

    private void setBooksIntoComboBox() {
        bookComboBox.removeAllItems();
        bookmarkPopupPresenter.getBooks((String) translationComboBox.getSelectedItem()).forEach(bookComboBox::addItem);
    }

    private void setLocationByScreenSize() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - getWidth()) / 2, (screenSize.height - getHeight()) / 2);
    }

    private void setContentPanesBackground() {
        getContentPane().setBackground(selectedTheme.getPopupContentColor());
    }

    private JPanel createMainPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBorder(new LineBorder(selectedTheme.getPopupBorderColor(), 3));
        return mainPanel;
    }

    private JPanel createInnerPanel() {
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));
        innerPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        return innerPanel;
    }

    private JComboBox<String> createCategoryComboBox() {
        categoryComboBox = new JComboBox<>();
        categoryComboBox.setEditable(true);
        categoryComboBox.setOpaque(false);
        categoryComboBox.getEditor().getEditorComponent().setBackground(selectedTheme.getPopupContentColor());
        categoryComboBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
        bookmarkPopupPresenter.getCategories().forEach(categoryComboBox::addItem);
        addEscapeListener(categoryComboBox.getEditor().getEditorComponent());
        return categoryComboBox;
    }

    private JPanel createMiddlePanel() {
        JPanel middlePanel = new JPanel();
        middlePanel.setLayout(new GridBagLayout());
        middlePanel.setOpaque(false);
        middlePanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
        createTranslationComboBox(middlePanel);
        createBookComboBox(middlePanel);
        createChapterComboBox(middlePanel);
        return middlePanel;
    }

    private void addEscapeListener(Component component) {
        component.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if ((component instanceof JComboBox<?> && !((JComboBox<?>) component).isPopupVisible())
                        || (component.getParent() instanceof JComboBox<?> && !((JComboBox<?>) component.getParent()).isPopupVisible())
                        || !(component instanceof JComboBox<?>) && !(component.getParent() instanceof JComboBox<?>)) {
                    if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        e.consume();
                        BookmarkPopup.this.dispose();
                    }
                }
            }
        });
    }

    private void createTranslationComboBox(JPanel middlePanel) {
        translationComboBox = new JComboBox<>();
        translationComboBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        translationComboBox.setBackground(selectedTheme.getPopupContentColor());
        GridBagConstraints translationComboBoxConstraints = new GridBagConstraints();
        translationComboBoxConstraints.gridx = 0;
        translationComboBoxConstraints.gridy = 0;
        translationComboBoxConstraints.gridwidth = 1;
        translationComboBoxConstraints.weightx = 0.25;
        translationComboBoxConstraints.anchor = LINE_START;
        translationComboBoxConstraints.fill = HORIZONTAL;
        middlePanel.add(translationComboBox, translationComboBoxConstraints);
        bookmarkPopupPresenter.getTranslations().forEach(translationComboBox::addItem);
        translationComboBox.setSelectedItem(bookmarkPopupPresenter.getCurrentTranslation());
        translationComboBox.addActionListener(actionEvent -> bookmarkPopupPresenter.selectedTranslationChanged());
        addEscapeListener(translationComboBox);
        if (defaultTranslationName != null) {
            translationComboBox.setSelectedItem(defaultTranslationName);
        }
    }

    private void createBookComboBox(JPanel middlePanel) {
        bookComboBox = new JComboBox<>();
        bookComboBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        bookComboBox.setBackground(selectedTheme.getPopupContentColor());
        bookComboBox.setPrototypeDisplayValue("Mózes első könyve");
        GridBagConstraints bookComboBoxConstraints = new GridBagConstraints();
        bookComboBoxConstraints.gridx = 1;
        bookComboBoxConstraints.gridy = 0;
        bookComboBoxConstraints.gridwidth = 2;
        bookComboBoxConstraints.weightx = 0.5;
        bookComboBoxConstraints.fill = HORIZONTAL;
        middlePanel.add(bookComboBox, bookComboBoxConstraints);
        setBooksIntoComboBox();
        if (defaultBookName != null) {
            bookComboBox.setSelectedItem(defaultBookName);
        }
        bookComboBox.addActionListener(getBookComboBoxActionListener());
        addEscapeListener(bookComboBox);
    }

    private void createChapterComboBox(JPanel middlePanel) {
        chapterComboBox = new JComboBox<>();
        chapterComboBox.setBackground(selectedTheme.getPopupContentColor());
        GridBagConstraints chapterComboBoxConstraints = new GridBagConstraints();
        chapterComboBoxConstraints.gridx = 3;
        chapterComboBoxConstraints.gridy = 0;
        chapterComboBoxConstraints.gridwidth = 1;
        chapterComboBoxConstraints.weightx = 0.25;
        chapterComboBoxConstraints.anchor = LINE_END;
        chapterComboBoxConstraints.fill = HORIZONTAL;
        middlePanel.add(chapterComboBox, chapterComboBoxConstraints);
        setChaptersIntoComboBox();
        if (defaultChapterNumber != null) {
            chapterComboBox.setSelectedItem(defaultChapterNumber);
        }
        addEscapeListener(chapterComboBox);
    }

    private JTextField createVersesField() {
        versesField = new RoundedJTextField.Builder().limit(100).build();
        versesField.getActionMap().put("paste", new NullAction());
        versesField.setBackground(selectedTheme.getInputBackgroundColor());
        versesField.addKeyListener(getKeyListener());
        addEscapeListener(versesField);
        if (defaultVerseNumber != null) {
            versesField.setText(defaultVerseNumber);
        } else {
            PromptSupport.setPrompt(messagesBundle.getString("VERSES"), versesField);
        }
        return versesField;
    }

    private void setChaptersIntoComboBox() {
        chapterComboBox.removeAllItems();
        bookmarkPopupPresenter.getChapterNumbers((String) translationComboBox.getSelectedItem(), (String) bookComboBox.getSelectedItem())
                .forEach(num -> chapterComboBox.addItem(String.valueOf(num)));
    }

    private KeyAdapter getKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent event) {
                char keyChar = event.getKeyChar();
                if ((((keyChar < '0') || (keyChar > '9')) && (keyChar != KeyEvent.VK_BACK_SPACE) && (keyChar != ',') && (keyChar != '-'))) {
                    event.consume();
                }
            }
        };
    }

    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));
        JButton ok = new JButton(messagesBundle.getString("SAVE"));
        ok.addActionListener(event -> handleOk());
        addEscapeListener(ok);
        buttonPanel.add(ok);
        getRootPane().setDefaultButton(ok);
        JButton cancel = new JButton(messagesBundle.getString("CANCEL"));
        cancel.addActionListener(event -> dispose());
        addEscapeListener(cancel);
        buttonPanel.add(cancel);
        return buttonPanel;
    }

    private ActionListener getBookComboBoxActionListener() {
        return actionEvent -> {
            if (bookComboBox.getSelectedItem() != null) {
                setChaptersIntoComboBox();
            }
        };
    }

    private void handleOk() {
        if (save()) {
            dispose();
        }
    }

    private boolean save() {
        return saveButtonController.save((String) categoryComboBox.getSelectedItem(),
                (String) translationComboBox.getSelectedItem(),
                (String) bookComboBox.getSelectedItem(),
                (String) chapterComboBox.getSelectedItem(),
                versesField.getText());
    }

    private static class NullAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            // do nothing to disable pasting into verses fields
        }
    }
}
