/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.dizitart.no2.objects.Id;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Bookmark implements Comparable<Bookmark> {

    @Id
    private UUID id = UUID.randomUUID();
    private String category = "";
    private BookLabel bookLabel;
    private Chapter chapter;
    private List<Integer> verseNumbers;

    @Override
    public int compareTo(Bookmark bookmark) {
        int compareByCategory = category.compareTo(bookmark.getCategory());
        if (compareByCategory == 0) {
            return compareByTranslationBookLabelAndChapterAndVerse(bookmark);
        }
        return compareByCategory;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != Bookmark.class) {
            return false;
        }
        return this.compareTo((Bookmark) other) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, bookLabel, chapter, verseNumbers);
    }

    @Override
    public String toString() {
        return "\u202A" + chapter.getBookName() + "\u202C" + "  " + chapter.getNumber()
                + (!verseNumbers.isEmpty() ? ": " + verseNumbersAsString() : "")
                + " * " + chapter.getTranslationName();
    }

    private String verseNumbersAsString() {
        StringBuilder result = new StringBuilder();
        Integer previous = null;
        for (int current = 0; current < verseNumbers.size(); current++) {
            appendToResult(result, previous, current);
            previous = verseNumbers.get(current);
        }
        return result.toString();
    }

    private void appendToResult(StringBuilder result, Integer previous, int current) {
        if (isFirstNumber(previous)) {
            result.append(verseNumbers.get(current));
        } else if (previous + 1 < verseNumbers.get(current)) {
            result.append(',').append(verseNumbers.get(current));
        } else {
            if (verseNumbers.size() > current + 1 && verseNumbers.get(current) + 1 == verseNumbers.get(current + 1)) {
                if (result.charAt(result.length() - 1) != '-') {
                    result.append('-');
                }
            } else {
                if (result.charAt(result.length() - 1) != '-') {
                    result.append('-').append(verseNumbers.get(current));
                } else {
                    result.append(verseNumbers.get(current));
                }
            }
        }
    }

    private boolean isFirstNumber(Integer previous) {
        return previous == null;
    }

    private int compareByTranslationBookLabelAndChapterAndVerse(Bookmark bookmark) {
        int compareByTranslation = chapter.getTranslationName().compareTo(bookmark.getChapter().getTranslationName());
        if (compareByTranslation == 0) {
            return compareByBookLabelAndChapterAndVerse(bookmark);
        }
        return compareByTranslation;
    }

    private int compareByBookLabelAndChapterAndVerse(Bookmark bookmark) {
        if (bookLabel == bookmark.getBookLabel()) {
            return compareByChapterAndVerse(bookmark);
        }
        return compareByBookLabel(bookmark);
    }

    private int compareByBookLabel(Bookmark bookmark) {
        return Integer.compare(bookLabel.ordinal(), bookmark.bookLabel.ordinal());
    }

    private int compareByChapterAndVerse(Bookmark bookmark) {
        int compareByChapter = Integer.compare(this.chapter.getNumber(), bookmark.getChapter().getNumber());
        if (compareByChapter == 0) {
            return compareByVerse(bookmark);
        }
        return compareByChapter;
    }

    private int compareByVerse(Bookmark bookmark) {
        if (isNeitherEmpty(bookmark)) {
            return compareVerses(bookmark);
        }
        if (isBothEmpty(bookmark)) {
            return 0;
        }
        if (isFirstEmpty(verseNumbers)) {
            return -1;
        }
        return 1;
    }

    private int compareVerses(Bookmark bookmark) {
        int counter = 0;
        for (Integer verseNumber : verseNumbers) {
            int compare = bookmark.getVerseNumbers().size() > counter ?
                    Integer.compare(verseNumber, bookmark.getVerseNumbers().get(counter++)) : 1;
            if (compare != 0) {
                return compare;
            }
        }
        if (verseNumbers.size() < bookmark.verseNumbers.size()) {
            return -1;
        }
        return 0;
    }

    private boolean isBothEmpty(Bookmark bookmark) {
        return isFirstEmpty(verseNumbers) && isSecondEmpty(bookmark);
    }

    private boolean isSecondEmpty(Bookmark bookmark) {
        return bookmark.getVerseNumbers().isEmpty();
    }

    private boolean isFirstEmpty(List<Integer> verseNumbers) {
        return verseNumbers.isEmpty();
    }

    private boolean isNeitherEmpty(Bookmark bookmark) {
        return !verseNumbers.isEmpty() && !bookmark.getVerseNumbers().isEmpty();
    }
}
