/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.model.BookmarksChangedEvent;
import org.bitbucket.lorandkutvolgyi.repository.BookmarkRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class BookmarkService {

    private final BookmarkRepository repository;
    private final Set<Bookmark> bookmarks;
    private final MessageBus messageBus;

    public BookmarkService(BookmarkRepository repository, MessageBus messageBus) {
        this.repository = repository;
        this.bookmarks = repository.getBookmarks();
        this.messageBus = messageBus;
    }

    public void removeBookmarks(String category) {
        Set<Bookmark> toRemove = bookmarks.stream().filter(bookmark -> bookmark.getCategory().equals(category)).collect(Collectors.toSet());
        bookmarks.removeAll(toRemove);
        toRemove.forEach(repository::remove);
    }

    public void removeBookmark(String category, String translationName, String bookName, int chapterNumber, List<Integer> verseNumbers) {
        Optional<Bookmark> toRemove = bookmarks.stream()
                .filter(bookmark -> bookmark.getCategory().equals(category)
                        && bookmark.getVerseNumbers().equals(verseNumbers)
                        && bookmark.getChapter().getTranslationName().equals(translationName)
                        && bookmark.getChapter().getBookName().equals(bookName)
                        && bookmark.getChapter().getNumber() == chapterNumber)
                .findAny();
        toRemove.ifPresent(bm -> {
            bookmarks.remove(bm);
            repository.remove(bm);
        });
    }

    public void addBookmark(Bookmark bookmark) {
        bookmarks.add(bookmark);
        repository.save(bookmark);
        messageBus.publish(new BookmarksChangedEvent(bookmark));
    }

    public Set<Bookmark> getBookmarks() {
        return bookmarks;
    }
}
