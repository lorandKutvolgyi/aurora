/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;

import java.util.ArrayList;
import java.util.List;

import static org.bitbucket.lorandkutvolgyi.controller.BookmarkVersesConverter.verseNumbersAsListOfInteger;

public class BookmarkRemoveController {

    private final BookmarkService bookmarkService;

    public BookmarkRemoveController(BookmarkService bookmarkService) {
        this.bookmarkService = bookmarkService;
    }

    @Loggable
    public void remove(String category) {
        bookmarkService.removeBookmarks(category);
    }

    @Loggable
    public void remove(String category, String bookmark) {
        String[] parts = bookmark.split("[:*]");
        String[] bookAndChapter = parts[0].split(" {2}");
        String translationName = getTranslationName(parts);
        String bookName = getBookName(bookAndChapter);
        int chapterNumber = getChapterNumber(bookAndChapter[1]);
        List<Integer> verses = getVerses(parts);
        bookmarkService.removeBookmark(category, translationName, bookName, chapterNumber, verses);
    }

    private String getTranslationName(String[] parts) {
        return parts[parts.length - 1].trim();
    }

    private String getBookName(String[] bookAndChapter) {
        return bookAndChapter[0].trim().replaceAll("[\u202A\u202C]", "");
    }

    private int getChapterNumber(String chapterNumber) {
        return Integer.parseInt(chapterNumber.trim());
    }

    private List<Integer> getVerses(String[] parts) {
        List<Integer> versesNumbers = new ArrayList<>();
        if (hasVerses(parts)) {
            versesNumbers.addAll(verseNumbersAsListOfInteger(parts[1].trim()));
        }
        return versesNumbers;
    }

    private boolean hasVerses(String[] parts) {
        return parts.length == 3;
    }
}
