/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

public class BookmarkTreeController {

    private final TextPanelService textPanelService;
    private final TranslationService translationService;

    public BookmarkTreeController(TextPanelService textPanelService, TranslationService translationService) {
        this.textPanelService = textPanelService;
        this.translationService = translationService;
    }

    @Loggable
    public void open(String bookmark, boolean controlPressed) {
        openNewTabIfNeeded(controlPressed);
        openChapter(bookmark);
    }

    private void openNewTabIfNeeded(boolean controlPushed) {
        if (controlPushed) {
            textPanelService.openNewTab();
        }
    }

    private void openChapter(String bookmark) {
        String[] parts = bookmark.split("[:*]");
        String translationName = parts[parts.length - 1].trim();
        String bookName = parts[0].split(" {2}")[0].trim().replaceAll("[\u202A\u202C]", "");
        int numberOfChapter = Integer.parseInt(parts[0].split(" {2}")[1].trim());
        translationService.openNewChapter(translationName, bookName, numberOfChapter);
    }
}
