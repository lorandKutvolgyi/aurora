/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

class BookmarkVersesConverter {

    private BookmarkVersesConverter() {
    }

    static List<Integer> verseNumbersAsListOfInteger(String verses) {
        List<Integer> result = new ArrayList<>();
        String[] parts = verses.split(",");
        for (String part : parts) {
            if (part == null || part.isEmpty()) {
                continue;
            }
            addToResult(result, part);
        }
        return result;
    }

    private static void addToResult(List<Integer> result, String part) {
        if (isRange(part)) {
            result.addAll(getListOfIntegersFromRange(part));
        } else {
            result.add(Integer.valueOf(part));
        }
    }

    private static boolean isRange(String part) {
        return part.contains("-");
    }

    private static Collection<Integer> getListOfIntegersFromRange(String part) {
        String[] startAndEnd = part.split("-");
        Collection<Integer> result = new HashSet<>();
        for (int i = Integer.parseInt(startAndEnd[0]); i <= Integer.parseInt(startAndEnd[1]); i++) {
            result.add(i);
        }
        return result;
    }
}
