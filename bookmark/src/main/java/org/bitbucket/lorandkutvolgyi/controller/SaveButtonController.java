/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.UUID;

import static org.bitbucket.lorandkutvolgyi.controller.BookmarkVersesConverter.verseNumbersAsListOfInteger;

public class SaveButtonController {

    private final BookmarkService bookmarkService;
    private final TranslationService translationService;

    public SaveButtonController(BookmarkService bookmarkService, TranslationService translationService) {
        this.bookmarkService = bookmarkService;
        this.translationService = translationService;
    }

    @Loggable
    public boolean save(String category, String translationName, String bookName, String chapterNumber, String verses) {
        if (isVersesTextValid(verses)) {
            Book book = translationService.getBook(translationName, bookName);
            bookmarkService.addBookmark(Bookmark.builder()
                    .id(UUID.randomUUID())
                    .category(category == null ? "" : category)
                    .bookLabel(book.getLabel())
                    .chapter(book.getChapters().get(Integer.parseInt(chapterNumber) - 1))
                    .verseNumbers(verseNumbersAsListOfInteger(verses))
                    .build());
            return true;
        }
        return false;
    }

    private boolean isVersesTextValid(String verses) {
        return verses.trim().isEmpty() || (isMatching(verses) && isOrdered(verses));
    }

    private boolean isMatching(String verses) {
        assert verses.length() <= 100;
        String number = "[1-9][0-9]{0,2}";
        String range = "[1-9][0-9]{0,2}-[1-9][0-9]{0,2}";
        String numbersAfterComma = "(,[1-9][0-9]{0,2})+";
        String rangesAfterComma = "(,[1-9][0-9]{0,2}-[1-9][0-9]{0,2})+";
        return verses.matches("(" + number + "|" + range + ")(" + numbersAfterComma + "|" + rangesAfterComma + ")*");
    }

    private boolean isOrdered(String verses) {
        String[] numbers = verses.split("[,-]");
        for (int i = 0; i < numbers.length - 1; i++) {
            if (Integer.parseInt(numbers[i]) >= Integer.parseInt(numbers[i + 1])) {
                return false;
            }
        }
        return true;
    }
}
