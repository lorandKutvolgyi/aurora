/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.*;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.List;
import java.util.stream.Collectors;

public class BookmarkPopupPresenter {

    private final BookmarkService bookmarkService;
    private final TranslationService translationService;
    private final MessageBus messageBus;
    private String translationName;
    private String bookName;
    private String chapterNumber;
    private String verseNumber;

    public BookmarkPopupPresenter(BookmarkService bookmarkService, TranslationService translationService, MessageBus messageBus) {
        this.bookmarkService = bookmarkService;
        this.translationService = translationService;
        this.messageBus = messageBus;
    }

    public List<String> getCategories() {
        return bookmarkService.getBookmarks()
                .stream()
                .map(Bookmark::getCategory)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> getTranslations() {
        return translationService.getDownloadedTranslations().stream().map(Translation::getName).collect(Collectors.toList());
    }

    public String getCurrentTranslation() {
        return translationService.getCurrentTranslation().getName();
    }

    public List<String> getBooks(String translationName) {
        return translationService.getDownloadedTranslations().stream()
                .filter(translation -> translation.getName().equals(translationName))
                .map(Translation::getBooks)
                .findAny()
                .orElseThrow(IllegalArgumentException::new)
                .stream()
                .map(Book::getName)
                .collect(Collectors.toList());
    }

    public List<Integer> getChapterNumbers(String translationName, String bookName) {
        return translationService.getDownloadedTranslations().stream()
                .filter(translation -> translation.getName().equals(translationName))
                .map(Translation::getBooks)
                .findAny()
                .orElseThrow(IllegalArgumentException::new)
                .stream()
                .filter(book -> book.getName().equals(bookName))
                .findAny()
                .orElseThrow(IllegalArgumentException::new)
                .getChapters()
                .stream()
                .map(Chapter::getNumber)
                .collect(Collectors.toList());
    }

    public void selectedTranslationChanged() {
        messageBus.publish(TranslationChangedEvent.INSTANCE);
    }

    public void setDefaults(String translationName, String bookName, int chapterNumber, int verseNumber) {
        this.translationName = translationName == null ? translationService.getCurrentTranslationName() : translationName;
        this.bookName = bookName;
        this.chapterNumber = chapterNumber == 0 ? "" : String.valueOf(chapterNumber);
        this.verseNumber = verseNumber == 0 ? "" : String.valueOf(verseNumber);
    }

    public String getDefaultTranslation() {
        String result = this.translationName;
        this.translationName = null;
        return result;
    }

    public String getDefaultBookName() {
        String result = this.bookName;
        this.bookName = null;
        return result;
    }

    public String getDefaultChapterNumber() {
        String result = this.chapterNumber;
        this.chapterNumber = null;
        return result;
    }

    public String getDefaultVerse() {
        String result = this.verseNumber;
        this.verseNumber = null;
        return result;
    }
}
