/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.annotations.Subscribe;
import lombok.Getter;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.model.BookmarkPanelModel;
import org.bitbucket.lorandkutvolgyi.model.BookmarksChangedEvent;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class BookmarkPanelPresenter {

    private final BookmarkService service;

    @Getter
    private final BookmarkPanelModel model;

    public BookmarkPanelPresenter(BookmarkService service) {
        this.service = service;
        this.model = new BookmarkPanelModel();
    }

    @Subscribe
    public void publishLabels(BookmarksChangedEvent event) {
        Bookmark bookmark = event.getBookmark();
        model.setNewLabel(bookmark.getCategory(), bookmark.toString());
    }

    public Boolean isCollapsed(int row) {
        String collapsedRowsAsString = PreferencesService.get(PreferencesService.Key.COLLAPSED_BOOKMARKS);
        String[] collapsedRows = collapsedRowsAsString != null ? collapsedRowsAsString.split("_") : new String[]{};
        return Set.of(collapsedRows).contains(String.valueOf(row));
    }

    public Map<String, List<String>> getBookmarkLabels() {
        return getBookmarkLabels(service.getBookmarks());
    }

    private Map<String, List<String>> getBookmarkLabels(Set<Bookmark> bookmarks) {
        Map<String, List<Bookmark>> bookmarkLabels = bookmarks.stream()
                .collect(Collectors.groupingBy(Bookmark::getCategory));
        Map<String, List<String>> result = new TreeMap<>();
        bookmarkLabels.forEach((key, value) ->
                result.put(key, value.stream().map(Bookmark::toString).collect(Collectors.toList())));
        return result;
    }
}
