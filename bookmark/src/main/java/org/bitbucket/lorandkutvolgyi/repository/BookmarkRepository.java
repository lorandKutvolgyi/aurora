/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.util.Set;
import java.util.TreeSet;

import static org.dizitart.no2.Nitrite.builder;

public class BookmarkRepository {

    private final ObjectRepository<Bookmark> repository;

    public BookmarkRepository(String userHome) {
        Nitrite bookmarksDb = builder().compressed().filePath(userHome + "/.aurora/db/bookmarks.db").openOrCreate();
        repository = bookmarksDb.getRepository(Bookmark.class);
    }

    public Set<Bookmark> getBookmarks() {
        return new TreeSet<>(repository.find().toList());
    }

    public void save(Bookmark bookmark) {
        repository.insert(bookmark);
    }

    public void remove(Bookmark bookmark) {
        repository.remove(bookmark);
    }
}
