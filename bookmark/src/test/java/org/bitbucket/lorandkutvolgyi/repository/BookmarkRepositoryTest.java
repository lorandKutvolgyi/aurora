/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;

@Slf4j
class BookmarkRepositoryTest {

    private static BookmarkRepository underTest;

    @BeforeAll
    static void setUp() throws IOException {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
        Files.createDirectories(Paths.get("src/test/resources/home/.aurora/db"));
        underTest = new BookmarkRepository(new File("src/test/resources/home").getAbsolutePath());
        underTest.getBookmarks().forEach(bookmark -> underTest.remove(bookmark));
    }

    @AfterAll
    static void tearDown() throws IOException {
        Files.deleteIfExists(Paths.get("src/test/resources/home/.aurora/db/bookmarks.db"));
        Files.deleteIfExists(Paths.get("src/test/resources/home/.aurora/db/"));
        Files.deleteIfExists(Paths.get("src/test/resources/home/.aurora/"));
        Files.deleteIfExists(Paths.get("src/test/resources/home/"));
        Files.deleteIfExists(Paths.get("src/test/resources/"));
        try {
            Files.deleteIfExists(Paths.get("src/test/"));
        } catch (DirectoryNotEmptyException ex) {
            log.info("Run from {} folder", Paths.get(".").toAbsolutePath());
        }
    }

    @AfterEach
    void removeAllBookmarks() {
        underTest.getBookmarks().forEach(bookmark -> underTest.remove(bookmark));
    }

    @Test
    void getBookmarks_shouldReturnEmptySet_whenItRunsFirst() {
        Set<Bookmark> result = underTest.getBookmarks();

        assertEquals(0, result.size());
    }

    @Test
    void getBookmarks_shouldReturnWhatWasSaved() {
        Bookmark bookmark = createBookmark(BookLabel.MATTHEW);
        underTest.save(bookmark);

        Set<Bookmark> result = underTest.getBookmarks();

        assertEquals(1, result.size());
        assertEquals(bookmark.getId(), result.iterator().next().getId());
    }

    @Test
    void getBookmarks_shouldNotReturnWhatWasRemoved() {
        Bookmark toRemove = createBookmark(BookLabel.MARK);
        underTest.save(toRemove);
        underTest.save(createBookmark(BookLabel.LUKE));
        underTest.remove(toRemove);

        Set<Bookmark> result = underTest.getBookmarks();

        assertEquals(1, result.size());
        assertNotEquals(toRemove.getId(), result.iterator().next().getId());
    }

    private Bookmark createBookmark(BookLabel bookLabel) {
        return Bookmark.builder()
                .id(UUID.randomUUID())
                .category("cat")
                .chapter(new Chapter("RÚF", bookLabel.name(), 1, new ArrayList<>(), "", new HashMap<>()))
                .bookLabel(bookLabel)
                .verseNumbers(new ArrayList<>())
                .build();
    }
}
