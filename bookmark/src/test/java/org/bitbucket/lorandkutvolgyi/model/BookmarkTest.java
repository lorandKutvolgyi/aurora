/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;

class BookmarkTest {

    private Bookmark underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        underTest = new Bookmark();
    }

    @Test
    void compareTo_shouldWorkByCategory() {
        underTest.setCategory("categoryA");
        Bookmark comparable = Bookmark.builder()
                .category("categoryB")
                .build();

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByTranslation_whenCategoriesAreIdentical() {
        String category = "category";
        underTest.setCategory(category);
        underTest.setChapter(new Chapter("translationA", null, 0, null, null, null));
        Bookmark comparable = Bookmark.builder()
                .category(category)
                .chapter(new Chapter("translationB", null, 0, null, null, null))
                .build();

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByBookLabel_whenCategoriesAndTranslationsAreIdentical() {
        String category = "category";
        String translation = "translation";
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, 0, null, null, null));
        underTest.setBookLabel(BookLabel.GENESIS);
        Bookmark comparable = Bookmark.builder()
                .category(category)
                .chapter(new Chapter(translation, null, 0, null, null, null))
                .bookLabel(BookLabel.ZECHARIAH)
                .build();

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByChapter_whenCategoriesAndTranslationsAndBookLabelsAreIdentical() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, 1, null, null, null));
        underTest.setBookLabel(bookLabel);
        Bookmark comparable = Bookmark.builder()
                .category(category)
                .chapter(new Chapter(translation, null, 150, null, null, null))
                .bookLabel(bookLabel)
                .build();

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdentical() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Arrays.asList(1, 2, 3));
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Arrays.asList(1, 2, 4));

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdenticalAndFirstVersesListIsShorter() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Arrays.asList(1, 2));
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Arrays.asList(1, 2, 3));

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdenticalAndFirstVersesListIsEmpty() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Collections.emptyList());
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Collections.singletonList(1));

        int result = underTest.compareTo(comparable);

        assertEquals(-1, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdenticalAndSecondVersesListIsEmpty() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Collections.singletonList(1));
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Collections.emptyList());

        int result = underTest.compareTo(comparable);

        assertEquals(1, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdenticalAndVersesListsAreEmpty() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Collections.emptyList());
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Collections.emptyList());

        int result = underTest.compareTo(comparable);

        assertEquals(0, result);
    }

    @Test
    void compareTo_shouldWorkByVerses_whenCategoriesAndTranslationsAndBookLabelsAndChaptersAreIdenticalAndVersesListsAreIdentical() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Arrays.asList(1, 2, 3));
        Bookmark comparable = getBookmark(category, translation, bookLabel, chapter, Arrays.asList(1, 2, 3));

        int result = underTest.compareTo(comparable);

        assertEquals(0, result);
    }

    @Test
    void equals_shouldWorkByCategoriesAndTranslationsAndBookLabelsAndChaptersAndVerses() {
        String category = "category";
        String translation = "translation";
        BookLabel bookLabel = BookLabel.GENESIS;
        int chapter = 1;
        underTest.setCategory(category);
        underTest.setChapter(new Chapter(translation, null, chapter, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Arrays.asList(1, 2, 3));
        Set<Bookmark> holder = new HashSet<>();
        holder.add(underTest);
        holder.add(getBookmark(category, translation, bookLabel, chapter, Arrays.asList(1, 2, 3)));

        assertEquals(1, holder.size());

        holder.add(null);
        assertEquals(2, holder.size());
    }


    @Test
    void equals_shouldReturnFalse_whenClassesAreDifferent() {
        assertNotEquals(underTest, new Object());
    }

    @Test
    void toString_shouldProduceBookNameChapterVersesAndTranslation() {
        BookLabel bookLabel = BookLabel.GENESIS;
        underTest.setCategory("category");
        underTest.setChapter(new Chapter("NIV", "Genesis", 1, null, null, null));
        underTest.setBookLabel(bookLabel);
        underTest.setVerseNumbers(Arrays.asList(1, 2, 3, 5, 7, 8));

        String toString = underTest.toString();

        assertEquals("\u202A" + "Genesis" + "\u202C  1: 1-3,5,7-8 * NIV", toString);
    }

    private Bookmark getBookmark(String category, String translation, BookLabel bookLabel, int chapter, List<Integer> verseNumbers) {
        return Bookmark.builder()
                .category(category)
                .chapter(new Chapter(translation, null, chapter, null, null, null))
                .bookLabel(bookLabel)
                .verseNumbers(verseNumbers)
                .build();
    }
}
