/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.*;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.COLLAPSED_BOOKMARKS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookmarkPanelPresenterTest {

    private BookmarkPanelPresenter underTest;
    private BookmarkService service;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        service = mock(BookmarkService.class);
        underTest = new BookmarkPanelPresenter(service);
    }

    @Test
    void getBookmarks_shouldReturnTheResultOfToStringOfWhatGotFromServiceByCategories() {
        Bookmark bookmark1 = getBookmark("categoryA", "NIV", "Acts", 1, Arrays.asList(1, 2, 3), BookLabel.ACTS);
        Bookmark bookmark2 = getBookmark("categoryB", "RÚF", "Matthew", 5, Arrays.asList(1, 3), BookLabel.MATTHEW);
        Bookmark bookmark3 = getBookmark("categoryA", "NIV", "Acts", 2, Arrays.asList(1, 2, 4), BookLabel.ACTS);
        when(service.getBookmarks()).thenReturn(new HashSet<>(Arrays.asList(bookmark1, bookmark2, bookmark3)));

        Map<String, List<String>> result = underTest.getBookmarkLabels();

        assertEquals(2, result.size());
        assertEquals(2, result.get("categoryA").size());
        assertEquals(1, result.get("categoryB").size());
        assertEquals(bookmark1.toString(), result.get("categoryA").get(0));
        assertEquals(bookmark3.toString(), result.get("categoryA").get(1));
        assertEquals(bookmark2.toString(), result.get("categoryB").get(0));
    }

    @Test
    void setNewLabel_shouldBeSetIntoTheModel() {
        Bookmark bookmark = getBookmark("categoryA", "NIV", "Acts", 1, Arrays.asList(1, 2, 3), BookLabel.ACTS);
        Map<String, String> expectedResult = new HashMap<>();
        expectedResult.put("categoryA", bookmark.toString());

        underTest.publishLabels(new BookmarksChangedEvent(bookmark));
        BookmarkPanelModel model = underTest.getModel();

        assertEquals(expectedResult, model.getNewLabel());
    }

    @Test
    void isCollapsed_shouldReturnFalse_whenThereIsNoSavedState() {
        Boolean result = underTest.isCollapsed(1);
        assertFalse(result);
    }

    @Test
    void isCollapsed_shouldReturnFalse_forRowWhichWasNotCollapsedBeforeClosingTheApplication() {
        PreferencesService.put(COLLAPSED_BOOKMARKS, "2_3");

        Boolean result = underTest.isCollapsed(1);

        assertFalse(result);

        PreferencesService.remove(COLLAPSED_BOOKMARKS);
    }

    @Test
    void isCollapsed_shouldReturnTrue_forRowWhichWasCollapsedBeforeClosingTheApplication() {
        PreferencesService.put(COLLAPSED_BOOKMARKS, "1_2_3");

        Boolean result = underTest.isCollapsed(1);

        assertTrue(result);

        PreferencesService.remove(COLLAPSED_BOOKMARKS);
    }

    private Bookmark getBookmark(String category, String translation, String bookName, int chapter, List<Integer> verseNumbers, BookLabel bookLabel) {
        return Bookmark.builder()
                .category(category)
                .chapter(new Chapter(translation, bookName, chapter, null, null, null))
                .bookLabel(bookLabel)
                .verseNumbers(verseNumbers)
                .build();
    }
}
