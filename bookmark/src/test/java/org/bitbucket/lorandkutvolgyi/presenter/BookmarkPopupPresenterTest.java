/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.*;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BookmarkPopupPresenterTest {

    private static Bookmark bookmark1;
    private static Bookmark bookmark2;
    private static Bookmark bookmark3;

    private BookmarkPopupPresenter underTest;
    private BookmarkService bookmarkService;
    private TranslationService translationService;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
        bookmark1 = getBookmark("categoryA", "NIV", "Acts", 1, Arrays.asList(1, 2, 3), BookLabel.ACTS);
        bookmark2 = getBookmark("categoryB", "RÚF", "Matthew", 12, Arrays.asList(1, 3), BookLabel.MATTHEW);
        bookmark3 = getBookmark("categoryA", "NIV", "Acts", 2, Arrays.asList(1, 2, 4), BookLabel.ACTS);

    }

    @BeforeEach
    void setUp() {
        bookmarkService = mock(BookmarkService.class);
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
        underTest = new BookmarkPopupPresenter(bookmarkService, translationService, messageBus);
    }

    @Test
    void getCategories_shouldReturnDistinctCategoryNamesOfBookmarkServicesResult() {
        when(bookmarkService.getBookmarks()).thenReturn(getBookmarks());

        List<String> result = underTest.getCategories();

        assertEquals(2, result.size());
        assertTrue(result.contains(bookmark1.getCategory()));
        assertTrue(result.contains(bookmark2.getCategory()));
    }

    @Test
    void getTranslations_shouldReturnNamesOfTranslationsReturnedByTranslationService() {
        when(translationService.getDownloadedTranslations()).thenReturn(getTranslations());

        List<String> result = underTest.getTranslations();

        assertEquals(2, result.size());
        assertTrue(result.contains("NIV"));
        assertTrue(result.contains("RÚF"));
    }

    @Test
    void getCurrentTranslations_shouldReturnNameOfTranslationReturnedByTranslationService() {
        Translation niv = new Translation();
        niv.setName("NIV");
        when(translationService.getCurrentTranslation()).thenReturn(niv);

        String result = underTest.getCurrentTranslation();

        assertEquals("NIV", result);
    }

    @Test
    void getBooks_shouldReturnNameOfTranslationReturnedByTranslationService() {
        Translation niv = new Translation();
        niv.setName("EFO");
        ArrayList<Book> newTestament = new ArrayList<>();
        newTestament.add(new Book("Máté", null, null));
        newTestament.add(new Book("Márk", null, null));
        niv.setNewTestament(newTestament);
        when(translationService.getDownloadedTranslations()).thenReturn(Collections.singletonList(niv));

        List<String> result = underTest.getBooks("EFO");

        assertEquals(2, result.size());
        assertEquals("Máté", result.get(0));
        assertEquals("Márk", result.get(1));
    }

    @Test
    void getChapterNumbers_shouldReturnNameOfTranslationReturnedByTranslationService() {
        Translation niv = new Translation();
        niv.setName("EFO");
        ArrayList<Book> newTestament = new ArrayList<>();
        ArrayList<Chapter> chapters = new ArrayList<>();
        chapters.add(new Chapter("EFO", "Máté", 1, new ArrayList<>(), null, new HashMap<>()));
        chapters.add(new Chapter("EFO", "Máté", 2, new ArrayList<>(), null, new HashMap<>()));
        newTestament.add(new Book("Máté", null, chapters));
        newTestament.add(new Book("Márk", null, null));
        niv.setNewTestament(newTestament);
        when(translationService.getDownloadedTranslations()).thenReturn(Collections.singletonList(niv));

        List<Integer> result = underTest.getChapterNumbers("EFO", "Máté");

        assertEquals(2, result.size());
        assertEquals(1, result.get(0));
        assertEquals(2, result.get(1));
    }

    @Test
    void selectedTranslationChanged_shouldPublishTheTranslationToMessageBus() {
        underTest.selectedTranslationChanged();

        verify(messageBus).publish(TranslationChangedEvent.INSTANCE);
    }

    @Test
    void setDefaults_shouldSetDefaultsForOnlyOneTime() {
        underTest.setDefaults("translationName", "bookName", 1, 2);

        assertEquals("translationName", underTest.getDefaultTranslation());
        assertNull(underTest.getDefaultTranslation());
        assertEquals("bookName", underTest.getDefaultBookName());
        assertNull(underTest.getDefaultBookName());
        assertEquals("1", underTest.getDefaultChapterNumber());
        assertNull(underTest.getDefaultChapterNumber());
        assertEquals("2", underTest.getDefaultVerse());
        assertNull(underTest.getDefaultVerse());
    }

    private static Bookmark getBookmark(String category, String translation, String bookName, int chapter, List<Integer> verseNumbers, BookLabel bookLabel) {
        return Bookmark.builder()
                .category(category)
                .chapter(new Chapter(translation, bookName, chapter, null, null, null))
                .bookLabel(bookLabel)
                .verseNumbers(verseNumbers)
                .build();
    }

    private List<Translation> getTranslations() {
        Translation niv = new Translation();
        niv.setName("NIV");
        Translation ruf = new Translation();
        ruf.setName("RÚF");
        return Arrays.asList(niv, ruf);
    }

    private Set<Bookmark> getBookmarks() {
        return new HashSet<>(Arrays.asList(bookmark1, bookmark2, bookmark3));
    }
}
