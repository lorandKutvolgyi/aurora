/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class BookmarkRemoveControllerTest {

    private BookmarkRemoveController underTest;
    private BookmarkService bookmarkService;

    @BeforeEach
    void setUp() {
        bookmarkService = mock(BookmarkService.class);
        underTest = new BookmarkRemoveController(bookmarkService);
    }

    @Test
    void removeByCategory_shouldDelegateTheCallToService() {
        String category = "category";

        underTest.remove(category);

        verify(bookmarkService).removeBookmarks(category);
    }

    @Test
    void removeByCategoryAndBookmark_shouldDelegateTheCallToService() {
        String category = "category";
        String bookmark = "Matthew  10: 3-5,7,,9-13 * NIV";

        underTest.remove(category, bookmark);

        verify(bookmarkService).removeBookmark(category, "NIV", "Matthew", 10,
                Arrays.asList(3, 4, 5, 7, 9, 10, 11, 12, 13));
    }
}
