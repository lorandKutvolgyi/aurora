/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.BookmarkService;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SaveButtonControllerTest {

    private BookmarkService bookmarkService;
    private TranslationService translationService;
    private SaveButtonController underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        bookmarkService = mock(BookmarkService.class);
        translationService = mock(TranslationService.class);
        underTest = new SaveButtonController(bookmarkService, translationService);
    }

    @Test
    void save_shouldCallServiceToAddBookmarkIfVersesTextIsEmpty() {
        when(translationService.getBook("EFO", "Máté")).thenReturn(getBook());

        underTest.save("category", "EFO", "Máté", "1", "");

        verify(bookmarkService).addBookmark(Bookmark.builder()
                .category("category")
                .bookLabel(BookLabel.MATTHEW)
                .chapter(getChapter(new ArrayList<>()))
                .verseNumbers(new ArrayList<>())
                .build());
    }

    @Test
    void save_shouldCallServiceToAddBookmarkIfVersesTextIsValid() {
        List<Integer> verseNumbers = Arrays.asList(1, 2, 3, 5, 7, 8, 9, 10, 11, 13, 31);
        when(translationService.getBook("EFO", "Máté")).thenReturn(getBook());

        underTest.save("category", "EFO", "Máté", "1", "1-3,5,7-11,13,31");

        verify(bookmarkService).addBookmark(Bookmark.builder()
                .category("category")
                .bookLabel(BookLabel.MATTHEW)
                .chapter(getChapter(getVerses(verseNumbers)))
                .verseNumbers(verseNumbers)
                .build());
    }

    @Test
    void save_shouldCallServiceToAddBookmarkIfVersesTextIsValidAndLong() {
        List<Integer> verseNumbers =
                Arrays.asList(1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19, 21, 22, 23, 25, 26, 27, 29, 30, 31, 33, 34, 35, 37, 38,
                        39, 41, 42, 43, 45, 46, 47, 49, 50, 51, 53, 54, 55, 57, 58, 59, 61, 62, 63, 65, 66, 67, 100);
        when(translationService.getBook("EFO", "Máté")).thenReturn(getBook());

        underTest.save("category", "EFO", "Máté", "1",
                "1-3,5-7,9-11,13-15,17-19,21-23,25-27,29-31,33-35,37-39,41-43,45-47,49-51,53-55,57-59,61-63,65-67,100");

        verify(bookmarkService).addBookmark(Bookmark.builder()
                .category("category")
                .bookLabel(BookLabel.MATTHEW)
                .chapter(getChapter(getVerses(verseNumbers)))
                .verseNumbers(verseNumbers)
                .build());
    }

    @Test
    void save_shouldNotCallServiceToAddBookmarkIfVersesTextContainsUnOrderedNumbers() {
        when(translationService.getBook("EFO", "Máté")).thenReturn(getBook());

        underTest.save("category", "EFO", "Máté", "1", "1-3,5,7-11,10,31");

        verify(bookmarkService, never()).addBookmark(any());
    }

    @Test
    void save_shouldNotCallServiceToAddBookmarkIfVersesTextContainsInvalidCharacterSequence() {
        when(translationService.getBook("EFO", "Máté")).thenReturn(getBook());

        underTest.save("category", "EFO", "Máté", "1", "1-3,5,7-10-11,31");

        verify(bookmarkService, never()).addBookmark(any());
    }

    private Book getBook() {
        List<Chapter> chapters = new ArrayList<>();
        chapters.add(getChapter(new ArrayList<>()));
        return new Book("Máté", BookLabel.MATTHEW, chapters);
    }

    private Chapter getChapter(List<Verse> verses) {
        return new Chapter("EFO", "Máté", 1, verses, "", new HashMap<>());
    }

    private List<Verse> getVerses(List<Integer> verseNumbers) {
        return verseNumbers.stream().map(this::createVerse).collect(Collectors.toList());
    }

    private Verse createVerse(Integer num) {
        return new Verse(num, "text", "", "EFO", "Máté", BookLabel.MATTHEW, 1, new ArrayList<>());
    }
}
