/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class BookmarkTreeControllerTest {

    private BookmarkTreeController underTest;
    private TranslationService translationService;
    private TextPanelService textPanelService;

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        textPanelService = mock(TextPanelService.class);
        underTest = new BookmarkTreeController(textPanelService, translationService);
    }

    @Test
    void open_shouldCallTextPanelServiceToOpenNewTab_whenControlPushedParameterIsTrue() {
        String bookmark = "\u202AMáté\u202C  1: 1 * EFO";

        underTest.open(bookmark, true);

        verify(textPanelService).openNewTab();
    }

    @Test
    void open_shouldCallTranslationServiceToOpenNewChapter() {
        String bookmark = "\u202AMáté\u202C  1: 1-2,4 * EFO";

        underTest.open(bookmark, false);

        verify(translationService).openNewChapter("EFO", "Máté", 1);
    }
}
