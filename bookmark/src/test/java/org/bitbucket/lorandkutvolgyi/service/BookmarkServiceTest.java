/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Bookmark;
import org.bitbucket.lorandkutvolgyi.model.BookmarksChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.repository.BookmarkRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class BookmarkServiceTest {

    private BookmarkRepository bookmarkRepository;
    private BookmarkService underTest;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        bookmarkRepository = mock(BookmarkRepository.class);
        messageBus = mock(MessageBus.class);

        when(bookmarkRepository.getBookmarks()).thenReturn(new HashSet<>());

        underTest = new BookmarkService(bookmarkRepository, messageBus);
    }

    @Test
    void addBookmark_shouldSaveIt() {
        Bookmark bookmark = createBookmark("Category", BookLabel.MARK, "Mark", 2);

        underTest.addBookmark(bookmark);

        verify(bookmarkRepository).save(bookmark);
        verify(messageBus).publish(new BookmarksChangedEvent(bookmark));
        assertTrue(underTest.getBookmarks().contains(bookmark));
    }

    @Test
    void removeBookmark_shouldDeleteIt() {
        Bookmark bookmark = createBookmark("Category", BookLabel.MATTHEW, "Matthew", 1);
        underTest.addBookmark(bookmark);

        underTest.removeBookmark("Category", "NIV", "Matthew", 1, Collections.singletonList(1));

        verify(bookmarkRepository).remove(bookmark);
        assertTrue(underTest.getBookmarks().isEmpty());
    }

    @Test
    void removeBookmarks_shouldDeleteAllWithGivenCategory() {
        String category = "Category";
        Bookmark matthew1WithCategory = createBookmark(category, BookLabel.MATTHEW, "Matthew", 1);
        Bookmark mark2WithCategory = createBookmark(category, BookLabel.MARK, "Mark", 2);
        Bookmark bookmarkWithDifferentCategory = createBookmark("Different category", BookLabel.MARK, "Mark", 2);
        underTest.addBookmark(matthew1WithCategory);
        underTest.addBookmark(mark2WithCategory);
        underTest.addBookmark(bookmarkWithDifferentCategory);

        underTest.removeBookmarks(category);

        verify(bookmarkRepository).remove(matthew1WithCategory);
        verify(bookmarkRepository).remove(mark2WithCategory);
        assertEquals(1, underTest.getBookmarks().size());
        assertTrue(underTest.getBookmarks().contains(bookmarkWithDifferentCategory));
    }

    private Bookmark createBookmark(String category, BookLabel bookLabel, String bookName, int chapterNumber) {
        return new Bookmark(UUID.randomUUID(), category,
                bookLabel,
                new Chapter("NIV", bookName, chapterNumber, new ArrayList<>(), "comments", new HashMap<>()),
                Collections.singletonList(1));
    }
}
