::
:: Copyright (c) 2021-2023 Lóránd Kútvölgyi. This file is part of Aurora.
::
::                               Aurora is free software: you can redistribute it and/or modify
::                               it under the terms of the GNU General Public License as published by
::                               the Free Software Foundation, either version 3 of the License, or
::                               (at your option) any later version.
::
::                               Aurora is distributed in the hope that it will be useful,
::                               but WITHOUT ANY WARRANTY; without even the implied warranty of
::                               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
::                               GNU General Public License for more details.
::
::                               You should have received a copy of the GNU General Public License
::                               along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
::
chcp 65001 > NUL

SET /p VERSION=<target\result\version
jlink --output jdk-17 --module-path C:\Users\User\build\jdk-17.0.1\jmods --add-modules ALL-MODULE-PATH
jpackage --type "msi" --input ".\target\result" --icon "..\common\src\main\resources\logo.ico" --vendor "Lóránd Kútvölgyi" ^
--description "Aurora Bible Study App" --copyright "Copyright (c) 2021-2023 Lóránd Kútvölgyi" --main-jar "distribution-%VERSION%.jar" ^
--name "Aurora" --license-file  "resources\copyrightWithLicense" --app-version %VERSION%  --win-shortcut --win-menu ^
--resource-dir ".\resources" --java-options "-splash:'C:\Program Files\Aurora\app\splash.png' -Dorg.eclipse.swt.browser.DefaultType=edge" ^
--runtime-image jdk-17
echo y|rmdir /s jdk-17

