#!/bin/sh
#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#
jlink --output jdk-17 --module-path "$JAVA_HOME"/jmods --add-modules ALL-MODULE-PATH

jpackage --type deb --input ./target/result --icon "$LOGO_DIR"/logo.png --install-dir /opt \
--vendor "Lóránd Kútvölgyi"  --copyright "Copyright (c) 2021-2023 Lóránd Kútvölgyi" \
--main-jar distribution-"$(head -n 1 ./target/result/version)".jar --name Aurora \
--license-file  "$LICENSE_FILE_DIR"/gpl-3.0.txt --app-version "$(head -n 1 ./target/result/version)" \
--linux-menu-group "Education" --resource-dir ./resources --linux-deb-maintainer aurorabible.app@gmail.com \
--java-options "-splash:/opt/aurora/lib/app/splash.png" --runtime-image jdk-17

rm -Rf ./jdk-17
