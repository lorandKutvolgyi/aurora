/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;

import static java.awt.Color.BLACK;

public class DefaultTheme implements Theme {

    private static final String FONT_NAME = Font.SANS_SERIF;

    public DefaultTheme() {
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        HTMLEditorKit editorKit = new HTMLEditorKit();
        StyleSheet styleSheet = editorKit.getStyleSheet();
        styleSheet.addRule("body { color: #3B525C; font-family: sans-serif; margin: 4px; font-size: 14px; }");
        styleSheet.addRule("span.number { color: #000; font-family: sans-serif; margin: 4px; font-size: 11px; }");
        styleSheet.addRule("a { color: #668795; font-family: sans-serif; margin: 4px; font-size: 12px; }");
        styleSheet.addRule("a.disabled { color: #dddddd; font-family: sans-serif; margin: 4px; font-size: 12px; }");

        UIManager.put("Tree.font", new Font(FONT_NAME, Font.BOLD, 14));
        UIManager.put("TabbedPane.font", new Font(FONT_NAME, Font.BOLD, 14));
        UIManager.put("TabbedPane.darkShadow", new Color(59, 122, 112, 243));
        UIManager.put("Label.font", new Font(FONT_NAME, Font.BOLD, 15));
        UIManager.put("Label.foreground", new Color(59, 82, 92));
        UIManager.put("Table.font", new Font(FONT_NAME, Font.BOLD, 15));
        UIManager.put("Menu.font", new Font(FONT_NAME, Font.BOLD, 13));
        UIManager.put("MenuItem.font", new Font(FONT_NAME, Font.BOLD, 13));
        UIManager.put("MenuItem.background", new Color(217, 241, 252, 243));
        UIManager.put("RadioButton.font", new Font(FONT_NAME, Font.BOLD, 16));
        UIManager.put("RadioButton.foreground", new Color(59, 82, 92));
        UIManager.put("ButtonGroup.font", new Font(FONT_NAME, Font.BOLD, 16));
        UIManager.put("ButtonGroup.foreground", new Color(59, 82, 92));
        UIManager.put("ComboBox.font", new Font(FONT_NAME, Font.BOLD, 15));
        UIManager.put("ComboBox.foreground", new Color(59, 82, 92));
        UIManager.put("TextField.font", new Font(FONT_NAME, Font.PLAIN, 17));
        UIManager.put("TextField.foreground", new Color(59, 82, 92));
        UIManager.put("FormattedTextField.font", new Font(FONT_NAME, Font.PLAIN, 17));
        UIManager.put("FormattedTextField.foreground", new Color(59, 82, 92));
        UIManager.put("ScrollBar.background", new Color(217, 241, 252, 243));
        UIManager.put("ScrollBar.darkShadow", new Color(217, 241, 252, 243));
        UIManager.put("ScrollBar.shadow", new Color(217, 241, 252, 243));
        UIManager.put("ScrollPane.font", new Font(FONT_NAME, Font.BOLD, 20));
        UIManager.put("TitledBorder.font", new Font(FONT_NAME, Font.ITALIC | Font.BOLD, 14));
        UIManager.put("TitledBorder.titleColor", new Color(59, 82, 92));
        UIManager.put("OptionPane.background", new Color(217, 241, 252, 243));
        UIManager.put("Panel.background", new Color(217, 241, 252, 243));
        UIManager.put("PopupMenu.background", new Color(217, 241, 252, 243));
        UIManager.put("ToolTip.background", new Color(255, 176, 111));
        UIManager.put("ToolTip.foreground", BLACK);
        UIManager.put("ToolTip.font", new Font(FONT_NAME, Font.PLAIN, 16));
    }

    @Override
    public Color getMenuBarBackgroundColor() {
        return new Color(102, 135, 149, 243);
    }

    @Override
    public Color getMenuBarForegroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getToolBarBackgroundColor() {
        return new Color(181, 201, 210, 243);
    }

    @Override
    public Color getStatusBarBackgroundColor() {
        return new Color(59, 82, 92, 243);
    }

    @Override
    public Color getStatusBarForegroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getMainBackgroundColor() {
        return new Color(255, 176, 111);
    }

    @Override
    public Color getContentBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getSelectedTabBackgroundColor() {
        return new Color(102, 135, 149, 243);
    }

    @Override
    public Color getUnselectedTabBackgroundColor() {
        return new Color(165, 172, 175, 243);
    }

    @Override
    public Color getTabForegroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getButtonBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getButtonForegroundColor() {
        return BLACK;
    }

    @Override
    public Color getEvenRowBackgroundColor() {
        return new Color(215, 234, 159);
    }

    @Override
    public Color getOddRowBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getMapPanelBackgroundColor() {
        return new Color(217, 241, 252, 243);
    }

    @Override
    public Color getInputBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getSearchInputBackgroundColor() {
        return new Color(217, 241, 252, 243);
    }

    @Override
    public Color getDisabledSearchInputBackgroundColor() {
        return Color.lightGray;
    }

    @Override
    public Color getSearchResultBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public Color getPopupContentColor() {
        return new Color(217, 241, 252, 243);
    }

    @Override
    public Color getPopupBorderColor() {
        return new Color(255, 176, 111);
    }

    @Override
    public Color getAboutMenuBackground() {
        return new Color(181, 201, 210, 243);
    }
}
