/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

public enum BookLabel {

    GENESIS,
    EXODUS,
    LEVITICUS,
    NUMBERS,
    DEUTERONOMY,
    JOSHUA,
    JUDGES,
    RUTH,
    I_SAMUEL,
    II_SAMUEL,
    I_KINGS,
    II_KINGS,
    I_CHRONICLES,
    II_CHRONICLES,
    EZRA,
    NEHEMIAH,
    ESTHER,
    JOB,
    PSALMS,
    PROVERBS,
    ECCLESIASTES,
    SONG_OF_SOLOMON,
    ISAIAH,
    JEREMIAH,
    LAMENTATIONS,
    EZEKIEL,
    DANIEL,
    HOSEA,
    JOEL,
    AMOS,
    OBADIAH,
    JONAH,
    MICAH,
    NAHUM,
    HABAKKUK,
    ZEPHANIAH,
    HAGGAI,
    ZECHARIAH,
    MALACHI,
    MATTHEW,
    MARK,
    LUKE,
    JOHN,
    ACTS,
    ROMANS,
    I_CORINTHIANS,
    II_CORINTHIANS,
    GALATIANS,
    EPHESIANS,
    PHILIPPIANS,
    COLOSSIANS,
    I_THESSALONIANS,
    II_THESSALONIANS,
    I_TIMOTHY,
    II_TIMOTHY,
    TITUS,
    PHILEMON,
    HEBREWS,
    JAMES,
    I_PETER,
    II_PETER,
    I_JOHN,
    II_JOHN,
    III_JOHN,
    JUDE,
    REVELATION;

    private final List<File> maps;

    BookLabel() {
        this.maps = getMaps(StaticFieldHolder.workingDirectory + "/.aurora/map/" + StaticFieldHolder.localeService.getSelectedLanguage() + "/" + name().toLowerCase() + "/");
    }

    public List<File> getMaps() {
        return maps;
    }

    @SneakyThrows
    private List<File> getMaps(String uriString) {
        if (new File(uriString).exists()) {
            List<File> mapsWithDirectory = Files.walk(Path.of(uriString)).map(Path::toFile).collect(Collectors.toList());
            List<File> files = mapsWithDirectory.subList(1, mapsWithDirectory.size());
            files.sort(Comparator.comparing(File::getName));
            return files;
        }
        return new ArrayList<>(); // this should return only in case of unit tests
    }

    @UtilityClass
    public static class StaticFieldHolder {

        private static LocaleService localeService;
        private static String workingDirectory;

        public static void setAttributes(LocaleService localeService, String... workingDirectory) {
            StaticFieldHolder.localeService = localeService;
            StaticFieldHolder.workingDirectory = workingDirectory.length == 0 ? WORKING_DIRECTORY : workingDirectory[0];
        }
    }
}

