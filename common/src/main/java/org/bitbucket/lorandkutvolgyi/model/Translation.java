/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Translation {

    private String name;
    private String info;
    private String language;
    private List<Book> oldTestament;
    private List<Book> newTestament;
    private Map<String, String> statistics;

    @JsonIgnore
    public List<Book> getBooks() {
        ArrayList<Book> result = new ArrayList<>();
        if (oldTestament != null) {
            result.addAll(oldTestament);
        }
        if (newTestament != null) {
            result.addAll(newTestament);
        }
        return result;
    }

    @JsonIgnore
    public String getBookName(BookLabel bookLabel) {
        return getBooks().stream()
                .filter(book -> book.getLabel() == bookLabel)
                .map(Book::getName)
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }
}
