/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class Chapter {

    private String translationName;
    private String bookName;
    private BookLabel bookLabel;
    private int number;
    private String title;
    private List<Verse> verses;
    private Map<String, String> statistics;
    private String comments;

    public Chapter(String translationName, String bookName, int number, List<Verse> verses, String comments, Map<String, String> statistics) {
        this.translationName = translationName;
        this.bookName = bookName;
        this.number = number;
        this.verses = verses;
        this.comments = comments;
        this.statistics = statistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chapter chapter = (Chapter) o;
        return number == chapter.number &&
                translationName.equals(chapter.translationName) &&
                bookName.equals(chapter.bookName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(translationName, bookName, number);
    }

    @Override
    public String toString() {
        if (number == -1) {
            return "";
        }
        return bookName + " " + number + " - " + translationName;
    }
}
