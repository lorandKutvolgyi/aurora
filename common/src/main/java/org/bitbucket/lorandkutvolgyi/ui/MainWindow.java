/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.CloseWindowEvent;
import org.bitbucket.lorandkutvolgyi.event.CursorChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.StatusBarTextChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.TabToCloseEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.MainWindowPresenter;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.TextTabManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.UUID;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.*;

public class MainWindow extends JFrame implements TextTabManager {

    public static final String LEFT_AREA = "LEFT_AREA";
    public static final String MIDDLE_AREA = "MIDDLE_AREA";
    public static final String TOP_RIGHT_AREA = "TOP_RIGHT_AREA";
    public static final String BOTTOM_RIGHT_AREA = "BOTTOM_RIGHT_AREA";
    private static final String APPLICATION_TITLE = "Aurora";
    private final transient Theme selectedTheme;
    private final LeftArea leftArea;
    private final MiddleArea middleArea;
    private final TopRightArea topRightArea;
    private final BottomRightArea bottomRightArea;
    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private final StatusBar statusBar;
    private JSplitPane rightArea;
    private JSplitPane middleAndRightArea;
    private JSplitPane mainArea;

    public MainWindow(Map<String, JTabbedPane> areas, MainWindowPresenter mainWindowPresenter,
                      Theme selectedTheme, JMenuBar mainMenu, JToolBar mainToolbar) {

        this.selectedTheme = selectedTheme;
        this.statusBar = new StatusBar(mainWindowPresenter, selectedTheme);
        this.leftArea = (LeftArea) areas.get(LEFT_AREA);
        this.middleArea = (MiddleArea) areas.get(MIDDLE_AREA);
        this.topRightArea = (TopRightArea) areas.get(TOP_RIGHT_AREA);
        this.bottomRightArea = (BottomRightArea) areas.get(BOTTOM_RIGHT_AREA);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocation(calculateLocation());
        this.setSize(calculateSize());
        this.add(createMainArea());
        this.setIconImage(createMainIcon());
        this.setTitle(APPLICATION_TITLE);
        this.setJMenuBar(mainMenu);
        this.add(mainToolbar, BorderLayout.NORTH);
        this.add(statusBar, BorderLayout.SOUTH);
        this.setVisible(true);
        this.setDividerPositions();
        this.addWindowListener(new MainWindowListener());
    }

    @Subscribe
    public void setStatusLabelText(StatusBarTextChangedEvent event) {
        statusBar.setText(event.getText());
    }

    @Subscribe
    public void closeWindow(CloseWindowEvent event) {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    @Subscribe
    public void closeTab(TabToCloseEvent event) {
        middleArea.closeTab(event.getTabId());
    }

    @Subscribe
    public void changeCursor(CursorChangeEvent event) {
        int cursor = event.getCursor();
        if (cursor == 0) {
            setCursor(Cursor.getDefaultCursor());
        } else if (cursor == Cursor.WAIT_CURSOR) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }

    @Override
    public UUID getSelectedTextTab() {
        return (UUID) ((JComponent) middleArea.getSelectedComponent()).getClientProperty("tabId");
    }

    private Point calculateLocation() {
        int posX = PreferencesService.getInt(WINDOW_LOCATION_X);
        if (posX == -1) {
            return new Point(0, 0);
        }
        int posY = PreferencesService.getInt(WINDOW_LOCATION_Y);
        return new Point(posX, posY);
    }

    private Dimension calculateSize() {
        int width = PreferencesService.getInt(WINDOW_WIDTH);
        if (width == -1) {
            return screenSize;
        }
        int height = PreferencesService.getInt(WINDOW_HEIGHT);
        return new Dimension(width, height);
    }

    private JSplitPane createMainArea() {
        mainArea = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, leftArea, createMiddleAndRightArea());
        mainArea.setBackground(selectedTheme.getMainBackgroundColor());
        return mainArea;
    }

    private Image createMainIcon() {
        return Toolkit.getDefaultToolkit().createImage(ClassLoader.getSystemResource("logo.png"));
    }

    private JSplitPane createMiddleAndRightArea() {
        middleAndRightArea = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, middleArea, createRightArea());
        middleAndRightArea.setBackground(selectedTheme.getMainBackgroundColor());
        return middleAndRightArea;
    }

    private JSplitPane createRightArea() {
        rightArea = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, topRightArea, bottomRightArea);
        rightArea.setBackground(selectedTheme.getMainBackgroundColor());
        rightArea.setMinimumSize(new Dimension(0, 0));
        return rightArea;
    }

    private void setDividerPositions() {
        if (PreferencesService.getInt(PreferencesService.Key.LEFT_DIVIDER_POSITION) != -1) {
            mainArea.setDividerLocation(PreferencesService.getInt(PreferencesService.Key.LEFT_DIVIDER_POSITION));
        }
        if (PreferencesService.getInt(PreferencesService.Key.MIDDLE_DIVIDER_POSITION) != -1) {
            middleAndRightArea.setDividerLocation(PreferencesService.getInt(PreferencesService.Key.MIDDLE_DIVIDER_POSITION));
        }
        if (PreferencesService.getInt(PreferencesService.Key.RIGHT_DIVIDER_POSITION) != -1) {
            rightArea.setDividerLocation(PreferencesService.getInt(PreferencesService.Key.RIGHT_DIVIDER_POSITION));
        }
    }

    private class MainWindowListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent windowEvent) {
            leftArea.saveState();
            middleArea.saveState();
            topRightArea.saveState();
            bottomRightArea.saveState();

            PreferencesService.put(WINDOW_LOCATION_X, getLocation().x);
            PreferencesService.put(WINDOW_LOCATION_Y, getLocation().y);
            PreferencesService.put(WINDOW_WIDTH, getSize().width);
            PreferencesService.put(WINDOW_HEIGHT, getSize().height);
            PreferencesService.put(PreferencesService.Key.LEFT_DIVIDER_POSITION, mainArea.getDividerLocation());
            PreferencesService.put(PreferencesService.Key.MIDDLE_DIVIDER_POSITION, middleAndRightArea.getDividerLocation());
            PreferencesService.put(PreferencesService.Key.RIGHT_DIVIDER_POSITION, rightArea.getDividerLocation());
        }
    }
}
