/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.LEFT_ACTIVE_TAB;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.LEFT_AREA_HEIGHT;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.LEFT_AREA_WIDTH;

public class LeftArea extends JTabbedPane {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private final Map<String, SavablePanel> panels;

    public LeftArea(int position, Map<String, SavablePanel> panels) {
        super(position);

        this.panels = panels;
        panels.forEach(this::addTab);
        setIconAt(0, new ImageIcon(Objects.requireNonNull(MainWindow.class.getResource("/book-tab.png"))));
        setIconAt(1, new ImageIcon(Objects.requireNonNull(MainWindow.class.getResource("/search-tab.png"))));
        setMinimumSize(new Dimension(0, 0));
        setSelectedIndex(PreferencesService.getInt(LEFT_ACTIVE_TAB, 0));
        setLeftAreasPreferredSize();
    }

    public void saveState() {
        PreferencesService.put(LEFT_AREA_WIDTH, getSize().width);
        PreferencesService.put(LEFT_AREA_HEIGHT, getSize().height);
        PreferencesService.put(LEFT_ACTIVE_TAB, getSelectedIndex());
        panels.values().forEach(SavablePanel::saveState);
    }

    private void setLeftAreasPreferredSize() {
        if (PreferencesService.getInt(LEFT_AREA_WIDTH) == -1) {
            setPreferredSize(new Dimension((int) (screenSize.getWidth() / 4), 0));
        } else {
            Dimension size = new Dimension(PreferencesService.getInt(LEFT_AREA_WIDTH), PreferencesService.getInt(LEFT_AREA_HEIGHT));
            setPreferredSize(size);
        }
    }
}
