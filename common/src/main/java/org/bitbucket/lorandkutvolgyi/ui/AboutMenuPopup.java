/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.io.File;
import java.util.ResourceBundle;

import static java.awt.GridBagConstraints.*;

public class AboutMenuPopup extends JDialog {

    private static final String MIME_TYPE = "text/html; charset=UTF-8";

    public AboutMenuPopup(ResourceBundle messagesBundle, Theme selectedTheme, LocaleService localeService, String workingDirectory) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(750, 440));
        setModal(true);
        setUndecorated(true);
        setResizable(false);
        setLocation((screenSize.width - getWidth()) / 2, (screenSize.height - getHeight()) / 3);
        setLayout(new BorderLayout());

        JPanel mainPanel = createMainPanel(selectedTheme);
        add(mainPanel);

        JEditorPane pane = createJEditorPane(localeService, workingDirectory);
        mainPanel.add(pane, createConstraintsForEditorPane());

        JButton closeButton = createCloseButton(messagesBundle, mainPanel);
        mainPanel.add(closeButton, createConstraintsForCloseButton());

        pack();
    }

    private GridBagConstraints createConstraintsForEditorPane() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.weightx = 0.25;
        constraints.anchor = LINE_START;
        constraints.fill = HORIZONTAL;
        return constraints;
    }

    private JPanel createMainPanel(Theme selectedTheme) {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setBackground(selectedTheme.getAboutMenuBackground());
        mainPanel.setBorder(new LineBorder(selectedTheme.getPopupBorderColor(), 3));
        return mainPanel;
    }

    @SneakyThrows
    private JEditorPane createJEditorPane(LocaleService localeService, String workingDirectory) {
        JEditorPane pane = new JEditorPane();
        pane.setContentType(MIME_TYPE);
        pane.setPage("file:///" + new File(workingDirectory + "/.aurora/about_" + localeService.getSelectedLanguage() + ".html").getPath());
        pane.setEditorKit(new HTMLEditorKit());
        pane.setEditable(false);
        pane.setEnabled(true);
        pane.setOpaque(false);
        pane.addHyperlinkListener(this::openInBrowser);
        return pane;
    }

    @SneakyThrows
    private void openInBrowser(HyperlinkEvent event) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType()) && Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(event.getURL().toURI());
        }
    }

    private JButton createCloseButton(ResourceBundle messagesBundle, JPanel mainPanel) {
        JButton close = new JButton(messagesBundle.getString("CLOSE"));
        close.addActionListener(event -> setVisible(false));
        return close;
    }

    private GridBagConstraints createConstraintsForCloseButton() {
        GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.gridx = 1;
        constraints1.gridy = 2;
        constraints1.anchor = PAGE_END;
        return constraints1;
    }

    public void showPopup() {
        setVisible(true);
    }
}
