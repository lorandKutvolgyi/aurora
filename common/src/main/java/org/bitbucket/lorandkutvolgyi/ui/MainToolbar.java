/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.CrossReferencesHidingController;
import org.bitbucket.lorandkutvolgyi.controller.ToolBarListener;
import org.bitbucket.lorandkutvolgyi.controller.UpdateController;
import org.bitbucket.lorandkutvolgyi.controller.VerseNumberHidingController;
import org.bitbucket.lorandkutvolgyi.event.CursorChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.UpdateEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.CrossReferencesHidingPresenter;
import org.bitbucket.lorandkutvolgyi.presenter.VerseNumberHidingPresenter;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.VersionProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;
import java.util.Objects;
import java.util.ResourceBundle;

import static java.awt.event.InputEvent.ALT_DOWN_MASK;
import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.KeyStroke.getKeyStroke;
import static org.bitbucket.lorandkutvolgyi.controller.ToolBarListener.*;

public class MainToolbar extends JToolBar {

    private final transient ToolBarListener toolBarListener;
    private final transient VerseNumberHidingController verseNumberHidingController;
    private final transient VerseNumberHidingPresenter verseNumberHidingPresenter;
    private final transient CrossReferencesHidingController crossReferencesHidingController;
    private final transient CrossReferencesHidingPresenter crossReferencesHidingPresenter;
    private final transient ResourceBundle messagesBundle;
    private final transient MessageBus messageBus;
    private transient Window parentWindow;

    public MainToolbar(ResourceBundle messagesBundle, ToolBarListener toolBarListener, Theme selectedTheme,
                       VerseNumberHidingController verseNumberHidingController, VerseNumberHidingPresenter verseNumberHidingPresenter,
                       CrossReferencesHidingController crossReferencesHidingController, CrossReferencesHidingPresenter crossReferencesHidingPresenter,
                       UpdateController updateController, MessageBus messageBus) {
        this.toolBarListener = toolBarListener;
        this.verseNumberHidingController = verseNumberHidingController;
        this.verseNumberHidingPresenter = verseNumberHidingPresenter;
        this.crossReferencesHidingController = crossReferencesHidingController;
        this.crossReferencesHidingPresenter = crossReferencesHidingPresenter;
        this.messagesBundle = messagesBundle;
        this.messageBus = messageBus;

        this.setBackground(selectedTheme.getToolBarBackgroundColor());
        this.setMargin(new Insets(3, 0, 3, 0));
        this.setFloatable(false);

        this.add(createToolBarButton("previous-chapter.png", messagesBundle.getString("PREVIOUS_CHAPTER"),
                PREVIOUS_CHAPTER, getKeyStroke(KeyEvent.VK_LEFT, ALT_DOWN_MASK)));

        this.add(createToolBarButton("next-chapter.png", messagesBundle.getString("NEXT_CHAPTER"),
                NEXT_CHAPTER, getKeyStroke(KeyEvent.VK_RIGHT, ALT_DOWN_MASK)));

        this.addSeparator();

        this.add(createToolBarButton("previous-book.png", messagesBundle.getString("PREVIOUS_BOOK"),
                PREVIOUS_BOOK, getKeyStroke(KeyEvent.VK_LEFT, CTRL_DOWN_MASK | ALT_DOWN_MASK)));

        this.add(createToolBarButton("next-book.png", messagesBundle.getString("NEXT_BOOK"),
                NEXT_BOOK, getKeyStroke(KeyEvent.VK_RIGHT, CTRL_DOWN_MASK | ALT_DOWN_MASK)));

        this.addSeparator();
        this.addSeparator();
        this.add(createToolBarShowHideNumbersButton());
        this.add(createToolBarShowHideCrossReferencesButton());
        this.add(Box.createHorizontalGlue());
        this.add(createUpdateButton(messagesBundle, updateController));
        this.addSeparator();
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    @Subscribe
    public void showUpdatePopup(UpdateEvent event) {
        messageBus.publish(new CursorChangeEvent(0));
        if (event.getInstallerPath() == null) {
            JOptionPane.showMessageDialog(this.getParent(),
                    MessageFormat.format(messagesBundle.getString("DOWNLOAD_FAILED"), event.getInstallerPath()),
                    messagesBundle.getString("ERROR_POPUP_TITLE"),
                    ERROR_MESSAGE);
        } else if (isOkChosen(event)) {
            parentWindow.dispatchEvent(new WindowEvent(parentWindow, WindowEvent.WINDOW_CLOSING));
        }
    }

    private boolean isOkChosen(UpdateEvent event) {
        return getChosenOption(event) == 0;
    }

    private int getChosenOption(UpdateEvent event) {
        return JOptionPane.showOptionDialog(this.getParent(),
                MessageFormat.format(messagesBundle.getString("CLOSE_POPUP_MESSAGE"), event.getInstallerPath()),
                messagesBundle.getString("CLOSE_POPUP_TITLE"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[]{messagesBundle.getString("YES"), messagesBundle.getString("LATER")},
                messagesBundle.getString("YES"));
    }

    private JButton createToolBarButton(String url, String toolTip, String actionCommand, KeyStroke keyStroke) {
        JButton button = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(url))));
        button.setToolTipText(toolTip);
        button.setBorder(null);
        button.setOpaque(false);
        button.setActionCommand(actionCommand);
        button.addActionListener(toolBarListener);
        button.registerKeyboardAction(toolBarListener, actionCommand, keyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
        return button;
    }

    private JToggleButton createToolBarShowHideNumbersButton() {
        JToggleButton button = new JToggleButton();
        button.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("verse-numbers-shown.png"))));
        button.setSelectedIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("verse-numbers-hidden.png"))));
        button.setToolTipText(verseNumberHidingPresenter.getTooltip());
        button.setBorder(null);
        button.setBackground(null);
        button.addActionListener(event -> {
            verseNumberHidingController.changeState(button.isSelected());
            button.setToolTipText(verseNumberHidingPresenter.getTooltip());
            PreferencesService.put(PreferencesService.Key.VERSE_NUMBER_STATE, button.isSelected());
        });
        button.registerKeyboardAction(event -> button.doClick(), "", getKeyStroke(KeyEvent.VK_N, CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);
        button.setSelected(PreferencesService.getBooleanOrFalse(PreferencesService.Key.VERSE_NUMBER_STATE));
        return button;
    }

    private JToggleButton createToolBarShowHideCrossReferencesButton() {
        JToggleButton button = new JToggleButton();
        button.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("cross-references-shown.png"))));
        button.setSelectedIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("cross-references-hidden.png"))));
        button.setToolTipText(crossReferencesHidingPresenter.getTooltip());
        button.setBorder(null);
        button.setBackground(null);
        button.addActionListener(event -> {
            crossReferencesHidingController.changeState(button.isSelected());
            button.setToolTipText(crossReferencesHidingPresenter.getTooltip());
            PreferencesService.put(PreferencesService.Key.CROSS_REFERENCE_STATE, button.isSelected());
        });
        button.registerKeyboardAction(event -> button.doClick(), "", getKeyStroke(KeyEvent.VK_R, CTRL_DOWN_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);
        button.setSelected(PreferencesService.getBooleanOrTrue(PreferencesService.Key.CROSS_REFERENCE_STATE));
        return button;
    }

    private JButton createUpdateButton(ResourceBundle messagesBundle, UpdateController updateController) {
        JButton update = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("download.png"))));
        update.setToolTipText(messagesBundle.getString("UPDATE"));
        update.setBorder(null);
        update.setOpaque(false);
        update.setVisible(false);
        update.addActionListener(event -> updateController.update(this.getParent()));
        new Thread(() -> {
            boolean hasNewerVersion = updateController.hasNewerVersion(VersionProvider.getVersion());
            SwingUtilities.invokeLater(() -> update.setVisible(hasNewerVersion));
        }).start();
        return update;
    }
}
