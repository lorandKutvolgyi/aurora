/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.MiddleAreaTabChangeListener;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.MIDDLE_AREA_HEIGHT;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.MIDDLE_AREA_WIDTH;

public class MiddleArea extends JTabbedPane {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private final SavablePanel panel;

    public MiddleArea(int position, SavablePanel panel, MiddleAreaTabChangeListener middleAreaTabChangeListener) {
        super(position, SCROLL_TAB_LAYOUT);
        this.panel = panel;
        addTab("", panel);

        setTabComponentAt(0, createTitle(getIcon()));

        addChangeListener(middleAreaTabChangeListener);
        setMiddleAreasPreferredSize();
    }

    public void closeTab(String tabId) {
        remove(indexOfComponent(findTab(tabId)));
    }

    private Component findTab(String tabId) {
        return Arrays.stream(getComponents())
                .filter(comp -> {
                    Object prop = ((JComponent) comp).getClientProperty("tabId");
                    return prop != null && tabId.equals((prop).toString());
                })
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    public void saveState() {
        PreferencesService.put(MIDDLE_AREA_WIDTH, getSize().width);
        PreferencesService.put(MIDDLE_AREA_HEIGHT, getSize().height);

        panel.saveState();
    }

    private ImageIcon getIcon() {
        return new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("close.png")));
    }

    private JPanel createTitle(ImageIcon icon) {
        JPanel title = new JPanel();
        title.setOpaque(false);
        title.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));
        JLabel titleLabel = new JLabel();
        titleLabel.setHorizontalTextPosition(SwingConstants.LEFT);
        titleLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
        titleLabel.setOpaque(false);
        title.add(titleLabel);
        JButton closeButton = new JButton();
        closeButton.setVisible(true);
        closeButton.setDisabledIcon(icon);
        closeButton.setVerticalAlignment(SwingConstants.TOP);
        closeButton.setPreferredSize(new Dimension(icon.getIconWidth() - 2, icon.getIconHeight() - 2));
        closeButton.setOpaque(false);
        closeButton.setContentAreaFilled(false);
        closeButton.setBorder(null);
        Arrays.stream(getMouseListeners()).forEach(closeButton::addMouseListener);
        title.add(closeButton);
        return title;
    }

    private void setMiddleAreasPreferredSize() {
        if (PreferencesService.getInt(MIDDLE_AREA_WIDTH) == -1) {
            setPreferredSize(new Dimension(((int) (screenSize.getWidth() / 2) - 62), 0));
        } else {
            Dimension size = new Dimension(PreferencesService.getInt(MIDDLE_AREA_WIDTH), PreferencesService.getInt(MIDDLE_AREA_HEIGHT));
            setPreferredSize(size);
        }
    }
}
