/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.MainWindowPresenter;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class StatusBar extends JPanel {

    private final transient MainWindowPresenter mainWindowPresenter;
    private final transient Theme selectedTheme;
    private JLabel statusLabel;

    public StatusBar(MainWindowPresenter mainWindowPresenter, Theme selectedTheme) {
        this.mainWindowPresenter = mainWindowPresenter;
        this.selectedTheme = selectedTheme;
        setBackground(selectedTheme.getStatusBarBackgroundColor());
        setBorder(new BevelBorder(BevelBorder.LOWERED));
        setPreferredSize(new Dimension(getWidth(), 26));
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(createStatusLabel());
    }

    public void setText(String text) {
        statusLabel.setText(text);
    }

    private JLabel createStatusLabel() {
        statusLabel = new JLabel("");
        statusLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusLabel.setText(mainWindowPresenter.getStatusLabelText());
        statusLabel.setForeground(selectedTheme.getStatusBarForegroundColor());
        return statusLabel;
    }
}
