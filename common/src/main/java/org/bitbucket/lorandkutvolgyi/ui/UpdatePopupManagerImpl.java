/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.UpdatePopupManager;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

public class UpdatePopupManagerImpl implements UpdatePopupManager {

    private final ResourceBundle messagesBundle;

    public UpdatePopupManagerImpl(ResourceBundle messagesBundle) {
        this.messagesBundle = messagesBundle;
    }

    @Override
    public int showPopup(Component parent) {
        return JOptionPane.showOptionDialog(parent,
                messagesBundle.getString("UPDATE_POPUP_MESSAGE"),
                messagesBundle.getString("UPDATE_POPUP_TITLE"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                new String[]{messagesBundle.getString("YES"), messagesBundle.getString("NO")},
                messagesBundle.getString("YES"));
    }
}
