/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.controller.LanguageSelectionController;
import org.bitbucket.lorandkutvolgyi.controller.TranslationSelectionListener;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.MainWindowPresenter;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.List;
import java.util.ResourceBundle;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;

public class MainMenu extends JMenuBar {

    private static final String ORIGINAL = "ORIGINAL";
    private final transient TranslationSelectionListener translationSelectionListener;
    private final transient Theme selectedTheme;
    private final transient ResourceBundle messagesBundle;
    private final transient MainWindowPresenter presenter;
    private final transient LanguageSelectionController languageSelectionController;
    private final AboutMenuPopup aboutMenuPopup;
    private final String webPageUrl;

    public MainMenu(TranslationSelectionListener translationSelectionListener, Theme selectedTheme, ResourceBundle messagesBundle,
                    MainWindowPresenter presenter, LanguageSelectionController languageSelectionController, AboutMenuPopup aboutMenuPopup,
                    LocaleService localeService) {
        this.translationSelectionListener = translationSelectionListener;
        this.languageSelectionController = languageSelectionController;
        this.selectedTheme = selectedTheme;
        this.messagesBundle = messagesBundle;
        this.presenter = presenter;
        this.aboutMenuPopup = aboutMenuPopup;
        this.webPageUrl = "https://www.aurora-bible.com/" + localeService.getSelectedLanguage() + "/help.html";

        setBackground(selectedTheme.getMenuBarBackgroundColor());
        setPreferredSize(new Dimension(0, 30));
        add(createTranslationsMenu());
        add(createLanguagesMenu());
        add(createHelpMenu());
    }

    private JMenu createTranslationsMenu() {
        JMenu translations = new JMenu(messagesBundle.getString("TRANSLATIONS"));
        translations.setForeground(selectedTheme.getMenuBarForegroundColor());
        createTranslationsMenuItems(translations);
        return translations;
    }

    private JMenu createLanguagesMenu() {
        JMenu language = new JMenu(messagesBundle.getString("LANGUAGES"));
        language.setForeground(selectedTheme.getMenuBarForegroundColor());
        createLanguagesMenuItems(language);
        return language;
    }

    private JMenu createHelpMenu() {
        JMenu help = new JMenu(messagesBundle.getString("HELP"));
        help.setForeground(selectedTheme.getMenuBarForegroundColor());
        createHelpMenuItems(help);
        return help;
    }

    private void createTranslationsMenuItems(JMenu menu) {
        presenter.getDownloadedTranslationNames().forEach((key, value) -> createSubMenu(menu, key, value));
    }

    private void createLanguagesMenuItems(JMenu menu) {
        presenter.getLanguages().forEach((abbreviation, language) -> {
            JMenuItem menuItem = new JMenuItem(language);
            menuItem.setActionCommand(abbreviation);
            menuItem.addActionListener(getActionListener());
            menuItem.setIconTextGap(10);
            menu.add(menuItem);
        });
    }

    private ActionListener getActionListener() {
        return event -> {
            if (isOkChosen()) {
                String language = event.getActionCommand();
                languageSelectionController.selectLanguage(language);
            }
        };
    }

    private boolean isOkChosen() {
        return getChosenOptionButton() == 0;
    }

    private int getChosenOptionButton() {
        return JOptionPane.showOptionDialog(this.getParent(),
                messagesBundle.getString("RESTART_REQUIRED"),
                "",
                OK_CANCEL_OPTION,
                QUESTION_MESSAGE,
                null,
                new String[]{messagesBundle.getString("RESTART_NOW"), messagesBundle.getString("CANCEL")},
                messagesBundle.getString("RESTART_NOW")
        );
    }

    private void createHelpMenuItems(JMenu menu) {
        JMenuItem usageMenuItem = new JMenuItem(messagesBundle.getString("USAGE"));
        usageMenuItem.setIconTextGap(10);
        usageMenuItem.addActionListener(event -> openWebPageInBrowser());
        JMenuItem aboutMenuItem = new JMenuItem(messagesBundle.getString("ABOUT"));
        aboutMenuItem.setIconTextGap(10);
        aboutMenuItem.addActionListener(event -> aboutMenuPopup.showPopup());
        menu.add(usageMenuItem);
        menu.add(aboutMenuItem);
    }

    private void createSubMenu(JMenu menu, String key, List<String> value) {
        JMenu subMenu = new JMenu(messagesBundle.getString(key.toUpperCase()));
        subMenu.setIconTextGap(10);
        if (!key.equals("original")) {
            menu.add(subMenu);
        }
        value.stream().sorted().forEach(translationName -> {
            JMenuItem menuItem = createMenuItem(translationName);
            addMenuItemToItsParent(menu, subMenu, translationName, menuItem);
        });
    }

    @SneakyThrows
    private void openWebPageInBrowser() {
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(URI.create(webPageUrl));
        }
    }

    private JMenuItem createMenuItem(String translationName) {
        JMenuItem menuItem = new JMenuItem(getMenuItemText(translationName));
        menuItem.addActionListener(translationSelectionListener);
        menuItem.setActionCommand(getActionCommand(translationName));
        menuItem.setToolTipText(presenter.getTooltip(translationName));
        menuItem.setIconTextGap(10);
        return menuItem;
    }

    private void addMenuItemToItsParent(JMenu menu, JMenu subMenu, String translationName, JMenuItem menuItem) {
        boolean original = translationName.equals(ORIGINAL);
        if (original) {
            menu.add(menuItem, 0);
            menu.insertSeparator(1);
        } else {
            subMenu.add(menuItem);
        }
    }

    private String getMenuItemText(String translationName) {
        boolean original = translationName.equals(ORIGINAL);
        return original ? messagesBundle.getString(translationName) : translationName;
    }

    private String getActionCommand(String translationName) {
        boolean original = translationName.equals(ORIGINAL);
        return original ? ORIGINAL : getMenuItemText(translationName);
    }
}
