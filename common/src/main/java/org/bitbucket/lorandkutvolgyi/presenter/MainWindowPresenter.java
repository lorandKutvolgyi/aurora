/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.StatusBarTextChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.TabToCloseEvent;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.*;

public class MainWindowPresenter {

    private final TranslationService translationService;
    private final MessageBus messageBus;
    private final ResourceBundle messagesBundle;
    private final Map<String, String> languages = new TreeMap<>();
    private final List<Translation> translations;

    public MainWindowPresenter(TranslationService translationService, MessageBus messageBus, ResourceBundle messagesBundle) {
        this.translationService = translationService;
        this.messageBus = messageBus;
        this.messagesBundle = messagesBundle;

        this.languages.put("en", "English");
        this.languages.put("hu", "Magyar");

        this.translations = translationService.getDownloadedTranslations();
    }

    @Subscribe
    public void setStatusLabel(CurrentTranslationChangedEvent event) {
        String text = getStatusLabelText(event.getTranslation());
        messageBus.publish(new StatusBarTextChangedEvent(text));
    }

    public String getStatusLabelText() {
        return getStatusLabelText(translationService.getCurrentTranslation());
    }

    public Map<String, List<String>> getDownloadedTranslationNames() {
        Map<String, List<String>> result = new TreeMap<>();
        translations
                .forEach(tr -> result.computeIfAbsent(tr.getLanguage(), v -> new ArrayList<>()).add(tr.getName()));
        return result;
    }

    public String getTooltip(String translationName) {
        return translations.stream()
                .filter(tr -> tr.getName().equals(translationName))
                .map(tr ->
                        tr.getInfo().equals("ORIGINAL_INFO") ? messagesBundle.getString("ORIGINAL_INFO") : tr.getInfo()
                ).findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    public void closeTextTab(String tabId) {
        messageBus.publish(new TabToCloseEvent(tabId));
    }

    public Map<String, String> getLanguages() {
        return languages;
    }

    private String getStatusLabelText(Translation translation) {
        String info = translation.getInfo();
        String translationName = translation.getName();
        String infoText = info.equals("ORIGINAL_INFO") ? messagesBundle.getString("ORIGINAL_INFO") : info;
        String translationNameText = translationName.equals("ORIGINAL") ? messagesBundle.getString("ORIGINAL") : translationName;
        return translationNameText + " - " + infoText;
    }
}
