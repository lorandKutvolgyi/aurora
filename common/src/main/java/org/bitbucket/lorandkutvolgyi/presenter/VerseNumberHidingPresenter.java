/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class VerseNumberHidingPresenter {

    private final Map<Boolean, String> tooltips;
    private String tooltip;

    public VerseNumberHidingPresenter(ResourceBundle messagesBundle) {
        tooltips = new HashMap<>();
        tooltips.put(TRUE, messagesBundle.getString("SHOW_NUMBERS"));
        tooltips.put(FALSE, messagesBundle.getString("HIDE_NUMBERS"));

        setTooltip(PreferencesService.getBooleanOrFalse(PreferencesService.Key.VERSE_NUMBER_STATE));
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(boolean tooltipKey) {
        this.tooltip = tooltips.get(tooltipKey);
    }
}
