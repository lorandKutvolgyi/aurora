/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.HiddenVerseNumberStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.presenter.VerseNumberHidingPresenter;

public class VerseNumberHidingController {

    private final MessageBus messageBus;
    private final VerseNumberHidingPresenter presenter;

    public VerseNumberHidingController(MessageBus messageBus, VerseNumberHidingPresenter presenter) {
        this.messageBus = messageBus;
        this.presenter = presenter;
    }

    public void changeState(boolean hidden) {
        messageBus.publish(new HiddenVerseNumberStateChangedEvent(hidden));
        presenter.setTooltip(hidden);
    }
}
