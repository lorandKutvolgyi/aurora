/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class ToolBarListener implements ActionListener {

    public static final String PREVIOUS_CHAPTER = "PREVIOUS_CHAPTER";
    public static final String NEXT_CHAPTER = "NEXT_CHAPTER";
    public static final String PREVIOUS_BOOK = "PREVIOUS_BOOK";
    public static final String NEXT_BOOK = "NEXT_BOOK";

    private final TranslationService translationService;

    public ToolBarListener(TranslationService translationService) {
        this.translationService = translationService;
    }

    @Loggable
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        changeChapter(actionEvent);
    }

    private void changeChapter(ActionEvent actionEvent) {
        Chapter currentChapter = translationService.getCurrentChapter();
        if (currentChapter != null) {
            openNewChapter(actionEvent, currentChapter);
        }
    }

    private void openNewChapter(ActionEvent actionEvent, Chapter currentChapter) {
        List<Book> books = getBooks(currentChapter);
        Book currentBook = getCurrentBook(currentChapter, books);
        Chapter newChapter = getNewChapter(actionEvent, currentChapter, books, currentBook);
        if (!currentChapter.equals(newChapter)) {
            translationService.openNewChapter(newChapter);
        }
    }

    private List<Book> getBooks(Chapter currentChapter) {
        return translationService.getTranslation(currentChapter.getTranslationName()).getBooks();
    }

    private Book getCurrentBook(Chapter currentChapter, List<Book> books) {
        return books.stream()
                .filter(book -> book.getName().equals(currentChapter.getBookName()))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    private Chapter getNewChapter(ActionEvent actionEvent, Chapter currentChapter, List<Book> books, Book currentBook) {
        return switch (actionEvent.getActionCommand()) {
            case PREVIOUS_CHAPTER -> getPreviousChapter(currentBook, currentChapter);
            case NEXT_CHAPTER -> getNextChapter(currentBook, currentChapter);
            case PREVIOUS_BOOK -> getPreviousBook(books, books.indexOf(currentBook), currentBook, currentChapter);
            case NEXT_BOOK -> getNextBook(books, books.indexOf(currentBook), currentBook, currentChapter);
            default -> throw new IllegalArgumentException("Invalid action command");
        };
    }

    private Chapter getPreviousChapter(Book currentBook, Chapter currentChapter) {
        int chapterNumber = currentChapter.getNumber();
        if (chapterNumber > 1) {
            return currentBook.getChapters().get(chapterNumber - 2);
        }
        return currentChapter;
    }

    private Chapter getNextChapter(Book currentBook, Chapter currentChapter) {
        int chapterNumber = currentChapter.getNumber();
        if (chapterNumber < currentBook.getChapters().size()) {
            return currentBook.getChapters().get(chapterNumber);
        }
        return currentChapter;
    }

    private Chapter getPreviousBook(List<Book> books, int bookIndex, Book currentBook, Chapter currentChapter) {
        if (currentBook.getLabel() != BookLabel.GENESIS && bookIndex != 0) {
            return books.get(bookIndex - 1).getChapters().get(0);
        }
        return currentChapter;
    }

    private Chapter getNextBook(List<Book> books, int bookIndex, Book currentBook, Chapter currentChapter) {
        if (currentBook.getLabel() != BookLabel.REVELATION && bookIndex != books.size() - 1) {
            return books.get(bookIndex + 1).getChapters().get(0);
        }
        return currentChapter;
    }
}
