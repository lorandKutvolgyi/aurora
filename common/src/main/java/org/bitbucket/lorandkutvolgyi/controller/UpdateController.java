/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CursorChangeEvent;
import org.bitbucket.lorandkutvolgyi.service.UpdateService;

import java.awt.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UpdateController {

    private final UpdateService updateService;
    private final UpdatePopupManager updatePopupManager;
    private final MessageBus messageBus;
    private final Executor executor;

    public UpdateController(UpdateService updateService, UpdatePopupManager updatePopupManager, MessageBus messageBus) {
        this.updateService = updateService;
        this.updatePopupManager = updatePopupManager;
        this.messageBus = messageBus;
        this.executor = Executors.newSingleThreadExecutor();
    }

    public boolean hasNewerVersion(String version) {
        return updateService.hasNewerVersion(version);
    }

    public void update(Component parent) {
        int result = updatePopupManager.showPopup(parent);
        int ok = 0;
        if (result == ok) {
            messageBus.publish(new CursorChangeEvent(Cursor.WAIT_CURSOR));
            executor.execute(updateService::update);
        }
    }
}
