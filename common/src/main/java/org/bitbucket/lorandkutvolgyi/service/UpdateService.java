/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.event.UpdateEvent;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;

import static java.nio.file.StandardOpenOption.*;

@Slf4j
public class UpdateService {

    private static final String BASE_URL = "https://bitbucket.org/lorandKutvolgyi/aurora-installers/downloads/aurora-latest.";
    private static final String EXTENSION = OsTypeProvider.getExtension();

    private final MessageBus messageBus;
    private final HttpClient httpClient;
    private final String workingDirectory;

    public UpdateService(MessageBus messageBus, String workingDirectory) {
        this.messageBus = messageBus;
        this.workingDirectory = workingDirectory;
        this.httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(20))
                .build();
    }

    public boolean hasNewerVersion(String version) {
        try {
            String response = httpClient
                    .send(createRequest(), stringBodyHandler())
                    .body();

            return !response.equals(version);
        } catch (IOException | InterruptedException | URISyntaxException ex) {
            log.error(ex.getMessage());
            return false;
        }
    }

    public void update() {
        try {
            HttpResponse<Path> response = sendRequest();
            Optional<Path> path = getPath(response);

            messageBus.publish(new UpdateEvent(path.orElseThrow(IOException::new)));
        } catch (URISyntaxException | IOException | InterruptedException ex) {
            log.error("error happened during the update: {}", ex.getMessage());
            messageBus.publish(new UpdateEvent(null));
        }
    }

    private HttpResponse<Path> sendRequest() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest
                .newBuilder(new URI(BASE_URL + EXTENSION))
                .GET()
                .build();

        HttpResponse.BodyHandler<Path> responseBodyHandler = HttpResponse.BodyHandlers.ofFileDownload(
                Path.of(workingDirectory), CREATE, TRUNCATE_EXISTING, WRITE);

        return httpClient.send(request, responseBodyHandler);
    }

    private HttpRequest createRequest() throws URISyntaxException {
        return HttpRequest.newBuilder(new URI("https://www.aurora-bible.com" + "/version"))
                .GET()
                .timeout(Duration.ofSeconds(20))
                .build();
    }

    private HttpResponse.BodyHandler<String> stringBodyHandler() {
        return HttpResponse.BodyHandlers.ofString();
    }

    private Optional<Path> getPath(HttpResponse<Path> response) {
        if (response.statusCode() == 200) {
            Path path = response.body().toAbsolutePath();
            log.info("installer downloaded to {}", path);
            return Optional.of(path);
        }
        log.error("update response status: {}", response.statusCode());
        return Optional.empty();
    }
}
