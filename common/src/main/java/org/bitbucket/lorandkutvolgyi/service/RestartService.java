/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.event.CloseWindowEvent;

public class RestartService {

    private final static String APP_PATH;

    static {
        if (OsTypeProvider.isLinux()) {
            APP_PATH = "/opt/aurora/bin/Aurora";
        } else {
            APP_PATH = "C:\\Program Files\\Aurora\\Aurora\\";
        }
    }

    private final MessageBus messageBus;

    public RestartService(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public void restart() {
        new Thread(this::startNewInstance).start();
        closeCurrentInstance();
    }

    @SneakyThrows
    void startNewInstance() {
        new ProcessBuilder().command(APP_PATH).start();
    }

    private void closeCurrentInstance() {
        messageBus.publish(new CloseWindowEvent());
    }
}
