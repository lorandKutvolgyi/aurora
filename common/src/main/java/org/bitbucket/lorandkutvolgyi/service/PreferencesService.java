/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;


import java.util.prefs.Preferences;

public class PreferencesService {

    public static final String WORKING_DIRECTORY = "true".equals(System.getProperty("devenv"))
            ? System.getProperty("user.home") + "/dev"
            : "true".equals(System.getProperty("testenv"))
            ? System.getProperty("user.home") + "/test"
            : System.getProperty("user.home");

    public static final Preferences PREFERENCES = Preferences.userRoot().node(WORKING_DIRECTORY);

    public static void put(Key key, String value) {
        PREFERENCES.put(key.name(), value);
    }

    public static void put(Key key, boolean value) {
        PREFERENCES.putBoolean(key.name(), value);
    }

    public static void put(Key key, int value) {
        PREFERENCES.putInt(key.name(), value);
    }

    public static String get(Key key) {
        return PREFERENCES.get(key.name(), null);
    }

    public static void remove(Key key) {
        PREFERENCES.remove(key.name());
    }

    public static boolean getBooleanOrFalse(Key key) {
        return PREFERENCES.getBoolean(key.name(), false);
    }

    public static boolean getBooleanOrTrue(Key key) {
        return PREFERENCES.getBoolean(key.name(), true);
    }

    public static int getInt(Key key) {
        return PREFERENCES.getInt(key.name(), -1);
    }

    public static int getInt(Key key, int defaultValue) {
        return PREFERENCES.getInt(key.name(), defaultValue);
    }

    public enum Key {
        LANGUAGE,
        CURRENT_TRANSLATION_INDEX,
        VERSE_NUMBER_STATE,
        CROSS_REFERENCE_STATE,
        WINDOW_LOCATION_X,
        WINDOW_LOCATION_Y,
        WINDOW_WIDTH,
        WINDOW_HEIGHT,
        LEFT_AREA_WIDTH,
        LEFT_AREA_HEIGHT,
        MIDDLE_AREA_WIDTH,
        MIDDLE_AREA_HEIGHT,
        TOP_RIGHT_AREA_WIDTH,
        TOP_RIGHT_AREA_HEIGHT,
        BOTTOM_RIGHT_AREA_WIDTH,
        BOTTOM_RIGHT_AREA_HEIGHT,
        LEFT_DIVIDER_POSITION,
        MIDDLE_DIVIDER_POSITION,
        RIGHT_DIVIDER_POSITION,
        LEFT_ACTIVE_TAB,
        TOP_RIGHT_ACTIVE_TAB,
        BOTTOM_RIGHT_ACTIVE_TAB,
        OT_TREE_ELEMENT_CLOSED,
        NT_TREE_ELEMENT_CLOSED,
        OPENED_CHAPTER_TRANSLATION,
        OPENED_CHAPTER_NAME,
        OPENED_CHAPTER_NUMBER,
        SELECTED_SEARCH_BUTTON,
        COLLAPSED_BOOKMARKS,
        TEST_KEY
    }
}
