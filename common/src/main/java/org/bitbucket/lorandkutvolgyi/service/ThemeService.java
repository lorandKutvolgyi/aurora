/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.DefaultTheme;
import org.bitbucket.lorandkutvolgyi.model.Theme;

import javax.swing.*;
import java.awt.*;

public class ThemeService {

    private final Theme selectedTheme;

    public ThemeService() {
        this.selectedTheme = new DefaultTheme();
        UIManager.put("TabbedPane.selected", selectedTheme.getSelectedTabBackgroundColor());
        UIManager.put("TabbedPane.unselectedBackground", selectedTheme.getUnselectedTabBackgroundColor());
        UIManager.put("TabbedPane.foreground", selectedTheme.getTabForegroundColor());
        UIManager.put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        UIManager.put("Button.background", selectedTheme.getButtonBackgroundColor());
        UIManager.put("Button.foreground", selectedTheme.getButtonForegroundColor());
        UIManager.put("Tree.background", selectedTheme.getContentBackgroundColor());
        UIManager.put("Tree.textBackground", selectedTheme.getContentBackgroundColor());
        UIManager.put("EditorPane.background", selectedTheme.getContentBackgroundColor());
        UIManager.put("TableHeader.background", selectedTheme.getContentBackgroundColor());
        UIManager.put("Panel.contentAreaColor", selectedTheme.getContentBackgroundColor());
        UIManager.put("ScrollPane.foreground", selectedTheme.getContentBackgroundColor());
        UIManager.put("ComboBox.disabledBackground", selectedTheme.getContentBackgroundColor());
        UIManager.put("TabbedPane.shadow", selectedTheme.getSelectedTabBackgroundColor());
    }

    public Theme getSelectedTheme() {
        return selectedTheme;
    }
}
