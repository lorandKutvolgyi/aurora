/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CommentsChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.repository.TranslationRepository;

import java.util.*;
import java.util.stream.Collectors;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.CURRENT_TRANSLATION_INDEX;

public class TranslationService {

    private final List<Translation> downloadedTranslations;
    private final Map<UUID, Chapter> openChapters;
    private final TranslationRepository repository;
    private final MessageBus messageBus;
    private int currentTranslationIndex;
    private Chapter currentChapter;
    private TextTabManager textTabManager;

    public TranslationService(TranslationRepository repository, MessageBus messageBus) {
        this.repository = repository;
        this.downloadedTranslations = repository.getDownloadedTranslations();
        this.openChapters = new HashMap<>();
        this.messageBus = messageBus;

        int previousOrDefaultIndex = PreferencesService.getInt(CURRENT_TRANSLATION_INDEX, 0);
        this.currentTranslationIndex = previousOrDefaultIndex < downloadedTranslations.size() ? previousOrDefaultIndex : 0;
    }

    public void setTextTabManager(TextTabManager textTabManager) {
        this.textTabManager = textTabManager;
    }

    public void openNewChapter(Chapter chapter) {
        openNewChapterWithGivenTranslation(chapter.getBookName(), chapter.getNumber(), getTranslation(chapter.getTranslationName()));
    }

    public void openNewChapter(String bookName, int numberOfChapter) {
        openNewChapterWithGivenTranslation(bookName, numberOfChapter, downloadedTranslations.get(currentTranslationIndex));
    }

    public void openNewChapter(String translationName, String bookName, int numberOfChapter) {
        Translation translation = downloadedTranslations.stream()
                .filter(tr -> tr.getName().equals(translationName))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
        openNewChapterWithGivenTranslation(bookName, numberOfChapter, translation);
    }

    public void openNewChapter(String translationName, BookLabel bookLabel, int numberOfChapter) {
        Translation translation = downloadedTranslations.stream()
                .filter(tr -> tr.getName().equals(translationName))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
        String bookName = translation
                .getBooks()
                .stream()
                .filter(book -> book.getLabel() == bookLabel)
                .map(Book::getName)
                .findAny()
                .orElseThrow(IllegalArgumentException::new);

        openNewChapterWithGivenTranslation(bookName, numberOfChapter, translation);
    }

    public void openNewChapter(BookLabel bookLabel, int numberOfChapter) {
        Translation translation = downloadedTranslations.get(currentTranslationIndex);
        String bookName = translation
                .getBooks()
                .stream()
                .filter(book -> book.getLabel() == bookLabel)
                .map(Book::getName)
                .findAny()
                .orElseThrow(IllegalArgumentException::new);

        openNewChapterWithGivenTranslation(bookName, numberOfChapter, translation);
    }

    public void selectChapter(UUID tabId) {
        currentChapter = openChapters.get(tabId);
        messageBus.publish(new CurrentChapterChangingEvent(currentChapter));
    }

    public void closeChapter(UUID tabId) {
        openChapters.remove(tabId);
    }

    public Chapter getCurrentChapter() {
        return currentChapter;
    }

    public String getCurrentChaptersComments() {
        return currentChapter.getComments();
    }

    public String getCurrentChaptersTranslationName() {
        return currentChapter.getTranslationName();
    }

    public List<Translation> getDownloadedTranslations() {
        return downloadedTranslations;
    }

    public Set<String> getDownloadedTranslationNames() {
        return downloadedTranslations.stream().map(Translation::getName).collect(Collectors.toSet());
    }

    public void selectTranslation(String name) {
        currentTranslationIndex = downloadedTranslations.indexOf(downloadedTranslations.stream()
                .filter(translation -> translation.getName().equals(name))
                .findAny()
                .orElseThrow(IllegalArgumentException::new));
        PreferencesService.put(CURRENT_TRANSLATION_INDEX, currentTranslationIndex);
        messageBus.publish(new CurrentTranslationChangedEvent(downloadedTranslations.get(currentTranslationIndex)));
    }

    public Translation getCurrentTranslation() {
        return downloadedTranslations.get(currentTranslationIndex);
    }

    public String getCurrentTranslationName() {
        Translation currentTranslation = getCurrentTranslation();
        return currentTranslation.getName();
    }

    public Translation getTranslation(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return getTranslationFromDownloadedOnes(name);
    }

    private Translation getTranslationFromDownloadedOnes(String name) {
        return downloadedTranslations.stream()
                .filter(translation -> translation.getName().equals(name))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    public void saveNewComments(String comments) {
        currentChapter.setComments(comments);
        repository.save(currentChapter);
        messageBus.publish(new CommentsChangedEvent(comments));
    }

    public Book getBook(String translationName, String bookName) {
        return getTranslation(translationName).getBooks()
                .stream()
                .filter(book -> book.getName().equals(bookName))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    public int getNumberOfChapters(String bookName) {
        return getCurrentTranslation().getBooks().stream()
                .filter(book -> book.getName().equals(bookName))
                .findAny().orElseThrow(IllegalArgumentException::new)
                .getChapters()
                .size();
    }

    private void openNewChapterWithGivenTranslation(String bookName, int numberOfChapter, Translation translation) {
        setCurrentChapter(translation.getName(), bookName, numberOfChapter);
        openChapters.put(getSelectedTextTabId(), currentChapter);
        messageBus.publish(new NewChapterEvent(currentChapter));
    }

    private void setCurrentChapter(String translationName, String bookName, int numberOfChapter) {
        currentChapter = downloadedTranslations.stream()
                .filter(translation -> translation.getName().equals(translationName))
                .flatMap(translation -> translation.getBooks().stream())
                .filter(book -> book.getName().equals(bookName))
                .flatMap(book -> book.getChapters().stream())
                .filter(chapter -> chapter.getNumber() == numberOfChapter)
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
        messageBus.publish(new CurrentChapterChangingEvent(currentChapter));
    }

    private UUID getSelectedTextTabId() {
        return textTabManager.getSelectedTextTab();
    }
}