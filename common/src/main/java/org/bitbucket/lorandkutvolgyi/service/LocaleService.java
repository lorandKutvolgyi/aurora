/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.LANGUAGE;

public class LocaleService {

    private final Set<Locale> locales;
    private final RestartService restartService;
    private Locale selectedLocale;

    public LocaleService(RestartService restartService) {
        this.restartService = restartService;
        this.locales = new HashSet<>();

        locales.add(Locale.ENGLISH);
        locales.add(new Locale.Builder().setLanguage("hu").setRegion("HU").build());

        setSelectedLanguage(PreferencesService.get(LANGUAGE));
    }

    public void setSelectedLanguageWithRestart(String language) {
        setSelectedLanguage(language);
        restartService.restart();
    }

    public Locale getSelectedLocale() {
        return selectedLocale;
    }

    public String getSelectedLanguage() {
        return selectedLocale.getLanguage();
    }

    private void setSelectedLanguage(String language) {
        selectedLocale = locales.stream()
                .filter(locale -> locale.getLanguage().equalsIgnoreCase(language))
                .findAny()
                .orElse(getDefault());

        PreferencesService.put(LANGUAGE, selectedLocale.getLanguage());
    }

    private Locale getDefault() {
        return locales.contains(Locale.getDefault()) ? Locale.getDefault() : Locale.ENGLISH;
    }
}
