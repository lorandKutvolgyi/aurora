/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.dizitart.no2.Nitrite;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.dizitart.no2.Document.createDocument;
import static org.dizitart.no2.filters.Filters.regex;

public class TranslationRepository {

    private final List<Translation> translations;
    private final Nitrite commentsDb;
    private final String workingDirectory;

    public TranslationRepository(String workingDirectory) {
        this.workingDirectory = workingDirectory;
        this.translations = new ArrayList<>(readTexts());
        this.commentsDb = Nitrite.builder().compressed().filePath(this.workingDirectory + "/.aurora/db/comments.db").openOrCreate();
        this.setComments();
    }

    public void save(Chapter chapter) {
        saveCommentsIntoDb(chapter);
        saveCommentsIntoTranslations(chapter);
    }

    public List<Translation> getDownloadedTranslations() {
        return translations;
    }

    void closeDb() {
        commentsDb.close();
    }

    private List<Translation> readTexts() {
        try (Stream<Path> paths = Files.walk(new File(workingDirectory + "/.aurora/text").toPath())) {
            return paths.filter(Files::isRegularFile).map(this::getTranslation).collect(Collectors.toList());
        } catch (IOException ex) {
            throw new TranslationNotFoundException();
        }
    }

    private void setComments() {
        translations.forEach(this::setComments);
    }

    private void setComments(Translation translation) {
        translation.getBooks().forEach(this::setCommentsForChapters);
    }

    private void saveCommentsIntoDb(Chapter chapter) {
        commentsDb.getCollection(chapter.getBookLabel().name())
                .update(
                        regex(String.valueOf(chapter.getNumber()), ".*"),
                        createDocument(String.valueOf(chapter.getNumber()), chapter.getComments())
                );
    }

    private void saveCommentsIntoTranslations(Chapter chapter) {
        getDownloadedTranslations().stream()
                .filter(translation -> !translation.getName().equals(chapter.getTranslationName()))
                .forEach(translation ->
                        translation.getBooks().stream()
                                .filter(book -> book.getLabel() == chapter.getBookLabel())
                                .filter(book -> book.getChapters().size() >= chapter.getNumber())
                                .forEach(book -> book.getChapters().get(chapter.getNumber() - 1).setComments(chapter.getComments())));
    }

    @SneakyThrows(IOException.class)
    private Translation getTranslation(Path path) {
        return new ObjectMapper().readValue(Files.readString(Paths.get(String.valueOf(path))), Translation.class);
    }


    private void setCommentsForChapters(Book book) {
        book.getChapters().forEach(chapter ->
                chapter.setComments(
                        (String) commentsDb.getCollection(book.getLabel().name())
                                .find()
                                .toList()
                                .get(chapter.getNumber() - 1)
                                .get(String.valueOf(chapter.getNumber()))
                )
        );
    }
}
