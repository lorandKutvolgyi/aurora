/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.Test;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class MiddleAreaTabChangeListenerTest {

    @Test
    void stateChanged_shouldCallTranslationServiceToSelectChapter() {
        JTabbedPane source = new JTabbedPane();
        JPanel component = new JPanel();
        UUID uuid = UUID.randomUUID();
        component.putClientProperty("tabId", uuid);
        source.add(component);
        source.setSelectedComponent(component);

        TranslationService translationService = mock(TranslationService.class);
        MiddleAreaTabChangeListener underTest = new MiddleAreaTabChangeListener(translationService);

        underTest.stateChanged(new ChangeEvent(source));

        verify(translationService).selectChapter(uuid);
    }
}
