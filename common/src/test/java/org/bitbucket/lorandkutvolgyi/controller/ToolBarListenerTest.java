/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;

import static org.bitbucket.lorandkutvolgyi.TestFixtureUtil.createChapter;
import static org.bitbucket.lorandkutvolgyi.TestFixtureUtil.createTranslation;
import static org.bitbucket.lorandkutvolgyi.controller.ToolBarListener.*;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ToolBarListenerTest {

    private TranslationService translationService;
    private ToolBarListener underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        underTest = new ToolBarListener(translationService);
    }

    @Test
    void actionPerformed_shouldNotCallTranslationServiceToOpenNewChapter_whenThereIsNoCurrentOne() {
        when(translationService.getCurrentChapter()).thenReturn(null);

        underTest.actionPerformed(new ActionEvent(new Object(), 0, ""));

        verify(translationService, never()).openNewChapter(any());
    }

    @Test
    void actionPerformed_shouldNotCallTranslationServiceToOpenNewChapter_whenActionCommandIsPreviousChapterAndCurrentOneIsTheFirstOne() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 1));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, PREVIOUS_CHAPTER));

        verify(translationService, never()).openNewChapter(any());
    }

    @Test
    void actionPerformed_shouldNotCallTranslationServiceToOpenNewChapter_whenActionCommandIsNextChapterAndCurrentOneIsTheLastOne() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, NEXT_CHAPTER));

        verify(translationService, never()).openNewChapter(any());
    }

    @Test
    void actionPerformed_shouldNotCallTranslationServiceToOpenNewChapter_whenActionCommandIsNextBookAndCurrentBookIsTheLastOne() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MARK, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, NEXT_BOOK));

        verify(translationService, never()).openNewChapter(any());
    }

    @Test
    void actionPerformed_shouldNotCallTranslationServiceToOpenNewChapter_whenActionCommandIsPreviousBookAndCurrentBookIsTheFirstOne() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, PREVIOUS_BOOK));

        verify(translationService, never()).openNewChapter(any());
    }

    @Test
    void actionPerformed_shouldThrowException_whenActionCommandIsInvalid() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        ActionEvent invalidActionCommand = new ActionEvent(new Object(), 0, "Invalid action command");

        assertThrows(IllegalArgumentException.class, () -> underTest.actionPerformed(invalidActionCommand));
    }

    @Test
    void actionPerformed_shouldCallTranslationServiceToOpenNewChapter_whenCurrentHasNextOnaAndActionCommandIsNextChapter() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 1));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, NEXT_CHAPTER));

        verify(translationService).openNewChapter(createChapter("NIV", MATTHEW, 2));
    }

    @Test
    void actionPerformed_shouldCallTranslationServiceToOpenNewChapter_whenCurrentHasPreviousOnaAndActionCommandIsPreviousChapter() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, PREVIOUS_CHAPTER));

        verify(translationService).openNewChapter(createChapter("NIV", MATTHEW, 1));
    }

    @Test
    void actionPerformed_shouldCallTranslationServiceToOpenNewChapter_whenCurrentsBookHasNextOnaAndActionCommandIsNextBook() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MATTHEW, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, NEXT_BOOK));

        verify(translationService).openNewChapter(createChapter("NIV", MARK, 1));
    }

    @Test
    void actionPerformed_shouldCallTranslationServiceToOpenNewChapter_whenCurrentsBookHasPreviousOnaAndActionCommandIsNPreviousBook() {
        when(translationService.getCurrentChapter()).thenReturn(createChapter("NIV", MARK, 2));
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation("NIV", "en"));
        underTest.actionPerformed(new ActionEvent(new Object(), 0, PREVIOUS_BOOK));

        verify(translationService).openNewChapter(createChapter("NIV", MATTHEW, 1));
    }
}
