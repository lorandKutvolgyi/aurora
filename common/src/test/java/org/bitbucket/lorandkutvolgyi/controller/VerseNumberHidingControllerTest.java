/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.HiddenVerseNumberStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.presenter.VerseNumberHidingPresenter;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class VerseNumberHidingControllerTest {

    @Test
    void test_changeHidingState() {
        MessageBus messageBus = mock(MessageBus.class);
        VerseNumberHidingPresenter presenter = mock(VerseNumberHidingPresenter.class);
        VerseNumberHidingController underTest = new VerseNumberHidingController(messageBus, presenter);

        underTest.changeState(true);
        verify(messageBus).publish(new HiddenVerseNumberStateChangedEvent(true));
        verify(presenter).setTooltip(true);

        underTest.changeState(false);
        verify(messageBus).publish(new HiddenVerseNumberStateChangedEvent(false));
        verify(presenter).setTooltip(false);
    }
}
