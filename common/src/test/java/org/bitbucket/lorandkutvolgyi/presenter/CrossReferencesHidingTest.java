/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.controller.CrossReferencesHidingController;
import org.bitbucket.lorandkutvolgyi.event.HiddenCrossReferencesStateChangedEvent;
import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CrossReferencesHidingTest {

    @Test
    void test_tooltipChanges() {
        ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
        CrossReferencesHidingPresenter presenter = new CrossReferencesHidingPresenter(messagesBundle);
        MessageBus messageBus = mock(MessageBus.class);
        CrossReferencesHidingController controller = new CrossReferencesHidingController(messageBus, presenter);

        controller.changeState(true);
        assertEquals(messagesBundle.getString("SHOW_CROSS_REFERENCES"), presenter.getTooltip());
        verify(messageBus).publish(new HiddenCrossReferencesStateChangedEvent(true));

        controller.changeState(false);
        assertEquals(messagesBundle.getString("HIDE_CROSS_REFERENCES"), presenter.getTooltip());
        verify(messageBus).publish(new HiddenCrossReferencesStateChangedEvent(false));
    }
}
