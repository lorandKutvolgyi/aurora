/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.TestFixtureUtil;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.StatusBarTextChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.TabToCloseEvent;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class MainWindowPresenterTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private TranslationService translationService;
    private MainWindowPresenter underTest;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);

        ArrayList<Translation> translations = new ArrayList<>();
        translations.add(TestFixtureUtil.createTranslation("ORIGINAL", "original"));
        translations.add(TestFixtureUtil.createTranslation("NIV", "en"));
        translations.add(TestFixtureUtil.createTranslation("RÚF", "hu"));

        when(translationService.getDownloadedTranslations()).thenReturn(translations);

        underTest = new MainWindowPresenter(translationService, messageBus, messagesBundle);
    }

    @Test
    void getStatusLabelText_shouldReturnTranslationsNameWithInfo() {
        when(translationService.getCurrentTranslation()).thenReturn(TestFixtureUtil.createTranslation("NIV", "en"));

        String statusLabelText = underTest.getStatusLabelText();

        assertEquals("NIV - NIV info text", statusLabelText);
    }

    @Test
    void getStatusLabelText_shouldReturnTranslationsNameWithInfoFromMessagesBundle_whenTranslationIsORIGINAL() {
        Translation translation = TestFixtureUtil.createTranslation("ORIGINAL", "ORIGINAL");
        translation.setInfo("ORIGINAL_INFO");
        when(translationService.getCurrentTranslation()).thenReturn(translation);

        String statusLabelText = underTest.getStatusLabelText();

        assertEquals("Eredeti - Nem jogvédett. Ósz-The Westminster Leningrad Codex, https://www.biblegateway.com/; Úsz-B.F.Westcott és F.J.A. Hort, https://archive.org/details/NewTestamentGreekTextWescottHort18811903/page/n1/mode/2up", statusLabelText);
    }

    @Test
    void getDownloadedTranslationNames_shouldReturnDownloadedTranslationsNames() {
        Map<String, List<String>> result = underTest.getDownloadedTranslationNames();

        assertEquals("NIV", result.get("en").get(0));
        assertEquals("RÚF", result.get("hu").get(0));
    }

    @Test
    void closeTextTab_shouldPublishTabToCloseEventToMessageBus() {
        String tabId = "tabId";
        underTest.closeTextTab(tabId);

        verify(messageBus).publish(new TabToCloseEvent(tabId));
    }

    @Test
    void setStatusLabel_shouldPublishStatusBarTextChangedEventToMessageBus() {
        Translation translation = TestFixtureUtil.createTranslation("NIV", "en");

        underTest.setStatusLabel(new CurrentTranslationChangedEvent(translation));

        verify(messageBus).publish(new StatusBarTextChangedEvent("NIV - NIV info text"));
    }

    @Test
    void getLanguages_shouldReturnDefaults() {
        Map<String, String> languages = underTest.getLanguages();

        assertEquals("English", languages.get("en"));
        assertEquals("Magyar", languages.get("hu"));
    }

    @Test
    void getTooltip_shouldReturnTranslatedText_inCaseOfOriginalText() {
        String result = underTest.getTooltip("ORIGINAL");

        assertEquals(messagesBundle.getString("ORIGINAL_INFO"), result);
    }

    @Test
    void getTooltip_shouldReturnInfoText_inCaseOfOtherThanOriginalText() {
        String result = underTest.getTooltip("NIV");

        assertEquals("NIV info text", result);
    }
}
