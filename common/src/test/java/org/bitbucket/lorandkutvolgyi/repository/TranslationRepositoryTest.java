/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TranslationRepositoryTest {

    private static final File home = new File(Objects.requireNonNull(TranslationRepositoryTest.class.getClassLoader().getResource("home")).getFile());
    private static TranslationRepository underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)), home.getAbsolutePath());
    }

    @BeforeEach
    void setUp() {
        underTest = new TranslationRepository(home.getAbsolutePath());
    }

    @AfterEach
    void reset() {
        removeTempWithContent();
        underTest.closeDb();
    }

    @Test
    void getDownloadedTranslation_shouldReturnAllTranslationUnderTextDirectory() {
        List<Translation> result = underTest.getDownloadedTranslations();
        Set<String> names = result.stream().map(Translation::getName).collect(Collectors.toSet());

        assertEquals(2, result.size());
        assertTrue(names.contains("HUNKAR"));
        assertTrue(names.contains("RÚF"));
    }

    @Test
    void save_shouldSaveChapterIntoDb() {
        List<Translation> downloadedTranslations = underTest.getDownloadedTranslations();
        Chapter chapter = downloadedTranslations.get(0).getOldTestament().get(0).getChapters().get(0);
        chapter.setComments("new comments");

        underTest.save(chapter);

        underTest.closeDb();
        underTest = new TranslationRepository(home.getAbsolutePath());
        assertEquals("new comments", underTest.getDownloadedTranslations().get(0).getOldTestament().get(0).getChapters().get(0).getComments());
    }

    @Test
    void getDownloadedTranslation_shouldReturnAllTranslationWithMaps() {
        List<Translation> result = underTest.getDownloadedTranslations();

        assertEquals(2, result.size());
        assertNotNull(result.get(0).getBooks().get(0).getLabel().getMaps());
        assertNotNull(result.get(1).getBooks().get(0).getLabel().getMaps());
    }

    private static void removeTempWithContent() {
        File temp = new File(Paths.get("src/test/temp").toString());

        removeContent(temp);
        removeDir(temp);
    }

    private static void removeContent(File dir) {
        File[] allContents = dir.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                if (!file.delete()) {
                    throw new FileCannotBeDeletedException(file.getAbsolutePath());
                }
            }
        }
    }

    private static void removeDir(File dir) {
        dir.deleteOnExit();
    }
}
