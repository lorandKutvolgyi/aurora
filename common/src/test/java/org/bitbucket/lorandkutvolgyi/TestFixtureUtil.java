/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;

import java.util.ArrayList;
import java.util.HashMap;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;

public class TestFixtureUtil {

    public static Translation createTranslation(String name, String language) {
        Translation result = new Translation();
        result.setOldTestament(new ArrayList<>());
        ArrayList<Book> newTestament = new ArrayList<>();
        ArrayList<Chapter> chaptersOfMatthew = new ArrayList<>();
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 1));
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 2));
        ArrayList<Chapter> chaptersOfMark = new ArrayList<>();
        chaptersOfMark.add(createChapter(name, MARK, 1));
        newTestament.add(new Book(MATTHEW.name(), MATTHEW, chaptersOfMatthew));
        newTestament.add(new Book(MARK.name(), MARK, chaptersOfMark));
        result.setNewTestament(newTestament);
        result.setName(name);
        result.setInfo(name.equals("ORIGINAL") ? "ORIGINAL_INFO" : name + " info text");
        result.setLanguage(language);
        return result;
    }

    public static Chapter createChapter(String translation, BookLabel bookLabel, int number) {
        return new Chapter(translation, bookLabel.name(), number, new ArrayList<>(), "", new HashMap<>());
    }
}
