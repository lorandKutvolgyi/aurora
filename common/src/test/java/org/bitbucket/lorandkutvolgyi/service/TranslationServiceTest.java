/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.TestFixtureUtil;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.repository.TranslationRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TranslationServiceTest {

    private final ArrayList<Translation> translationsFromRepository = new ArrayList<>();
    private TranslationService underTest;
    private TextTabManager textTabManager;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        TranslationRepository translationRepository = mock(TranslationRepository.class);
        translationsFromRepository.add(TestFixtureUtil.createTranslation("NIV", "en"));
        translationsFromRepository.add(TestFixtureUtil.createTranslation("RÚF", "hu"));
        messageBus = mock(MessageBus.class);
        when(translationRepository.getDownloadedTranslations()).thenReturn(translationsFromRepository);
        underTest = new TranslationService(translationRepository, messageBus);
        textTabManager = mock(TextTabManager.class);
        underTest.setTextTabManager(textTabManager);
    }

    @Test
    void openNewChapter_byChapter_shouldSetCurrentChapter_andStoreIt_andPublishNewChapterEventToMessageBus() {
        Chapter chapter = TestFixtureUtil.createChapter("NIV", MATTHEW, 1);

        underTest.openNewChapter(chapter);

        assertEquals(chapter, underTest.getCurrentChapter());
        verify(messageBus).publish(new CurrentChapterChangingEvent(chapter));
        verify(messageBus).publish(new NewChapterEvent(chapter));
    }

    @Test
    void openNewChapter_byBookNameAndChapter_shouldSetCurrentChapterWithCurrentTranslation_andStoreIt_andPublishNewChapterEventToMessageBus() {
        Chapter chapter = TestFixtureUtil.createChapter("NIV", MATTHEW, 1);
        underTest.selectTranslation("NIV");

        underTest.openNewChapter(MATTHEW.name(), 1);

        assertEquals(MATTHEW.name(), underTest.getCurrentChapter().getBookName());
        assertEquals("NIV", underTest.getCurrentChapter().getTranslationName());
        assertEquals(1, underTest.getCurrentChapter().getNumber());
        verify(messageBus).publish(new CurrentChapterChangingEvent(chapter));
        verify(messageBus).publish(new NewChapterEvent(chapter));
    }

    @Test
    void openNewChapter_byBookNameAndChapterAndTranslation_shouldSetCurrentChapter_andStoreIt_andPublishNewChapterEventToMessageBus() {
        Chapter chapter = TestFixtureUtil.createChapter("RÚF", MATTHEW, 1);
        underTest.openNewChapter("RÚF", MATTHEW.name(), 1);

        assertEquals(MATTHEW.name(), underTest.getCurrentChapter().getBookName());
        assertEquals("RÚF", underTest.getCurrentChapter().getTranslationName());
        assertEquals(1, underTest.getCurrentChapter().getNumber());
        verify(messageBus).publish(new CurrentChapterChangingEvent(chapter));
        verify(messageBus).publish(new NewChapterEvent(chapter));
    }

    @Test
    void openNewChapter_byBookLabelAndChapterAndTranslation_shouldSetCurrentChapter_andStoreIt_andPublishNewChapterEventToMessageBus() {
        Chapter chapter = TestFixtureUtil.createChapter("RÚF", MATTHEW, 1);
        underTest.openNewChapter("RÚF", MATTHEW, 1);

        assertEquals(MATTHEW.name(), underTest.getCurrentChapter().getBookName());
        assertEquals("RÚF", underTest.getCurrentChapter().getTranslationName());
        assertEquals(1, underTest.getCurrentChapter().getNumber());
        verify(messageBus).publish(new CurrentChapterChangingEvent(chapter));
        verify(messageBus).publish(new NewChapterEvent(chapter));
    }

    @Test
    void selectChapter_shouldSetCurrentChapterWithStoredChapter_andPublishCurrentChapterChangingEventToMessageBus() {
        UUID tabId = UUID.randomUUID();
        when(textTabManager.getSelectedTextTab()).thenReturn(tabId);
        Chapter chapter1 = TestFixtureUtil.createChapter("NIV", MATTHEW, 1);
        underTest.openNewChapter(chapter1);
        when(textTabManager.getSelectedTextTab()).thenReturn(UUID.randomUUID());
        Chapter chapter2 = TestFixtureUtil.createChapter("RÚF", MATTHEW, 2);
        underTest.openNewChapter(chapter2);

        underTest.selectChapter(tabId);

        assertEquals(chapter1, underTest.getCurrentChapter());
        assertEquals(chapter1.getTranslationName(), underTest.getCurrentChaptersTranslationName());
        verify(messageBus, times(2)).publish(new CurrentChapterChangingEvent(chapter1));
        verify(messageBus, times(1)).publish(new CurrentChapterChangingEvent(chapter2));
    }

    @Test
    void afterClosingAChapter_itCannotBeOpen() {
        UUID tabId = UUID.randomUUID();
        when(textTabManager.getSelectedTextTab()).thenReturn(tabId);
        Chapter chapter = TestFixtureUtil.createChapter("NIV", MATTHEW, 1);
        underTest.openNewChapter(chapter);

        underTest.closeChapter(tabId);
        underTest.selectChapter(tabId);

        assertNull(underTest.getCurrentChapter());
    }

    @Test
    void selectTranslation_shouldSetTranslationCorrectly_andPublishCurrentTranslationChangingEventToMessageBus() {
        underTest.selectTranslation("NIV");

        assertEquals("NIV", underTest.getCurrentTranslationName());
        verify(messageBus).publish(new CurrentTranslationChangedEvent(underTest.getCurrentTranslation()));
    }

    @Test
    void getDownloadedTranslations_shouldProvideWhatComesFromRepository() {
        List<Translation> downloadedTranslations = underTest.getDownloadedTranslations();

        assertEquals(2, downloadedTranslations.size());
        assertEquals(translationsFromRepository.get(0), downloadedTranslations.get(0));
        assertEquals(translationsFromRepository.get(1), downloadedTranslations.get(1));
    }

    @Test
    void saveNewComments_shouldSaveCommentsIntoCurrentChapter() {
        Chapter chapter = TestFixtureUtil.createChapter("NIV", MATTHEW, 1);
        chapter.setComments("old comments");
        underTest.openNewChapter(chapter);

        underTest.saveNewComments("some comments");

        assertEquals("some comments", underTest.getCurrentChapter().getComments());
    }

    @Test
    void getTranslation_shouldProvideNull_whenTranslationNameIsEmpty() {
        assertNull(underTest.getTranslation(""));
    }

    @Test
    void getBook_shouldProvideBookByBookNameAndTranslationName() {
        Book book = underTest.getBook("NIV", "MATTHEW");

        assertEquals("MATTHEW", book.getName());
    }

    @Test
    void getNumberOfChapters_shouldProvideTheNumberForBookNameInCurrentTranslation() {
        underTest.selectTranslation("NIV");

        int number = underTest.getNumberOfChapters("MATTHEW");

        assertEquals(2, number);
    }

    @Test
    void getDownloadedTranslationNames_shouldProvideWhatComesFromRepository() {
        Set<String> downloadedTranslationNames = underTest.getDownloadedTranslationNames();

        assertEquals(2, downloadedTranslationNames.size());
        assertTrue(downloadedTranslationNames.contains(translationsFromRepository.get(0).getName()));
        assertTrue(downloadedTranslationNames.contains(translationsFromRepository.get(1).getName()));
    }

    @Test
    void getCurrentChapterComments_shouldProvideCurrentChapterComments() {
        String comments = "this is it";
        Chapter chapter = TestFixtureUtil.createChapter("RÚF", MATTHEW, 1);
        underTest.openNewChapter(chapter);
        underTest.saveNewComments(comments);

        String result = underTest.getCurrentChaptersComments();

        assertEquals(comments, result);
    }
}
