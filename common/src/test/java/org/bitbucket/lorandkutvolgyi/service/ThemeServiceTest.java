/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.DefaultTheme;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ThemeServiceTest {

    @Test
    void shouldGetTheDefaultTheme_whenNothingHasBeenSet() {
        ThemeService underTest = new ThemeService();

        assertTrue(underTest.getSelectedTheme() instanceof DefaultTheme);
        assertEquals(new Color(102, 135, 149, 243), underTest.getSelectedTheme().getMenuBarBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getMenuBarForegroundColor());
        assertEquals(new Color(181, 201, 210, 243), underTest.getSelectedTheme().getToolBarBackgroundColor());
        assertEquals(new Color(59, 82, 92, 243), underTest.getSelectedTheme().getStatusBarBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getStatusBarForegroundColor());
        assertEquals(new Color(255, 176, 111), underTest.getSelectedTheme().getMainBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getContentBackgroundColor());
        assertEquals(new Color(102, 135, 149, 243), underTest.getSelectedTheme().getSelectedTabBackgroundColor());
        assertEquals(new Color(165, 172, 175, 243), underTest.getSelectedTheme().getUnselectedTabBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getTabForegroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getButtonBackgroundColor());
        assertEquals(new Color(0, 0, 0), underTest.getSelectedTheme().getButtonForegroundColor());
        assertEquals(new Color(215, 234, 159), underTest.getSelectedTheme().getEvenRowBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getOddRowBackgroundColor());
        assertEquals(new Color(217, 241, 252, 243), underTest.getSelectedTheme().getMapPanelBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getInputBackgroundColor());
        assertEquals(new Color(217, 241, 252, 243), underTest.getSelectedTheme().getSearchInputBackgroundColor());
        assertEquals(new Color(192, 192, 192), underTest.getSelectedTheme().getDisabledSearchInputBackgroundColor());
        assertEquals(new Color(255, 255, 255), underTest.getSelectedTheme().getSearchResultBackgroundColor());
        assertEquals(new Color(217, 241, 252, 243), underTest.getSelectedTheme().getPopupContentColor());
        assertEquals(new Color(255, 176, 111), underTest.getSelectedTheme().getPopupBorderColor());
        assertEquals(new Color(181, 201, 210, 243), underTest.getSelectedTheme().getAboutMenuBackground());
    }
}
