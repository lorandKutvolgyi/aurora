/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.junit.jupiter.api.*;

import java.util.Locale;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.LANGUAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class LocaleServiceTest {

    private static String LANGUAGE_VALUE;
    private LocaleService underTest;
    private Locale originalDefault;

    @BeforeAll
    static void beforeAll() {
        LANGUAGE_VALUE = PreferencesService.get(LANGUAGE);
    }

    @AfterAll
    static void afterAll() {
        if (LANGUAGE_VALUE != null) {
            PreferencesService.put(LANGUAGE, LANGUAGE_VALUE);
        }
    }

    @BeforeEach
    void setUp() {
        originalDefault = Locale.getDefault();
        PreferencesService.remove(LANGUAGE);
    }

    @AfterEach
    void tearDown() {
        Locale.setDefault(originalDefault);
    }

    @Test
    void testLocaleIsTheDefault_ifItIsValid_andThePreferencesIsEmpty() {
        Locale hu = new Locale.Builder().setLanguage("hu").setRegion("HU").build();
        Locale.setDefault(hu);
        underTest = new LocaleService(mock(RestartService.class));

        Locale locale = underTest.getSelectedLocale();

        assertEquals(hu.getLanguage(), locale.getLanguage());
    }

    @Test
    void testLocaleIsEnglish_ifTheDefaultIsInvalid_andThePreferencesIsEmpty() {
        Locale unknown = new Locale.Builder().setLanguage("de").setRegion("DE").build();
        Locale.setDefault(unknown);
        underTest = new LocaleService(mock(RestartService.class));

        Locale locale = underTest.getSelectedLocale();

        assertEquals(Locale.ENGLISH.getLanguage(), locale.getLanguage());
    }

    @Test
    void testLocaleIsTheSetOne_ifItIsKnown_andThePreferencesIsEmpty() {
        underTest = new LocaleService(mock(RestartService.class));
        underTest.setSelectedLanguageWithRestart("en");

        Locale locale = underTest.getSelectedLocale();

        assertEquals("en", locale.getLanguage());
    }

    @Test
    void testLocaleIsTheDefault_ifTheSetOneIsUnknown_andThePreferencesIsEmpty() {
        Locale hu = new Locale.Builder().setLanguage("hu").setRegion("HU").build();
        Locale.setDefault(hu);
        underTest = new LocaleService(mock(RestartService.class));
        underTest.setSelectedLanguageWithRestart("unknown");

        Locale locale = underTest.getSelectedLocale();

        assertEquals(hu.getLanguage(), locale.getLanguage());
    }

    @Test
    void testLocaleIsEnglish_ifBothTheSetOneAndTheDefaultAreUnknown_andThePreferencesIsEmpty() {
        Locale.setDefault(new Locale.Builder().setLanguage("de").setRegion("DE").build());
        underTest = new LocaleService(mock(RestartService.class));
        underTest.setSelectedLanguageWithRestart("fr");

        Locale locale = underTest.getSelectedLocale();

        assertEquals(Locale.ENGLISH.getLanguage(), locale.getLanguage());
    }

    @Test
    void testLocaleIsWhatInPreferences() {
        PreferencesService.put(LANGUAGE, "hu");
        Locale.setDefault(Locale.ENGLISH);
        underTest = new LocaleService(mock(RestartService.class));

        Locale locale = underTest.getSelectedLocale();

        assertEquals("hu", locale.getLanguage());
    }
}
