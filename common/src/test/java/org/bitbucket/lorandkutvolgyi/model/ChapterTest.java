/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ChapterTest {

    private Chapter chapter;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(new Verse(1, "text", "title", "NIV", BookLabel.MATTHEW.name(), BookLabel.MATTHEW, 1, new ArrayList<>()));
        HashMap<String, String> statistics = new HashMap<>();
        chapter = new Chapter("NIV", "Matthew", 1, verses, "comments", statistics);
    }

    @Test
    void shouldBeEquals_whenTranslationAndBookAndNumberEquals_EvenIfOthersAreNotEquals() {
        HashMap<String, String> statistics = new HashMap<>();
        statistics.put("key", "value");
        Chapter otherChapter = new Chapter("NIV", "Matthew", 1, new ArrayList<>(), "comments", statistics);

        assertEquals(chapter.hashCode(), otherChapter.hashCode());
        assertEquals(chapter, otherChapter);
        assertNotEquals(chapter.getStatistics(), otherChapter.getStatistics());
    }

    @Test
    void shouldBeEqualsWithItself() {
        assertEquals(chapter, chapter);
    }

    @Test
    void shouldNotBeEquals_inCaseOfNullOrHavingOtherClass() {
        assertNotEquals(chapter, new Object());
    }

    @Test
    void toString_shouldReturnEmptyString_whenNumberIsMinusOne() {
        chapter.setNumber(-1);

        assertEquals("", chapter.toString());
    }

    @Test
    void toString_shouldReturnOnlyTranslationAndBookAndNumber() {
        assertTrue(chapter.toString().contains(chapter.getTranslationName()));
        assertTrue(chapter.toString().contains(chapter.getBookName()));
        assertTrue(chapter.toString().contains(String.valueOf(chapter.getNumber())));

        assertFalse(chapter.toString().contains(String.valueOf(chapter.getVerses().get(0).getText())));
        assertFalse(chapter.toString().contains(String.valueOf(chapter.getTitle())));
        assertFalse(chapter.toString().contains(String.valueOf(chapter.getComments())));
    }
}
