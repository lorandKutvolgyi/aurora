/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import dorkbox.messageBus.MessageBus;
import org.awaitility.Awaitility;
import org.bitbucket.lorandkutvolgyi.controller.UpdateController;
import org.bitbucket.lorandkutvolgyi.controller.UpdatePopupManager;
import org.bitbucket.lorandkutvolgyi.event.CursorChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.UpdateEvent;
import org.bitbucket.lorandkutvolgyi.service.UpdateService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class UpdateControllerTest {

    private static HttpClient.Builder httpClientBuilder;
    @SuppressWarnings("rawtypes")
    private static HttpResponse httpResponse;
    private static UpdatePopupManager popupManager;
    private static MessageBus messageBus;
    private static MockedStatic<HttpClient> httpClientMockedStatic;
    private final String currentVersion = "0.0.16";
    private final String WORKING_DIRECTORY = System.getProperty("user.home") != null ? System.getProperty("user.home") + "/test" : "." + "/test";
    private HttpClient httpClient;
    private UpdateController underTest;

    @BeforeAll
    static void init() {
        httpClientMockedStatic = mockStatic(HttpClient.class);
        httpClientBuilder = mock(HttpClient.Builder.class);
        httpClientMockedStatic.when(HttpClient::newBuilder).thenReturn(httpClientBuilder);
    }

    @AfterAll
    public static void close() {
        httpClientMockedStatic.close();
    }

    @BeforeEach
    @SuppressWarnings({"unchecked"})
    void setUp() throws IOException, InterruptedException {
        httpResponse = mock(HttpResponse.class);
        popupManager = mock(UpdatePopupManager.class);
        messageBus = mock(MessageBus.class);
        httpClient = mock(HttpClient.class);

        when(httpClientBuilder.version(HttpClient.Version.HTTP_1_1)).thenReturn(httpClientBuilder);
        when(httpClientBuilder.followRedirects(HttpClient.Redirect.NORMAL)).thenReturn(httpClientBuilder);
        when(httpClientBuilder.connectTimeout(Duration.ofSeconds(20))).thenReturn(httpClientBuilder);
        when(httpClientBuilder.build()).thenReturn(httpClient);

        doReturn(httpResponse).when(httpClient).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));

        underTest = new UpdateController(new UpdateService(messageBus, WORKING_DIRECTORY), popupManager, messageBus);
    }

    @Test
    void hasNewerVersion_shouldReturnTrue_whenThereIsNewerVersionOnTheServer() {
        String newerVersion = "1.0.1";
        when(httpResponse.body()).thenReturn(newerVersion);

        boolean response = underTest.hasNewerVersion(currentVersion);

        assertTrue(response);
    }

    @Test
    void hasNewerVersion_shouldReturnFalse_whenThereIsNoNewerVersionOnTheServer() {
        when(httpResponse.body()).thenReturn(currentVersion);

        boolean response = underTest.hasNewerVersion(currentVersion);

        assertFalse(response);
    }

    @Test
    void hasNewerVersion_shouldReturnFalse_whenTheWebsiteCannotBeReached() throws IOException, InterruptedException {
        doThrow(new IOException()).when(httpClient).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));

        boolean response = underTest.hasNewerVersion(currentVersion);

        assertFalse(response);
    }

    @Test
    void update_whenSuccessful_shouldPublishUpdateEventWithPath() {
        JFrame parent = mock(JFrame.class);
        when(popupManager.showPopup(parent)).thenReturn(0);
        when(httpResponse.statusCode()).thenReturn(200);
        when(httpResponse.body()).thenReturn(Path.of(WORKING_DIRECTORY));

        underTest.update(parent);

        Awaitility.await().untilAsserted(() -> verify(messageBus).publish(eq(new UpdateEvent(Path.of(WORKING_DIRECTORY).toAbsolutePath()))));
    }

    @Test
    void update_whenUnsuccessful_shouldPublishUpdateEventWithNull() {
        JFrame parent = mock(JFrame.class);
        when(popupManager.showPopup(parent)).thenReturn(0);
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpResponse.body()).thenReturn(null);

        underTest.update(parent);

        Awaitility.await().untilAsserted(() -> verify(messageBus).publish(eq(new UpdateEvent(null))));
    }

    @Test
    void update_shouldPublishCursorChangeEvent() {
        JFrame parent = mock(JFrame.class);
        when(popupManager.showPopup(parent)).thenReturn(0);
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpResponse.body()).thenReturn(null);

        underTest.update(parent);

        verify(messageBus).publish(new CursorChangeEvent(Cursor.WAIT_CURSOR));
    }
}
