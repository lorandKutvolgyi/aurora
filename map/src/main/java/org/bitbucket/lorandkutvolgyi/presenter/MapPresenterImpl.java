/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import lombok.Getter;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.MapPanelModel;
import org.bitbucket.lorandkutvolgyi.service.MapPresenter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MapPresenterImpl implements MapPresenter {

    @Getter
    private final MapPanelModel mapPanelModel;

    private final List<File> maps = new ArrayList<>();

    public MapPresenterImpl() {
        this.mapPanelModel = new MapPanelModel();
    }

    @Override
    public void setCurrentBookWithIndex(BookLabel bookLabel, int index) {
        populateMaps(bookLabel);
        setModelProperties(index);
    }

    @Override
    public void setIndex(int index) {
        setModelProperties(index);
    }

    private void populateMaps(BookLabel bookLabel) {
        maps.clear();
        maps.addAll(bookLabel.getMaps());
    }

    private void setModelProperties(int index) {
        mapPanelModel.setCurrentMap(maps.get(index));
        mapPanelModel.setPreviousEnabled(index > 0);
        mapPanelModel.setNextEnabled(index < maps.size() - 1);
    }
}
