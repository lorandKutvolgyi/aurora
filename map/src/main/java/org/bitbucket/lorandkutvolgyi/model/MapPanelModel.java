/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.model;

import lombok.Getter;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;

@Getter
public class MapPanelModel {

    private final PropertyChangeSupport support = new PropertyChangeSupport(this);

    private File currentMap;
    private boolean isPreviousEnabled;
    private boolean isNextEnabled;

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    public void setCurrentMap(File currentMap) {
        File oldValue = this.currentMap;
        this.currentMap = currentMap;
        support.firePropertyChange("currentMap", oldValue, currentMap);
    }

    public void setPreviousEnabled(boolean isPreviousEnabled) {
        boolean oldValue = this.isPreviousEnabled;
        this.isPreviousEnabled = isPreviousEnabled;
        support.firePropertyChange("isPreviousEnabled", oldValue, isPreviousEnabled);
    }

    public void setNextEnabled(boolean isNextEnabled) {
        boolean oldValue = this.isNextEnabled;
        this.isNextEnabled = isNextEnabled;
        support.firePropertyChange("isNextEnabled", oldValue, isNextEnabled);
    }
}
