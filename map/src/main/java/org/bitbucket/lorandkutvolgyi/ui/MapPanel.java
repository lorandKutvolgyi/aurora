/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import lombok.SneakyThrows;
import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.util.XMLResourceDescriptor;
import org.bitbucket.lorandkutvolgyi.controller.MapController;
import org.bitbucket.lorandkutvolgyi.model.MapPanelModel;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.service.MapPresenter;
import org.w3c.dom.svg.SVGDocument;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MapPanel extends SavablePanel {

    private final transient Theme selectedTheme;
    private final transient MapController controller;
    private final transient MapPresenter presenter;
    private JButton nextMapActivator;
    private JButton previousMapActivator;
    private JSVGCanvas canvas;
    private MapPanelModel viewModel;

    public MapPanel(Theme selectedTheme, MapController controller, MapPresenter presenter) {
        this.selectedTheme = selectedTheme;
        this.controller = controller;
        this.presenter = presenter;

        createCanvas();
        setLayout(new BorderLayout());
        createPreviousMapActivator();
        createNextMapActivator();
        listenToViewModel();

        canvas.add(previousMapActivator, getGridBagConstraintsOfPrevious());
        canvas.add(nextMapActivator, getGridBagConstraintsOfNext());

        add(canvas);
    }

    @SneakyThrows
    private void createCanvas() {
        canvas = new JSVGCanvas();
        canvas.setLayout(new GridBagLayout());
        canvas.setBackground(selectedTheme.getMapPanelBackgroundColor());
        showCurrentMapOnCanvas();
    }

    private void createPreviousMapActivator() {
        previousMapActivator = new JButton("<");
        previousMapActivator.setSize(10, 10);
        previousMapActivator.setOpaque(false);
        previousMapActivator.setEnabled(false);
        previousMapActivator.addActionListener(evt -> controller.setPreviousToCurrent());
    }

    private void createNextMapActivator() {
        nextMapActivator = new JButton(">");
        nextMapActivator.setSize(10, 10);
        nextMapActivator.setOpaque(false);
        nextMapActivator.setEnabled(viewModel.isNextEnabled());
        nextMapActivator.addActionListener(evt -> controller.setNextToCurrent());
    }

    private void listenToViewModel() {
        viewModel.addPropertyChangeListener(this::handlePropertyChange);
    }

    private GridBagConstraints getGridBagConstraintsOfPrevious() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 0.5;
        constraints.gridwidth = 1;
        constraints.insets = new Insets(0, 10, 0, 0);
        constraints.anchor = GridBagConstraints.LINE_START;
        return constraints;
    }

    private GridBagConstraints getGridBagConstraintsOfNext() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 0.5;
        constraints.insets = new Insets(0, 0, 0, 10);
        constraints.anchor = GridBagConstraints.LINE_END;
        return constraints;
    }

    private void showCurrentMapOnCanvas() throws IOException {
        viewModel = presenter.getMapPanelModel();
        File currentMap = viewModel.getCurrentMap();
        if (currentMap != null) {
            canvas.setSVGDocument((SVGDocument) new SAXSVGDocumentFactory(XMLResourceDescriptor.getXMLParserClassName())
                    .createDocument("", new FileReader(currentMap)));
        }
    }

    @SneakyThrows
    private void handlePropertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "currentMap" -> canvas.setSVGDocument((SVGDocument) new SAXSVGDocumentFactory(XMLResourceDescriptor.getXMLParserClassName())
                    .createDocument("", new FileReader((File) evt.getNewValue())));
            case "isPreviousEnabled" -> previousMapActivator.setEnabled((boolean) evt.getNewValue());
            case "isNextEnabled" -> nextMapActivator.setEnabled((boolean) evt.getNewValue());
        }
    }
}