/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.controller.MapService;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MapServiceImpl implements MapService {

    private final Map<UUID, Integer> indicesByTab = new HashMap<>();
    private final Map<UUID, BookLabel> bookLabelsByTab = new HashMap<>();
    private final MapPresenter presenter;
    private TextTabManager textTabManager;

    public MapServiceImpl(MapPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setTextTabManager(TextTabManager textTabManager) {
        this.textTabManager = textTabManager;
    }

    @Override
    public void handleChapterChange(BookLabel bookLabel) {
        UUID currentTab = textTabManager.getSelectedTextTab();
        if (isNewBookInTheCurrentTab(bookLabel, currentTab)) {
            indicesByTab.put(currentTab, 0);
            bookLabelsByTab.put(currentTab, bookLabel);
            presenter.setCurrentBookWithIndex(bookLabel, 0);
        } else {
            presenter.setCurrentBookWithIndex(bookLabel, indicesByTab.get(currentTab));
        }
    }

    @Override
    public void setPreviousToCurrent() {
        UUID currentTab = textTabManager.getSelectedTextTab();
        Integer savedIndex = indicesByTab.get(currentTab);
        indicesByTab.put(currentTab, --savedIndex);
        presenter.setIndex(savedIndex);
    }

    @Override
    public void setNextToCurrent() {
        UUID currentTab = textTabManager.getSelectedTextTab();
        Integer savedIndex = indicesByTab.get(currentTab);
        indicesByTab.put(currentTab, ++savedIndex);
        presenter.setIndex(savedIndex);
    }

    private boolean isNewBookInTheCurrentTab(BookLabel bookLabel, UUID selectedTab) {
        return indicesByTab.get(selectedTab) == null || bookLabelsByTab.get(selectedTab) != bookLabel;
    }
}
