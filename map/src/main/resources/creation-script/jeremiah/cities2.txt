35.216667    31.783333    #Jerusalem Jeruzsálem
35.195       32.276389    #Samaria Samária
35.2647      31.8141      #Anathoth Anátót
35.229033    31.653017    #Tekoa Tekóa
34.566667    31.666667    #Ashkelon Askelón
34.45        31.516667    #Gaza Gáza
34.65        31.8         #Ashdod Asdód
34.84992     31.77889     #Ekron Ekrón
35.1124      31.8048      #Kiriath Jearim Kirjat-Jeárim
35.183333    31.847222    #Gibeon Gibeón
35.181       31.833       #Ramah Rámá
34.935833    31.700278    #Azekah Azéká
34.848889    31.565       #Lachish Lákís
35.462222    31.855556    #Jericho Jerikó
35.216389    31.885278    #Mizpah Micpa
35.221944    31.941944    #Bethel Bétel
35.289528    32.055556    #Shiloh Siló
35.311111    32.203056    #Shechem Sikem
