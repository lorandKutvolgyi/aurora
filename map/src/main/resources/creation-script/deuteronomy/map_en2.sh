#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#

gmt begin map2
gmt coast -R33.5/36.5/30.5/33.5 -Ir/0.5p,20/255/247 -JM6i -GlightBrown -S28/255/255 -Ln0.75/0.06+c32+w50k+f+u
gmt psxy -Sp0.1 cities.txt
gmt text en/horizontal-texts2.txt
gmt pstext -F+a75 en/canaan-text2.txt
gmt pstext -F+a85 en/jordan-text.txt
gmt pstext -F+a85 en/salt-sea-text.txt
gmt end
pdf2svg map2.pdf en/map2.svg
rm map2.pdf
