#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#

gmt begin map
gmt coast -R11.60/32/34.50/43 -I1/0.5p,20/255/247 -JM6i -GlightBrown -S28/255/255 -Ln0.8/0.09+c32+w500k+f+u -A251
gmt psxy -Sp0.08 cities.txt
gmt pstext en/horizontal-texts.txt
gmt pstext -F+a30 en/macedonia-text.txt
gmt end
pdf2svg map.pdf en/map.svg
rm map.pdf
