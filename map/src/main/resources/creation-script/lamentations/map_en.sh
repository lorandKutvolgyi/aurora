#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#

gmt begin map
gmt coast -R29/40.90/29/36.7 -I1/1p,20/255/247 -JM6i -GlightBrown -S28/255/255 -Ln0.75/0.07+c32+w400k+f+u
gmt psxy -Sp0.1 cities.txt
gmt pstext en/horizontal-texts.txt
gmt end
pdf2svg map.pdf en/map.svg
rm map.pdf
