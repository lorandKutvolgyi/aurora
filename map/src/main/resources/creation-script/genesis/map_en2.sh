#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#

gmt begin map2
gmt coast -R28.5/49.9/24/40 -Ir/0.5p,20/255/247 -JM6i -GlightBrown -S28/255/255 -Ln0.79/0.06+c32+w400k+f+u
gmt text en/horizontal-texts2.txt
gmt text en/mediterranean-sea-text2.txt
gmt psxy -Sp0.1 cities2.txt
gmt pstext -F+a80 en/canaan-text2.txt
gmt pstext -F+a-53 en/nile-text.txt
gmt pstext -F+a-37 en/chaldea-text.txt
gmt pstext -F+a-45 tigris-text.txt
gmt end
pdf2svg map2.pdf en/map2.svg
rm map2.pdf
