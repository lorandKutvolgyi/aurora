/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.presenter.MapPresenterImpl;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.MapServiceImpl;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TextTabManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MapControllerTest {

    private final TextTabManager textTabManager = mock(TextTabManager.class);
    private final UUID uuid1 = UUID.randomUUID();
    private final UUID uuid2 = UUID.randomUUID();
    private final File fileA = new File("/a");
    private final File fileB = new File("/b");
    private final File fileC = new File("/c");
    private final File fileD = new File("/d");
    private final File fileE = new File("/e");

    private MapController controller;
    private MapPresenterImpl presenter;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        presenter = new MapPresenterImpl();
        controller = new MapController(getMapService());
    }

    @Test
    void whenChapterWithOneMapIsShownFirstTime_thenItsMapShouldBeShown() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapA()));

        assertEquals(fileA, presenter.getMapPanelModel().getCurrentMap());
    }

    @Test
    void whenChapterWithOneMapIsShownFirstTime_thenBothButtonsShouldBeDisabled() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapA()));

        assertFalse(presenter.getMapPanelModel().isPreviousEnabled());
        assertFalse(presenter.getMapPanelModel().isNextEnabled());
    }

    @Test
    void whenChapterWithTwoMapsIsShownFirstTime_thenNextButtonsShouldBeEnabledAndPreviousDisabled() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));

        assertFalse(presenter.getMapPanelModel().isPreviousEnabled());
        assertTrue(presenter.getMapPanelModel().isNextEnabled());
    }

    @Test
    void whenChapterWithTwoMapsIsShownFirstTime_andShowTheSecondMap_thenNextButtonsShouldBeDisabledAndPreviousEnabled() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));
        controller.setNextToCurrent();

        assertTrue(presenter.getMapPanelModel().isPreviousEnabled());
        assertFalse(presenter.getMapPanelModel().isNextEnabled());
    }

    @Test
    void whenChapterWithTwoMapsIsShownFirstTime_andShowTheSecondMapThenTheFirstOne_thenNextButtonsShouldBeEnabledAndPreviousDisabled() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1).thenReturn(uuid1).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));
        controller.setNextToCurrent();
        controller.setPreviousToCurrent();

        assertFalse(presenter.getMapPanelModel().isPreviousEnabled());
        assertTrue(presenter.getMapPanelModel().isNextEnabled());
    }

    @Test
    void whenSecondMapIsShown_andChapterChanges_thenFirstMapOfTheNewChapterShouldBeShown() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid1).thenReturn(uuid1).thenReturn(uuid1);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));
        controller.setNextToCurrent();
        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsDAndE()));

        assertEquals(fileD, presenter.getMapPanelModel().getCurrentMap());
    }

    @Test
    void whenSecondMapIsShown_andTabChangesBackAndForth_thenStillSecondMapShouldBeShown() {
        when(textTabManager.getSelectedTextTab()).thenReturn(uuid2).thenReturn(uuid2).thenReturn(uuid1).thenReturn(uuid2);

        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));
        controller.setNextToCurrent();
        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsDAndE()));
        controller.handleChapterChange(new CurrentChapterChangingEvent(createChapterWithMapsBAndC()));

        assertEquals(fileC, presenter.getMapPanelModel().getCurrentMap());
    }

    private MapService getMapService() {
        MapService service = new MapServiceImpl(presenter);
        service.setTextTabManager(textTabManager);
        return service;
    }

    private Chapter createChapterWithMapA() {
        Chapter chapter = new Chapter();
        chapter.setBookLabel(BookLabel.MATTHEW);
        BookLabel.MATTHEW.getMaps().clear();
        BookLabel.MATTHEW.getMaps().add(fileA);
        return chapter;
    }

    private Chapter createChapterWithMapsBAndC() {
        Chapter chapter = new Chapter();
        chapter.setBookLabel(BookLabel.GENESIS);
        BookLabel.GENESIS.getMaps().clear();
        BookLabel.GENESIS.getMaps().add(fileB);
        BookLabel.GENESIS.getMaps().add(fileC);
        return chapter;
    }

    private Chapter createChapterWithMapsDAndE() {
        Chapter chapter = new Chapter();
        chapter.setBookLabel(BookLabel.DEUTERONOMY);
        BookLabel.DEUTERONOMY.getMaps().clear();
        BookLabel.DEUTERONOMY.getMaps().add(fileD);
        BookLabel.DEUTERONOMY.getMaps().add(fileE);
        return chapter;
    }
}
