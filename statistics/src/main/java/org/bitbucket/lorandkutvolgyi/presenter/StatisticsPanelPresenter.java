/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.StatisticsChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class StatisticsPanelPresenter {

    private final TranslationService translationService;
    private final MessageBus messageBus;
    private String[][] chapterRelatedData;
    private String[][] translationRelatedData;
    private final ResourceBundle messagesBundle;

    public StatisticsPanelPresenter(TranslationService translationService, MessageBus messageBus, ResourceBundle messagesBundle) {
        this.translationService = translationService;
        this.messageBus = messageBus;
        this.messagesBundle = messagesBundle;
    }

    @Subscribe
    public void setStatisticsData(CurrentChapterChangingEvent event) {
        Chapter newValue = event.getChapter();
        Map<String, String> translationStatistics = new HashMap<>();
        Map<String, String> chapterStatistics = new HashMap<>();
        if (newValue != null) {
            translationStatistics = translationService.getTranslation(newValue.getTranslationName()).getStatistics();
            chapterStatistics = newValue.getStatistics();
        }
        setTranslationRelatedData(translationStatistics);
        setChapterRelatedData(chapterStatistics);
        messageBus.publish(new StatisticsChangedEvent(chapterRelatedData, translationRelatedData));
    }

    public String[][] getChapterRelatedData() {
        return chapterRelatedData;
    }

    public String[][] getTranslationRelatedData() {
        return translationRelatedData;
    }

    private void setTranslationRelatedData(Map<String, String> statistics) {
        translationRelatedData = new String[statistics.size()][2];
        int i = 0;
        for (Map.Entry<String, String> entry : statistics.entrySet()) {
            translationRelatedData[i][0] = messagesBundle.getString(entry.getKey());
            translationRelatedData[i++][1] = entry.getValue();
        }
    }

    private void setChapterRelatedData(Map<String, String> statistics) {
        chapterRelatedData = new String[statistics.size()][2];
        int i = 0;
        for (Map.Entry<String, String> entry : statistics.entrySet()) {
            chapterRelatedData[i][0] = messagesBundle.getString(entry.getKey());
            chapterRelatedData[i++][1] = entry.getValue();
        }
    }
}
