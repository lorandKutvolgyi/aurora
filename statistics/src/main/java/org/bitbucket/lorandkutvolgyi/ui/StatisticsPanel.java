/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.StatisticsChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.Arrays;

public class StatisticsPanel extends JPanel {

    private final DefaultTableModel tableModel;

    public StatisticsPanel(Theme selectedTheme) {
        tableModel = new DefaultTableModel();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        Arrays.stream(new String[]{"", ""}).forEach(tableModel::addColumn);
        JScrollPane scrollPane = new JScrollPane(new StatisticsTable(tableModel));
        scrollPane.getViewport().setBackground(selectedTheme.getContentBackgroundColor());
        add(scrollPane);
    }

    @Subscribe
    public void update(StatisticsChangedEvent event) {
        while (tableModel.getRowCount() > 0) {
            tableModel.removeRow(0);
        }
        update(getGraphics());
        fillTableModel(event.getChapterRelatedData());
        fillTableModel(event.getTranslationRelatedData());
    }

    private void fillTableModel(String[][] data) {
        if (data != null) {
            Arrays.stream(data).forEach(tableModel::addRow);
        }
    }

    private static class StatisticsTable extends JTable {

        StatisticsTable(TableModel tableModel) {
            super(tableModel);
            setOpaque(false);
            setEnabled(false);
            setShowVerticalLines(false);
            setRowHeight(23);
        }

        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            JComponent cellRenderer = (JComponent) super.prepareRenderer(renderer, row, column);
            String cellContent = getValueAt(row, column).toString();
            cellRenderer.setToolTipText(cellContent);
            cellRenderer.setOpaque(false);
            return cellRenderer;
        }
    }
}
