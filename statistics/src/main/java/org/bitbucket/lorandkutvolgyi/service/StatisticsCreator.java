/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.TranslationRepository;

import java.util.*;

public class StatisticsCreator {

    private final TranslationRepository repository;

    public StatisticsCreator(TranslationRepository repository) {
        this.repository = repository;
    }

    public void create() {
        repository.getDownloadedTranslations().forEach(this::setStatistics);
    }

    private void setStatistics(Translation translation) {
        translation.getBooks()
                .forEach(book -> createStatistics(book.getChapters()));
        createStatistics(translation);
    }

    private void createStatistics(List<Chapter> chapters) {
        chapters.forEach(this::createStatistics);
    }

    private void createStatistics(Chapter chapter) {
        Map<String, String> statistics = new HashMap<>();
        statistics.put("CHAPTERS_SHORTEST_VERSE", getChaptersShortestVerse(chapter));
        statistics.put("CHAPTERS_LONGEST_VERSE", getChaptersLongestVerse(chapter));
        statistics.put("NUMBER_OF_VERSES", getNumberOfVerses(chapter));
        statistics.put("NUMBER_OF_CHARACTERS", getNumberOfCharacters(chapter));
        statistics.put("NUMBER_OF_WORDS", getNumberOfWords(chapter));
        chapter.setStatistics(statistics);
    }

    private void createStatistics(Translation translation) {
        Map<String, String> statistics = new LinkedHashMap<>();
        List<Book> books = translation.getBooks();
        statistics.put("NUMBER_OF_ALL_BOOKS", getNumberOfAllBooks(books));
        statistics.put("NUMBER_OF_ALL_CHAPTERS", getNumberOfAllChapters(books));
        statistics.put("NUMBER_OF_ALL_VERSES", getNumberOfAllVerses(books));
        statistics.put("NUMBER_OF_ALL_CHARACTERS", getNumberOfAllCharacters(books));
        statistics.put("SHORTEST_BOOK_BY_CHARACTERS", getShortestBookByCharacters(books));
        statistics.put("LONGEST_BOOK_BY_CHARACTERS", getLongestBookByCharacters(books));
        statistics.put("SHORTEST_CHAPTER_BY_VERSES", getShortestChapterByVerses(books));
        statistics.put("LONGEST_CHAPTER_BY_VERSES", getLongestChapterByVerses(books));
        statistics.put("SHORTEST_CHAPTER_BY_CHARACTERS", getShortestChapterByCharacters(books));
        statistics.put("LONGEST_CHAPTER_BY_CHARACTERS", getLongestChapterByCharacters(books));
        statistics.put("SHORTEST_VERSE", getShortestVerse(books));
        statistics.put("LONGEST_VERSE", getLongestVerse(books));
        translation.setStatistics(statistics);
    }

    private String getChaptersShortestVerse(Chapter chapter) {
        return String.valueOf(chapter.getVerses().parallelStream()
                .filter(verse -> verse.getText().length() > 0)
                .min(Comparator.comparing(verse -> verse.getText().replaceAll("<[^>]*>", "").length()))
                .orElseThrow(IllegalArgumentException::new)
                .getNumber());
    }

    private String getChaptersLongestVerse(Chapter chapter) {
        return String.valueOf(chapter.getVerses().parallelStream()
                .max(Comparator.comparing(verse -> verse.getText().replaceAll("<[^>]*>", "").length()))
                .orElseThrow(IllegalArgumentException::new)
                .getNumber());
    }

    private String getNumberOfVerses(Chapter chapter) {
        return String.valueOf(chapter.getVerses().size());
    }

    private String getNumberOfCharacters(Chapter chapter) {
        int whiteSpacesBetweenVerses = chapter.getVerses().size() - 1;
        return String.valueOf(chapter.getVerses().parallelStream()
                .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length())
                .reduce(0, Integer::sum) + whiteSpacesBetweenVerses);
    }

    private String getNumberOfWords(Chapter chapter) {
        return String.valueOf(chapter.getVerses().parallelStream()
                .map(verse -> verse.getText().replaceAll("<[^>]*>", ""))
                .flatMap(text -> Arrays.stream(text.split(" ")))
                .count());
    }

    private String getNumberOfAllBooks(List<Book> books) {
        return String.valueOf(books.size());
    }

    private String getNumberOfAllChapters(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .count());
    }

    private String getNumberOfAllVerses(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .flatMap(chapter -> chapter.getVerses().parallelStream())
                .count());
    }

    private String getNumberOfAllCharacters(List<Book> books) {
        int numberOfChapters = books.stream().map(book -> book.getChapters().size()).reduce(0, Integer::sum);
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .flatMap(chapter -> chapter.getVerses().parallelStream())
                .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length() + 1)
                .reduce(0, Integer::sum) - numberOfChapters);
    }

    private String getShortestBookByCharacters(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .min(Comparator.comparing(book -> book.getChapters().parallelStream()
                        .flatMap(chapter -> chapter.getVerses().parallelStream())
                        .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length())
                        .reduce(0, Integer::sum)))
                .orElseThrow(IllegalArgumentException::new)
                .getName());
    }

    private String getLongestBookByCharacters(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .max(Comparator.comparing(book -> book.getChapters().parallelStream()
                        .flatMap(chapter -> chapter.getVerses().parallelStream())
                        .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length())
                        .reduce(0, Integer::sum)))
                .orElseThrow(IllegalArgumentException::new)
                .getName());
    }

    private String getShortestChapterByVerses(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .min(Comparator.comparing(chapter -> chapter.getVerses().size()))
                .orElseThrow(IllegalArgumentException::new)
                .toString()
                .split(" -")[0]);
    }

    private String getLongestChapterByVerses(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .max(Comparator.comparing(chapter -> chapter.getVerses().size()))
                .orElseThrow(IllegalArgumentException::new)
                .toString()
                .split(" -")[0]);
    }

    private String getShortestChapterByCharacters(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .min(Comparator.comparing(chapter -> chapter.getVerses().parallelStream()
                        .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length())
                        .reduce(0, Integer::sum)))
                .orElseThrow(IllegalArgumentException::new)
                .toString()
                .split(" -")[0]);
    }

    private String getLongestChapterByCharacters(List<Book> books) {
        return String.valueOf(books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .max(Comparator.comparing(chapter -> chapter.getVerses().parallelStream()
                        .map(verse -> verse.getText().replaceAll("<[^>]*>", "").length())
                        .reduce(0, Integer::sum)))
                .orElseThrow(IllegalArgumentException::new)
                .toString()
                .split(" -")[0]);
    }

    private String getShortestVerse(List<Book> books) {
        Verse shortestVerse = books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .flatMap(chapter -> chapter.getVerses().parallelStream())
                .filter(verse -> verse.getText().length() > 0)
                .min(Comparator.comparing(verse -> verse.getText().replaceAll("<[^>]*>", "").length()))
                .orElseThrow(IllegalArgumentException::new);
        return shortestVerse.getBookName() + " " + shortestVerse.getChapterNumber() + ":" + shortestVerse.getNumber()
                + " (" + shortestVerse.getText().replaceAll("<[^>]*>", "").length() + ")";
    }

    private String getLongestVerse(List<Book> books) {
        Verse longestVerse = books.parallelStream()
                .flatMap(book -> book.getChapters().parallelStream())
                .flatMap(chapter -> chapter.getVerses().parallelStream())
                .max(Comparator.comparing(verse -> verse.getText().replaceAll("<[^>]*>", "").length()))
                .orElseThrow(IllegalArgumentException::new);
        return longestVerse.getBookName() + " " + longestVerse.getChapterNumber() + ":" + longestVerse.getNumber()
                + " (" + longestVerse.getText().replaceAll("<[^>]*>", "").length() + ")";
    }
}
