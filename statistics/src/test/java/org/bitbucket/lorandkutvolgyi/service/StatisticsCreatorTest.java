/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.TranslationRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.GENESIS;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StatisticsCreatorTest {

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @Test
    void create_shouldFillTranslationsWithStatisticsData() {
        TranslationRepository repository = mock(TranslationRepository.class);
        StatisticsCreator underTest = new StatisticsCreator(repository);
        Translation translation = createTranslation();

        when(repository.getDownloadedTranslations()).thenReturn(Collections.singletonList(translation));

        underTest.create();

        assertChapterStatistics(translation);
        assertTranslationStatistics(translation);
    }

    private static Translation createTranslation() {
        Translation result = new Translation();

        ArrayList<Book> oldTestament = new ArrayList<>();
        oldTestament.add(new Book(GENESIS.name(), GENESIS, getChaptersOfGenesis()));
        result.setOldTestament(oldTestament);

        ArrayList<Book> newTestament = new ArrayList<>();
        newTestament.add(new Book(MATTHEW.name(), MATTHEW, getChaptersOfMatthew()));
        newTestament.add(new Book(MARK.name(), MARK, getChaptersOfMark()));
        result.setNewTestament(newTestament);

        result.setName("HUNKAR");
        return result;
    }

    private static List<Chapter> getChaptersOfGenesis() {
        ArrayList<Chapter> chaptersOfGenesis = new ArrayList<>();
        chaptersOfGenesis.add(createMiddleChapter(GENESIS, 1));
        chaptersOfGenesis.add(createMiddleChapter(GENESIS, 2));
        return chaptersOfGenesis;
    }

    private static ArrayList<Chapter> getChaptersOfMatthew() {
        ArrayList<Chapter> chaptersOfMatthew = new ArrayList<>();
        chaptersOfMatthew.add(createMiddleChapter(MATTHEW, 1));
        chaptersOfMatthew.add(createLongChapter());
        chaptersOfMatthew.add(createShortChapter());
        return chaptersOfMatthew;
    }

    private static ArrayList<Chapter> getChaptersOfMark() {
        ArrayList<Chapter> chaptersOfMark = new ArrayList<>();
        chaptersOfMark.add(createMiddleChapter(MARK, 1));
        return chaptersOfMark;
    }

    private static Chapter createShortChapter() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(Verse.builder().chapterNumber(3).number(1).bookLabel(MATTHEW).bookName("MATTHEW").translationName("HUNKAR").text("short1")
                .build());
        verses.add(Verse.builder().chapterNumber(3).number(2).bookLabel(MATTHEW).bookName("MATTHEW").translationName("HUNKAR").text("short12")
                .build());
        return new Chapter("HUNKAR", "MATTHEW", 3, verses, "", new HashMap<>());
    }

    private static Chapter createMiddleChapter(BookLabel bookLabel, int number) {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(Verse.builder().chapterNumber(number).number(1).bookLabel(bookLabel).bookName(bookLabel.name()).translationName("HUNKAR").text("middle1")
                .build());
        verses.add(Verse.builder().chapterNumber(number).number(2).bookLabel(bookLabel).bookName(bookLabel.name()).translationName("HUNKAR").text("middle12")
                .build());
        verses.add(Verse.builder().chapterNumber(number).number(3).bookLabel(bookLabel).bookName(bookLabel.name()).translationName("HUNKAR").text("middle123")
                .build());
        return new Chapter("HUNKAR", bookLabel.name(), number, verses, "", new HashMap<>());
    }

    private static Chapter createLongChapter() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(Verse.builder().chapterNumber(2).number(1).bookLabel(BookLabel.MATTHEW).bookName("MATTHEW").translationName("HUNKAR")
                .text("longest1").build());
        verses.add(Verse.builder().chapterNumber(2).number(2).bookLabel(BookLabel.MATTHEW).bookName("MATTHEW").translationName("HUNKAR")
                .text("longest12").build());
        verses.add(Verse.builder().chapterNumber(2).number(3).bookLabel(BookLabel.MATTHEW).bookName("MATTHEW").translationName("HUNKAR")
                .text("longest123").build());
        verses.add(Verse.builder().chapterNumber(2).number(4).bookLabel(BookLabel.MATTHEW).bookName("MATTHEW").translationName("HUNKAR")
                .text("longest 1234").build());
        return new Chapter("HUNKAR", "MATTHEW", 2, verses, "", new HashMap<>());
    }

    private void assertChapterStatistics(Translation translation) {
        Chapter matthew2 = translation.getBooks().get(1).getChapters().get(1);
        assertEquals("1", matthew2.getStatistics().get("CHAPTERS_SHORTEST_VERSE"));
        assertEquals("4", matthew2.getStatistics().get("CHAPTERS_LONGEST_VERSE"));
        assertEquals("4", matthew2.getStatistics().get("NUMBER_OF_VERSES"));
        assertEquals("42", matthew2.getStatistics().get("NUMBER_OF_CHARACTERS"));
        assertEquals("5", matthew2.getStatistics().get("NUMBER_OF_WORDS"));
    }

    private void assertTranslationStatistics(Translation translation) {
        assertEquals("3", translation.getStatistics().get("NUMBER_OF_ALL_BOOKS"));
        assertEquals("6", translation.getStatistics().get("NUMBER_OF_ALL_CHAPTERS"));
        assertEquals("18", translation.getStatistics().get("NUMBER_OF_ALL_VERSES"));
        assertEquals("160", translation.getStatistics().get("NUMBER_OF_ALL_CHARACTERS"));
        assertEquals(MARK.name(), translation.getStatistics().get("SHORTEST_BOOK_BY_CHARACTERS"));
        assertEquals("MATTHEW", translation.getStatistics().get("LONGEST_BOOK_BY_CHARACTERS"));
        assertEquals("MATTHEW" + " 3", translation.getStatistics().get("SHORTEST_CHAPTER_BY_VERSES"));
        assertEquals("MATTHEW" + " 2", translation.getStatistics().get("LONGEST_CHAPTER_BY_VERSES"));
        assertEquals("MATTHEW" + " 3", translation.getStatistics().get("SHORTEST_CHAPTER_BY_CHARACTERS"));
        assertEquals("MATTHEW" + " 2", translation.getStatistics().get("LONGEST_CHAPTER_BY_CHARACTERS"));
        assertEquals("MATTHEW" + " 3:1 (6)", translation.getStatistics().get("SHORTEST_VERSE"));
        assertEquals("MATTHEW" + " 2:4 (12)", translation.getStatistics().get("LONGEST_VERSE"));
    }
}
