/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.StatisticsChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Mockito.*;

class StatisticsPanelPresenterTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private StatisticsPanelPresenter underTest;
    private TranslationService translationService;
    private MessageBus messageBus;

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
        underTest = new StatisticsPanelPresenter(translationService, messageBus, messagesBundle);
    }

    @Test
    void setStatisticsData_shouldConvertBothTranslationAndChapterRelatedDataCorrectlyAndPublishStatisticsChangedEventToMessageBus() {
        Chapter chapter = new Chapter("RÚF",
                "name", 1, new ArrayList<>(), "comments", createChapterRelatedData());
        Translation translation = new Translation();
        translation.setStatistics(createTranslationRelatedData());

        when(translationService.getTranslation("RÚF")).thenReturn(translation);

        underTest.setStatisticsData(new CurrentChapterChangingEvent(chapter));

        assertArrayEquals(getExpectedTranslationRelatedData(), underTest.getTranslationRelatedData());
        assertArrayEquals(getExpectedChapterRelatedData(), underTest.getChapterRelatedData());
        verify(messageBus).publish(new StatisticsChangedEvent(getExpectedChapterRelatedData(), getExpectedTranslationRelatedData()));
    }


    private LinkedHashMap<String, String> createChapterRelatedData() {
        LinkedHashMap<String, String> chapterRelatedData = new LinkedHashMap<>();
        chapterRelatedData.put("LONGEST_VERSE", "ccc");
        chapterRelatedData.put("SHORTEST_VERSE", "ddd");
        return chapterRelatedData;
    }

    private LinkedHashMap<String, String> createTranslationRelatedData() {
        LinkedHashMap<String, String> translationRelatedData = new LinkedHashMap<>();
        translationRelatedData.put("NUMBER_OF_VERSES", "aaa");
        translationRelatedData.put("NUMBER_OF_WORDS", "bbb");
        return translationRelatedData;
    }

    private String[][] getExpectedChapterRelatedData() {
        String[][] result = new String[2][2];
        result[0][0] = "Leghosszabb vers";
        result[0][1] = "ccc";
        result[1][0] = "Legrövidebb vers";
        result[1][1] = "ddd";
        return result;
    }

    private String[][] getExpectedTranslationRelatedData() {
        String[][] result = new String[2][2];
        result[0][0] = "A fejezet verseinek száma";
        result[0][1] = "aaa";
        result[1][0] = "A fejezet szavainak száma";
        result[1][1] = "bbb";
        return result;
    }
}
