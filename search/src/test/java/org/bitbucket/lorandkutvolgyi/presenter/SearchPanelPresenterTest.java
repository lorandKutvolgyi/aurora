/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.SearchEnablingStateChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.SearchResultChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class SearchPanelPresenterTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private TranslationService translationService;
    private SearchPanelPresenter underTest;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
        underTest = new SearchPanelPresenter(translationService, messagesBundle, messageBus);
    }

    @Test
    void getLabels_shouldReturnResultBasedOnLocale() {
        assertEquals("Az aktuális fordításban", underTest.getCurrentTranslationButtonLabel());
        assertEquals("Választok a fordítások közül", underTest.getDownloadedTranslationButtonLabel());
        assertEquals("A jegyzetekben", underTest.getInNotesButtonLabel());
        assertEquals("Hol", underTest.getTopRadioButtonPanelTitle());
        assertEquals("Kifejezés", underTest.getExpressionButtonLabel());
        assertEquals("Minden szó", underTest.getAllWordsButtonLabel());
        assertEquals("Bármelyik szó", underTest.getAnyWordButtonLabel());
    }

    @Test
    void setSearchResult_shouldPublishSearchResultChangedEventToMessageBusAfterSettingTheResult() {
        Verse verse = new Verse(1, "text", "title", "NIV", BookLabel.ACTS.name(), BookLabel.ACTS, 1, new ArrayList<>());

        underTest.setSearchResult(Collections.singletonList(verse));

        assertEquals(Collections.singletonList(verse), underTest.getSearchResult());
        verify(messageBus).publish(new SearchResultChangedEvent(Collections.singletonList(verse)));
    }

    @Test
    void getTranslations_shouldReturnTranslationNames() {
        when(translationService.getDownloadedTranslations()).thenReturn(createTranslations());

        List<String> result = underTest.getTranslations();

        assertEquals("NIV", result.get(0));
        assertEquals("EFO", result.get(1));
    }

    @Test
    void getBooks_shouldReturnExtendedBookNamesList() {
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation());

        List<String> result = underTest.getBooks("NIV");

        assertEquals("Összes könyv", result.get(0));
        assertEquals("Ószövetség", result.get(1));
        assertEquals("Újszövetség", result.get(2));
        assertEquals("Genesis", result.get(3));
        assertEquals("Matthew", result.get(4));
        assertEquals("Mark", result.get(5));
        assertEquals("Luke", result.get(6));
    }

    @Test
    void getCurrentTranslation_shouldReturnCurrentTranslationsName() {
        when(translationService.getCurrentTranslation()).thenReturn(createTranslation());

        String result = underTest.getCurrentTranslation();

        assertEquals("NIV", result);
    }

    @Test
    void getText_shouldReturnFormattedHtmlText() {
        when(translationService.getTranslation("NIV")).thenReturn(createTranslation());

        String result = underTest.getText(new Verse(1, "text", "title", "NIV", BookLabel.MATTHEW.name(), BookLabel.MATTHEW, 1, new ArrayList<>()));

        assertEquals(getExpectedHtmlText(), result);
    }

    @Test
    void isSearchEnabled_shouldReturnFalseAfterDisabling() {
        underTest.setSearchDisabled();

        assertFalse(underTest.isSearchEnabled());
    }

    @Test
    void isSearchEnabled_shouldReturnTrueAfterEnabling() {
        underTest.setSearchEnabled();

        assertTrue(underTest.isSearchEnabled());
    }

    @Test
    void setSearchEnabled_shouldPublishBoth_SearchEnablingStateChangeEvent_and_CursorChangeEvent() {
        underTest.setSearchEnabled();

        verify(messageBus).publish(eq(new SearchEnablingStateChangeEvent()));
    }

    @Test
    void setSearchDisabled_shouldPublishBoth_SearchEnablingStateChangeEvent_and_CursorChangeEvent() {
        underTest.setSearchDisabled();

        verify(messageBus).publish(eq(new SearchEnablingStateChangeEvent()));
    }

    @Test
    void selectedSearchButton_returnsFromPreferencesCorrectly() {
        int selection = PreferencesService.getInt(PreferencesService.Key.SELECTED_SEARCH_BUTTON);

        PreferencesService.remove(PreferencesService.Key.SELECTED_SEARCH_BUTTON);
        assertTrue(underTest.isExpressionSelected());
        assertFalse(underTest.isAllWordsSelected());
        assertFalse(underTest.isAnyWordSelected());

        PreferencesService.put(PreferencesService.Key.SELECTED_SEARCH_BUTTON, 1);
        assertTrue(underTest.isExpressionSelected());
        assertFalse(underTest.isAllWordsSelected());
        assertFalse(underTest.isAnyWordSelected());

        PreferencesService.put(PreferencesService.Key.SELECTED_SEARCH_BUTTON, 2);
        assertFalse(underTest.isExpressionSelected());
        assertTrue(underTest.isAllWordsSelected());
        assertFalse(underTest.isAnyWordSelected());

        PreferencesService.put(PreferencesService.Key.SELECTED_SEARCH_BUTTON, 3);
        assertFalse(underTest.isExpressionSelected());
        assertFalse(underTest.isAllWordsSelected());
        assertTrue(underTest.isAnyWordSelected());

        PreferencesService.put(PreferencesService.Key.SELECTED_SEARCH_BUTTON, selection);
    }

    private List<Translation> createTranslations() {
        Translation translation1 = new Translation();
        translation1.setName("NIV");
        Translation translation2 = new Translation();
        translation2.setName("EFO");
        return Arrays.asList(translation1, translation2);
    }

    private Translation createTranslation() {
        Translation translation = new Translation();
        translation.setName("NIV");
        Book book1 = new Book("Matthew", BookLabel.MATTHEW, new ArrayList<>());
        Book book2 = new Book("Mark", BookLabel.MARK, new ArrayList<>());
        Book book3 = new Book("Luke", BookLabel.LUKE, new ArrayList<>());
        Book book4 = new Book("Genesis", BookLabel.GENESIS, new ArrayList<>());
        translation.setNewTestament(Arrays.asList(book1, book2, book3));
        translation.setOldTestament(List.of(book4));
        return translation;
    }

    private String getExpectedHtmlText() {
        return "<html><body style='width: " + 100 + "%; margin: 4px;'>"
                + "Matthew 1: 1"
                + "<p>text"
                + "<hr>"
                + "</body></html>";
    }
}
