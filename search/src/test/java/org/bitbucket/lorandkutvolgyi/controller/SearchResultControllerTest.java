/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class SearchResultControllerTest {

    private SearchResultController underTest;
    private TranslationService translationService;
    private TextPanelService textPanelService;
    private BookmarkPopupManager bookmarkPopupManager;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        textPanelService = mock(TextPanelService.class);
        bookmarkPopupManager = mock(BookmarkPopupManager.class);
        underTest = new SearchResultController(translationService, textPanelService, bookmarkPopupManager);
    }

    @Test
    void openChapter_shouldCallTextPanelServiceToOpenNewTab_whenControlPushedParameterIsTrue() {
        Verse verse = new Verse(1, "text", "title", "RÚF", MARK.name(), MARK, 1, new ArrayList<>());

        underTest.openChapter(verse, true);

        verify(textPanelService).openNewTab();
    }

    @Test
    void openChapter_shouldCallTranslationServiceToOpenNewChapter() {
        Verse verse = new Verse(2, "text", "title", "RÚF", MATTHEW.name(), MATTHEW, 1, new ArrayList<>());

        underTest.openChapter(verse, false);

        verify(translationService).openNewChapter("RÚF", MATTHEW, 1);
    }

    @Test
    void showBookmarkPopupWithDefaults_shouldSetDefaultsAndShowPopup() {
        Verse verse = new Verse(2, "text", "title", "RÚF", MATTHEW.name(), MATTHEW, 1, new ArrayList<>());

        underTest.showBookmarkPopupWithDefaults(verse);

        verify(bookmarkPopupManager).setDefaults("RÚF", MATTHEW.name(), 1, 2);
        verify(bookmarkPopupManager).showPopup();
    }
}
