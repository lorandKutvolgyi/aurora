/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.SearchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class SearchTextFieldControllerTest {

    private static final String TO_SEARCH = "toSearch";
    private static final String TRANSLATION = "RÚF";
    private static final String BOOK = "Mark";
    private SearchTextFieldController underTest;
    private SearchService service;

    @BeforeEach
    void setUp() {
        service = mock(SearchService.class);
        underTest = new SearchTextFieldController(service);
    }

    @Test
    void searchIfNeeded_shouldCallSearchExpressionOnService_whenSearchModeIsExpression() {
        underTest.searchIfNeeded(TO_SEARCH, TRANSLATION, BOOK, "EXPRESSION");

        verify(service).searchExpression(TO_SEARCH, TRANSLATION, BOOK);
    }

    @Test
    void searchIfNeeded_shouldCallSearchAllWordOnService_whenSearchModeIsAllWord() {
        underTest.searchIfNeeded(TO_SEARCH, TRANSLATION, BOOK, "ALL_WORDS");

        verify(service).searchAllWords(TO_SEARCH, TRANSLATION, BOOK);
    }

    @Test
    void searchIfNeeded_shouldCallSearchAnyWordOnService_whenSearchModeIsExpression() {
        underTest.searchIfNeeded(TO_SEARCH, TRANSLATION, BOOK, "ANY_WORD");

        verify(service).searchAnyWord(TO_SEARCH, TRANSLATION, BOOK);
    }

    @Test
    void searchIfNeeded_shouldCallEmptyOnService_whenTrimmedTextIsLessThanThreeCharacters() {
        underTest.searchIfNeeded(" ab ", TRANSLATION, BOOK, "ANY_WORD");

        verify(service).empty();
    }

    @Test
    void searchIfNeeded_shouldThrowException_whenSearchModeDoesNotExist() {
        assertThrows(IllegalArgumentException.class, () ->
                underTest.searchIfNeeded(TO_SEARCH, TRANSLATION, BOOK, "NON_EXISTENT_SEARCH_MODE")
        );
    }
}
