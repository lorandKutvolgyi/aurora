/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.awaitility.Awaitility;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;
import org.bitbucket.lorandkutvolgyi.repository.SearchRepository;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.SearchService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TranslationComboBoxControllerTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @Test
    void reloadTranslation_shouldDelegateCallToSearchRepository() {
        SearchRepository repository = mock(SearchRepository.class);
        SearchPanelPresenter presenter = mock(SearchPanelPresenter.class);
        TranslationService translationService = mock(TranslationService.class);
        when(translationService.getCurrentTranslationName()).thenReturn("RÚF");
        SearchService searchService = new SearchService(repository, presenter, messagesBundle, translationService);
        TranslationComboBoxController underTest = new TranslationComboBoxController(searchService);

        underTest.reloadTranslation("HUNKAR");

        Awaitility.await().untilAsserted(() -> verify(repository).reloadTranslation("HUNKAR"));
    }
}
