/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SearchRepositoryTest {

    private SearchRepository underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() throws IOException, URISyntaxException {
        TranslationRepository translationRepository = mock(TranslationRepository.class);
        when(translationRepository.getDownloadedTranslations()).thenReturn(getTranslations());
        underTest = new SearchRepository(translationRepository);
        underTest.reloadTranslation("HUNKAR");
    }

    @Test
    void searchExpression_shouldReturnVersesContainTheExactText() {
        List<Verse> result = underTest.searchExpression("teremté Isten az eget", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).getChapterNumber());
        assertEquals(1, result.get(0).getNumber());
    }

    @Test
    void searchExpression_shouldNotReturnVersesNotContainTheExactText() {
        List<Verse> result = underTest.searchExpression("teremté az eget", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(0, result.size());
    }

    @Test
    void searchAnyWord_shouldReturnVersesContainAnyWordOfSearched() {
        List<Verse> result = underTest.searchAnyWord("Kezdetben wrong", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).getChapterNumber());
        assertEquals(1, result.get(0).getNumber());
    }

    @Test
    void searchAnyWord_shouldNotReturnVersesNotContainAtLeastOneWord() {
        List<Verse> result = underTest.searchAnyWord("wrong non-contained words", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(0, result.size());
    }

    @Test
    void searchAllWords_shouldReturnVersesContainAllSearchedWords() {
        List<Verse> result = underTest.searchAllWords("Kezdetben teremté az eget", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).getChapterNumber());
        assertEquals(1, result.get(0).getNumber());
    }

    @Test
    void searchAllWords_shouldNotReturnVersesNotContainAllWords() {
        List<Verse> result = underTest.searchAllWords("teremté az wrong", "HUNKAR", Collections.singletonList("MÓZES ELSŐ KÖNYVE A TEREMTÉSRŐL"));

        assertEquals(0, result.size());
    }

    private List<Translation> getTranslations() throws IOException, URISyntaxException {
        List<Translation> result = new ArrayList<>();
        result.add(getTranslation("text/test_ruf.json"));
        result.add(getTranslation("text/test_hunkar.json"));
        return result;
    }

    private Translation getTranslation(String fileName) throws IOException, URISyntaxException {
        URL url = SearchRepositoryTest.class.getClassLoader().getResource(fileName);
        Path path = new File(Objects.requireNonNull(url).toURI()).toPath();
        return new ObjectMapper().readValue(new String(Files.readAllBytes(path)), Translation.class);
    }
}
