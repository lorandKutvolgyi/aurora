/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;

import java.util.ArrayList;
import java.util.HashMap;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.RUTH;

public class TestFixtureUtil {

    public static Translation createTranslation(String name) {
        Translation result = new Translation();
        ArrayList<Book> oldTestament = new ArrayList<>();
        ArrayList<Chapter> chaptersOfRuth = new ArrayList<>();
        chaptersOfRuth.add(createChapter(name, RUTH, 1));
        oldTestament.add(new Book(RUTH.name(), RUTH, chaptersOfRuth));
        result.setOldTestament(oldTestament);
        ArrayList<Book> newTestament = new ArrayList<>();
        ArrayList<Chapter> chaptersOfMatthew = new ArrayList<>();
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 1));
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 2));
        ArrayList<Chapter> chaptersOfMark = new ArrayList<>();
        chaptersOfMark.add(createChapter(name, MARK, 1));
        newTestament.add(new Book(MATTHEW.name(), MATTHEW, chaptersOfMatthew));
        newTestament.add(new Book(MARK.name(), MARK, chaptersOfMark));
        result.setNewTestament(newTestament);
        result.setName(name);
        result.setInfo(name + " info text");
        return result;
    }

    public static Chapter createChapter(String translation, BookLabel bookLabel, int number) {
        ArrayList<Verse> verses = new ArrayList<>();
        Verse verse = new Verse(1, "text", "", translation, bookLabel.name(), bookLabel, number, new ArrayList<>());
        verses.add(verse);
        return new Chapter(translation, bookLabel.name(), number, verses, "", new HashMap<>());
    }
}
