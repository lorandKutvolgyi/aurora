/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.awaitility.Awaitility;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;
import org.bitbucket.lorandkutvolgyi.repository.SearchRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.RUTH;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SearchServiceTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private static final String TO_SEARCH = "toSearch";
    private static final String TRANSLATION = "RÚF";
    private static final String BOOK = "Mark";
    private SearchService underTest;
    private SearchRepository repository;
    private SearchPanelPresenter presenter;
    private List<Verse> searchResult;
    private TranslationService translationService;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        repository = mock(SearchRepository.class);
        presenter = mock(SearchPanelPresenter.class);
        translationService = mock(TranslationService.class);
        when(translationService.getCurrentTranslationName()).thenReturn("RÚF");
        underTest = new SearchService(repository, presenter, messagesBundle, translationService);
        searchResult = Collections.singletonList(new Verse(1, "text", "title", "RÚF", MARK.name(), MARK, 1, new ArrayList<>()));
        verify(presenter).setSearchDisabled();
        Awaitility.await().untilAsserted(() -> verify(repository).reloadTranslation("RÚF"));
        Awaitility.await().untilAsserted(() -> verify(presenter).setSearchEnabled());
    }

    @Test
    void searchExpression_shouldDelegateTheSearchCallToRepository_andSetTheResultInThePresenter() {
        when(repository.searchExpression(TO_SEARCH, TRANSLATION, Collections.singletonList(BOOK))).thenReturn(searchResult);

        underTest.searchExpression(TO_SEARCH, TRANSLATION, BOOK);

        verify(presenter).setSearchResult(searchResult);
    }

    @Test
    void searchAllWords_shouldDelegateTheSearchCallToRepository_andSetTheResultInThePresenter() {
        when(repository.searchAllWords(TO_SEARCH, TRANSLATION, Collections.singletonList(BOOK))).thenReturn(searchResult);

        underTest.searchAllWords(TO_SEARCH, TRANSLATION, BOOK);

        verify(presenter).setSearchResult(searchResult);
    }

    @Test
    void searchAnyWords_shouldDelegateTheSearchCallToRepository_andSetTheResultInThePresenter() {
        when(repository.searchAnyWord(TO_SEARCH, TRANSLATION, Collections.singletonList(BOOK))).thenReturn(searchResult);

        underTest.searchAnyWord(TO_SEARCH, TRANSLATION, BOOK);

        verify(presenter).setSearchResult(searchResult);
    }

    @Test
    void searchInAllBooks_shouldDelegateTheSearchCallToRepositoryWithAllBookNames() {
        Translation translation = TestFixtureUtil.createTranslation(TRANSLATION);
        when(translationService.getDownloadedTranslations()).thenReturn(Collections.singletonList(translation));
        when(translationService.getTranslation(TRANSLATION)).thenReturn(translation);

        underTest.searchAnyWord(TO_SEARCH, TRANSLATION, "Összes könyv");

        verify(repository).searchAnyWord(TO_SEARCH, TRANSLATION, Arrays.asList(RUTH.name(), MATTHEW.name(), MARK.name()));
    }

    @Test
    void searchInOldTestament_shouldDelegateTheSearchCallToRepositoryWithAllBookNamesFromOldTestament() {
        Translation translation = TestFixtureUtil.createTranslation(TRANSLATION);
        when(translationService.getDownloadedTranslations()).thenReturn(Collections.singletonList(translation));
        when(translationService.getTranslation(TRANSLATION)).thenReturn(translation);

        underTest.searchAnyWord(TO_SEARCH, TRANSLATION, "Ószövetség");

        verify(repository).searchAnyWord(TO_SEARCH, TRANSLATION, Collections.singletonList(RUTH.name()));
    }

    @Test
    void searchInAllBooks_shouldDelegateTheSearchCallToRepositoryWithAllBookNamesFromNewTestament() {
        Translation translation = TestFixtureUtil.createTranslation(TRANSLATION);
        when(translationService.getDownloadedTranslations()).thenReturn(Collections.singletonList(translation));
        when(translationService.getTranslation(TRANSLATION)).thenReturn(translation);

        underTest.searchAnyWord(TO_SEARCH, TRANSLATION, "Újszövetség");

        verify(repository).searchAnyWord(TO_SEARCH, TRANSLATION, Arrays.asList(MATTHEW.name(), MARK.name()));
    }

    @Test
    void empty_shouldSetEmptyArrayAsSearchResultOnPresenter() {
        underTest.empty();

        verify(presenter).setSearchResult(new ArrayList<>());
    }
}
