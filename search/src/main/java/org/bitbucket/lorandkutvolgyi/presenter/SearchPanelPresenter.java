/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.SearchEnablingStateChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.SearchResultChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.SELECTED_SEARCH_BUTTON;

public class SearchPanelPresenter {

    private static final int TEXT_MARGIN = 4;

    private final TranslationService translationService;
    private final ResourceBundle messagesBundle;
    private final MessageBus messageBus;
    private List<Verse> searchResult;
    private boolean searchEnabled;

    public SearchPanelPresenter(TranslationService translationService, ResourceBundle messagesBundle, MessageBus messageBus) {
        this.translationService = translationService;
        this.messagesBundle = messagesBundle;
        this.messageBus = messageBus;
    }

    public String getCurrentTranslationButtonLabel() {
        return messagesBundle.getString("IN_THE_CURRENT_TRANSLATION");
    }

    public String getDownloadedTranslationButtonLabel() {
        return messagesBundle.getString("CHOOSE_FROM_TRANSLATIONS");
    }

    public String getInNotesButtonLabel() {
        return messagesBundle.getString("IN_NOTES");
    }

    public String getTopRadioButtonPanelTitle() {
        return messagesBundle.getString("WHERE");
    }

    public List<String> getTranslations() {
        return translationService.getDownloadedTranslations().stream()
                .map(Translation::getName)
                .collect(Collectors.toList());
    }

    public List<String> getBooks(String translationName) {
        ArrayList<String> result = new ArrayList<>();

        Translation translation = translationService.getTranslation(translationName);

        boolean emptyOldTestament = translation.getOldTestament().isEmpty();
        boolean emptyNewTestament = translation.getNewTestament().isEmpty();
        if (!emptyOldTestament && !emptyNewTestament) {
            result.add(messagesBundle.getString("ALL_BOOKS"));
        }
        if (!emptyOldTestament) {
            result.add(messagesBundle.getString("OLD_TESTAMENT"));
        }
        if (!emptyNewTestament) {
            result.add(messagesBundle.getString("NEW_TESTAMENT"));
        }

        result.addAll(translation.getBooks().stream().map(Book::getName).toList());

        return result;
    }

    public String getExpressionButtonLabel() {
        return messagesBundle.getString("EXPRESSION");
    }

    public String getAllWordsButtonLabel() {
        return messagesBundle.getString("ALL_WORDS");
    }

    public String getAnyWordButtonLabel() {
        return messagesBundle.getString("ANY_WORD");
    }

    public String getCurrentTranslation() {
        return translationService.getCurrentTranslation().getName();
    }

    public List<Verse> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<Verse> verses) {
        searchResult = verses;
        messageBus.publish(new SearchResultChangedEvent(verses));
    }

    public String getText(Verse verse) {
        String bookName = translationService.getTranslation(verse.getTranslationName()).getBookName(verse.getBookLabel());
        String header = bookName + " " + verse.getChapterNumber() + ": " + verse.getNumber();
        return "<html><body style='width: " + 100 + "%; margin: " + TEXT_MARGIN + "px;'>"
                + header
                + "<p>" + verse.getText()
                + "<hr>"
                + "</body></html>";
    }

    public synchronized void setSearchEnabled() {
        searchEnabled = true;
        messageBus.publish(new SearchEnablingStateChangeEvent());
    }

    public synchronized void setSearchDisabled() {
        searchEnabled = false;
        messageBus.publish(new SearchEnablingStateChangeEvent());
    }

    public synchronized boolean isSearchEnabled() {
        return searchEnabled;
    }

    public boolean isExpressionSelected() {
        int selectedIndex = PreferencesService.getInt(SELECTED_SEARCH_BUTTON);
        boolean noSelection = selectedIndex == -1;
        int expressionButtonIndex = 1;

        return noSelection || selectedIndex == expressionButtonIndex;
    }

    public boolean isAllWordsSelected() {
        return PreferencesService.getInt(SELECTED_SEARCH_BUTTON) == 2;
    }

    public boolean isAnyWordSelected() {
        return PreferencesService.getInt(SELECTED_SEARCH_BUTTON) == 3;
    }
}
