/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectFilter;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.util.Arrays;
import java.util.List;

import static org.dizitart.no2.IndexOptions.indexOptions;
import static org.dizitart.no2.objects.filters.ObjectFilters.*;

public class SearchRepository {

    private static final String TEXT = "text";
    private static final String TRANSLATION_NAME = "translationName";
    private static final String BOOK_NAME = "bookName";
    private static final String SPACE = " ";

    private final Nitrite db = Nitrite.builder().compressed().openOrCreate();
    private final ObjectRepository<Verse> repository = db.getRepository(Verse.class);
    private final TranslationRepository translationRepository;

    public SearchRepository(TranslationRepository translationRepository) {
        this.translationRepository = translationRepository;

        repository.createIndex(TEXT, indexOptions(IndexType.Fulltext));
        repository.createIndex(TRANSLATION_NAME, indexOptions(IndexType.NonUnique));
        repository.createIndex(BOOK_NAME, indexOptions(IndexType.NonUnique));
    }

    public List<Verse> searchExpression(String toSearch, String translation, List<String> books) {
        return repository.find(
                and(
                        eq(TRANSLATION_NAME, translation),
                        in(BOOK_NAME, books.isEmpty() ? new String[]{""} : books.toArray()),
                        regex(TEXT, "(?i)(?u)" + toSearch)
                )
        ).toList();
    }

    public List<Verse> searchAnyWord(String toSearch, String translation, List<String> books) {
        return repository.find(
                and(
                        eq(TRANSLATION_NAME, translation),
                        in(BOOK_NAME, books.isEmpty() ? new String[]{""} : books.toArray()),
                        or(createFiltersForEveryEachWord(toSearch))
                )
        ).toList();
    }

    public List<Verse> searchAllWords(String toSearch, String translation, List<String> books) {
        return repository.find(
                and(
                        eq(TRANSLATION_NAME, translation),
                        in(BOOK_NAME, books.isEmpty() ? new String[]{""} : books.toArray()),
                        and(createFiltersForEveryEachWord(toSearch))
                )
        ).toList();
    }

    public void reloadTranslation(String translationName) {
        repository.remove(ObjectFilters.ALL);
        translationRepository.getDownloadedTranslations().stream()
                .filter(translation -> translation.getName().equals(translationName))
                .flatMap(translation -> translation.getBooks().stream())
                .flatMap(book -> book.getChapters().stream())
                .flatMap(chapter -> chapter.getVerses().stream())
                .forEach(repository::insert);
    }

    private ObjectFilter[] createFiltersForEveryEachWord(String toSearch) {
        return Arrays.stream(toSearch.split(SPACE))
                .filter(word -> !word.isBlank())
                .map(word -> regex(TEXT, "(?i)(?u)" + word))
                .toArray(ObjectFilter[]::new);
    }
}
