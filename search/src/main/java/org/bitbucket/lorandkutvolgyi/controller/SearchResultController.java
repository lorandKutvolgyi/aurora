/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

public class SearchResultController {

    private final TranslationService translationService;
    private final TextPanelService textPanelService;
    private final BookmarkPopupManager bookmarkPopupManager;

    public SearchResultController(TranslationService translationService, TextPanelService textPanelService, BookmarkPopupManager bookmarkPopupManager) {
        this.translationService = translationService;
        this.textPanelService = textPanelService;
        this.bookmarkPopupManager = bookmarkPopupManager;
    }

    @Loggable
    public void openChapter(Verse verse, boolean controlPushed) {
        if (controlPushed) {
            openNewTab();
        }
        openNewChapter(verse);
    }

    @Loggable
    public void showBookmarkPopupWithDefaults(Verse verse) {
        bookmarkPopupManager.setDefaults(verse.getTranslationName(), verse.getBookName(), verse.getChapterNumber(), verse.getNumber());
        bookmarkPopupManager.showPopup();
    }

    private void openNewChapter(Verse verse) {
        translationService.openNewChapter(verse.getTranslationName(), verse.getBookLabel(), verse.getChapterNumber());
    }

    private void openNewTab() {
        textPanelService.openNewTab();
    }
}
