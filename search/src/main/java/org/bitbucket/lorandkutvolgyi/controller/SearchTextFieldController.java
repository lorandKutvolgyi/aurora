/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.SearchService;

public class SearchTextFieldController {

    private final SearchService service;

    public SearchTextFieldController(SearchService service) {
        this.service = service;
    }

    @Loggable
    public void searchIfNeeded(String toSearch, String translation, String book, String searchMode) {
        if (isSearchNeeded(toSearch)) {
            search(toSearch.trim(), translation, book, searchMode);
        } else {
            service.empty();
        }
    }

    private boolean isSearchNeeded(String toSearch) {
        return toSearch.trim().length() > 2;
    }

    private void search(String toSearch, String translation, String book, String searchMode) {
        switch (SearchMode.valueOf(searchMode)) {
            case EXPRESSION -> service.searchExpression(toSearch, translation, book);
            case ALL_WORDS -> service.searchAllWords(toSearch, translation, book);
            case ANY_WORD -> service.searchAnyWord(toSearch, translation, book);
            default -> {
            }
        }
    }
}
