/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;
import org.bitbucket.lorandkutvolgyi.repository.SearchRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class SearchService {

    private final SearchRepository repository;
    private final SearchPanelPresenter presenter;
    private final ResourceBundle messagesBundle;
    private final TranslationService translationService;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private Future<?> translationLoadingTask;

    public SearchService(SearchRepository repository, SearchPanelPresenter presenter, ResourceBundle messagesBundle, TranslationService translationService) {
        this.repository = repository;
        this.presenter = presenter;
        this.messagesBundle = messagesBundle;
        this.translationService = translationService;

        makeSearchable(translationService.getCurrentTranslationName());
    }

    public synchronized void searchExpression(String toSearch, String translation, String book) {
        List<Verse> verses = repository.searchExpression(toSearch, translation, getBook(translation, book));
        presenter.setSearchResult(verses);
    }

    public synchronized void searchAllWords(String toSearch, String translation, String book) {
        List<Verse> verses = repository.searchAllWords(toSearch, translation, getBook(translation, book));
        presenter.setSearchResult(verses);
    }

    public synchronized void searchAnyWord(String toSearch, String translation, String book) {
        List<Verse> verses = repository.searchAnyWord(toSearch, translation, getBook(translation, book));
        presenter.setSearchResult(verses);
    }

    public synchronized void empty() {
        presenter.setSearchResult(new ArrayList<>());
    }

    public void makeSearchable(String translationName) {
        presenter.setSearchDisabled();
        cancelLoadingTranslationIfItIsInProgress();

        translationLoadingTask = submitTranslationLoadingTask(translationName);
    }

    private void cancelLoadingTranslationIfItIsInProgress() {
        if (translationLoadingTask != null && !translationLoadingTask.isDone() && !translationLoadingTask.isCancelled()) {
            translationLoadingTask.cancel(true);
        }
    }

    private Future<?> submitTranslationLoadingTask(String translationName) {
        return executor.submit(() -> {
            repository.reloadTranslation(translationName);
            presenter.setSearchEnabled();
        });
    }

    private List<String> getBook(String translationName, String bookName) {
        if (messagesBundle.getString("ALL_BOOKS").equals(bookName)) {
            return translationService.getTranslation(translationName).getBooks().stream().map(Book::getName).collect(Collectors.toList());
        }
        if (messagesBundle.getString("OLD_TESTAMENT").equals(bookName)) {
            return translationService.getTranslation(translationName).getOldTestament().stream().map(Book::getName).collect(Collectors.toList());
        }
        if (messagesBundle.getString("NEW_TESTAMENT").equals(bookName)) {
            return translationService.getTranslation(translationName).getNewTestament().stream().map(Book::getName).collect(Collectors.toList());
        }
        return Collections.singletonList(bookName);
    }
}
