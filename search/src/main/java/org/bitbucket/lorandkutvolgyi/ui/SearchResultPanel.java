/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.SearchResultController;
import org.bitbucket.lorandkutvolgyi.event.SearchResultChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.PAGE_END;

public class SearchResultPanel extends JPanel {

    private final transient SearchPanelPresenter presenter;
    private final transient SearchResultController searchResultController;
    private GridBagConstraints constraints;
    private Dimension maximumLabelSize;

    SearchResultPanel(SearchPanelPresenter presenter, SearchResultController searchResultController, Theme selectedTheme) {
        this.presenter = presenter;
        this.searchResultController = searchResultController;

        setBackground(selectedTheme.getSearchResultBackgroundColor());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        createGridBagConstraints();
    }

    @Subscribe
    public void showResult(SearchResultChangedEvent event) {
        if (maximumLabelSize == null) {
            maximumLabelSize = new Dimension((int) (getWidth() * 0.95), 200);
        }
        removeAll();
        addResult(event.getResult());
        updateUI();

    }

    GridBagConstraints getConstraints() {
        return constraints;
    }

    private void createGridBagConstraints() {
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.weighty = 1.0;
        constraints.anchor = PAGE_END;
        constraints.fill = BOTH;
    }

    private void addResult(List<Verse> result) {
        result.forEach(verse -> add(createLabel(verse)));
    }

    private JLabel createLabel(Verse verse) {
        JLabel label = new JLabel(presenter.getText(verse));
        label.setMaximumSize(maximumLabelSize);
        label.setOpaque(false);
        label.addMouseListener(new ResultLabelListener(searchResultController, verse));
        return label;
    }
}
