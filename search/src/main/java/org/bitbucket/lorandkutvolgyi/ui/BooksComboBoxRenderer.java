/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import java.awt.Component;
import java.awt.Dimension;

public class BooksComboBoxRenderer extends JLabel implements ListCellRenderer<String> {

    public BooksComboBoxRenderer() {
        this.setOpaque(true);
        this.setBorder(new EmptyBorder(1, 1, 1, 1));
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends String> list, String value, int index, boolean isSelected, boolean cellHasFocus) {
        setColors(list, isSelected);
        setText(value);
        setTooltip(list, value, index);
        return this;
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.getText() != null && !this.getText().equals("")) {
            return super.getPreferredSize();
        }
        this.setText(" ");
        Dimension dimension = super.getPreferredSize();
        this.setText("");
        return dimension;
    }

    private void setColors(JList<? extends String> list, boolean isSelected) {
        if (isSelected) {
            setColorsForSelectedRows(list);
        } else {
            setColorsForUnselectedRows(list);
        }
    }

    private void setTooltip(JList<? extends String> list, String value, int index) {
        if (index > -1) {
            list.setToolTipText(value);
        }
    }

    private void setColorsForUnselectedRows(JList<? extends String> list) {
        this.setBackground(list.getBackground());
        this.setForeground(list.getForeground());
    }

    private void setColorsForSelectedRows(JList<? extends String> list) {
        this.setBackground(list.getSelectionBackground());
        this.setForeground(list.getSelectionForeground());
    }
}