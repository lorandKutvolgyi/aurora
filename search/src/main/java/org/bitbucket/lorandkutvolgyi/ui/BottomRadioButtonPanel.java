/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.SearchMode;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

import static java.awt.GridBagConstraints.HORIZONTAL;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.SELECTED_SEARCH_BUTTON;

public class BottomRadioButtonPanel extends SavablePanel {

    private final JRadioButton expressionButton;
    private final JRadioButton allWordsButton;
    private final JRadioButton anyWordButton;
    private GridBagConstraints bottomRadioPanelConstraints;

    BottomRadioButtonPanel(SearchPanelPresenter presenter, ButtonGroup buttonGroup) {
        expressionButton = createRadioButton(presenter.getExpressionButtonLabel(), presenter.isExpressionSelected(), SearchMode.EXPRESSION);
        allWordsButton = createRadioButton(presenter.getAllWordsButtonLabel(), presenter.isAllWordsSelected(), SearchMode.ALL_WORDS);
        anyWordButton = createRadioButton(presenter.getAnyWordButtonLabel(), presenter.isAnyWordSelected(), SearchMode.ANY_WORD);

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.add(expressionButton);
        this.add(allWordsButton);
        this.add(anyWordButton);

        putButtonsIntoOneGroup(buttonGroup, expressionButton, allWordsButton, anyWordButton);
        createGridBagConstraints();
    }

    GridBagConstraints getConstraints() {
        return bottomRadioPanelConstraints;
    }

    private JRadioButton createRadioButton(String buttonLabel, boolean select, SearchMode searchMode) {
        JRadioButton radioButton = new JRadioButton(buttonLabel);
        radioButton.setMargin(new Insets(0, 5, 0, 5));
        radioButton.setSelected(select);
        radioButton.setActionCommand(searchMode.name());
        radioButton.setOpaque(false);
        return radioButton;
    }

    private void putButtonsIntoOneGroup(ButtonGroup buttonGroup, JRadioButton... buttons) {
        Arrays.stream(buttons).forEach(buttonGroup::add);
    }

    private void createGridBagConstraints() {
        bottomRadioPanelConstraints = new GridBagConstraints();
        bottomRadioPanelConstraints.gridx = 0;
        bottomRadioPanelConstraints.gridy = 3;
        bottomRadioPanelConstraints.fill = HORIZONTAL;
    }

    @Override
    public void saveState() {
        if (expressionButton.isSelected()) {
            PreferencesService.put(SELECTED_SEARCH_BUTTON, 1);
        } else if (allWordsButton.isSelected()) {
            PreferencesService.put(SELECTED_SEARCH_BUTTON, 2);
        } else {
            assert anyWordButton.isSelected();
            PreferencesService.put(SELECTED_SEARCH_BUTTON, 3);
        }
    }
}
