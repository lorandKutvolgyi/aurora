/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Arrays;

import static java.awt.GridBagConstraints.HORIZONTAL;

public class TopRadioButtonPanel extends JPanel {

    private final transient SearchPanelPresenter presenter;
    private final JRadioButton currentTranslationButton;
    private GridBagConstraints constraints;

    TopRadioButtonPanel(SearchPanelPresenter presenter, JComboBox<String> translationsComboBox) {
        this.presenter = presenter;

        currentTranslationButton = createCurrentTranslationRadioButton(translationsComboBox);
        JRadioButton downloadedTranslationButton = createDownloadedTranslationsRadioButton(translationsComboBox);
        JRadioButton availableTranslationButton = createAvailableTranslationsRadioButton();

        this.setBorder(new BorderUIResource.TitledBorderUIResource(presenter.getTopRadioButtonPanelTitle()));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(currentTranslationButton);
        this.add(downloadedTranslationButton);
        this.add(availableTranslationButton);

        putButtonsIntoOneGroup(new ButtonGroup(), currentTranslationButton, downloadedTranslationButton, availableTranslationButton);
        createTopGridBagConstraints();
    }

    GridBagConstraints getConstraints() {
        return constraints;
    }

    public boolean isCurrentTranslationSelected() {
        return currentTranslationButton.isSelected();
    }

    private JRadioButton createCurrentTranslationRadioButton(JComboBox<String> translationsComboBox) {
        JRadioButton currentTranslationButton = new JRadioButton(presenter.getCurrentTranslationButtonLabel());
        currentTranslationButton.setOpaque(false);
        currentTranslationButton.setMargin(createMargin(currentTranslationButton, 15));
        currentTranslationButton.setSelected(true);
        currentTranslationButton.addActionListener(createActionListener(translationsComboBox));
        return currentTranslationButton;
    }

    private JRadioButton createDownloadedTranslationsRadioButton(JComboBox<String> translationsComboBox) {
        JRadioButton downloadedTranslationButton = new JRadioButton(presenter.getDownloadedTranslationButtonLabel());
        downloadedTranslationButton.setOpaque(false);
        downloadedTranslationButton.setMargin(createMargin(downloadedTranslationButton, 0));
        downloadedTranslationButton.addActionListener(actionEvent -> translationsComboBox.setEnabled(true));
        return downloadedTranslationButton;
    }

    private JRadioButton createAvailableTranslationsRadioButton() {
        JRadioButton availableTranslationButton = new JRadioButton(presenter.getInNotesButtonLabel());
        availableTranslationButton.setOpaque(false);
        availableTranslationButton.setMargin(createMargin(availableTranslationButton, 15));
        availableTranslationButton.setEnabled(false);
        return availableTranslationButton;
    }

    private void putButtonsIntoOneGroup(ButtonGroup buttonGroup, JRadioButton... buttons) {
        Arrays.stream(buttons).forEach(buttonGroup::add);
    }

    private void createTopGridBagConstraints() {
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.fill = HORIZONTAL;
    }

    private Insets createMargin(JRadioButton button, int topAndBottom) {
        int leftAndRight = button.getWidth() < 36 ? 36 - button.getWidth() : 0;
        return new Insets(topAndBottom, leftAndRight, topAndBottom, leftAndRight);
    }

    private ActionListener createActionListener(JComboBox<String> translationsComboBox) {
        return actionEvent -> {
            selectCurrentTranslation(translationsComboBox);
            translationsComboBox.setEnabled(false);
        };
    }

    private void selectCurrentTranslation(JComboBox<String> translationsComboBox) {
        String currentTranslation = presenter.getCurrentTranslation();
        if (!currentTranslation.equals(translationsComboBox.getSelectedItem())) {
            translationsComboBox.setSelectedItem(currentTranslation);
        }
    }
}
