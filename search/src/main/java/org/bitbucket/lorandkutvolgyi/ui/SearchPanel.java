/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.controller.SearchResultController;
import org.bitbucket.lorandkutvolgyi.controller.SearchTextFieldController;
import org.bitbucket.lorandkutvolgyi.controller.TranslationComboBoxController;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CursorChangeEvent;
import org.bitbucket.lorandkutvolgyi.event.SearchEnablingStateChangeEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.SearchPanelPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static java.awt.GridBagConstraints.HORIZONTAL;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;

public class SearchPanel extends SavablePanel {

    private final transient SearchPanelPresenter presenter;
    private final transient SearchTextFieldController searchTextFieldController;
    private final transient MessageBus messageBus;
    private final transient Theme selectedTheme;
    private final JComboBox<String> translationsComboBox;
    private final JComboBox<String> booksComboBox;
    private final ButtonGroup bottomButtonGroup;
    private final transient TranslationComboBoxController translationComboBoxController;
    private RoundedJTextField searchTextField;
    private SearchResultPanel resultPanel;
    private TopRadioButtonPanel topRadioButtonPanel;
    private BottomRadioButtonPanel bottomRadioButtonPanel;

    public SearchPanel(SearchPanelPresenter presenter, SearchTextFieldController searchTextFieldController, SearchResultController searchResultController,
                       TranslationComboBoxController translationComboBoxController, MessageBus messageBus, Theme selectedTheme) {
        this.presenter = presenter;
        this.searchTextFieldController = searchTextFieldController;
        this.translationComboBoxController = translationComboBoxController;
        this.translationsComboBox = new JComboBox<>();
        this.booksComboBox = new JComboBox<>();
        this.bottomButtonGroup = new ButtonGroup();
        this.messageBus = messageBus;
        this.selectedTheme = selectedTheme;
        this.translationsComboBox.setBackground(selectedTheme.getContentBackgroundColor());
        this.booksComboBox.setBackground(selectedTheme.getContentBackgroundColor());
        setBackground(selectedTheme.getContentBackgroundColor());
        setLayout();
        createTopRadioButtonPanel();
        createComboBoxPanel();
        createSearchTextField();
        createBottomRadioButtonPanel();
        createResultPanel(presenter, searchResultController);
    }

    @Override
    public void saveState() {
        bottomRadioButtonPanel.saveState();
    }

    @Subscribe
    public void refresh(CurrentTranslationChangedEvent event) {
        changeCurrentTranslationIfItIsSelected(event.getTranslation().getName());
        updateUI();
    }

    @Subscribe
    public synchronized void changeSearchFieldState(SearchEnablingStateChangeEvent event) {
        boolean searchEnabled = presenter.isSearchEnabled();
        searchTextField.setEnabled(searchEnabled);
        if (searchEnabled) {
            searchTextField.setBackground(selectedTheme.getSearchInputBackgroundColor());
            changeCursor(Cursor.DEFAULT_CURSOR);
        } else {
            searchTextField.setBackground(selectedTheme.getDisabledSearchInputBackgroundColor());
            changeCursor(Cursor.WAIT_CURSOR);
        }
        updateUI();
    }

    @Subscribe
    public void changeCursor(CursorChangeEvent event) {
        changeCursor(event.getCursor());
    }

    private void setLayout() {
        GridBagLayout searchPanelLayout = new GridBagLayout();
        searchPanelLayout.rowHeights = new int[]{50, 50, 50, 50};
        setLayout(searchPanelLayout);
    }

    private void createTopRadioButtonPanel() {
        topRadioButtonPanel = new TopRadioButtonPanel(presenter, translationsComboBox);
        topRadioButtonPanel.setOpaque(false);
        add(topRadioButtonPanel, topRadioButtonPanel.getConstraints());
    }

    private void createComboBoxPanel() {
        JPanel comboBoxPanel = new JPanel();
        comboBoxPanel.setOpaque(false);
        comboBoxPanel.setLayout(new GridLayout(1, 2, 10, 0));
        comboBoxPanel.add(translationsComboBox);
        comboBoxPanel.add(booksComboBox);
        initTranslationComboBox();
        initBooksComboBox();
        add(comboBoxPanel, createComboBoxGridBagConstraints());
    }

    private void createSearchTextField() {
        GridBagConstraints searchTextFieldConstraints = new GridBagConstraints();
        searchTextFieldConstraints.gridx = 0;
        searchTextFieldConstraints.gridy = 2;
        searchTextFieldConstraints.ipady = 10;
        searchTextFieldConstraints.fill = HORIZONTAL;

        searchTextField = new RoundedJTextField.Builder().size(36).limit(500).build();
        searchTextField.setBackground(selectedTheme.getSearchInputBackgroundColor());
        searchTextField.addKeyListener(new SearchTextFieldKeyListener());
        add(searchTextField, searchTextFieldConstraints);
    }

    private void createBottomRadioButtonPanel() {
        bottomRadioButtonPanel = new BottomRadioButtonPanel(presenter, bottomButtonGroup);
        bottomRadioButtonPanel.setOpaque(false);
        add(bottomRadioButtonPanel, bottomRadioButtonPanel.getConstraints());
    }

    private void createResultPanel(SearchPanelPresenter presenter, SearchResultController searchResultController) {
        resultPanel = new SearchResultPanel(presenter, searchResultController, selectedTheme);
        messageBus.subscribe(resultPanel);
        JScrollPane scrollPane = new JScrollPane(resultPanel);
        scrollPane.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.getVerticalScrollBar().setUnitIncrement(30);
        scrollPane.setBorder(null);
        add(scrollPane, resultPanel.getConstraints());
    }

    private synchronized void changeCursor(int cursor) {
        if (cursor == 0) {
            setCursor(Cursor.getDefaultCursor());
        } else if (cursor == Cursor.WAIT_CURSOR) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }

    private void initTranslationComboBox() {
        translationsComboBox.removeAllItems();
        presenter.getTranslations().forEach(translationsComboBox::addItem);
        translationsComboBox.setSelectedItem(presenter.getCurrentTranslation());
        translationsComboBox.setEnabled(false);
        translationsComboBox.addActionListener(event -> reset((String) translationsComboBox.getSelectedItem()));
    }

    private void reset(String translation) {
        resultPanel.removeAll();
        searchTextField.setText(null);
        initBooksComboBox();
        translationComboBoxController.reloadTranslation(translation);
    }

    private void initBooksComboBox() {
        booksComboBox.setRenderer(new BooksComboBoxRenderer());
        booksComboBox.setPrototypeDisplayValue("Apostolok Cselekedetei   ");
        booksComboBox.removeAllItems();
        presenter.getBooks((String) translationsComboBox.getSelectedItem()).forEach(booksComboBox::addItem);
        updateUI();
    }

    private GridBagConstraints createComboBoxGridBagConstraints() {
        GridBagConstraints comboBoxConstraints = new GridBagConstraints();
        comboBoxConstraints.gridx = 0;
        comboBoxConstraints.gridy = 1;
        comboBoxConstraints.fill = HORIZONTAL;
        return comboBoxConstraints;
    }

    private void changeCurrentTranslationIfItIsSelected(String translation) {
        if (topRadioButtonPanel.isCurrentTranslationSelected()) {
            changeCurrentTranslation(translation);
        }
    }

    private void changeCurrentTranslation(String translation) {
        boolean enabled = translationsComboBox.isEnabled();
        translationsComboBox.setEnabled(true);
        translationsComboBox.setSelectedItem(translation);
        translationsComboBox.setEnabled(enabled);
    }

    private class SearchTextFieldKeyListener extends KeyAdapter {

        @Loggable
        @Override
        public void keyTyped(KeyEvent event) {
            char character = event.getKeyChar();
            if (((character >= '0') && (character <= '9')) || (character == '<') || (character == '>')) {
                event.consume();
            }
        }

        @Override
        public void keyReleased(KeyEvent event) {
            if (event.getKeyCode() == KeyEvent.VK_ENTER) {
                search();
            }
        }

        private void search() {
            searchTextFieldController.searchIfNeeded(searchTextField.getText(),
                    (String) translationsComboBox.getSelectedItem(),
                    (String) booksComboBox.getSelectedItem(),
                    bottomButtonGroup.getSelection().getActionCommand());
        }
    }
}
