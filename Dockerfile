FROM ubuntu

RUN mkdir -p /work
RUN mkdir -p /root/test
RUN apt-get update && apt-get install -y locales \
    openjdk-17-jdk \
    openjdk-17-jre \
    libxtst6 \
    libxrender1 \
    libxi6 \
    libgtk-3-0 \
    libgtk-3-bin \
    libgtk-3-common \
    fakeroot \
    chromium-bsu \
    xdg-utils \
    && rm -rf /var/lib/apt/lists/* \
	&& localedef -i hu_HU -c -f UTF-8 -A /usr/share/locale/locale.alias hu_HU.UTF-8

ENV LANG hu_HU.UTF-8
ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64
ENV LICENSE_FILE_DIR /work/license
ENV LOGO_DIR /work/logo

COPY test/src/test/script/system_test.sh /work
COPY distribution/resources /work/resources/
COPY distribution/target/result /work/target/result/
COPY distribution/jpackage_deb.sh /work/jpackage_deb.sh
COPY main/src/main/resources/.aurora/gpl-3.0.txt /work/license/
COPY common/src/main/resources/logo.png /work/logo/
COPY test/src/test/script/resources_files.txt /work
COPY test/src/test/script/db_resources.txt /work
COPY test/src/test/script/map_resources.txt /work
COPY test/src/test/script/text_and_crossreferences_files.txt /work
COPY test/src/test/jmxcmd.jar /work

WORKDIR /work

ENTRYPOINT ["./system_test.sh"]
