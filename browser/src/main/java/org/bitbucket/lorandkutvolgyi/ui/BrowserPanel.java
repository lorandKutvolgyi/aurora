/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowOpeningEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowWillOpenEvent;
import org.bitbucket.lorandkutvolgyi.presenter.BrowserPresenter;

import java.awt.*;

public class BrowserPanel extends SavablePanel {

    private final BrowserPresenter presenter;

    public BrowserPanel(BrowserPresenter presenter) {
        this.presenter = presenter;

        NativeInterface.open();
        Runtime.getRuntime().addShutdownHook(new Thread(NativeInterface::close));

        setLayout(new BorderLayout());
        add(createWebBrowser());
    }

    private JWebBrowser createWebBrowser() {
        JWebBrowser webBrowser = new JWebBrowser();
        webBrowser.setMenuBarVisible(false);
        webBrowser.setStatusBarVisible(false);
        webBrowser.navigate(presenter.getStartUrl());

        addListenerTo(webBrowser);

        return webBrowser;
    }

    private void addListenerTo(JWebBrowser webBrowser) {

        webBrowser.addWebBrowserListener(new WebBrowserAdapter() {

            @Override
            public void windowWillOpen(WebBrowserWindowWillOpenEvent event) {
                event.setNewWebBrowser(webBrowser);
            }

            @Override
            public void windowOpening(WebBrowserWindowOpeningEvent event) {
                JWebBrowser newWebBrowser = event.getNewWebBrowser();
                newWebBrowser.setMenuBarVisible(false);
                newWebBrowser.setStatusBarVisible(false);
            }
        });
    }
}
