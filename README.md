# Aurora

Aurora is a Bible study application, with which you can read God's message in different translations or even in the
original languages. Also, you can compare translations to each other, make comments and bookmarks for chapters, see
where the stories happened on maps, and check what original words mean in either English or Hungarian languages.

#### Developer: Lóránd Kútvölgyi (aurorabible.app@gmail.com)

#### Download source code: [https://bitbucket.org/lorandKutvolgyi/aurora/src/master/](https://bitbucket.org/lorandKutvolgyi/aurora/src/master/)

#### Download installers: [https://bitbucket.org/lorandKutvolgyi/aurora-installers/downloads/](https://bitbucket.org/lorandKutvolgyi/aurora-installers/downloads/)

#### Licence: [https://www.gnu.org/licenses/gpl-3.0-standalone.html](https://www.gnu.org/licenses/gpl-3.0-standalone.html)

#### Wiki: [https://bitbucket.org/lorandKutvolgyi/aurora/wiki/Home](https://bitbucket.org/lorandKutvolgyi/aurora/wiki/Home)

### Build

#### On Linux:

    export testenv=true
    export homepath=$HOME/test
    mvn clean install -DskipITs
    mvn failsafe:integration-test -f test/pom.xml

#### On Windows:

    mvn clean install -DskipTests -DskipITs
