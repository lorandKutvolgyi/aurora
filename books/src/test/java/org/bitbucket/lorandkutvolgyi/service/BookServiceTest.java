/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookServiceTest {

    private static final String CHAPTER_NAME = "Apostolok Cselekedetei";

    @Test
    void getNumberOfChapters_shouldDelegateTheCallToTranslationService() {
        TranslationService service = Mockito.mock(TranslationService.class);
        Mockito.when(service.getNumberOfChapters(CHAPTER_NAME)).thenReturn(28);
        BooksService underTest = new BooksService(service);

        int result = underTest.getNumberOfChapters(CHAPTER_NAME);

        assertEquals(28, result);
    }
}
