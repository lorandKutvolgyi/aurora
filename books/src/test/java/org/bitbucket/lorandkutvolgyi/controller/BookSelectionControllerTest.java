/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.BooksService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class BookSelectionControllerTest {

    private BooksService service;
    private ChaptersPopupManager chaptersPopupManager;
    private BookSelectionController underTest;
    private BookmarkPopupManager bookmarkPopupManager;

    @BeforeEach
    void setUp() {
        service = mock(BooksService.class);
        chaptersPopupManager = mock(ChaptersPopupManager.class);
        bookmarkPopupManager = mock(BookmarkPopupManager.class);
        underTest = new BookSelectionController(service, chaptersPopupManager, bookmarkPopupManager);
    }

    @Test
    void showChapterPopup_shouldDoNothing_whenTreePathIsNull() {
        underTest.showChapterPopup(new JTree(), null);

        verify(chaptersPopupManager, never()).showPopup(any(), any(), anyInt());
    }

    @Test
    void showChapterPopup_shouldDoNothing_whenTreePathIsSmallerThanThree() {
        underTest.showChapterPopup(new JTree(), new TreePath(new String[]{"Biblia", "Ószövetség"}));

        verify(chaptersPopupManager, never()).showPopup(any(), any(), anyInt());
    }

    @Test
    void showChapterPopup_shouldDelegateTheCallToChaptersPopupManager_whenTreePathContainsThreeElement() {
        JTree source = new JTree();

        when(service.getNumberOfChapters("Ruth")).thenReturn(4);

        underTest.showChapterPopup(source, new TreePath(new DefaultMutableTreeNode[]{new DefaultMutableTreeNode("Biblia"),
                new DefaultMutableTreeNode("Ószövetség"), new DefaultMutableTreeNode("Ruth")}));

        verify(chaptersPopupManager).showPopup(source, "Ruth", 4);
    }

    @Test
    void showBookmarkPopup_shouldSetBookNameAsDefaultAndDelegateShowCallToBookmarkPopupManager() {
        underTest.showBookmarkPopup("Ruth");

        verify(bookmarkPopupManager).setDefaults(null, "Ruth", 0, 0);
        verify(bookmarkPopupManager).showPopup();
    }
}
