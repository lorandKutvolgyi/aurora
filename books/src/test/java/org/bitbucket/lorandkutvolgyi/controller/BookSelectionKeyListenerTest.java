/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.awt.event.KeyEvent;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class BookSelectionKeyListenerTest {

    private BookSelectionController bookSelectionController;
    private BookSelectionKeyListener underTest;

    @BeforeEach
    void setUp() {
        bookSelectionController = Mockito.mock(BookSelectionController.class);
        underTest = new BookSelectionKeyListener(bookSelectionController);
    }

    @Test
    void keyPressed_shouldDoNothing_whenKeyCodeIsNotEnter() {
        KeyEvent keyEvent = new KeyEvent(new JTree(), 0, 0L, 0, KeyEvent.VK_BACK_SPACE, '\u0000', 0);

        underTest.keyPressed(keyEvent);

        verify(bookSelectionController, never()).showChapterPopup(any(), any());
    }

    @Test
    void keyPressed_shouldDelegateTheCallToBookSelectionController_whenKeyCodeIsEnter() {
        JTree source = new JTree();
        TreePath treePath = new TreePath("");
        source.setSelectionPath(treePath);
        KeyEvent keyEvent = new KeyEvent(source, 0, 0L, 0, KeyEvent.VK_ENTER, '\u0000', 0);

        underTest.keyPressed(keyEvent);

        verify(bookSelectionController).showChapterPopup(source, treePath);
    }
}
