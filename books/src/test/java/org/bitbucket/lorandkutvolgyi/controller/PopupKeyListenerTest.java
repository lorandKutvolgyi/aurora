/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PopupKeyListenerTest {

    private PopupKeyListener underTest;
    private ChaptersPopupManager chaptersPopupManager;
    private ChapterSelectionListener chapterSelectionListener;
    private final List<Character> cache = new ArrayList<>(3);

    @BeforeEach
    void setUp() {
        cache.clear();
        underTest = new PopupKeyListener();
        chaptersPopupManager = mock(ChaptersPopupManager.class);
        chapterSelectionListener = mock(ChapterSelectionListener.class);
        underTest.setPopupManager(chaptersPopupManager, chapterSelectionListener, cache);
    }

    @Test
    void keyPressed_shouldDoNothing_whenKeyCodeIsNeitherEscapeNorEnterNorNumber() {
        KeyEvent keyEvent = new KeyEvent(new JButton(), 0, 0L, 0, KeyEvent.VK_BACK_SPACE, '\u0000', 0);

        underTest.keyPressed(keyEvent);

        verify(chaptersPopupManager, never()).stashPopup();
        verify(chaptersPopupManager, never()).stashPopup(anyInt());
    }

    @Test
    void keyPressed_shouldStashPopup_whenKeyCodeIsEscape() {
        KeyEvent keyEvent = new KeyEvent(new JButton(), 0, 0L, 0, KeyEvent.VK_ESCAPE, '\u0000', 0);

        underTest.keyPressed(keyEvent);

        verify(chaptersPopupManager).stashPopup();
    }

    @Test
    void keyPressed_shouldCallChapterSelectionListener_whenKeyCodeIsEnter() {
        KeyEvent keyEvent = new KeyEvent(new JButton(), 0, 0L, 0, KeyEvent.VK_ENTER, '\u0000', 0);

        underTest.keyPressed(keyEvent);

        verify(chapterSelectionListener).actionPerformed(any(ActionEvent.class));
    }

    @Test
    void keyPressed_shouldDoNothing_whenKeyCodeIsZeroAndCacheIsEmpty() {
        KeyEvent keyEvent = new KeyEvent(new JButton(), 0, 0L, 0, KeyEvent.VK_0, '0', 0);

        underTest.keyPressed(keyEvent);

        verify(chaptersPopupManager, never()).stashPopup();
        verify(chaptersPopupManager, never()).stashPopup(anyInt());
    }

    @Test
    void keyPressed_shouldCallChapterSelectionListener_whenKeyCodeIsANonZeroNumber() {
        KeyEvent keyEvent = new KeyEvent(new JButton(), 0, 0L, 0, KeyEvent.VK_1, '1', 0);
        when(chaptersPopupManager.getLabelsNumber()).thenReturn(1);

        underTest.keyPressed(keyEvent);

        verify(chapterSelectionListener).actionPerformed(any(ActionEvent.class));
    }
}
