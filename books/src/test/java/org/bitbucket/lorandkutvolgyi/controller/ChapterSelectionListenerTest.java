/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ChapterSelectionListenerTest {

    private ChaptersPopupManager chaptersPopupManager;
    private TranslationService translationService;
    private TextPanelService textPanelService;
    private ChapterSelectionListener underTest;
    private ActionEvent actionEvent;

    @BeforeEach
    void setUp() {
        chaptersPopupManager = mock(ChaptersPopupManager.class);
        translationService = mock(TranslationService.class);
        textPanelService = mock(TextPanelService.class);
        underTest = new ChapterSelectionListener(translationService, textPanelService);
        underTest.setPopupManager(chaptersPopupManager);
        actionEvent = mock(ActionEvent.class);
    }

    @Test
    void actionPerformed_shouldSetCurrentChapterAndStashPopup() {
        when(actionEvent.getActionCommand()).thenReturn("Máté:1");

        underTest.actionPerformed(actionEvent);

        verify(translationService).openNewChapter("Máté", 1);
        verify(chaptersPopupManager).stashPopup();
    }

    @Test
    void actionPerformed_shouldOpenNewTabSetCurrentChapterAndStashPopup_whenCtrlIsDown() {
        when(actionEvent.getActionCommand()).thenReturn("Máté:1");
        when(actionEvent.getModifiers()).thenReturn(ActionEvent.CTRL_MASK);

        underTest.actionPerformed(actionEvent);

        verify(textPanelService).openNewTab();
        verify(translationService).openNewChapter("Máté", 1);
        verify(chaptersPopupManager).stashPopup();
    }

    @Test
    void actionPerformed_shouldSetCurrentChapterAndStashPopupWithDelay_whenActionCommandHasThreeParts() {
        int delay = 1000;
        when(actionEvent.getActionCommand()).thenReturn("Máté:1:" + delay);

        underTest.actionPerformed(actionEvent);

        verify(translationService).openNewChapter("Máté", 1);
        verify(chaptersPopupManager).stashPopup(delay);
    }
}
