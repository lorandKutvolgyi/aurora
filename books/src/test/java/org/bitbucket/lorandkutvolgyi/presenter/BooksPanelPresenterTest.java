/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.BookNamesChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class BooksPanelPresenterTest {

    private BooksPanelPresenter underTest;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        RestartService restartService = mock(RestartService.class);
        LocaleService localeService = new LocaleService(restartService);
        localeService.setSelectedLanguageWithRestart("hu");
        TranslationService translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
        when(translationService.getCurrentTranslation()).thenReturn(getTranslation("RÚF"));

        underTest = new BooksPanelPresenter(translationService, messageBus, localeService);
    }

    @Test
    void getBooksTreeLabels_shouldReturnAllBookLabels() {
        Map<String, Map<String, List<String>>> result = underTest.getBooksTreeLabels();

        assertEquals(1, result.size());
        assertTrue(result.containsKey("Biblia - RÚF"));
        assertEquals(2, result.get("Biblia - RÚF").size());
        assertTrue(result.get("Biblia - RÚF").containsKey("Ószövetség"));
        assertTrue(result.get("Biblia - RÚF").containsKey("Újszövetség"));
        assertEquals(1, result.get("Biblia - RÚF").get("Ószövetség").size());
        assertEquals(2, result.get("Biblia - RÚF").get("Újszövetség").size());
    }

    @Test
    void changeBookLabels_shouldSetNewLabels_andPublishItToMessageBus() {
        underTest.changeBookLabels(new CurrentTranslationChangedEvent(getTranslation("NIV")));

        Map<String, Map<String, List<String>>> labels = underTest.getBooksTreeLabels();

        assertEquals(1, labels.size());
        assertTrue(labels.containsKey("Biblia - NIV"));
        assertEquals(2, labels.get("Biblia - NIV").size());
        assertTrue(labels.get("Biblia - NIV").containsKey("Ószövetség"));
        assertTrue(labels.get("Biblia - NIV").containsKey("Újszövetség"));
        assertEquals(0, labels.get("Biblia - NIV").get("Ószövetség").size());
        assertEquals(1, labels.get("Biblia - NIV").get("Újszövetség").size());
        verify(messageBus).publish(new BookNamesChangedEvent(labels));
    }

    private Translation getTranslation(String name) {
        Translation translation = new Translation();
        translation.setOldTestament(getOldTestament(name));
        translation.setNewTestament(getNewTestament(name));
        translation.setName(name);
        translation.setInfo("info");
        return translation;
    }

    private ArrayList<Book> getOldTestament(String name) {
        ArrayList<Book> books = new ArrayList<>();
        if (name.equals("RÚF")) {
            books.add(new Book("Ruth", BookLabel.RUTH, new ArrayList<>()));
        }
        return books;
    }

    private ArrayList<Book> getNewTestament(String name) {
        ArrayList<Book> books = new ArrayList<>();
        if (name.equals("RÚF")) {
            books.add(new Book("Máté", BookLabel.MATTHEW, new ArrayList<>()));
            books.add(new Book("Márk", BookLabel.MARK, new ArrayList<>()));
        } else {
            books.add(new Book("Matthew", BookLabel.MATTHEW, new ArrayList<>()));
        }
        return books;
    }
}
