/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PopupKeyListener extends KeyAdapter {

    private static final int POPUP_CLOSING_DELAY = 1000;
    private static final int CACHE_CLEARING_DELAY = 1000;
    private static final int MAXIMUM_CACHE_SIZE = 3;

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private ChaptersPopupManager chaptersPopupManager;
    private ChapterSelectionListener chapterSelectionListener;
    private List<Character> cache = new ArrayList<>(3);

    public void setPopupManager(ChaptersPopupManager chaptersPopupManager, ChapterSelectionListener chapterSelectionListener, List<Character> cache) {
        this.chaptersPopupManager = chaptersPopupManager;
        this.chapterSelectionListener = chapterSelectionListener;
        this.cache = cache;
    }

    @Loggable
    @Override
    public void keyPressed(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
            chaptersPopupManager.stashPopup();
        } else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
            chapterSelectionListener.actionPerformed(createActionEvent(event));
        } else if (!isNumberCharacter(event)) {
            return;
        } else if (isStartingWithZero(event)) {
            return;
        } else if (!isCacheFull()) {
            cache.add(event.getKeyChar());
        }
        if (!chaptersPopupManager.isStashed() && !cache.isEmpty() && isLabelWithCachedNumberExist()) {
            String numberAsString = concatCachedChars();
            chapterSelectionListener.actionPerformed(createActionEvent(event, numberAsString));
        }
        clearCacheWithDelay();
    }

    private ActionEvent createActionEvent(KeyEvent event, String numberAsString) {
        String actionCommand = chaptersPopupManager.getBookName() + ":" + numberAsString + ":" + POPUP_CLOSING_DELAY;
        return new ActionEvent(new Object(), 0, actionCommand, 0, event.isControlDown() ? InputEvent.CTRL_MASK : 0);
    }

    private ActionEvent createActionEvent(KeyEvent event) {
        JButton source = (JButton) event.getSource();
        String actionCommand = source.getActionCommand();
        return new ActionEvent(source, 0, actionCommand, 0, event.isControlDown() ? InputEvent.CTRL_MASK : 0);
    }

    private boolean isNumberCharacter(final KeyEvent e) {
        return e.getKeyChar() >= '0' && e.getKeyChar() <= '9';
    }

    private boolean isStartingWithZero(final KeyEvent e) {
        return cache.isEmpty() && e.getKeyCode() == '0';
    }

    private boolean isCacheFull() {
        return cache.size() == MAXIMUM_CACHE_SIZE;
    }

    private boolean isLabelWithCachedNumberExist() {
        return chaptersPopupManager.getLabelsNumber() >= Integer.parseInt(concatCachedChars());
    }

    private void clearCacheWithDelay() {
        executor.schedule(cache::clear, CACHE_CLEARING_DELAY, TimeUnit.MILLISECONDS);
    }

    private String concatCachedChars() {
        StringBuilder result = new StringBuilder();
        for (Character cachedChar : cache) {
            result.append(cachedChar);
        }
        return result.toString();
    }
}
