/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChapterSelectionListener extends AbstractAction implements ActionListener {

    private final transient TranslationService translationService;
    private final transient TextPanelService textPanelService;
    private transient ChaptersPopupManager chaptersPopupManager;

    public ChapterSelectionListener(TranslationService translationService, TextPanelService textPanelService) {
        this.translationService = translationService;
        this.textPanelService = textPanelService;
    }

    public void setPopupManager(ChaptersPopupManager chaptersPopupManager) {
        this.chaptersPopupManager = chaptersPopupManager;
    }

    @Loggable
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        openNewTabIfNecessary(actionEvent);
        setCurrentChapter(actionEvent.getActionCommand());
        stashPopup(actionEvent.getActionCommand());
    }

    private void openNewTabIfNecessary(ActionEvent actionEvent) {
        String[] parts = actionEvent.getActionCommand().split(":");
        boolean noDelay = parts.length != 3;
        boolean firstNumberPushed = parts[1].length() == 1;
        if (isCtrlPressed(actionEvent) && (noDelay || firstNumberPushed)) {
            textPanelService.openNewTab();
        }
    }

    private void setCurrentChapter(String actionCommand) {
        String[] parts = actionCommand.split(":");
        String bookName = parts[0];
        int numberOfChapter = Integer.parseInt(parts[1]);
        translationService.openNewChapter(bookName, numberOfChapter);
    }

    private void stashPopup(String actionCommand) {
        String[] parts = actionCommand.split(":");
        if (parts.length != 3) {
            chaptersPopupManager.stashPopup();
        } else {
            chaptersPopupManager.stashPopup(Integer.parseInt(parts[2]));
        }
    }

    private boolean isCtrlPressed(ActionEvent actionEvent) {
        return ActionEvent.CTRL_MASK == (ActionEvent.CTRL_MASK & actionEvent.getModifiers());
    }
}
