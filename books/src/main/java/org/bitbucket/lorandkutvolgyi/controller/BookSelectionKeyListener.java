/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BookSelectionKeyListener extends KeyAdapter {

    private final BookSelectionController bookSelectionController;

    public BookSelectionKeyListener(BookSelectionController bookSelectionController) {
        this.bookSelectionController = bookSelectionController;
    }

    @Loggable
    @Override
    public void keyPressed(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_ENTER) {
            JTree source = (JTree) event.getSource();
            TreePath path = source.getSelectionPath();
            bookSelectionController.showChapterPopup(source, path);
        }
    }
}
