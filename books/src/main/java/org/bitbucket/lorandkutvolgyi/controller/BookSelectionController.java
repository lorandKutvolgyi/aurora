/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.BooksService;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class BookSelectionController {

    private final BooksService service;
    private final ChaptersPopupManager chaptersPopupManager;
    private final BookmarkPopupManager bookmarkPopupManager;

    public BookSelectionController(BooksService service, ChaptersPopupManager chaptersPopupManager, BookmarkPopupManager bookmarkPopupManager) {
        this.service = service;
        this.chaptersPopupManager = chaptersPopupManager;
        this.bookmarkPopupManager = bookmarkPopupManager;
    }

    public void showChapterPopup(JTree source, TreePath path) {
        if (path != null && path.getPath().length == 3) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getPath()[2];
            chaptersPopupManager.showPopup(source, (String) node.getUserObject(), service.getNumberOfChapters((String) node.getUserObject()));
        }
    }

    public void showBookmarkPopup(String bookName) {
        bookmarkPopupManager.setDefaults(null, bookName, 0, 0);
        bookmarkPopupManager.showPopup();
    }
}
