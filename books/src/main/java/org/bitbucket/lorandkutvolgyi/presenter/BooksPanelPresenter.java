/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.BookNamesChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.*;
import java.util.stream.Collectors;

public class BooksPanelPresenter {

    private final Map<String, Map<String, List<String>>> booksTreeLabels = new LinkedHashMap<>();
    private final ResourceBundle messagesBundle;
    private final MessageBus messageBus;

    public BooksPanelPresenter(TranslationService translationService, MessageBus messageBus, LocaleService localeService) {
        this.messageBus = messageBus;
        messagesBundle = ResourceBundle.getBundle("BooksMessagesBundle", localeService.getSelectedLocale());
        setBookLabels(translationService.getCurrentTranslation());
    }

    @Subscribe
    public void changeBookLabels(CurrentTranslationChangedEvent event) {
        setBookLabels(event.getTranslation());
        publishBookNamesChangedEvent();
    }

    public Map<String, Map<String, List<String>>> getBooksTreeLabels() {
        return booksTreeLabels;
    }

    private void setBookLabels(Translation translation) {
        booksTreeLabels.clear();
        Map<String, List<String>> booksOfTestaments = new HashMap<>();
        booksOfTestaments.put(messagesBundle.getString("OLD_TESTAMENT"), translation.getOldTestament().stream().map(Book::getName).collect(Collectors.toList()));
        booksOfTestaments.put(messagesBundle.getString("NEW_TESTAMENT"), translation.getNewTestament().stream().map(Book::getName).collect(Collectors.toList()));
        booksTreeLabels.put(messagesBundle.getString("BIBLE") + " - " + getTranslationName(translation), booksOfTestaments);
    }

    private void publishBookNamesChangedEvent() {
        messageBus.publish(new BookNamesChangedEvent(booksTreeLabels));
    }

    private String getTranslationName(Translation translation) {
        return translation.getName().equals("ORIGINAL") ? messagesBundle.getString("ORIGINAL") : translation.getName();
    }
}