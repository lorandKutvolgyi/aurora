/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.ChapterSelectionListener;
import org.bitbucket.lorandkutvolgyi.controller.PopupKeyListener;
import org.bitbucket.lorandkutvolgyi.model.Theme;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.ResourceBundle;

public class ChapterSelectorPopup extends JDialog {

    private final String tooltipText;
    private final transient ChapterSelectionListener chapterSelectionListener;
    private final transient PopupKeyListener popupKeyListener;
    private final transient Theme selectedTheme;
    private transient Window parentWindow;
    private int labelsNumber;
    private String bookName;

    public ChapterSelectorPopup(ChapterSelectionListener chapterSelectionListener, PopupKeyListener popupKeyListener, Theme selectedTheme,
                                ResourceBundle messagesBundle) {
        this.chapterSelectionListener = chapterSelectionListener;
        this.popupKeyListener = popupKeyListener;
        this.selectedTheme = selectedTheme;
        this.tooltipText = messagesBundle.getString("CHAPTER_SELECTOR_POPUP_TOOLTIP");
        setModal(true);
        setUndecorated(true);
        setResizable(false);
        setMinimumSize(new Dimension(150, 20));
        setLayout(new BorderLayout());
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public void show(JTree tree, String bookName, int numberOfChapters) {
        this.bookName = bookName;
        this.labelsNumber = numberOfChapters;
        setVisible(false);
        TreePath selectionPath = tree.getSelectionPath();
        getContentPane().removeAll();
        Rectangle rowBounds = tree.getRowBounds(tree.getRowForPath(selectionPath));
        setLocation(parentWindow.getLocation().x + rowBounds.width + 10, parentWindow.getLocation().y + rowBounds.getLocation().y);
        addButtons(bookName, numberOfChapters, createPanel(numberOfChapters));
        pack();
        update(getGraphics());
        setVisible(true);
    }

    public void stash() {
        this.labelsNumber = 0;
        this.bookName = null;
        setVisible(false);
    }

    public int getLabelsNumber() {
        return labelsNumber;
    }

    public String getBookName() {
        return bookName;
    }

    private JPanel createPanel(int numberOfChapters) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(numberOfChapters % 10 == 0 ? numberOfChapters / 10 : numberOfChapters / 10 + 1, 10));
        panel.setBackground(selectedTheme.getPopupContentColor());
        panel.setToolTipText(tooltipText);
        add(panel);
        return panel;
    }

    private void addButtons(String bookName, int numberOfChapters, JPanel panel) {
        for (int i = 1; i < numberOfChapters + 1; i++) {
            JButton btn = new JButton(String.valueOf(i));
            btn.setActionCommand(bookName + ":" + i);
            btn.setOpaque(false);
            btn.setContentAreaFilled(false);
            btn.setBorderPainted(false);
            btn.setMargin(new Insets(0, 0, 0, 0));
            btn.addActionListener(chapterSelectionListener);
            btn.addKeyListener(popupKeyListener);
            btn.setToolTipText(tooltipText);
            btn.setVisible(true);
            panel.add(btn);
        }
    }
}
