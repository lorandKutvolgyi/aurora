/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.controller.BookSelectionController;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BookSelectionMouseListener extends MouseAdapter {

    private final BookSelectionController bookSelectionController;

    public BookSelectionMouseListener(BookSelectionController bookSelectionController) {
        this.bookSelectionController = bookSelectionController;
    }

    @Loggable
    @Override
    public void mouseClicked(MouseEvent event) {
        JTree source = (JTree) event.getSource();
        TreePath path = source.getPathForLocation(event.getX(), event.getY());
        if (isLeftClick(event)) {
            bookSelectionController.showChapterPopup(source, path);
        } else if (isRightClickOnBookName(event, path)) {
            assert path != null;
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getPath()[2];
            String bookName = (String) node.getUserObject();
            bookSelectionController.showBookmarkPopup(bookName);
        }
    }

    private boolean isRightClickOnBookName(MouseEvent event, TreePath path) {
        return isRightClick(event) && isABookName(path);
    }

    private boolean isLeftClick(MouseEvent event) {
        return event.getButton() == 1;
    }

    private boolean isRightClick(MouseEvent event) {
        return event.getButton() == 3;
    }

    private boolean isABookName(TreePath path) {
        return path != null && path.getPath().length == 3;
    }
}