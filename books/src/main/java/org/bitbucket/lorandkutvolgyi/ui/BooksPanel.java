/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.BookSelectionKeyListener;
import org.bitbucket.lorandkutvolgyi.event.BookNamesChangedEvent;
import org.bitbucket.lorandkutvolgyi.presenter.BooksPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class BooksPanel extends SavablePanel {

    private final transient BookSelectionMouseListener bookSelectionMouseListener;
    private final transient BookSelectionKeyListener bookSelectionKeyListener;
    private JTree books;

    public BooksPanel(BooksPanelPresenter presenter, BookSelectionMouseListener bookSelectionMouseListener,
                      BookSelectionKeyListener bookSelectionKeyListener) {
        this.bookSelectionMouseListener = bookSelectionMouseListener;
        this.bookSelectionKeyListener = bookSelectionKeyListener;

        setLayout(new BorderLayout());
        setVisible(true);
        createContent(presenter.getBooksTreeLabels());
    }

    @Subscribe
    public void refresh(BookNamesChangedEvent event) {
        removeAll();
        update(getGraphics());
        createContent(event.getBooksTreeLabels());
        update(getGraphics());
    }

    @Override
    public void saveState() {
        PreferencesService.put(PreferencesService.Key.OT_TREE_ELEMENT_CLOSED, books.isCollapsed(1));
        PreferencesService.put(PreferencesService.Key.NT_TREE_ELEMENT_CLOSED, books.getPathForRow(books.getRowCount() - 1).getPathCount() < 3);
    }

    private void createContent(Map<String, Map<String, List<String>>> treeLabels) {
        treeLabels.forEach((key, value) -> {
            JPanel innerPanel = new JPanel();
            innerPanel.setLayout(new BorderLayout());
            innerPanel.add(createTree(key, value));
            add(new JScrollPane(innerPanel));
        });
    }

    private JTree createTree(String key, Map<String, List<String>> value) {
        DefaultMutableTreeNode nodes = createNodes(key, value);
        books = new JTree(nodes);
        books.setCellRenderer(new BooksCellRenderer());
        books.addMouseListener(bookSelectionMouseListener);
        books.addKeyListener(bookSelectionKeyListener);
        this.expandTree();
        this.collapseWhereNeeded();
        books.setVisible(true);
        return books;
    }

    private void expandTree() {
        for (int i = 0; i < books.getRowCount(); i++) {
            books.expandRow(i);
        }
    }

    private void collapseWhereNeeded() {
        if (PreferencesService.getBooleanOrFalse(PreferencesService.Key.OT_TREE_ELEMENT_CLOSED)) {
            books.collapseRow(1);
        }
        if (PreferencesService.getBooleanOrFalse(PreferencesService.Key.NT_TREE_ELEMENT_CLOSED)) {
            int ntRow = calculateNtRow();
            books.collapseRow(ntRow);
        }
    }

    private DefaultMutableTreeNode createNodes(String key, Map<String, List<String>> value) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(key);
        value.forEach((innerKey, innerValue) -> {
            DefaultMutableTreeNode innerRoot = new DefaultMutableTreeNode(innerKey);
            innerValue.forEach(label -> {
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(label);
                innerRoot.add(node);
            });
            root.add(innerRoot);
        });
        return root;
    }

    private int calculateNtRow() {
        TreePath path = books.getPathForRow(books.getRowCount() - 1);
        if (path.getPath().length == 3) {
            path = path.getParentPath();
        }
        int ntRow = books.getRowForPath(path);
        return ntRow;
    }

    static class BooksCellRenderer extends DefaultTreeCellRenderer {


        @Override
        public Component getTreeCellRendererComponent(JTree jTree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            setOpenIcon(new ImageIcon(Objects.requireNonNull(BooksPanel.class.getResource("/book-open.png"))));
            setClosedIcon(new ImageIcon(Objects.requireNonNull(BooksPanel.class.getResource("/book-tab.png"))));
            setLeafIcon(new ImageIcon(Objects.requireNonNull(BooksPanel.class.getResource("/book-tab.png"))));

            return super.getTreeCellRendererComponent(jTree, value, selected, expanded, leaf, row, hasFocus);
        }
    }
}
