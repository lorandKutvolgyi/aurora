/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.ChaptersPopupManager;

import javax.swing.*;

public class ChaptersPopupManagerImpl implements ChaptersPopupManager {

    private final ChapterSelectorPopup chapterSelectorPopup;

    public ChaptersPopupManagerImpl(ChapterSelectorPopup chapterSelectorPopup) {
        this.chapterSelectorPopup = chapterSelectorPopup;
    }

    @Override
    public void showPopup(JTree source, String bookName, int numberOfChapters) {
        chapterSelectorPopup.show(source, bookName, numberOfChapters);
    }

    @Override
    public void stashPopup() {
        chapterSelectorPopup.stash();
    }

    @Override
    public void stashPopup(int delay) {
        Timer timer = new Timer(delay, event -> chapterSelectorPopup.stash());
        timer.setRepeats(false);
        timer.start();
    }

    @Override
    public boolean isStashed() {
        return !chapterSelectorPopup.isShowing();
    }

    @Override
    public int getLabelsNumber() {
        return chapterSelectorPopup.getLabelsNumber();
    }

    @Override
    public String getBookName() {
        return chapterSelectorPopup.getBookName();
    }
}
