/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CurrentTranslationChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.StatusBarTextChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.presenter.MainWindowPresenter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.mockito.Mockito.*;

class MessagingIT {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));

    @Test
    void messageSending_shouldTriggerReceiverMethodsAnnotatedWithSubscribe() {
        MessageBus messageBus = spy(new MessageBus());
        MainWindowPresenter mainWindowPresenter = new MainWindowPresenter(mock(TranslationService.class), messageBus, messagesBundle);
        messageBus.subscribe(mainWindowPresenter);

        messageBus.publish(new CurrentTranslationChangedEvent(getTranslation()));

        verify(messageBus).publish(new StatusBarTextChangedEvent("HUNKAR - test info for HUNKAR"));
    }

    private Translation getTranslation() {
        Translation translation = new Translation();
        translation.setName("HUNKAR");
        translation.setInfo("test info for HUNKAR");
        return translation;
    }
}
