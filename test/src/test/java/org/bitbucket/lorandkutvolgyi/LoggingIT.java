/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.controller.CommentsToolbarListener;
import org.bitbucket.lorandkutvolgyi.controller.PanelTextReceiver;
import org.bitbucket.lorandkutvolgyi.presenter.CommentsPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class LoggingIT {

    private static final String EXECUTION_LOG = "execution(public void org.bitbucket.lorandkutvolgyi.controller.CommentsToolbarListener.actionPerformed(java.awt.event.ActionEvent))";
    private static final String ARGS_LOG = "[java.awt.event.ActionEvent[unknown type,cmd=edit,when=0,modifiers=] on java.lang.Object@";

    @Test
    void testLoggableAnnotationWorks() throws IOException {
        TranslationService translationService = mock(TranslationService.class);
        PanelTextReceiver panelTextReceiver = mock(PanelTextReceiver.class);
        CommentsPanelPresenter presenter = new CommentsPanelPresenter(new MessageBus());
        CommentsToolbarListener editCommentsListener = new CommentsToolbarListener(presenter, translationService, panelTextReceiver);

        editCommentsListener.actionPerformed(createActionEvent());

        Path path = Paths.get(System.getProperty("user.home") + "/test/.aurora/aurora.log");
        assertTrue(isExecutionLogInLogFile(path));
        assertTrue(isArgsInLogFile(path));
    }

    private boolean isExecutionLogInLogFile(Path path) throws IOException {
        return Files.readAllLines(path).stream().anyMatch(line -> line.contains(EXECUTION_LOG));
    }

    private boolean isArgsInLogFile(Path path) throws IOException {
        return Files.readAllLines(path).stream().anyMatch(line -> line.contains(ARGS_LOG));
    }

    private ActionEvent createActionEvent() {
        return new ActionEvent(new Object(), 0, "edit");
    }
}
