/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.service.UpdateService;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class UpdateServiceIT {

    private final MessageBus messageBus = mock(MessageBus.class);

    @Test
    void update() {
        File testDir = new File(System.getProperty("user.home") != null ? System.getProperty("user.home") + "/test" : "./test");
        UpdateService underTest = new UpdateService(messageBus, testDir.getAbsolutePath());

        underTest.update();

        assertEquals(1,
                Objects.requireNonNull(testDir.listFiles((a, b) -> a.getAbsolutePath().equals(testDir.getPath())
                        && b.equals("aurora-latest.deb"))).length);
        testDir.delete();
    }
}
