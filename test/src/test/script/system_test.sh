#!/bin/bash

#
#  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
#
#                                 Aurora is free software: you can redistribute it and/or modify
#                                 it under the terms of the GNU General Public License as published by
#                                 the Free Software Foundation, either version 3 of the License, or
#                                 (at your option) any later version.
#
#                                 Aurora is distributed in the hope that it will be useful,
#                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
#                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#                                 GNU General Public License for more details.
#
#                                 You should have received a copy of the GNU General Public License
#                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
#
#

install() {
  dpkg -i aurora_"$VERSION"-1_amd64.deb
}

startAurora() {
  /opt/aurora/bin/Aurora >/dev/null 2>&1 &
  echo "INFO: The application is started."
  sleep 25
}

checkUsedLanguageIsHu() {
  if [ "$1" == "HU" ]; then
    echo "INFO: Language is HU."
  else
    echo "ERROR: Language is not HU but $1"
    echo "ERROR: Test was failed."
    exit 1
  fi
}

stopAurora() {
  pkill "Aurora"
  echo "INFO: The application is stopped."
  sleep 5
}

checkFileExists() {
  FILE="$1"
  if test -f "$FILE" || test -d "$FILE"; then
    echo "INFO: $FILE exists."
  else
    echo "ERROR: $FILE does not exist."
    echo "ERROR: Test was failed."
    exit 1
  fi
}

checkFilesExist() {
  while IFS= read -r FILE; do
    checkFileExists "$FILE"
  done <"$1"
}

checkFileDoesNotExist() {
  FILE="$1"
  if test -f "$FILE" || test -d "$FILE"; then
    echo "ERROR: $FILE exists."
    echo "ERROR: Test was failed."
    exit 1
  else
    echo "INFO: $FILE does not exist."
  fi
}

deleteFiles() {
  while IFS= read -r FILE; do
    rm -f "$FILE" -d "$FILE"
    echo "INFO: $FILE was removed."
  done <"$1"
}

deleteFile() {
    rm "$1"
    echo "INFO: $1 was removed."
}

installNewerVersion() {
  touch /root/test/.aurora/updated0.0.9.txt
  rm -f /root/test/.aurora/updated"$VERSION".txt
  dpkg -i aurora_"$VERSION"-1_amd64.deb
}

export DISPLAY=:0
VERSION=$(head -n 1 ./target/result/version)

echo "INFO: Test: Deb package creation is successful."
# shellcheck disable=SC2002
cat jpackage_deb.sh | sed 's/--java-options "\([^"]\+\)"/--java-options "\1 -Dtestenv=true -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.rmi.port=9010 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false "/' > jpackage_deb_with_jmx.sh
chmod +x jpackage_deb_with_jmx.sh
./jpackage_deb_with_jmx.sh

echo "INFO: Test: Installation is successful."
mkdir -p "/usr/share/desktop-directories/"
install

echo "INFO: Test: Used language is HU"
startAurora
response="$(java -jar ./jmxcmd.jar - localhost:9010 org.bitbucket.lorandkutvolgyi:type=basic,name=langService Language 2>&1)"
language=$(echo "$response" | awk '{print $NF}')
checkUsedLanguageIsHu "$language"
stopAurora

echo "INFO: Test: All resources are created after the first start."
checkFilesExist "./resources_files.txt"
checkFilesExist "./db_resources.txt"
checkFilesExist "./map_resources.txt"
checkFilesExist "./text_and_crossreferences_files.txt"
checkFilesExist "./dictionary_resources.txt"

echo "INFO: Test: Map resources are recreated after restarting the application if they have been deleted."
deleteFiles "./map_resources.txt"
startAurora
stopAurora
checkFilesExist "./map_resources.txt"

echo "INFO: Test: Dictionary resources are recreated after restarting the application if they have been deleted."
deleteFiles "./dictionary_resources.txt"
startAurora
stopAurora
checkFilesExist "./dictionary_resources.txt"

echo "INFO: Test: Text resources and crossreferences are recreated after restarting the application if they have been deleted."
deleteFiles "./text_and_crossreferences_files.txt"
startAurora
stopAurora
checkFilesExist "./text_and_crossreferences_files.txt"

echo "INFO: Test: Resource files in the .aurora folder are recreated after restarting the application if they have been deleted."
deleteFiles "./resources_files.txt"
startAurora
stopAurora
checkFilesExist "./resources_files.txt"

echo "INFO: Test: All resources are recreated after restarting the application if they have been deleted.."
deleteFiles "./resources_files.txt"
deleteFiles "./db_resources.txt"
deleteFiles "./map_resources.txt"
deleteFiles "./text_and_crossreferences_files.txt"
deleteFiles "./dictionary_resources.txt"
startAurora
stopAurora
checkFilesExist "./resources_files.txt"
checkFilesExist "./db_resources.txt"
checkFilesExist "./map_resources.txt"
checkFilesExist "./text_and_crossreferences_files.txt"
checkFilesExist "./dictionary_resources.txt"

echo "INFO: Test: about_en.html file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/about_en.html"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/about_en.html"

echo "INFO: Test: map/en/genesis/map.svg file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/map/en/genesis/map.svg"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/map/en/genesis/map.svg"

echo "INFO: Test: map/hu/genesis/map.svg file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/map/hu/genesis/map.svg"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/map/hu/genesis/map.svg"

echo "INFO: Test: db/bookmarks.db file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/db/bookmarks.db"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/db/bookmarks.db"

echo "INFO: Test: dictionary/greek_en_dictionary.txt file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/dictionary/greek_en_dictionary.txt"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/dictionary/greek_en_dictionary.txt"

echo "INFO: Test: text/asv.json file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/text/asv.json"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/text/asv.json"

echo "INFO: Test: crossreferences/crossreferences.csv file in the .aurora folder are recreated after restarting the application if it has been deleted."
deleteFile "/root/test/.aurora/crossreferences/crossreferences.csv"
startAurora
stopAurora
checkFileExist "/root/test/.aurora/crossreferences/crossreferences.csv"

echo "INFO: Test: After updating to newer version updated<version>.txt file exists."
installNewerVersion
startAurora
stopAurora
checkFileExists "/root/test/.aurora/updated""$VERSION"".txt"
checkFileDoesNotExist "/root/test/.aurora/updated0.0.9.txt"

echo "INFO: Tests were successfully passed."
