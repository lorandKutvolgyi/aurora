/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;


import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.HistoryTableController;
import org.bitbucket.lorandkutvolgyi.model.HistoryChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.HistoryPanelPresenter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Arrays;

public class HistoryPanel extends SavablePanel {

    private final transient Theme selectedTheme;
    private final DefaultTableModel tableModel;
    private final JTable table;

    public HistoryPanel(HistoryPanelPresenter presenter, HistoryTableController controller, Theme selectedTheme) {
        this.tableModel = new DefaultTableModel();
        this.selectedTheme = selectedTheme;
        this.table = new HistoryTable(tableModel, presenter, controller);

        setLayout(new BorderLayout());
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.getViewport().setBackground(selectedTheme.getContentBackgroundColor());
        add(scrollPane);
    }

    @Subscribe
    public void update(HistoryChangedEvent event) {
        clearTable();
        addRows(event.getHistory());
        colorRows();
    }

    private void clearTable() {
        tableModel.setRowCount(0);
    }

    private void addRows(String[] history) {
        Arrays.stream(history).forEach(item -> tableModel.addRow(new String[]{item}));
    }

    private void colorRows() {
        for (int i = 0; i < table.getRowCount(); i++) {
            if (i % 2 != 0) {
                table.addRowSelectionInterval(i, i);
            }
        }
    }

    private class HistoryTable extends JTable {

        HistoryTable(DefaultTableModel tableModel, HistoryPanelPresenter presenter, HistoryTableController controller) {
            super(tableModel);
            setShowGrid(false);
            setEnabled(false);
            setRowHeight(22);
            setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            addMouseListener(controller);
            setBackground(selectedTheme.getEvenRowBackgroundColor());
            setSelectionBackground(selectedTheme.getOddRowBackgroundColor());
            Arrays.stream(presenter.getColumns()).forEach(tableModel::addColumn);
        }

        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            JComponent cellRenderer = (JComponent) super.prepareRenderer(renderer, row, column);
            String cellContent = getValueAt(row, column).toString();
            cellRenderer.setToolTipText(cellContent);
            return cellRenderer;
        }
    }
}
