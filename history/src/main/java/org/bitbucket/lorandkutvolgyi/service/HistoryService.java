/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.HistoryEntry;
import org.bitbucket.lorandkutvolgyi.presenter.HistoryPanelPresenter;

import java.util.ArrayList;
import java.util.List;

public class HistoryService {

    private static final List<HistoryEntry> history = new ArrayList<>();
    private final HistoryPanelPresenter presenter;
    private final TranslationService translationService;

    public HistoryService(TranslationService translationService, HistoryPanelPresenter presenter) {
        this.translationService = translationService;
        this.presenter = presenter;
    }

    @Subscribe
    public void addNewElement(NewChapterEvent event) {
        history.add(HistoryEntry.builder().chapter(event.getChapter()).build());
        presenter.setData(history);
    }

    public void open(int row) {
        translationService.openNewChapter(history.get(row).getChapter());
    }

    public Chapter get(int row) {
        return history.get(row).getChapter();
    }

    public void clear() {
        history.clear();
    }
}
