/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.HistoryChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.HistoryEntry;

import java.util.List;

public class HistoryPanelPresenter {

    private final String[] columns = new String[]{""};
    private final MessageBus messageBus;

    public HistoryPanelPresenter(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public void setData(List<HistoryEntry> history) {
        String[] data = new String[history.size()];
        for (int i = 0; i < history.size(); i++) {
            data[i] = history.get(i).getChapter().toString();
        }
        messageBus.publish(new HistoryChangedEvent(data));
    }

    public String[] getColumns() {
        return columns;
    }
}
