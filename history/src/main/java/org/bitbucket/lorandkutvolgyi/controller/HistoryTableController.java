/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.HistoryService;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HistoryTableController extends MouseAdapter {

    private final HistoryService historyService;
    private final TextPanelService textPanelService;
    private final BookmarkPopupManager bookmarkPopupManager;

    public HistoryTableController(HistoryService historyService, TextPanelService textPanelService, BookmarkPopupManager bookmarkPopupManager) {
        this.historyService = historyService;
        this.textPanelService = textPanelService;
        this.bookmarkPopupManager = bookmarkPopupManager;
    }

    @Loggable
    @Override
    public void mouseClicked(MouseEvent event) {
        if (event.getButton() == 1 && event.getClickCount() == 2) {
            openNewTabIfNeeded(event);
            openChapter(event);
        } else if (event.getButton() == 3) {
            Chapter chapter = getChapter(event);
            bookmarkPopupManager.setDefaults(chapter.getTranslationName(), chapter.getBookName(), chapter.getNumber(), 0);
            bookmarkPopupManager.showPopup();
        }
    }

    private void openNewTabIfNeeded(MouseEvent event) {
        if (isCtrlPressed(event)) {
            textPanelService.openNewTab();
        }
    }

    private void openChapter(MouseEvent event) {
        JTable table = (JTable) event.getSource();
        int row = table.rowAtPoint(event.getPoint());
        historyService.open(row);
    }

    private Chapter getChapter(MouseEvent event) {
        JTable table = (JTable) event.getSource();
        int row = table.rowAtPoint(event.getPoint());
        return historyService.get(row);
    }

    private boolean isCtrlPressed(MouseEvent event) {
        return (event.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) == InputEvent.CTRL_DOWN_MASK;
    }
}
