/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.HistoryEntry;
import org.bitbucket.lorandkutvolgyi.presenter.HistoryPanelPresenter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class HistoryServiceTest {

    private HistoryService underTest;
    private TranslationService translationService;
    private HistoryPanelPresenter presenter;

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        presenter = mock(HistoryPanelPresenter.class);
        underTest = new HistoryService(translationService, presenter);
        underTest.clear();
    }

    @Test
    void addNewElement_shouldPassItToThePresenter() {
        Chapter chapter = new Chapter("RÚF", "Máté", 1, new ArrayList<>(), "comments", new HashMap<>());

        underTest.addNewElement(new NewChapterEvent(chapter));

        verify(presenter).setData(argThat(new HistoryEntryListMatcher(chapter)));
    }

    @Test
    void open_shouldSetCurrentChapterWithCorrespondingStoredChapter() {
        Chapter chapter = new Chapter("RÚF", "Máté", 1, new ArrayList<>(), "comments", new HashMap<>());
        underTest.addNewElement(new NewChapterEvent(chapter));

        underTest.open(0);

        verify(translationService).openNewChapter(argThat(new ChapterMatcher(chapter)));
    }

    private static class HistoryEntryListMatcher implements ArgumentMatcher<List<HistoryEntry>> {

        private final Chapter chapter;

        HistoryEntryListMatcher(Chapter chapter) {
            this.chapter = chapter;
        }

        @Override
        public boolean matches(List<HistoryEntry> historyEntries) {
            return historyEntries.get(0).getChapter().equals(chapter);
        }
    }

    private static class ChapterMatcher implements ArgumentMatcher<Chapter> {

        private final Chapter expectedChapter;

        ChapterMatcher(Chapter expectedChapter) {
            this.expectedChapter = expectedChapter;
        }

        @Override
        public boolean matches(Chapter actualChapter) {
            return expectedChapter.equals(actualChapter);
        }
    }
}
