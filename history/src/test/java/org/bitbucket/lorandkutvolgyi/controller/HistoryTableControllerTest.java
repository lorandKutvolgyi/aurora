/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.presenter.HistoryPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.HistoryService;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class HistoryTableControllerTest {

    private HistoryTableController underTest;
    private HistoryService historyService;
    private TextPanelService textPanelService;
    private BookmarkPopupManager bookmarkPopupManager;

    @BeforeEach
    void setUp() {
        historyService = mock(HistoryService.class);
        textPanelService = mock(TextPanelService.class);
        bookmarkPopupManager = mock(BookmarkPopupManager.class);
        underTest = new HistoryTableController(historyService, textPanelService, bookmarkPopupManager);
    }

    @Test
    void mouseClicked_shouldDoNothing_whenOnlyOneLeftClickHappened() {
        int clickCount = 1;
        MouseEvent event = new MouseEvent(new JTable(), 0, 0, 0, 0, 0, clickCount, false, 1);

        underTest.mouseClicked(event);

        verify(textPanelService, never()).openNewTab();
        verify(historyService, never()).open(anyInt());
    }

    @Test
    void mouseClicked_shouldDoOpenChapterButNotInNewTab_whenDoubleLeftClickHappenedAndCtrlWasNotPressed() {
        int clickCount = 2;
        int modifiers = 0;
        MouseEvent event = new MouseEvent(new JTable(), 0, 0, modifiers, 0, 0, clickCount, false, 1);

        underTest.mouseClicked(event);

        verify(textPanelService, never()).openNewTab();
        verify(historyService).open(anyInt());
    }

    @Test
    void mouseClicked_shouldDoOpenChapterInNewTab_whenDoubleLeftClickHappenedAndCtrlWasPressed() {
        int clickCount = 2;
        int modifiers = InputEvent.CTRL_MASK;
        MouseEvent event = new MouseEvent(new JTable(), 0, 0, modifiers, 0, 0, clickCount, false, 1);

        underTest.mouseClicked(event);

        verify(textPanelService).openNewTab();
        verify(historyService).open(anyInt());
    }

    @Test
    void mouseClicked_shouldShowBookmarkPopupWithHistoryElement_whenRightClickHappened() {
        int rightButton = 3;
        JTable source = mock(JTable.class);
        when(source.getLocationOnScreen()).thenReturn(new Point(1, 1));
        when(source.rowAtPoint(any())).thenReturn(0);
        Chapter chapter = new Chapter("ASV", "Matthew", 1, new ArrayList<>(), "", new HashMap<>());
        MouseEvent event = new MouseEvent(source, 1, 1, 0, 1, 1, 1, false, rightButton);
        historyService = new HistoryService(mock(TranslationService.class), mock(HistoryPanelPresenter.class));
        historyService.clear();
        historyService.addNewElement(new NewChapterEvent(chapter));
        underTest = new HistoryTableController(historyService, textPanelService, bookmarkPopupManager);

        underTest.mouseClicked(event);

        verify(bookmarkPopupManager).setDefaults("ASV", "Matthew", 1, 0);
        verify(bookmarkPopupManager).showPopup();
    }
}
