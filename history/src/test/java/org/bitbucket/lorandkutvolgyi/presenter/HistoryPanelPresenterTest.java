/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.HistoryChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.HistoryEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class HistoryPanelPresenterTest {

    private HistoryPanelPresenter underTest;
    private MessageBus messageBus;

    @BeforeEach
    void setUp() {
        messageBus = mock(MessageBus.class);
        underTest = new HistoryPanelPresenter(messageBus);
    }

    @Test
    void setData_shouldPublishTheNewDataToMessageBus() {
        ArrayList<HistoryEntry> historyEntries = new ArrayList<>();
        Chapter chapter = new Chapter("RÚF", "Máté", 1, new ArrayList<>(), "comments", new HashMap<>());
        historyEntries.add(HistoryEntry.builder().chapter(chapter).build());

        underTest.setData(historyEntries);

        verify(messageBus).publish(new HistoryChangedEvent(new String[]{"Máté 1 - RÚF"}));
    }

    @Test
    void getColumns_shouldReturnOnlyOneColumnWithEmptyHeader() {
        String[] columns = underTest.getColumns();

        assertEquals(1, columns.length);
        assertTrue(columns[0].isEmpty());
    }
}
