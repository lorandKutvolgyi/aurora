/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.service.VersionProvider;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

@Slf4j
public class ResourceManager {

    private static final File auroraFolder = new File(WORKING_DIRECTORY + "/.aurora");

    @SneakyThrows
    public void create(ResourcesFileHandlerFacade resourcesFileHandler) {
        String version = VersionProvider.getVersion();
        if (auroraFolder.exists()) {
            if (isNotUpdated(version)) {
                deleteWhatShouldBeCreated(resourcesFileHandler);
                setUpdateIndicatorFile(version);
            }
            createMissingFiles(resourcesFileHandler);
        } else {
            createFullHierarchy(resourcesFileHandler);
            setUpdateIndicatorFile(version);
        }
    }

    private static boolean isNotUpdated(String version) {
        return !new File(WORKING_DIRECTORY + "/.aurora/updated" + version + ".txt").exists();
    }

    private static void deleteWhatShouldBeCreated(ResourcesFileHandlerFacade resourcesFileHandler) {
        resourcesFileHandler.deleteScriptureFilesIfAnyExist();
        resourcesFileHandler.deleteMapFilesIfAnyExist();
        resourcesFileHandler.deleteCrossReferencesFilesIfAnyExist();
        resourcesFileHandler.deleteDictionaryFilesIfAnyExist();
        resourcesFileHandler.deleteGeneralFiles();
    }

    private static void setUpdateIndicatorFile(String version) {
        deletePreviousUpdateIndicatorFile(version);
        createNewUpdateIndicatorFile(version);
    }

    private static void createMissingFiles(ResourcesFileHandlerFacade resourcesFileHandler) throws IOException {
        resourcesFileHandler.createMissingDbFiles();
        resourcesFileHandler.createMissingMapFiles();
        resourcesFileHandler.createMissingScriptureFiles();
        resourcesFileHandler.createMissingCrossReferencesFiles();
        resourcesFileHandler.createMissingDictionaryFiles();
        resourcesFileHandler.createMissingGeneralFiles();
    }

    private static void createFullHierarchy(ResourcesFileHandlerFacade resourcesFileHandler) throws IOException {
        createAuroraFolder();

        resourcesFileHandler.createGeneralFiles();
        resourcesFileHandler.createDbFiles();
        resourcesFileHandler.createMapFiles();
        resourcesFileHandler.createScriptureFiles();
        resourcesFileHandler.createCrossReferencesFiles();
        resourcesFileHandler.createDictionaryFiles();
    }

    @SneakyThrows
    private static void deletePreviousUpdateIndicatorFile(String version) {
        File[] files = new File(WORKING_DIRECTORY + "/.aurora/").listFiles();
        if (files != null) {
            Arrays.stream(files)
                    .filter(file -> file.getName().startsWith("updated"))
                    .filter(file -> !file.getName().contains(version))
                    .forEach(File::delete);
        }
    }

    @SneakyThrows
    private static void createNewUpdateIndicatorFile(String version) {
        Files.createFile(Path.of(WORKING_DIRECTORY + "/.aurora/updated" + version + ".txt"));
    }

    private static void createAuroraFolder() throws IOException {
        if (!auroraFolder.exists()) {
            Files.createDirectory(auroraFolder.toPath());
        }
    }
}
