/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import org.bitbucket.lorandkutvolgyi.service.VersionProvider;

import java.awt.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SplashScreenHandler {

    private static final SplashScreen splashScreen = SplashScreen.getSplashScreen();
    private static final Graphics2D graphics = splashScreen.createGraphics();
    volatile static AtomicInteger status = new AtomicInteger(0);

    static void show() {
        graphics.setColor(new Color(255, 176, 111));
        graphics.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 9));
        graphics.drawString("Copyright (c) 2021-2023 Lóránd Kútvölgyi - v " + VersionProvider.getVersion(), 170, 165);
        graphics.draw3DRect(6, 280, 588, 10, true);
        splashScreen.update();
    }

    static void incrementLoadingStatus() {
        int start = status.getAndIncrement();
        graphics.setColor(new Color(255, 176, 111));
        graphics.fill3DRect(6 + start * 196, 280, 196, 10, true);
        splashScreen.update();
    }
}
