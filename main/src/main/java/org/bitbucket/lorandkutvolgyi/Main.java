/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import dorkbox.messageBus.MessageBus;
import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.controller.*;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.*;
import org.bitbucket.lorandkutvolgyi.repository.*;
import org.bitbucket.lorandkutvolgyi.resourcefilehandler.*;
import org.bitbucket.lorandkutvolgyi.service.*;
import org.bitbucket.lorandkutvolgyi.ui.*;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.swing.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.*;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.*;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.PREFERENCES;
import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

public class Main {

    private static final String[] books = {"acts", "amos", "colossians", "daniel", "deuteronomy", "ecclesiastes",
            "ephesians", "esther", "exodus", "ezekiel", "ezra", "galatians", "genesis", "habakkuk", "haggai", "hebrews",
            "hosea", "i_chronicles", "ii_chronicles", "i_corinthians", "ii_corinthians", "i_john", "ii_john", "iii_john",
            "i_kings", "ii_kings", "i_peter", "ii_peter", "i_samuel", "ii_samuel", "i_thessalonians", "ii_thessalonians",
            "i_timothy", "ii_timothy", "isaiah", "james", "jeremiah", "job", "joel", "john", "jonah", "joshua", "jude",
            "judges", "lamentations", "leviticus", "luke", "malachi", "mark", "matthew", "micah", "nahum", "nehemiah",
            "numbers", "obadiah", "philemon", "philippians", "proverbs", "psalms", "revelation", "romans", "ruth",
            "song_of_solomon", "titus", "zechariah", "zephaniah"};
    private static final File mapFolder = new File(WORKING_DIRECTORY + "/.aurora/map");
    private static final File enFolder = new File(WORKING_DIRECTORY + "/.aurora/map/en");
    private static final File huFolder = new File(WORKING_DIRECTORY + "/.aurora/map/hu");
    private static final File dictionaryFolder = new File(WORKING_DIRECTORY + "/.aurora/dictionary");
    private static final File crossReferencesFolder = new File(WORKING_DIRECTORY + "/.aurora/crossreferences");
    private static final File dbFolder = new File(WORKING_DIRECTORY + "/.aurora/db");
    private static final File commentsFile = new File("%s/.aurora/db/comments.db".formatted(WORKING_DIRECTORY));
    private static final File bookmarksFile = new File("%s/.aurora/db/bookmarks.db".formatted(WORKING_DIRECTORY));
    private static final File textFolder = new File(WORKING_DIRECTORY + "/.aurora/text");
    private static final File licenseFile = new File(WORKING_DIRECTORY + "/.aurora/gpl-3.0.txt");
    private static final File aboutEnFile = new File(WORKING_DIRECTORY + "/.aurora/about_en.html");
    private static final File aboutHuFile = new File(WORKING_DIRECTORY + "/.aurora/about_hu.html");
    private static final File aboutUsedComponentsFile = new File(WORKING_DIRECTORY + "/.aurora/about_used_components.html");
    private static final File aboutUsedMaterialsEnFile = new File(WORKING_DIRECTORY + "/.aurora/about_used_materials_en.html");
    private static final File aboutUsedMaterialsHuFile = new File(WORKING_DIRECTORY + "/.aurora/about_used_materials_hu.html");
    private static final File facebookPngFile = new File(WORKING_DIRECTORY + "/.aurora/facebook.png");
    private static final File videoPngFile = new File(WORKING_DIRECTORY + "/.aurora/video.png");
    private static final File folderPngFile = new File(WORKING_DIRECTORY + "/.aurora/folder.png");
    private static final File startPageFile = new File(WORKING_DIRECTORY + "/.aurora/start_page.html");

    public static void main(String[] args) {
        NativeInterface.initialize();

        clearPreferencesIfNeeded();

        SplashScreenHandler.show();

        createResourcesFiles();

        SplashScreenHandler.incrementLoadingStatus();

        SwingUtilities.invokeLater(Main::createDependencies);

        NativeInterface.runEventPump();
    }

    @SneakyThrows
    private static void clearPreferencesIfNeeded() {
        if ("true".equalsIgnoreCase(System.getProperty("CLEAR_PREFERENCES"))) {
            PREFERENCES.clear();
        }
    }

    private static void createResourcesFiles() {
        new ResourceManager()
                .create(new ResourceFileHandlerFacadeImpl(new MapFileHandler(books, mapFolder, enFolder, huFolder),
                        new DbFileHandler(dbFolder, commentsFile, bookmarksFile),
                        new DictionaryFileHandler(dictionaryFolder),
                        new CrossReferencesFileHandler(crossReferencesFolder),
                        new TextFileHandler(textFolder),
                        new GeneralFileHandler(licenseFile, aboutEnFile, aboutHuFile, aboutUsedComponentsFile, aboutUsedMaterialsEnFile,
                                aboutUsedMaterialsHuFile, facebookPngFile, videoPngFile, folderPngFile, startPageFile)
                ));
    }

    private static void createDependencies() {
        MessageBus messageBus = new MessageBus();
        RestartService restartService = new RestartService(messageBus);
        LocaleService localeService = new LocaleService(restartService);
        BookLabel.StaticFieldHolder.setAttributes(localeService, WORKING_DIRECTORY);
        ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", localeService.getSelectedLocale());

        jmxEndpointForSystemTest(messagesBundle);

        ThemeService themeService = new ThemeService();
        Theme selectedTheme = themeService.getSelectedTheme();

        TranslationRepository translationRepository = new TranslationRepository(WORKING_DIRECTORY);
        SplashScreenHandler.incrementLoadingStatus();
        TranslationService translationService = new TranslationService(translationRepository, messageBus);

        TextPanelPresenter textPanelPresenter = new TextPanelPresenter(messageBus, selectedTheme,
                new CrossReferenceRepository(WORKING_DIRECTORY), messagesBundle, translationService);
        TextPanelService textPanelService = new TextPanelService(textPanelPresenter);

        Map<String, SavablePanel> leftPanels = new LinkedHashMap<>();

        BookmarkRepository bookmarkRepository = new BookmarkRepository(WORKING_DIRECTORY);
        BookmarkService bookmarkService = new BookmarkService(bookmarkRepository, messageBus);
        BookmarkPopupPresenter bookmarkPopupPresenter = new BookmarkPopupPresenter(bookmarkService, translationService, messageBus);
        BookmarkPanelPresenter bookmarkPanelPresenter = new BookmarkPanelPresenter(bookmarkService);
        SaveButtonController saveButtonController = new SaveButtonController(bookmarkService, translationService);
        BookmarkPopupManagerImpl bookmarkPopupManager = new BookmarkPopupManagerImpl(bookmarkPopupPresenter, saveButtonController, messagesBundle,
                messageBus, selectedTheme);
        BookmarkRemoveController bookmarkRemoveController = new BookmarkRemoveController(bookmarkService);
        BookmarkTreeController bookmarkTreeController = new BookmarkTreeController(textPanelService, translationService);
        BookmarkPanel bookmarkPanel = new BookmarkPanel(bookmarkPanelPresenter, bookmarkPopupManager, bookmarkRemoveController,
                bookmarkTreeController, selectedTheme, messagesBundle);

        ChapterSelectionListener chapterSelectionListener = new ChapterSelectionListener(translationService, textPanelService);
        PopupKeyListener popupKeyListener = new PopupKeyListener();
        ChapterSelectorPopup chapterSelectorPopup = new ChapterSelectorPopup(chapterSelectionListener, popupKeyListener, selectedTheme,
                messagesBundle);
        ChaptersPopupManager chaptersPopupManager = new ChaptersPopupManagerImpl(chapterSelectorPopup);
        BooksService booksService = new BooksService(translationService);
        BookSelectionController bookSelectionController = new BookSelectionController(booksService, chaptersPopupManager, bookmarkPopupManager);
        chapterSelectionListener.setPopupManager(chaptersPopupManager);
        popupKeyListener.setPopupManager(chaptersPopupManager, chapterSelectionListener, new ArrayList<>(3));
        BooksPanelPresenter booksPanelPresenter = new BooksPanelPresenter(translationService, messageBus, localeService);
        BookSelectionMouseListener bookSelectionMouseListener = new BookSelectionMouseListener(bookSelectionController);
        BookSelectionKeyListener bookSelectionKeyListener = new BookSelectionKeyListener(bookSelectionController);
        BooksPanel booksPanel = new BooksPanel(booksPanelPresenter, bookSelectionMouseListener, bookSelectionKeyListener);

        leftPanels.put(messagesBundle.getString("BOOKS"), booksPanel);

        SearchPanelPresenter searchPanelPresenter = new SearchPanelPresenter(translationService, messagesBundle, messageBus);
        SearchRepository searchRepository = new SearchRepository(translationRepository);
        SearchService searchService = new SearchService(searchRepository, searchPanelPresenter, messagesBundle, translationService);
        TranslationComboBoxController translationComboBoxController = new TranslationComboBoxController(searchService);
        SearchResultController searchResultController = new SearchResultController(translationService, textPanelService, bookmarkPopupManager);
        SearchTextFieldController searchTextFieldController = new SearchTextFieldController(searchService);
        SearchPanel searchPanel = new SearchPanel(searchPanelPresenter, searchTextFieldController, searchResultController, translationComboBoxController,
                messageBus, selectedTheme);

        leftPanels.put(messagesBundle.getString("SEARCH"), searchPanel);

        MainWindowPresenter mainWindowPresenter = new MainWindowPresenter(translationService, messageBus, messagesBundle);


        VerseComparingService verseComparingService = new VerseComparingService(translationService, messageBus);
        VerseComparingListener verseComparingListener = new VerseComparingListener(verseComparingService);
        TextAreaMouseListener textAreaMouseListener = new TextAreaMouseListener(textPanelPresenter);
        CrossReferencesController crossReferencesController = new CrossReferencesController(translationService, messagesBundle, textPanelService);
        CrossReferencesListener crossReferenceListener = new CrossReferencesListener(crossReferencesController);
        TextArea textArea = new TextArea(textAreaMouseListener, crossReferenceListener, messageBus);
        VerseComparingPopupPresenter presenter = new VerseComparingPopupPresenter(translationService, messagesBundle);
        VerseComparingPopupActionListener verseComparingPopupActionListener = new VerseComparingPopupActionListener(verseComparingService);
        CloseTabListener closeTabListener = new CloseTabListener(translationService, mainWindowPresenter);
        DictionaryService dictionaryService = new DictionaryService(translationService, new DictionaryRepository(WORKING_DIRECTORY, localeService), messageBus);
        TextPanelContextMenu textPanelContextMenu = new TextPanelContextMenu(messagesBundle, verseComparingListener, bookmarkPopupManager, translationService,
                new DictionaryMenuItemListener(dictionaryService), textPanelPresenter);
        SearchInputKeyController searchInputKeyListener = new SearchInputKeyController(textPanelService);
        SearchInputDocumentListener searchInputDocumentListener = new SearchInputDocumentListener(textPanelService);
        SearchInputField searchInputField = new SearchInputField(searchInputKeyListener, searchInputDocumentListener, messagesBundle, selectedTheme);
        VerseComparingPopup verseComparingPopup = new VerseComparingPopup(verseComparingPopupActionListener, presenter);
        DictionaryPopupPresenter dictionaryPopupPresenter = new DictionaryPopupPresenter(messageBus, dictionaryService);
        DictionaryPopup dictionaryPopup = new DictionaryPopup(dictionaryPopupPresenter, dictionaryService, messagesBundle);
        TextPanel textPanel = new TextPanel(textPanelPresenter, verseComparingPopup, dictionaryPopup, closeTabListener, textPanelContextMenu,
                searchInputField, textArea, messageBus);

        Map<String, SavablePanel> topRightPanels = new LinkedHashMap<>();

        MapPresenter mapPresenter = new MapPresenterImpl();
        MapService mapService = new MapServiceImpl(mapPresenter);
        MapController mapController = new MapController(mapService);
        MapPanel mapPanel = new MapPanel(selectedTheme, mapController, mapPresenter);
        topRightPanels.put(messagesBundle.getString("MAP"), mapPanel);
        topRightPanels.put(messagesBundle.getString("BOOKMARK"), bookmarkPanel);

        HistoryPanelPresenter historyPanelPresenter = new HistoryPanelPresenter(messageBus);
        HistoryService historyService = new HistoryService(translationService, historyPanelPresenter);
        HistoryTableController historyTableController = new HistoryTableController(historyService, textPanelService, bookmarkPopupManager);
        HistoryPanel historyPanel = new HistoryPanel(historyPanelPresenter, historyTableController, selectedTheme);
        topRightPanels.put(messagesBundle.getString("HISTORY"), historyPanel);
        topRightPanels.put(messagesBundle.getString("BROWSER"), new BrowserPanel(new BrowserPresenter(WORKING_DIRECTORY)));

        Map<String, JPanel> bottomRightPanels = new LinkedHashMap<>();

        CommentsPanelPresenter commentsPanelPresenter = new CommentsPanelPresenter(messageBus);
        PanelTextReceiverImpl panelTextReceiver = new PanelTextReceiverImpl();
        CommentsToolbarListener commentsToolbarListener = new CommentsToolbarListener(commentsPanelPresenter, translationService, panelTextReceiver);
        CommentsPanel commentsPanel = new CommentsPanel(commentsPanelPresenter, commentsToolbarListener, selectedTheme, messagesBundle);
        panelTextReceiver.setCommentsPanel(commentsPanel);
        bottomRightPanels.put(messagesBundle.getString("COMMENTS"), commentsPanel);

        StatisticsPanelPresenter statisticsPanelPresenter = new StatisticsPanelPresenter(translationService, messageBus, messagesBundle);
        StatisticsPanel statisticsPanel = new StatisticsPanel(selectedTheme);
        StatisticsCreator statisticsCreator = new StatisticsCreator(translationRepository);
        statisticsCreator.create();
        SplashScreenHandler.incrementLoadingStatus();
        bottomRightPanels.put(messagesBundle.getString("STATISTICS"), statisticsPanel);

        LeftArea leftArea = new LeftArea(SwingConstants.TOP, leftPanels);
        MiddleAreaTabChangeListener middleAreaTabChangeListener = new MiddleAreaTabChangeListener(translationService);
        MiddleArea middleArea = new MiddleArea(SwingConstants.TOP, textPanel, middleAreaTabChangeListener);
        TopRightArea topRightArea = new TopRightArea(SwingConstants.TOP, topRightPanels);
        BottomRightArea bottomRightArea = new BottomRightArea(SwingConstants.BOTTOM, bottomRightPanels);
        Map<String, JTabbedPane> areas = new HashMap<>();
        areas.put(MainWindow.LEFT_AREA, leftArea);
        areas.put(MainWindow.MIDDLE_AREA, middleArea);
        areas.put(MainWindow.TOP_RIGHT_AREA, topRightArea);
        areas.put(MainWindow.BOTTOM_RIGHT_AREA, bottomRightArea);

        TranslationSelectionListener translationSelectionListener = new TranslationSelectionListener(translationService);
        LanguageSelectionController languageSelectionController = new LanguageSelectionController(localeService);
        AboutMenuPopup aboutMenuPopup = new AboutMenuPopup(messagesBundle, selectedTheme, localeService, WORKING_DIRECTORY);
        MainMenu mainMenu = new MainMenu(translationSelectionListener, selectedTheme, messagesBundle, mainWindowPresenter, languageSelectionController,
                aboutMenuPopup, localeService);
        VerseNumberHidingPresenter verseNumberHidingPresenter = new VerseNumberHidingPresenter(messagesBundle);
        VerseNumberHidingController verseNumberHidingController = new VerseNumberHidingController(messageBus, verseNumberHidingPresenter);
        CrossReferencesHidingPresenter crossReferencesHidingPresenter = new CrossReferencesHidingPresenter(messagesBundle);
        CrossReferencesHidingController crossReferencesHidingController = new CrossReferencesHidingController(messageBus, crossReferencesHidingPresenter);
        UpdateService updateService = new UpdateService(messageBus, WORKING_DIRECTORY);
        UpdateController updateController = new UpdateController(updateService, new UpdatePopupManagerImpl(messagesBundle), messageBus);
        ToolBarListener toolBarListener = new ToolBarListener(translationService);
        MainToolbar mainToolbar = new MainToolbar(messagesBundle, toolBarListener, selectedTheme, verseNumberHidingController,
                verseNumberHidingPresenter, crossReferencesHidingController, crossReferencesHidingPresenter, updateController, messageBus);
        MainWindow mainWindow = new MainWindow(areas, mainWindowPresenter, selectedTheme, mainMenu, mainToolbar);
        mainToolbar.setParentWindow(mainWindow);
        chapterSelectorPopup.setParentWindow(mainWindow);
        translationService.setTextTabManager(mainWindow);
        mapService.setTextTabManager(mainWindow);

        messageBus.subscribe(historyService);
        messageBus.subscribe(historyPanelPresenter);
        messageBus.subscribe(booksPanelPresenter);
        messageBus.subscribe(commentsPanelPresenter);
        messageBus.subscribe(translationService);
        messageBus.subscribe(mapController);
        messageBus.subscribe(searchPanel);
        messageBus.subscribe(statisticsPanelPresenter);
        messageBus.subscribe(textArea);
        messageBus.subscribe(mainWindowPresenter);
        messageBus.subscribe(mainWindow);
        messageBus.subscribe(booksPanel);
        messageBus.subscribe(historyPanel);
        messageBus.subscribe(textPanel);
        messageBus.subscribe(commentsPanel);
        messageBus.subscribe(bookmarkPopupPresenter);
        messageBus.subscribe(statisticsPanel);
        messageBus.subscribe(verseComparingService);
        messageBus.subscribe(bookmarkPanelPresenter);
        messageBus.subscribe(textPanelPresenter);
        messageBus.subscribe(mainToolbar);
        messageBus.subscribe(dictionaryPopup);
        messageBus.subscribe(dictionaryPopupPresenter);
        messageBus.subscribe(verseComparingPopup);

        String translationName = PreferencesService.get(OPENED_CHAPTER_TRANSLATION);
        if (translationService.getDownloadedTranslationNames().contains(translationName)) {
            int chapterNumber = PreferencesService.getInt(OPENED_CHAPTER_NUMBER);
            BookLabel bookLabel = BookLabel.valueOf(PreferencesService.get(OPENED_CHAPTER_NAME));
            translationService.openNewChapter(translationName, bookLabel, chapterNumber);
        }
    }

    @SneakyThrows
    private static void jmxEndpointForSystemTest(ResourceBundle messagesBundle) {
        if ("true".equals(System.getProperty("testenv"))) {
            ObjectName objectName = new ObjectName("org.bitbucket.lorandkutvolgyi:type=basic,name=langService");
            MBeanServer server = ManagementFactory.getPlatformMBeanServer();
            server.registerMBean(new UsedLanguageProvider(messagesBundle), objectName);
        }
    }
}
