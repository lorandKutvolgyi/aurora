/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi;

import java.io.IOException;

public interface ResourcesFileHandlerFacade {

    void createMissingMapFiles() throws IOException;

    void createMapFiles() throws IOException;

    void deleteMapFilesIfAnyExist();

    void createMissingDbFiles() throws IOException;

    void createDbFiles() throws IOException;

    void createMissingDictionaryFiles() throws IOException;

    void createDictionaryFiles() throws IOException;

    void deleteDictionaryFilesIfAnyExist();

    void createMissingCrossReferencesFiles() throws IOException;

    void createCrossReferencesFiles() throws IOException;

    void deleteCrossReferencesFilesIfAnyExist();

    void createMissingScriptureFiles() throws IOException;

    void createScriptureFiles() throws IOException;

    void deleteScriptureFilesIfAnyExist();

    void createMissingGeneralFiles() throws IOException;

    void createGeneralFiles() throws IOException;

    void deleteGeneralFiles();
}
