/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

public class DbFileHandler {

    private final File dbFolder;
    private final File commentsFile;
    private final File bookmarksFile;

    public DbFileHandler(File dbFolder, File commentsFile, File bookmarksFile) {
        this.dbFolder = dbFolder;
        this.commentsFile = commentsFile;
        this.bookmarksFile = bookmarksFile;
    }

    public void createMissing() throws IOException {
        if (!dbFolder.exists()) {
            createDbFolder();
        }
        if (!commentsFile.exists()) {
            createCommentsFile();
        }
        if (!bookmarksFile.exists()) {
            createBookmarksFile();
        }
    }

    public void create() throws IOException {
        createDbFolder();
        createCommentsFile();
        createBookmarksFile();
    }

    private void createDbFolder() throws IOException {
        Files.createDirectory(dbFolder.toPath());
    }

    private void createCommentsFile() throws IOException {
        Files.copy(Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/db/comments.db")), commentsFile.toPath());
    }

    private void createBookmarksFile() throws IOException {
        Files.copy(Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/db/bookmarks.db")), bookmarksFile.toPath());
    }
}
