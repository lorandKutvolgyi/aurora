/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

public class GeneralFileHandler {

    private final File licenseFile;
    private final File aboutEnFile;
    private final File aboutHuFile;
    private final File aboutUsedComponentsFile;
    private final File aboutUsedMaterialsEnFile;
    private final File aboutUsedMaterialsHuFile;
    private final File facebookPngFile;
    private final File videoPngFile;
    private final File folderPngFile;
    private final File startPageFile;

    public GeneralFileHandler(File licenseFile, File aboutEnFile, File aboutHuFile, File aboutUsedComponentsFile,
                              File aboutUsedMaterialsEnFile, File aboutUsedMaterialsHuFile, File facebookPngFile,
                              File videoPngFile, File folderPngFile, File startPageFile) {
        this.licenseFile = licenseFile;
        this.aboutEnFile = aboutEnFile;
        this.aboutHuFile = aboutHuFile;
        this.aboutUsedComponentsFile = aboutUsedComponentsFile;
        this.aboutUsedMaterialsEnFile = aboutUsedMaterialsEnFile;
        this.aboutUsedMaterialsHuFile = aboutUsedMaterialsHuFile;
        this.facebookPngFile = facebookPngFile;
        this.videoPngFile = videoPngFile;
        this.folderPngFile = folderPngFile;
        this.startPageFile = startPageFile;
    }

    public void createMissing() throws IOException {
        if (isAnyNonExistentFile()) {
            createFiles();
        }
    }

    public void create() throws IOException {
        createFiles();
    }

    public void deleteFiles() {
        new File(WORKING_DIRECTORY + "/.aurora/about_en.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/about_hu.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/about_used_components.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/about_used_materials_en.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/about_used_materials_hu.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/start_page.html").delete();
        new File(WORKING_DIRECTORY + "/.aurora/facebook.png").delete();
        new File(WORKING_DIRECTORY + "/.aurora/folder.png").delete();
        new File(WORKING_DIRECTORY + "/.aurora/video.png").delete();
        new File(WORKING_DIRECTORY + "/.aurora/gpl-3.0.txt").delete();
    }

    private boolean isAnyNonExistentFile() {
        return !licenseFile.exists() || !aboutHuFile.exists() || !aboutEnFile.exists()
                || !aboutUsedMaterialsHuFile.exists() || !aboutUsedMaterialsEnFile.exists() || !aboutUsedComponentsFile.exists()
                || !facebookPngFile.exists() || !videoPngFile.exists() || !folderPngFile.exists() || !startPageFile.exists();
    }

    private void createFiles() throws IOException {
        copy("/.aurora/about_en.html");
        copy("/.aurora/about_hu.html");
        copy("/.aurora/about_used_components.html");
        copy("/.aurora/about_used_materials_en.html");
        copy("/.aurora/about_used_materials_hu.html");
        copy("/.aurora/start_page.html");
        copy("/.aurora/facebook.png");
        copy("/.aurora/folder.png");
        copy("/.aurora/video.png");
        copy("/.aurora/gpl-3.0.txt");
    }

    private void copy(String name) throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream(name)),
                new File(String.join("", WORKING_DIRECTORY, name)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }
}
