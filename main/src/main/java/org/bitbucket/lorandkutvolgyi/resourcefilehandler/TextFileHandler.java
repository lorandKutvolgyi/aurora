/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

@Slf4j
public class TextFileHandler {

    private final File textFolder;

    public TextFileHandler(File textFolder) {
        this.textFolder = textFolder;
    }

    public void createMissing() throws IOException {
        if (!textFolder.exists()) {
            createTextFolder();
        }
        if (isAnyNonExistentTextFile()) {
            createTextFiles();
        }
    }

    public void create() throws IOException {
        createTextFolder();
        createTextFiles();
    }

    public void deleteTextsIfExist() {
        if (textFolder.exists()) {
            deleteTexts();
        }
    }

    private void createTextFolder() throws IOException {
        Files.createDirectory(textFolder.toPath());
    }

    private boolean isAnyNonExistentTextFile() throws IOException {
        for (String text : getTextFiles()) {
            if (!new File(text).exists()) {
                return true;
            }
        }
        return false;
    }

    private void createTextFiles() throws IOException {
        getTextFiles().forEach(file -> {
            try {
                copyTextFiles(file);
            } catch (IOException exception) {
                log.error("%s does not exist".formatted(file));
            }
        });
    }

    private void deleteTexts() {
        Arrays.stream(Objects.requireNonNull(textFolder.listFiles())).forEach(file -> {
            if (file.delete()) {
                log.info("text file deleted: {}", file.getName());
            }
        });
        boolean deleted = textFolder.delete();
        log.info("text folder deleted: {}", deleted);
    }

    private List<String> getTextFiles() throws IOException {
        String path = ResourceManager.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        final File jarFile = new File(URLDecoder.decode(path, StandardCharsets.UTF_8));
        if (runWithinJar(jarFile)) {
            return getFileNamesFromJar(jarFile);
        } else {
            assert runFromIDE(jarFile);
            return getFileNamesFromClassPath();
        }
    }

    private void copyTextFiles(String file) throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/text/" + file)),
                new File("%s/.aurora/text/%s".formatted(WORKING_DIRECTORY, file)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private boolean runWithinJar(File jarFile) {
        return jarFile.isFile();
    }

    private List<String> getFileNamesFromJar(File jarFile) throws IOException {
        String prefix = ".aurora/text/";
        List<String> resourcesFiles = new ArrayList<>();
        final JarFile jar = new JarFile(jarFile);
        final Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
            final String name = entries.nextElement().getName();
            log.info("name: " + name);
            if (name.startsWith(prefix) && !name.equals(prefix)) {
                resourcesFiles.add(name.replace(prefix, ""));
            }
        }
        jar.close();
        return resourcesFiles;
    }

    private boolean runFromIDE(File jarFile) {
        return !runWithinJar(jarFile);
    }

    private List<String> getFileNamesFromClassPath() throws IOException {
        String prefix = ".aurora" + File.separator + "text";
        List<String> resourcesFiles = new ArrayList<>();
        try (InputStream in = ResourceManager.class.getClassLoader().getResourceAsStream(prefix);
             BufferedReader br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(in)))) {
            String resource;
            while ((resource = br.readLine()) != null) {
                resourcesFiles.add(resource);
            }
        }
        return resourcesFiles;
    }
}
