/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

@Slf4j
public class CrossReferencesFileHandler {

    private final File crossReferencesFolder;

    public CrossReferencesFileHandler(File crossReferencesFolder) {
        this.crossReferencesFolder = crossReferencesFolder;
    }

    public void createMissing() throws IOException {
        if (!crossReferencesFolder.exists()) {
            createCrossReferencesFolder();
        }
        if (isNonExistentCrossReferencesFile()) {
            createCrossReferencesFile();
        }
    }

    public void create() throws IOException {
        createCrossReferencesFolder();
        createCrossReferencesFile();
    }

    public void deleteCrossReferencesIfExist() {
        if (crossReferencesFolder.exists()) {
            deleteCrossReferences();
        }
    }

    private void createCrossReferencesFolder() throws IOException {
        Files.createDirectory(crossReferencesFolder.toPath());
    }

    private boolean isNonExistentCrossReferencesFile() {
        return !new File("%s/.aurora/crossreferences/crossreferences.csv".formatted(WORKING_DIRECTORY)).exists();
    }

    @SneakyThrows
    private void createCrossReferencesFile() {
        copyCrossReferencesFile();
    }

    private void deleteCrossReferences() {
        File crossReferencesFile = new File(crossReferencesFolder, "/crossreferences.csv");
        delete(crossReferencesFile);
        delete(crossReferencesFolder);
    }

    private void copyCrossReferencesFile() throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/crossreferences/crossreferences.csv")),
                new File("%s/.aurora/crossreferences/crossreferences.csv".formatted(WORKING_DIRECTORY)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void delete(File file) {
        if (file.delete()) {
            log.info("{} deleted", file.getName());
        }
    }
}
