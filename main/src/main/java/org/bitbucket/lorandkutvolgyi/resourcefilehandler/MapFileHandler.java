/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

@Slf4j
public class MapFileHandler {

    private final String[] books;
    private final File mapFolder;
    private final File enFolder;
    private final File huFolder;

    public MapFileHandler(String[] books, File mapFolder, File enFolder, File huFolder) {
        this.books = books;
        this.mapFolder = mapFolder;
        this.enFolder = enFolder;
        this.huFolder = huFolder;
    }

    public void createMissing() throws IOException {
        if (!mapFolder.exists()) {
            createMapFolder();
        }
        if (!enFolder.exists()) {
            createEnFolder();
        }
        if (!huFolder.exists()) {
            createHuFolder();
        }
        if (isAnyNonExistentMap("hu")) {
            createMapFiles("hu");
        }
        if (isAnyNonExistentMap("en")) {
            createMapFiles("en");
        }
    }

    public void create() throws IOException {
        createMapFolder();
        createEnFolder();
        createHuFolder();
        createMapFiles("hu");
        createMapFiles("en");
    }

    public void deleteMapsIfExist() {
        if (mapFolder.exists()) {
            deleteMaps();
        }
    }

    private void createMapFolder() throws IOException {
        Files.createDirectory(mapFolder.toPath());
    }

    private void createEnFolder() throws IOException {
        Files.createDirectory(enFolder.toPath());
    }

    private void createHuFolder() throws IOException {
        Files.createDirectory(huFolder.toPath());
    }

    private boolean isAnyNonExistentMap(String language) {
        for (String book : books) {
            if (hasNoMandatoryMap(language, book) || isThereAnyNonexistentAdditionalMap(language, book)) {
                return true;
            }
        }
        return false;
    }

    private void createMapFiles(String language) {
        Arrays.stream(books).forEach(book -> {
            try {
                copyMap(language, book);
            } catch (IOException exception) {
                log.error("Maps do not exist for book %s".formatted(book));
            }
        });
    }

    private void deleteMaps() {
        Arrays.stream(Objects.requireNonNull(mapFolder.listFiles())).forEach(langDir -> {
            Arrays.stream(Objects.requireNonNull(langDir.listFiles())).forEach(bookDir -> {
                Arrays.stream(Objects.requireNonNull(bookDir.listFiles())).forEach(this::delete);
                delete(bookDir);
            });
            delete(langDir);
        });
        delete(mapFolder);
    }

    private boolean isThereAnyNonexistentAdditionalMap(String language, String book) {
        for (int i = 2; i < 5; i++) {
            if (isNotExistingMap(language, book, i)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasNoMandatoryMap(String language, String book) {
        return !new File("%s/.aurora/map/%s/%s/map.svg".formatted(WORKING_DIRECTORY, language, book)).exists();
    }

    private void copyMap(String language, String folder) throws IOException {
        createFolderIfNecessary(language, folder);
        createFirstMapIfNecessary(language, folder);
        createAdditionalMapsIfNecessary(language, folder);
    }

    private void delete(File file) {
        if (file.delete()) {
            log.info("{} deleted", file.getName());
        }
    }

    private boolean isNotExistingMap(String language, String book, int i) {
        return ResourceManager.class.getResourceAsStream("/.aurora/map/%s/%s/map%s.svg".formatted(language, book, i)) != null
                && !new File("%s/.aurora/map/%s/%s/map%s.svg".formatted(WORKING_DIRECTORY, language, book, i)).exists();
    }

    private void createFolderIfNecessary(String language, String folder) throws IOException {
        if (!new File("%s/.aurora/map/%s/%s".formatted(WORKING_DIRECTORY, language, folder)).exists()) {
            Files.createDirectory(new File("%s/.aurora/map/%s/%s".formatted(WORKING_DIRECTORY, language, folder)).toPath());
        }
    }

    private void createFirstMapIfNecessary(String language, String folder) throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/map/%s/%s/map.svg".formatted(language, folder))),
                new File("%s/.aurora/map/%s/%s/map.svg".formatted(WORKING_DIRECTORY, language, folder)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void createAdditionalMapsIfNecessary(String language, String folder) throws IOException {
        for (int i = 2; i < 5; i++) {
            InputStream mapResources = ResourceManager.class.getResourceAsStream("/.aurora/map/%s/%s/map%s.svg".formatted(language, folder, i));
            if (mapResources != null) {
                copyMap(language, folder, i);
            }
        }
    }

    private void copyMap(String language, String folder, int i) throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/map/%s/%s/map%s.svg".formatted(language, folder, i))),
                new File("%s/.aurora/map/%s/%s/map%s.svg".formatted(WORKING_DIRECTORY, language, folder, i)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }
}
