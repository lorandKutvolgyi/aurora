/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import org.bitbucket.lorandkutvolgyi.ResourcesFileHandlerFacade;

import java.io.IOException;

public class ResourceFileHandlerFacadeImpl implements ResourcesFileHandlerFacade {

    private final MapFileHandler mapFileHandler;
    private final DbFileHandler dbFileHandler;
    private final DictionaryFileHandler dictionaryFileHandler;
    private final CrossReferencesFileHandler crossReferencesFileHandler;
    private final TextFileHandler textFileHandler;
    private final GeneralFileHandler generalFileHandler;

    public ResourceFileHandlerFacadeImpl(MapFileHandler mapFileHandler, DbFileHandler dbFileHandler, DictionaryFileHandler dictionaryFileHandler,
                                         CrossReferencesFileHandler crossReferencesFileHandler, TextFileHandler textFileHandler,
                                         GeneralFileHandler generalFileHandler) {
        this.mapFileHandler = mapFileHandler;
        this.dbFileHandler = dbFileHandler;
        this.dictionaryFileHandler = dictionaryFileHandler;
        this.crossReferencesFileHandler = crossReferencesFileHandler;
        this.textFileHandler = textFileHandler;
        this.generalFileHandler = generalFileHandler;
    }

    @Override
    public void createMissingMapFiles() throws IOException {
        mapFileHandler.createMissing();
    }

    @Override
    public void createMapFiles() throws IOException {
        mapFileHandler.create();
    }

    @Override
    public void deleteMapFilesIfAnyExist() {
        mapFileHandler.deleteMapsIfExist();
    }

    @Override
    public void createMissingDbFiles() throws IOException {
        dbFileHandler.createMissing();
    }

    @Override
    public void createDbFiles() throws IOException {
        dbFileHandler.create();
    }

    @Override
    public void createMissingDictionaryFiles() throws IOException {
        dictionaryFileHandler.createMissing();
    }

    @Override
    public void createDictionaryFiles() throws IOException {
        dictionaryFileHandler.create();
    }

    @Override
    public void deleteDictionaryFilesIfAnyExist() {
        dictionaryFileHandler.deleteDictionariesIfExist();
    }

    @Override
    public void createMissingCrossReferencesFiles() throws IOException {
        crossReferencesFileHandler.createMissing();
    }

    @Override
    public void createCrossReferencesFiles() throws IOException {
        crossReferencesFileHandler.create();
    }

    @Override
    public void deleteCrossReferencesFilesIfAnyExist() {
        crossReferencesFileHandler.deleteCrossReferencesIfExist();
    }

    @Override
    public void createMissingScriptureFiles() throws IOException {
        textFileHandler.createMissing();
    }

    @Override
    public void createScriptureFiles() throws IOException {
        textFileHandler.create();
    }

    @Override
    public void deleteScriptureFilesIfAnyExist() {
        textFileHandler.deleteTextsIfExist();
    }

    @Override
    public void createMissingGeneralFiles() throws IOException {
        generalFileHandler.createMissing();
    }

    @Override
    public void createGeneralFiles() throws IOException {
        generalFileHandler.create();
    }

    @Override
    public void deleteGeneralFiles() {
        generalFileHandler.deleteFiles();
    }
}
