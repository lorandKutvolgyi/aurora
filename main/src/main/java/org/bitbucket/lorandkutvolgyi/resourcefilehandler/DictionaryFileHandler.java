/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.resourcefilehandler;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.lorandkutvolgyi.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.WORKING_DIRECTORY;

@Slf4j
public class DictionaryFileHandler {

    private final File dictionaryFolder;

    public DictionaryFileHandler(File dictionaryFolder) {
        this.dictionaryFolder = dictionaryFolder;
    }

    public void createMissing() throws IOException {
        if (!dictionaryFolder.exists()) {
            createDictionaryFolder();
        }
        if (isAnyNonExistentGreekDictionaryFile()) {
            createGreekDictionaryFiles();
        }
    }

    public void create() throws IOException {
        createDictionaryFolder();
        createGreekDictionaryFiles();
    }

    public void deleteDictionariesIfExist() {
        if (dictionaryFolder.exists()) {
            deleteDictionaries();
        }
    }

    private void createDictionaryFolder() throws IOException {
        Files.createDirectory(dictionaryFolder.toPath());
    }

    private boolean isAnyNonExistentGreekDictionaryFile() {
        return !new File("%s/.aurora/dictionary/greek_en_dictionary.txt".formatted(WORKING_DIRECTORY)).exists()
                || !new File("%s/.aurora/dictionary/greek_hu_dictionary.txt".formatted(WORKING_DIRECTORY)).exists()
                || !new File("%s/.aurora/dictionary/greek_hu_dictionary_abbreviations.txt".formatted(WORKING_DIRECTORY)).exists();
    }

    private void createGreekDictionaryFiles() {
        try {
            copyGreekEnDictFile();
            copyGreekHuDictFile();
            copyGreekHuDictAbbreviationsFile();
        } catch (IOException exception) {
            log.error("greek_<used-language>_dictionary.txt does not exist");
        }
    }

    private void deleteDictionaries() {
        File englishDictionaryFile = new File(dictionaryFolder, "/greek_en_dictionary.txt");
        File hungarianDictionaryFile = new File(dictionaryFolder, "/greek_hu_dictionary.txt");
        File hungarianDictionaryAbbreviationsFile = new File(dictionaryFolder, "/greek_hu_dictionary_abbreviations.html");
        delete(englishDictionaryFile);
        delete(hungarianDictionaryFile);
        delete(hungarianDictionaryAbbreviationsFile);
        delete(dictionaryFolder);
    }

    private void copyGreekEnDictFile() throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/dictionary/greek_en_dictionary.txt")),
                new File("%s/.aurora/dictionary/greek_en_dictionary.txt".formatted(WORKING_DIRECTORY)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void copyGreekHuDictFile() throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/dictionary/greek_hu_dictionary.txt")),
                new File("%s/.aurora/dictionary/greek_hu_dictionary.txt".formatted(WORKING_DIRECTORY)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void copyGreekHuDictAbbreviationsFile() throws IOException {
        Files.copy(
                Objects.requireNonNull(ResourceManager.class.getResourceAsStream("/.aurora/dictionary/greek_hu_dictionary_abbreviations.html")),
                new File("%s/.aurora/dictionary/greek_hu_dictionary_abbreviations.html".formatted(WORKING_DIRECTORY)).toPath(),
                StandardCopyOption.REPLACE_EXISTING);
    }

    private void delete(File file) {
        if (file.delete()) {
            log.info("{} deleted", file.getName());
        }
    }
}
