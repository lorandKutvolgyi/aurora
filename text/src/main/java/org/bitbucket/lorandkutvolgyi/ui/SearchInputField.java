/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.controller.SearchInputDocumentListener;
import org.bitbucket.lorandkutvolgyi.controller.SearchInputKeyController;
import org.bitbucket.lorandkutvolgyi.model.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import static java.awt.GridBagConstraints.HORIZONTAL;

public class SearchInputField {

    private final SearchInputKeyController searchInputKeyController;
    private final SearchInputDocumentListener searchInputDocumentListener;
    private final ResourceBundle messagesBundle;
    private final Theme selectedTheme;
    private RoundedJTextField roundedJTextField;
    private GridBagConstraints constraints;

    public SearchInputField(SearchInputKeyController searchInputKeyController, SearchInputDocumentListener searchInputDocumentListener,
                            ResourceBundle messagesBundle, Theme selectedTheme) {
        this.searchInputKeyController = searchInputKeyController;
        this.searchInputDocumentListener = searchInputDocumentListener;
        this.messagesBundle = messagesBundle;
        this.selectedTheme = selectedTheme;

        createConstraints();
        createTextField(searchInputKeyController, searchInputDocumentListener, messagesBundle, selectedTheme);
    }

    public SearchInputField copy() {
        return new SearchInputField(searchInputKeyController, searchInputDocumentListener, messagesBundle, selectedTheme);
    }

    public void setParent(JComponent parent) {
        parent.add(roundedJTextField, constraints);
    }

    private void createConstraints() {
        constraints = new GridBagConstraints();
        constraints.fill = HORIZONTAL;
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.ipady = 10;
        constraints.anchor = GridBagConstraints.LINE_START;
    }

    private void createTextField(SearchInputKeyController searchInputKeyController,
                                 SearchInputDocumentListener searchInputDocumentListener,
                                 ResourceBundle messagesBundle,
                                 Theme selectedTheme) {
        roundedJTextField = new RoundedJTextField();
        roundedJTextField.setToolTipText(messagesBundle.getString("SEARCH"));
        roundedJTextField.setMargin(new Insets(0, 10, 0, 10));
        roundedJTextField.addKeyListener(createListener(searchInputKeyController));
        roundedJTextField.getDocument().addDocumentListener(searchInputDocumentListener);
        roundedJTextField.setBackground(selectedTheme.getInputBackgroundColor());
    }

    private KeyAdapter createListener(SearchInputKeyController searchInputKeyController) {
        return new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent event) {
                char character = event.getKeyChar();
                if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                    searchInputKeyController.search(roundedJTextField.getText());
                } else if (((character >= '0') && (character <= '9')) || (character == '<') || (character == '>')) {
                    event.consume();
                }
            }
        };
    }
}
