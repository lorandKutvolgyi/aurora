/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.NewTabEvent;
import org.bitbucket.lorandkutvolgyi.model.ShowMenuEvent;
import org.bitbucket.lorandkutvolgyi.model.TextChangedEvent;
import org.bitbucket.lorandkutvolgyi.presenter.TextPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.UUID;

import static org.bitbucket.lorandkutvolgyi.service.PreferencesService.Key.*;

public class TextPanel extends SavablePanel {

    private final transient TextPanelPresenter presenter;
    private final VerseComparingPopup verseComparingPopup;
    private final DictionaryPopup dictionaryPopup;
    private final transient ActionListener closeTabListener;
    private final transient TextPanelContextMenu textPanelContextMenu;
    private final transient SearchInputField searchInputField;
    private final transient TextArea textArea;
    private final transient MessageBus messageBus;
    private JTabbedPane tabbedPane;
    private UUID tabId;

    public TextPanel(TextPanelPresenter presenter,
                     VerseComparingPopup verseComparingPopup,
                     DictionaryPopup dictionaryPopup,
                     ActionListener closeTabListener,
                     TextPanelContextMenu textPanelContextMenu,
                     SearchInputField searchInputField,
                     TextArea textArea,
                     MessageBus messageBus) {
        this.presenter = presenter;
        this.verseComparingPopup = verseComparingPopup;
        this.dictionaryPopup = dictionaryPopup;
        this.closeTabListener = closeTabListener;
        this.textPanelContextMenu = textPanelContextMenu;
        this.searchInputField = searchInputField;
        this.textArea = textArea;
        this.messageBus = messageBus;

        setLayout(new GridBagLayout());
        setBackground(presenter.getMainBackgroundColor());
        searchInputField.setParent(this);
        textArea.setParent(this);

        setVisible(true);
        add(textPanelContextMenu);
    }

    @Subscribe
    public void openNewTab(NewTabEvent event) {
        initTabbedPaneIfNecessary();
        if (this.equals(tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()))) {
            TextPanel newTab = createNewTab();
            setupNewTab(tabbedPane, newTab);
            newTab.putClientProperty("tabId", newTab.tabId);
            tabbedPane.setSelectedComponent(newTab);
        }
    }

    @Subscribe
    public void changeText(TextChangedEvent event) {
        initTabbedPaneIfNecessary();
        if (this.equals(tabbedPane.getSelectedComponent())) {
            setText(event);
            setTitle(tabbedPane, presenter.getCurrentChapter().toString());
            setTooltip(tabbedPane, presenter.getCurrentChapter().toString());
        }
    }

    @Subscribe
    public void showContextMenu(ShowMenuEvent event) {
        textPanelContextMenu.show(event.getMouseEvent());
    }

    @Override
    public void saveState() {
        Chapter currentChapter = presenter.getCurrentChapter();
        if (currentChapter != null) {
            PreferencesService.put(OPENED_CHAPTER_TRANSLATION, currentChapter.getTranslationName());
            PreferencesService.put(OPENED_CHAPTER_NAME, currentChapter.getBookLabel().name());
            PreferencesService.put(OPENED_CHAPTER_NUMBER, currentChapter.getNumber());
        }
    }

    private void initTabbedPaneIfNecessary() {
        if (tabbedPane == null) {
            tabbedPane = (JTabbedPane) getParent();
        }
    }

    private TextPanel createNewTab() {
        TextArea newTextArea = textArea.copy();
        TextPanel newTextPanel = new TextPanel(presenter,
                verseComparingPopup,
                dictionaryPopup,
                closeTabListener,
                textPanelContextMenu,
                searchInputField.copy(),
                newTextArea,
                messageBus);
        newTextPanel.tabId = UUID.randomUUID();
        newTextPanel.setVisible(true);

        messageBus.subscribe(newTextPanel);
        messageBus.subscribe(newTextArea);
        return newTextPanel;
    }

    private void setupNewTab(JTabbedPane tabbedPane, TextPanel newTab) {
        int tabCount = tabbedPane.getTabCount();
        tabbedPane.insertTab("", null, newTab, null, tabCount);
        ImageIcon icon = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("close.png")));
        JPanel title = createTitle(icon, newTab);
        tabbedPane.setTabComponentAt(tabCount, title);
        messageBus.subscribe(newTab);
    }

    private void setText(TextChangedEvent event) {
        textArea.setText(event.getText());
    }

    private void setTitle(JTabbedPane tabbedPane, String chapterString) {
        int selectedIndex = tabbedPane.getSelectedIndex();
        JPanel tabComponent = (JPanel) tabbedPane.getTabComponentAt(selectedIndex);
        JLabel titleLabel = (JLabel) tabComponent.getComponent(0);
        titleLabel.setForeground(presenter.getTabForegroundColor());
        titleLabel.setText(chapterString);
    }

    private void setTooltip(JTabbedPane tabbedPane, String chapterString) {
        tabbedPane.setToolTipTextAt(tabbedPane.getSelectedIndex(), chapterString);
    }

    private JPanel createTitle(ImageIcon icon, TextPanel newTab) {
        JPanel title = new JPanel();
        title.setOpaque(false);
        title.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 2));
        JLabel titleLabel = new JLabel();
        titleLabel.setHorizontalTextPosition(SwingConstants.LEFT);
        titleLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
        titleLabel.setOpaque(false);
        title.add(titleLabel);
        title.add(createCloseButton(icon, newTab));
        return title;
    }

    private JButton createCloseButton(ImageIcon icon, TextPanel newTab) {
        JButton closeButton = new JButton(icon);
        closeButton.setActionCommand(newTab.tabId.toString());
        closeButton.setVerticalAlignment(SwingConstants.TOP);
        closeButton.setPreferredSize(new Dimension(icon.getIconWidth() - 2, icon.getIconHeight() - 2));
        closeButton.setOpaque(false);
        closeButton.setContentAreaFilled(false);
        closeButton.setBorder(null);
        closeButton.addActionListener(closeTabListener);
        return closeButton;
    }
}
