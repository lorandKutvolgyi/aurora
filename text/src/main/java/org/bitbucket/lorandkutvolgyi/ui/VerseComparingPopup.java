/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.VerseComparingPopupActionListener;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.model.VerseComparingEvent;
import org.bitbucket.lorandkutvolgyi.presenter.VerseComparingPopupPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Objects;

public class VerseComparingPopup extends JDialog {

    private final transient ActionListener actionListener;
    private final transient VerseComparingPopupPresenter presenter;
    private JEditorPane textArea;

    public VerseComparingPopup(ActionListener actionListener, VerseComparingPopupPresenter presenter) {
        this.actionListener = actionListener;
        this.presenter = presenter;

        setSize(new Dimension(600, 600));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - getWidth()) / 2, (screenSize.height - getHeight()) / 3);
        setLayout(new BorderLayout());
        setModalityType(ModalityType.APPLICATION_MODAL);
        setUndecorated(true);
        getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
        setIconImage(Toolkit.getDefaultToolkit().createImage(ClassLoader.getSystemResource("logo.png")));

        add(new JScrollPane(createPanel()));
        add(createToolBar(), BorderLayout.SOUTH);
        setVisible(false);
    }

    @Subscribe
    public void show(VerseComparingEvent event) {
        List<Verse> verses = event.getVerses();
        setPopupTitle(verses);
        setPopupText(verses);
        setVisible(true);
    }

    private Component createPanel() {
        textArea = new JEditorPane();
        textArea.setContentType("text/html");
        textArea.setPreferredSize(getParent().getSize());
        textArea.setEditable(false);
        textArea.addKeyListener(getEscapeListener());
        return textArea;
    }

    private JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(createButton("previous.png", VerseComparingPopupActionListener.PREVIOUS));
        toolBar.addSeparator(new Dimension(30, 0));
        toolBar.add(createButton("next.png", VerseComparingPopupActionListener.NEXT));
        toolBar.add(Box.createHorizontalGlue());
        return toolBar;
    }

    private void setPopupTitle(List<Verse> verses) {
        setTitle(presenter.getTitle(verses));
    }

    private void setPopupText(List<Verse> verses) {
        textArea.setText(getText(verses));
        textArea.setCaretPosition(0);
    }

    private JButton createButton(String iconUrl, String actionCommand) {
        JButton button = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(iconUrl))));
        button.setActionCommand(actionCommand);
        button.addActionListener(actionListener);
        button.addKeyListener(getEscapeListener());
        return button;
    }

    private KeyAdapter getEscapeListener() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    VerseComparingPopup.this.dispose();
                }
            }
        };
    }

    private String getText(List<Verse> verses) {
        return presenter.getText(verses);
    }
}
