/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.controller.CrossReferencesController;

import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class CrossReferencesListener implements HyperlinkListener {

    private final CrossReferencesController controller;

    public CrossReferencesListener(CrossReferencesController controller) {
        this.controller = controller;
    }

    @Loggable
    @Override
    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType())) {
            if (event.getInputEvent().isControlDown()) {
                controller.clickedWithCtrl(event.getDescription());
            } else {
                controller.clicked(event.getDescription());
            }
        }
    }
}
