/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.controller.VerseComparingPopupActionListener;
import org.bitbucket.lorandkutvolgyi.model.DictionaryPopupContentReadyEvent;
import org.bitbucket.lorandkutvolgyi.presenter.DictionaryPopupPresenter;
import org.bitbucket.lorandkutvolgyi.service.DictionaryService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Objects;
import java.util.ResourceBundle;

public class DictionaryPopup extends JDialog {

    private static final String MIME_TYPE = "text/html; charset=UTF-8";
    private final DictionaryPopupPresenter presenter;
    private final DictionaryService service;
    private final ResourceBundle messagesBundle;
    private JEditorPane textArea;
    private JEditorPane abbreviationsTextArea;

    public DictionaryPopup(DictionaryPopupPresenter presenter, DictionaryService service, ResourceBundle messagesBundle) {
        this.presenter = presenter;
        this.service = service;
        this.messagesBundle = messagesBundle;

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        setSize(new Dimension(600, 600));
        setLocation((screenSize.width - getWidth()) / 2, (screenSize.height - getHeight()) / 3);
        setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setUndecorated(true);
        getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
        setIconImage(Toolkit.getDefaultToolkit().createImage(ClassLoader.getSystemResource("logo.png")));
        setLayout(new BorderLayout());

        JPanel dictionaryPanel = new JPanel();
        dictionaryPanel.setLayout(new BorderLayout());
        dictionaryPanel.add(new JScrollPane(createPanel()));
        dictionaryPanel.add(createToolBar(), BorderLayout.SOUTH);

        if (presenter.hasAbbreviations()) {
            addTabbedPaneWithTwoTabs(messagesBundle, dictionaryPanel);
        } else {
            addOnlyDictionaryPanel(dictionaryPanel);
        }
        setVisible(false);
    }

    private void addTabbedPaneWithTwoTabs(ResourceBundle messagesBundle, JPanel dictionaryPanel) {
        JTabbedPane tabbedPane = new JTabbedPane();
        add(tabbedPane);
        JPanel abbreviationsPanel = new JPanel();
        abbreviationsPanel.setLayout(new BorderLayout());
        abbreviationsPanel.add(new JScrollPane(createAbbreviationsPanel()));
        tabbedPane.addTab(messagesBundle.getString("DICTIONARY"), dictionaryPanel);
        tabbedPane.addTab(messagesBundle.getString("ABBREVIATIONS"), abbreviationsPanel);
    }

    private void addOnlyDictionaryPanel(JPanel dictionaryPanel) {
        add(dictionaryPanel);
    }

    @Subscribe
    public void show(DictionaryPopupContentReadyEvent event) {
        textArea.setText(presenter.getContent());
        textArea.setCaretPosition(0);
        if (abbreviationsTextArea != null) {
            abbreviationsTextArea.setText(presenter.getAbbreviations());
            abbreviationsTextArea.setCaretPosition(0);
        }
        setTitle(presenter.getTitle());
        setVisible(true);
    }

    private Component createPanel() {
        textArea = new JEditorPane();
        textArea.setContentType(MIME_TYPE);
        textArea.setPreferredSize(getParent().getSize());
        textArea.setEditable(false);
        textArea.addKeyListener(getEscapeListener());
        return textArea;
    }

    private Component createAbbreviationsPanel() {
        abbreviationsTextArea = new JEditorPane();
        abbreviationsTextArea.setContentType(MIME_TYPE);
        abbreviationsTextArea.setPreferredSize(getParent().getSize());
        abbreviationsTextArea.setEditable(false);
        abbreviationsTextArea.addKeyListener(getEscapeListener());
        return abbreviationsTextArea;
    }

    private JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(createButton("previous.png", VerseComparingPopupActionListener.PREVIOUS));
        toolBar.addSeparator(new Dimension(30, 0));
        toolBar.add(createButton("next.png", VerseComparingPopupActionListener.NEXT));
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(createInfoLabel());
        return toolBar;
    }

    private KeyAdapter getEscapeListener() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    DictionaryPopup.this.dispose();
                }
            }
        };
    }

    private JButton createButton(String iconUrl, String actionCommand) {
        JButton button = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(iconUrl))));
        button.addKeyListener(getEscapeListener());
        button.setActionCommand(actionCommand);
        button.addActionListener(event -> {
            if (actionCommand.equals(VerseComparingPopupActionListener.NEXT)) {
                service.showNextEntriesIfExist(getTitle().split(":")[1]);
            } else {
                assert actionCommand.equals(VerseComparingPopupActionListener.PREVIOUS);
                service.showPreviousEntriesIfExist(getTitle().split(":")[1]);
            }
        });
        return button;
    }

    private JLabel createInfoLabel() {
        JLabel info = new JLabel(new ImageIcon(Objects.requireNonNull(MainWindow.class.getResource("/info.png"))));
        info.setToolTipText(messagesBundle.getString("DICTIONARY_INFO"));
        return info;
    }
}
