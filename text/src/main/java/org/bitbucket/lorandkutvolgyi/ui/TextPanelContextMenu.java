/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.presenter.TextPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.BookmarkPopupManager;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import javax.swing.*;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

public class TextPanelContextMenu extends JPopupMenu {

    private final TranslationService translationService;
    private final TextPanelPresenter textPanelPresenter;
    private final BookmarkPopupManager bookmarkPopupManager;
    private JMenuItem comparing;
    private JMenuItem dictionary;
    private JMenuItem bookmark;
    private Chapter chapter;
    private int verseNumber;

    public TextPanelContextMenu(ResourceBundle messagesBundle, ActionListener verseComparingListener, BookmarkPopupManager bookmarkPopupManager,
                                TranslationService translationService, ActionListener dictionaryMenuItemListener,
                                TextPanelPresenter textPanelPresenter) {
        this.translationService = translationService;
        this.textPanelPresenter = textPanelPresenter;
        this.bookmarkPopupManager = bookmarkPopupManager;

        createMenuItems(messagesBundle, verseComparingListener, dictionaryMenuItemListener);
        addItemsToMenu();
    }

    public void show(MouseEvent event) {
        String verseNumberAsString = getVerseNumber(event);

        setComparingMenuItem(verseNumberAsString);
        setBookmarkMenuItem(verseNumberAsString);
        setDictionaryMeuItem(verseNumberAsString);

        show(event.getComponent(), event.getX(), event.getY());
    }

    private void createMenuItems(ResourceBundle messagesBundle, ActionListener verseComparingListener, ActionListener dictionaryMenuItemListener) {
        comparing = createMenuItem(messagesBundle.getString("COMPARE"), verseComparingListener);
        bookmark = createMenuItem(messagesBundle.getString("BOOKMARK"), getBookmarkListener());
        dictionary = createMenuItem(messagesBundle.getString("DICTIONARY"), dictionaryMenuItemListener);
    }

    private void addItemsToMenu() {
        add(comparing);
        add(bookmark);
        add(dictionary);
    }

    private String getVerseNumber(MouseEvent event) {
        JEditorPane editor = (JEditorPane) event.getSource();
        HTMLDocument document = (HTMLDocument) editor.getDocument();
        int position = editor.getUI().viewToModel2D(editor, event.getPoint(), new Position.Bias[]{null});
        Element divElement = document.getCharacterElement(position).getParentElement().getParentElement();
        return (String) divElement.getAttributes().getAttribute(HTML.Attribute.ID);
    }

    private void setComparingMenuItem(String verseNumberAsString) {
        comparing.setActionCommand(verseNumberAsString);
    }

    private void setBookmarkMenuItem(String verseNumberAsString) {
        verseNumber = Integer.parseInt(verseNumberAsString);
        
        chapter = new Chapter();
        chapter.setTranslationName(translationService.getCurrentChaptersTranslationName());
        chapter.setBookName(translationService.getCurrentChapter().getBookName());
        chapter.setNumber(translationService.getCurrentChapter().getNumber());
    }

    private void setDictionaryMeuItem(String verseNumberAsString) {
        if (textPanelPresenter.isDictionaryMenuItemVisible()) {
            dictionary.setVisible(true);
            dictionary.setActionCommand(verseNumberAsString);
        } else {
            dictionary.setVisible(false);
        }
    }

    private JMenuItem createMenuItem(String text, ActionListener actionListener) {
        JMenuItem item = new JMenuItem(text);
        item.addActionListener(actionListener);
        return item;
    }

    private ActionListener getBookmarkListener() {
        return actionEvent -> {
            bookmarkPopupManager.setDefaults(chapter.getTranslationName(), chapter.getBookName(), chapter.getNumber(), verseNumber);
            bookmarkPopupManager.showPopup();
        };
    }
}
