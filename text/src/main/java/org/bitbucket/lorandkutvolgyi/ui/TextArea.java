/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.model.SelectionChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.TextRanges;

import javax.swing.*;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.MouseListener;

import static java.awt.GridBagConstraints.BOTH;

public class TextArea {

    private final JEditorPane jEditorPane = new JEditorPane();
    private final GridBagConstraints constraints;
    private final MouseListener textAreaMouseListener;
    private final HyperlinkListener crossReferenceListener;
    private final MessageBus messageBus;

    public TextArea(MouseListener textAreaMouseListener, HyperlinkListener crossReferenceListener, MessageBus messageBus) {
        this.textAreaMouseListener = textAreaMouseListener;
        this.crossReferenceListener = crossReferenceListener;
        this.messageBus = messageBus;

        constraints = new GridBagConstraints();
        constraints.fill = BOTH;
        constraints.insets = new Insets(0, 5, 5, 5);
        constraints.weightx = 1.0;
        constraints.weighty = 0.95;
        constraints.gridwidth = 2;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.anchor = GridBagConstraints.PAGE_END;

        jEditorPane.setContentType("text/html");
        jEditorPane.setEditable(false);
        jEditorPane.setMinimumSize(new Dimension(20, 40));
        jEditorPane.addMouseListener(textAreaMouseListener);
        jEditorPane.addHyperlinkListener(crossReferenceListener);
    }

    @Subscribe
    public void selectText(SelectionChangedEvent event) {
        Highlighter highlighter = jEditorPane.getHighlighter();
        highlighter.removeAllHighlights();
        TextRanges newValue = event.getTextRanges();
        if (newValue.getLength() > 1) {
            highlightText(highlighter, newValue);
            int scrollPosition = newValue.getStartPositions().isEmpty() ? 0 : newValue.getStartPositions().get(0);
            jEditorPane.setCaretPosition(scrollPosition);
        }
    }

    public TextArea copy() {
        return new TextArea(textAreaMouseListener, crossReferenceListener, messageBus);
    }

    public void setParent(JComponent parent) {
        parent.add(new JScrollPane(jEditorPane), constraints);
    }

    public void setText(String text) {
        jEditorPane.setText(text);
        jEditorPane.setCaretPosition(0);
    }

    private void highlightText(Highlighter highlighter, TextRanges newValue) {
        newValue.getStartPositions().forEach(position -> {
            if (!newValue.getStartPositions().isEmpty()) {
                try {
                    highlighter.addHighlight(position + 1, position + 1 + newValue.getLength(), DefaultHighlighter.DefaultPainter);
                } catch (BadLocationException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }
}
