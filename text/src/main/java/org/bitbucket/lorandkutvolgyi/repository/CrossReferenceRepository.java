/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class CrossReferenceRepository {

    private final Map<String, Map<Integer, List<String>>> unorderedReferences = new HashMap<>();
    private final Map<String, List<String>> references = new HashMap<>();

    public CrossReferenceRepository(String workingDirectory) {
        BufferedReader bufferedReader = getReader(workingDirectory);

        populateUnorderedReferences(bufferedReader);
        populateReferencesFromUnorderedReferencesBySort();
    }

    public List<String> getCrossReferencesFor(String from) {
        return references.get(from);
    }

    @SneakyThrows(FileNotFoundException.class)
    private BufferedReader getReader(String workingDirectory) {
        String filePath = workingDirectory + "/.aurora/crossreferences/crossreferences.csv";
        return new BufferedReader(new FileReader(filePath));
    }

    private void populateUnorderedReferences(BufferedReader bufferedReader) {
        bufferedReader.lines().forEach(this::process);
    }

    private void populateReferencesFromUnorderedReferencesBySort() {
        unorderedReferences.forEach((k, v) -> {
            v.values().forEach(Collections::sort);
            references.put(k, v.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
        });
    }

    private void process(String line) {
        String[] columns = line.split("\t");
        String from = columns[0];
        String to = columns[1];
        String priority = columns[2];

        Map<Integer, List<String>> referencesForVerseByPriority = getReferencesForVersesByPriority(from);
        List<String> referencesForPriority = getReferencesForPriority(priority, referencesForVerseByPriority);
        referencesForPriority.add(to);

        unorderedReferences.put(from, referencesForVerseByPriority);
    }

    private List<String> getReferencesForPriority(String priority, Map<Integer, List<String>> referencesForVerseByPriority) {
        return referencesForVerseByPriority.computeIfAbsent(Integer.valueOf(priority), pr -> new ArrayList<>());
    }

    private Map<Integer, List<String>> getReferencesForVersesByPriority(String from) {
        Map<Integer, List<String>> referencesForVerseByPriority = unorderedReferences.get(from);
        if (referencesForVerseByPriority == null) {
            referencesForVerseByPriority = new TreeMap<>(Collections.reverseOrder());
        }
        return referencesForVerseByPriority;
    }
}
