/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DictionaryRepository {

    private final Map<String, String> dictionary = new HashMap<>();
    private final String abbreviations;

    public DictionaryRepository(String workingDirectory, LocaleService localeService) {
        String selectedLanguage = localeService.getSelectedLanguage();
        populateDictionary(getReader(workingDirectory, selectedLanguage));
        abbreviations = readAbbreviations(workingDirectory, selectedLanguage).orElse(null);
    }

    public String getDefinition(String strongNumber) {
        String strongNumberPrefix = "SG";
        String result = dictionary.get(strongNumberPrefix + strongNumber);
        return result == null ? "" : result;
    }

    public String getAbbreviations() {
        return abbreviations;
    }

    @SneakyThrows(IOException.class)
    private BufferedReader getReader(String workingDirectory, String language) {
        String filePath = String.format("%s/.aurora/dictionary/greek_%s_dictionary.txt", workingDirectory, language);
        return new BufferedReader(new FileReader(filePath, StandardCharsets.UTF_8));
    }

    private void populateDictionary(BufferedReader bufferedReader) {
        bufferedReader.lines().forEach(this::process);
    }

    private Optional<String> readAbbreviations(String workingDirectory, String selectedLanguage) {
        String filePath = String.format("%s/.aurora/dictionary/greek_%s_dictionary_abbreviations.html", workingDirectory, selectedLanguage);
        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
            return Optional.of(String.join("\n", lines));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private void process(String line) {
        String[] columns = line.split("\t", 2);
        String strongNumber = columns[0];
        String definition = columns[1];

        dictionary.put(strongNumber, definition);
    }
}
