/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.DictionaryShowingEvent;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.DictionaryRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.List;

public class DictionaryService {

    private final TranslationService translationService;
    private final DictionaryRepository repository;
    private final MessageBus messageBus;

    public DictionaryService(TranslationService translationService, DictionaryRepository repository, MessageBus messageBus) {
        this.translationService = translationService;
        this.repository = repository;
        this.messageBus = messageBus;
    }

    public void showEntries(String verseNumber) {
        Chapter currentChapter = translationService.getCurrentChapter();
        if (currentChapter.getVerses().size() > Integer.parseInt(verseNumber) - 1) {
            Verse verse = getVerse(verseNumber, currentChapter);
            List<String> definitions = getDefinitions(verse);
            messageBus.publish(new DictionaryShowingEvent(verse, definitions));
        }
    }

    public void showNextEntriesIfExist(String verseNumber) {
        if (verseNumber != null && !verseNumber.isEmpty()) {
            int nextIndex = Integer.parseInt(verseNumber) + 1;
            Chapter currentChapter = translationService.getCurrentChapter();
            if (currentChapter.getVerses().size() > nextIndex - 1) {
                showNextEntries(nextIndex, currentChapter);
            }
        }
    }

    public String getAbbreviations() {
        return repository.getAbbreviations();
    }

    private void showNextEntries(int nextIndex, Chapter currentChapter) {
        Verse verse = getNextNonEmptyVerse(nextIndex, currentChapter);
        List<String> definitions = getDefinitions(verse);
        messageBus.publish(new DictionaryShowingEvent(verse, definitions));
    }

    private Verse getNextNonEmptyVerse(int nextIndex, Chapter currentChapter) {
        Verse verse = getVerse(String.valueOf(nextIndex), currentChapter);
        while (verse.getText().isBlank()) {
            verse = getVerse(String.valueOf(++nextIndex), currentChapter);
        }
        return verse;
    }

    public void showPreviousEntriesIfExist(String verseNumber) {
        if (verseNumber != null && !verseNumber.isEmpty() && !verseNumber.equals("1")) {
            int previousIndex = Integer.parseInt(verseNumber) - 1;
            Chapter currentChapter = translationService.getCurrentChapter();
            if (previousIndex - 1 >= 0) {
                showPreviousEntries(previousIndex, currentChapter);
            }
        }
    }

    private void showPreviousEntries(int previousIndex, Chapter currentChapter) {
        Verse verse = getPreviousNonEmptyVerse(previousIndex, currentChapter);
        if (!verse.getText().isBlank()) {
            List<String> definitions = getDefinitions(verse);
            messageBus.publish(new DictionaryShowingEvent(verse, definitions));
        }
    }

    private Verse getPreviousNonEmptyVerse(int previousIndex, Chapter currentChapter) {
        Verse verse = getVerse(String.valueOf(previousIndex), currentChapter);
        while (verse.getText().isBlank() && previousIndex > 1) {
            verse = getVerse(String.valueOf(--previousIndex), currentChapter);
        }
        return verse;
    }

    private Verse getVerse(String verseNumber, Chapter currentChapter) {
        return currentChapter.getVerses().get(Integer.parseInt(verseNumber) - 1);
    }

    private List<String> getDefinitions(Verse verse) {
        Document doc = Jsoup.parse(verse.getText());
        Elements spans = doc.select("span.strong");
        return spans.stream()
                .map(span -> span.text().replaceAll("[,.;()\\[\\]–]+", "").trim()
                        + " " + repository.getDefinition(span.attr("num")))
                .toList();
    }
}
