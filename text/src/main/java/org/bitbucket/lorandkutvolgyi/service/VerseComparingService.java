/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class VerseComparingService {

    private final TranslationService translationService;
    private final MessageBus messageBus;
    private List<Verse> versesToCompare;

    public VerseComparingService(TranslationService translationService, MessageBus messageBus) {
        this.translationService = translationService;
        this.messageBus = messageBus;
    }

    public void setNextVersesToCompare() {
        int verseNumber = getNextVerses();
        if (translationService.getCurrentChapter().getVerses().size() >= verseNumber) {
            setVersesToCompare(String.valueOf(verseNumber));
        }
    }

    public void setPreviousVersesToCompare() {
        int verseNumber = getPreviousVerses();
        if (verseNumber > 0) {
            setVersesToCompare(String.valueOf(verseNumber));
        }
    }

    public void setVersesToCompare(String actionCommand) {
        if (actionCommand.isEmpty()) {
            return;
        }
        Chapter currentChapter = translationService.getCurrentChapter();
        BookLabel bookLabel = translationService.getBook(currentChapter.getTranslationName(), currentChapter.getBookName()).getLabel();
        int chapterNumber = currentChapter.getNumber();
        int verseNumber = Integer.parseInt(actionCommand.replaceAll("[\\u202A\\u202B\\u202C]", ""));
        versesToCompare = getVersesInAllTranslations(bookLabel, chapterNumber, verseNumber);
        messageBus.publish(new VerseComparingEvent(versesToCompare));
    }

    private Integer getNextVerses() {
        return versesToCompare.stream()
                .filter(Objects::nonNull)
                .filter(verse -> verse.getTranslationName().equals(translationService.getCurrentChapter().getTranslationName()))
                .map(verse -> verse.getNumber() + 1)
                .findAny()
                .orElse(Integer.MAX_VALUE);
    }

    private Integer getPreviousVerses() {
        return versesToCompare.stream()
                .filter(Objects::nonNull)
                .filter(verse -> verse.getTranslationName().equals(translationService.getCurrentChapter().getTranslationName()))
                .map(verse -> verse.getNumber() - 1)
                .findAny()
                .orElse(0);
    }

    private List<Verse> getVersesInAllTranslations(BookLabel bookLabel, int chapterNumber, int verseNumber) {
        return translationService.getDownloadedTranslations().stream()
                .map(translation -> getVerseIfExisting(bookLabel, chapterNumber, verseNumber, translation))
                .collect(Collectors.toList());
    }

    private Verse getVerseIfExisting(BookLabel bookLabel, int chapterNumber, int verseNumber, Translation translation) {
        Optional<Book> bookOptional = translation.getBooks().stream().filter(book -> book.getLabel() == bookLabel).findAny();
        return bookOptional
                .filter(book -> isVerseExisting(chapterNumber, verseNumber, book))
                .map(book -> getVerse(chapterNumber, verseNumber, book))
                .orElse(null);
    }

    private boolean isVerseExisting(int chapterNumber, int verseNumber, Book book) {
        List<Chapter> chapters = book.getChapters();
        return chapters.size() > chapterNumber - 1 && chapters.get(chapterNumber - 1).getVerses().size() > verseNumber - 1;
    }

    private Verse getVerse(int chapterNumber, int verseNumber, Book book) {
        return book.getChapters().get(chapterNumber - 1).getVerses().get(verseNumber - 1);
    }
}
