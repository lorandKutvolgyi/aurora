/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.presenter.TextPanelPresenter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

public class TextPanelService {

    private final TextPanelPresenter textPanelPresenter;

    public TextPanelService(TextPanelPresenter textPanelPresenter) {
        this.textPanelPresenter = textPanelPresenter;
    }

    public void openNewTab() {
        textPanelPresenter.openNewTab();
    }

    public void searchText(String text) {
        List<Integer> startPositions = new ArrayList<>();
        String textPanelText = textPanelPresenter.getText();
        if (textPanelText != null && withoutHtmlTags(textPanelText).contains(text)) {
            findStartPositions(text, startPositions);
        }
        textPanelPresenter.setSelectionPosition(startPositions, text.length());
    }

    private String withoutHtmlTags(String textPanelText) {
        Document document = Jsoup.parse(textPanelText);
        return document.body().text();
    }

    private void findStartPositions(String text, List<Integer> startPositions) {
        Document document = Jsoup.parse(textPanelPresenter.getText());
        int start = 0;
        do {
            start = text.isEmpty() ? -1 : document.body().text().indexOf(text, start);
            if (start >= 0) {
                startPositions.add(start);
                start += text.length();
            }
        } while (start >= 0);
    }
}
