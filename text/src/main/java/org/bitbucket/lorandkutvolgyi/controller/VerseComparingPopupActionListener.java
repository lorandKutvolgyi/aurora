/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.VerseComparingService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VerseComparingPopupActionListener implements ActionListener {

    public static final String PREVIOUS = "previous";
    public static final String NEXT = "next";

    private final VerseComparingService verseComparingService;

    public VerseComparingPopupActionListener(VerseComparingService verseComparingService) {
        this.verseComparingService = verseComparingService;
    }

    @Loggable
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals(PREVIOUS)) {
            verseComparingService.setPreviousVersesToCompare();
        } else if (actionEvent.getActionCommand().equals(NEXT)) {
            verseComparingService.setNextVersesToCompare();
        }
    }
}
