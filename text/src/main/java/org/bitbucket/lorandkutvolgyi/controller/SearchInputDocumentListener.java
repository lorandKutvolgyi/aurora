/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class SearchInputDocumentListener implements DocumentListener {

    private final TextPanelService textPanelService;

    public SearchInputDocumentListener(TextPanelService textPanelService) {
        this.textPanelService = textPanelService;
    }

    @Loggable
    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        update(documentEvent);
    }

    @Loggable
    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        update(documentEvent);
    }

    @Loggable
    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        // Do nothing
    }

    private void update(DocumentEvent documentEvent) {
        try {
            Document document = documentEvent.getDocument();
            String text = document.getText(0, document.getLength());
            textPanelService.searchText(text);
        } catch (BadLocationException exception) {
            throw new IllegalArgumentException(exception);
        }
    }
}
