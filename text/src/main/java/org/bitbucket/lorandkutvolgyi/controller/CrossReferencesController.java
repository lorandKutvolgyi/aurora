/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.ResourceBundle;

public class CrossReferencesController {

    private final TranslationService translationService;
    private final ResourceBundle messagesBundle;
    private final TextPanelService textPanelService;

    public CrossReferencesController(TranslationService translationService, ResourceBundle messagesBundle,
                                     TextPanelService textPanelService) {
        this.translationService = translationService;
        this.messagesBundle = messagesBundle;
        this.textPanelService = textPanelService;
    }

    public void clicked(String linkText) {
        openNewChapter(linkText);
    }

    public void clickedWithCtrl(String linkText) {
        openNewTab();
        openNewChapter(linkText);
    }

    private void openNewTab() {
        textPanelService.openNewTab();
    }

    private void openNewChapter(String linkText) {
        String[] bookAndChapter = linkText.split(" ");
        translationService.openNewChapter(BookLabel.valueOf(messagesBundle.getString(bookAndChapter[0])),
                Integer.parseInt(bookAndChapter[1]));
    }
}
