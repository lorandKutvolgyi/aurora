/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class VerseComparingPopupPresenter {

    private static final String ORIGINAL = "ORIGINAL";
    private final TranslationService service;
    private final ResourceBundle messagesBundle;

    public VerseComparingPopupPresenter(TranslationService service, ResourceBundle messagesBundle) {
        this.service = service;
        this.messagesBundle = messagesBundle;
    }

    public String getText(List<Verse> verses) {
        String base = service.getCurrentChaptersTranslationName();
        return verses.stream()
                .filter(Objects::nonNull)
                .sorted((verse, other) -> compare(base, verse, other))
                .map(verse -> "<p><b>"
                        + (verse.getTranslationName().equals(ORIGINAL) ? messagesBundle.getString(ORIGINAL) : verse.getTranslationName())
                        + "</b><br><p>" + verse.getText())
                .collect(Collectors.joining("<br>"));
    }

    public String getTitle(List<Verse> verses) {
        return verses.stream()
                .filter(Objects::nonNull)
                .filter(verse -> verse.getTranslationName().equals(service.getCurrentChaptersTranslationName()))
                .findAny()
                .map(verse -> verse.getBookName() + " " + verse.getChapterNumber() + ": " + verse.getNumber())
                .orElseThrow(IllegalArgumentException::new);
    }

    private int compare(String base, Verse verse, Verse other) {
        if (verse.getTranslationName().equals(base)) {
            return -1;
        }
        if (verse.getTranslationName().equals(ORIGINAL) && !other.getTranslationName().equals(base)) {
            return -1;
        }
        if (other.getTranslationName().equals(base)) {
            return 1;
        }
        if (other.getTranslationName().equals(ORIGINAL) && !verse.getTranslationName().equals(base)) {
            return 1;
        }
        return verse.getTranslationName().compareTo(other.getTranslationName());
    }
}
