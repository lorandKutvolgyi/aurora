/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.CrossReferenceRepository;

import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class CrossReferencesDecorator implements TextDecorator {

    private final TextDecorator textDecorator;
    private final CrossReferenceRepository repository;
    private final ResourceBundle messagesBundle;

    public CrossReferencesDecorator(TextDecorator textDecorator, CrossReferenceRepository repository,
                                    ResourceBundle messagesBundle) {
        this.textDecorator = textDecorator;
        this.repository = repository;
        this.messagesBundle = messagesBundle;
    }

    @Override
    public String getText(String text, Verse verse) {
        String keyForReferences = verse.getBookLabel().name() + "." + verse.getChapterNumber() + "." + verse.getNumber();
        String crossReferencesText = getCrossReferencesTagOrEmptyString(verse, keyForReferences);
        if (textDecorator == null) {
            return text + crossReferencesText;
        }
        return textDecorator.getText(text + crossReferencesText, verse);
    }

    private String getCrossReferencesTagOrEmptyString(Verse verse, String keyForReferences) {
        String links = getLinksIfReferencesExist(keyForReferences);
        if (links.isEmpty()) {
            return "";
        }
        return getCrossReferencesTag(verse, links);
    }

    private String getLinksIfReferencesExist(String keyForReferences) {
        List<String> crossReferences = repository.getCrossReferencesFor(keyForReferences);
        if (crossReferences == null) {
            return "";
        }
        return getLinks(crossReferences);
    }

    private String getCrossReferencesTag(Verse verse, String links) {
        String crossReferencesText = "<div disabled class=crossref>" + links + "</div>";

        if (verse.getTranslationName().equals("ORIGINAL") && verse.getBookLabel().ordinal() < 39) {
            crossReferencesText = "\u202B" + crossReferencesText + "\u202C";
        }
        return crossReferencesText;
    }

    private String getLinks(List<String> crossReferences) {
        return crossReferences.stream().map(ref -> {
            if (ref.contains("-")) {
                String[] range = ref.split("-");
                String start = getTranslated(range[0]);
                String end = getTranslated(range[1]);
                return "<a href='" + getBookAndChapter(start) + "'>" + start + "-" + end + "</a>";
            } else {
                String translated = getTranslated(ref);
                return "<a href='" + getBookAndChapter(translated) + "'>" + translated + "</a>";
            }
        }).collect(Collectors.joining(" "));
    }

    private String getTranslated(String ref) {
        String[] parts = ref.split("\\.");
        return messagesBundle.getString(parts[0]) + "." + parts[1] + "." + parts[2];
    }

    private String getBookAndChapter(String bookChapterVerse) {
        String[] parts = bookChapterVerse.split("\\.");
        return parts[0] + " " + parts[1];
    }
}
