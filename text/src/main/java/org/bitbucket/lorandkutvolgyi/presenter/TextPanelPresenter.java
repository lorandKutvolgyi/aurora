/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.HiddenCrossReferencesStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.HiddenVerseNumberStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.*;
import org.bitbucket.lorandkutvolgyi.repository.CrossReferenceRepository;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class TextPanelPresenter {

    private final MessageBus messageBus;
    private final Theme selectedTheme;
    private final CrossReferenceRepository repository;
    private final TranslationService translationService;
    private final ResourceBundle messagesBundle;

    private String text;
    private Chapter currentChapter;
    private boolean hiddenVerseNumber;
    private boolean hiddenCrossReferences;

    public TextPanelPresenter(MessageBus messageBus, Theme selectedTheme, CrossReferenceRepository repository,
                              ResourceBundle messagesBundle, TranslationService translationService) {
        this.messageBus = messageBus;
        this.selectedTheme = selectedTheme;
        this.repository = repository;
        this.messagesBundle = messagesBundle;
        this.translationService = translationService;

        boolean numberState = PreferencesService.getBooleanOrFalse(PreferencesService.Key.VERSE_NUMBER_STATE);
        setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(numberState));
        boolean crossReferencesState = PreferencesService.getBooleanOrTrue(PreferencesService.Key.CROSS_REFERENCE_STATE);
        setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(crossReferencesState));
    }

    @Subscribe
    public void setVerseNumberHiddenState(HiddenVerseNumberStateChangedEvent event) {
        setHiddenVerseNumber(event.isHidden());
        if (currentChapter != null) {
            setText(getCurrentChaptersTextInHtmlFormat());
        }
    }

    @Subscribe
    public void setCrossReferencesHiddenState(HiddenCrossReferencesStateChangedEvent event) {
        setHiddenCrossReferences(event.isHidden());
        if (currentChapter != null) {
            setText(getCurrentChaptersTextInHtmlFormat());
        }
    }

    public void setSelectionPosition(List<Integer> startPositions, int length) {
        messageBus.publish(new SelectionChangedEvent(TextRanges.builder().startPositions(startPositions).length(length).build()));
    }

    public void showContextMenu(MouseEvent event) {
        messageBus.publish(new ShowMenuEvent(event));
    }

    public void openNewTab() {
        messageBus.publish(NewTabEvent.INSTANCE);
    }

    public Chapter getCurrentChapter() {
        return currentChapter;
    }

    public String getText() {
        return text;
    }

    @Subscribe
    public void setText(CurrentChapterChangingEvent event) {
        this.currentChapter = event.getChapter();
        if (currentChapter != null) {
            setText(getCurrentChaptersTextInHtmlFormat());
        }
    }

    public void setHiddenVerseNumber(boolean hiddenVerseNumber) {
        this.hiddenVerseNumber = hiddenVerseNumber;
    }

    public void setHiddenCrossReferences(boolean hiddenCrossReferences) {
        this.hiddenCrossReferences = hiddenCrossReferences;
    }

    public Color getTabForegroundColor() {
        return selectedTheme.getTabForegroundColor();
    }

    public Color getMainBackgroundColor() {
        return selectedTheme.getMainBackgroundColor();
    }

    public boolean isDictionaryMenuItemVisible() {
        return "ORIGINAL".equals(translationService.getCurrentChaptersTranslationName())
                && translationService.getCurrentChapter().getBookLabel().ordinal() >= BookLabel.MATTHEW.ordinal();
    }

    private void setText(String text) {
        this.text = text;
        messageBus.publish(new TextChangedEvent(text));
    }

    private String getCurrentChaptersTextInHtmlFormat() {
        return currentChapter.getVerses().stream()
                .map(this::getTextWithNecessaryDecoration)
                .collect(Collectors.joining(""));
    }

    private String getTextWithNecessaryDecoration(Verse verse) {
        if (hiddenVerseNumber && hiddenCrossReferences) {
            return getNonDecoratedText(verse);
        } else if (!hiddenVerseNumber && hiddenCrossReferences) {
            return getTextWithVerseNumbers(verse);
        } else if (hiddenVerseNumber) {
            return getTextWithCrossReferences(verse);
        }
        return getTextWithVerseNumbersAndCrossReferences(verse);
    }

    private String getNonDecoratedText(Verse verse) {
        TextProvider textProvider = new TextProvider(null, verse);
        return textProvider.getText();
    }

    private String getTextWithVerseNumbers(Verse verse) {
        TextProvider textProvider = new TextProvider(new VerseNumberDecorator(null), verse);
        return textProvider.getText();
    }

    private String getTextWithCrossReferences(Verse verse) {
        TextProvider textProvider = new TextProvider(new CrossReferencesDecorator(null, repository, messagesBundle), verse);
        return textProvider.getText();
    }

    private String getTextWithVerseNumbersAndCrossReferences(Verse verse) {
        TextProvider textProvider = new TextProvider(new VerseNumberDecorator(new CrossReferencesDecorator(null, repository, messagesBundle)), verse);
        return textProvider.getText();
    }
}
