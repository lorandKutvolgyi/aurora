/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.Verse;

public class VerseNumberDecorator implements TextDecorator {

    private final TextDecorator textDecorator;

    public VerseNumberDecorator(TextDecorator textDecorator) {
        this.textDecorator = textDecorator;
    }

    @Override
    public String getText(String text, Verse verse) {
        String numberText = "<span disabled class=number>" + verse.getNumber() + "</span>";
        if (verse.getTranslationName().equals("ORIGINAL") && verse.getBookLabel().ordinal() < 39) {
            numberText = "<span disabled class=number>\u202B" + verse.getNumber() + "\u202C</span>";
        }
        if (textDecorator == null) {
            return numberText + text;
        }
        return textDecorator.getText(numberText + text, verse);
    }
}
