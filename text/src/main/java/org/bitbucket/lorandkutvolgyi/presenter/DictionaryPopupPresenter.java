/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import lombok.Getter;
import org.bitbucket.lorandkutvolgyi.model.DictionaryPopupContentReadyEvent;
import org.bitbucket.lorandkutvolgyi.model.DictionaryShowingEvent;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.DictionaryService;

import java.util.List;
import java.util.stream.Collectors;

public class DictionaryPopupPresenter {

    private final MessageBus messageBus;
    private final String abbreviations;
    @Getter
    private String content = "";
    @Getter
    private String title = "";
    @Getter
    private String verseNumber = "";

    public DictionaryPopupPresenter(MessageBus messageBus, DictionaryService service) {
        this.messageBus = messageBus;
        this.abbreviations = service.getAbbreviations();
    }

    @Subscribe
    public void setContent(DictionaryShowingEvent event) {
        List<String> entries = event.getDefinitions().stream().map(this::format).collect(Collectors.toList());
        content = String.join("<hr>", entries);
        title = createTitle(event.getVerse());
        verseNumber = String.valueOf(event.getVerse().getNumber());
        messageBus.publish(new DictionaryPopupContentReadyEvent());
    }

    public boolean hasAbbreviations() {
        return abbreviations != null;
    }

    public String getAbbreviations() {
        return abbreviations;
    }

    private String format(String text) {
        String[] wordAndTheRest = text.split(" (?=[0-9]+)", 2);
        String word = "<b>" + wordAndTheRest[0] + "</b>";
        String strongNum = "";
        String definition = "";
        if (wordAndTheRest.length > 1) { // This condition check should be removed when Hebrew dictionary has been introduced
            String[] strongNumAndDefinition = wordAndTheRest[1].split(" ", 2);
            strongNum = "<i>" + strongNumAndDefinition[0] + "</i>";
            definition = strongNumAndDefinition[1].replaceAll("\t", "<br>");
        }
        return word + " " + strongNum + " " + definition;
    }

    private String createTitle(Verse verse) {
        return verse.getBookName() + " " + verse.getChapterNumber() + ":" + verse.getNumber();
    }
}
