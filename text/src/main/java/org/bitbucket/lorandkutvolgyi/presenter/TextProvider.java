/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.Verse;

public class TextProvider {

    private final TextDecorator textDecorator;
    private final Verse verse;
    private final String text;

    public TextProvider(TextDecorator textDecorator, Verse verse) {
        this.textDecorator = textDecorator;
        this.verse = verse;
        this.text = verse.getText().isBlank() ? "" : ("<span class=text>" + verse.getText() + "</span>");
    }

    public String getText() {
        if (text.isBlank()) {
            return "";
        }
        return getNonEmptyText();
    }

    private String getNonEmptyText() {
        String align = "left";
        if (verse.getTranslationName().equals("ORIGINAL") && verse.getBookLabel().ordinal() < 39) {
            align = "right";
        }
        if (textDecorator == null) {
            return "<div align=" + align + " id=" + verse.getNumber() + ">" + text + "</div>";
        }
        return "<div align=" + align + " id=" + verse.getNumber() + ">" + textDecorator.getText(text, verse) + "</div>";
    }
}
