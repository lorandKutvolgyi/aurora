/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.HiddenCrossReferencesStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.HiddenVerseNumberStateChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.*;
import org.bitbucket.lorandkutvolgyi.repository.CrossReferenceRepository;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.PreferencesService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TextPanelPresenterTest {

    private static boolean originalState;
    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private TranslationService translationService;
    private TextPanelPresenter underTest;
    private MessageBus messageBus;
    private CrossReferenceRepository repository;
    private DefaultTheme selectedTheme;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
        originalState = PreferencesService.getBooleanOrFalse(PreferencesService.Key.VERSE_NUMBER_STATE);
        PreferencesService.put(PreferencesService.Key.VERSE_NUMBER_STATE, false);
    }

    @AfterAll
    static void tearDown() {
        PreferencesService.put(PreferencesService.Key.VERSE_NUMBER_STATE, originalState);
    }

    @BeforeEach
    void setUp() {
        messageBus = mock(MessageBus.class);
        repository = mock(CrossReferenceRepository.class);
        translationService = mock(TranslationService.class);
        selectedTheme = new DefaultTheme();
        underTest = new TextPanelPresenter(messageBus, selectedTheme, repository, messagesBundle, translationService);
    }

    @Test
    void openNewTab_shouldPublishNewTabEventToMessageBus() {
        underTest.openNewTab();

        verify(messageBus).publish(any(NewTabEvent.class));
    }

    @Test
    void showContextMenu_shouldPublishShowMenuEventToMessageBus() {
        MouseEvent mouseEvent = createMouseEvent();

        underTest.showContextMenu(mouseEvent);

        verify(messageBus).publish(new ShowMenuEvent(mouseEvent));
    }

    @Test
    void setSelectionPosition_shouldPublishSelectionChangedEventToMessageBus() {
        ArrayList<Integer> startPositions = new ArrayList<>();
        startPositions.add(5);
        startPositions.add(15);
        startPositions.add(25);
        int length = 3;

        underTest.setSelectionPosition(startPositions, length);

        verify(messageBus).publish(new SelectionChangedEvent(TextRanges.builder().startPositions(startPositions).length(length).build()));
    }

    @Test
    void setText_shouldSetTheCurrentChapterAndItsText() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerse(1));
        verses.add(createVerse(2));
        Chapter chapter = new Chapter("NIV", "Matthew", 1, verses, "", new HashMap<>());
        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));
        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(false));

        underTest.setText(new CurrentChapterChangingEvent(chapter));

        assertEquals(chapter, underTest.getCurrentChapter());
        assertEquals(getTexWithNumberInHtmlFormat(), underTest.getText());
        verify(messageBus).publish(new TextChangedEvent(getTexWithNumberInHtmlFormat()));
    }

    @Test
    void setText_shouldSetTheCurrentChapterAndItsTextInLeftDirection_whenCurrentChapterIsInTheOldTestamentOfTheOriginalText() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerseInRtlLanguage(1));
        verses.add(createVerseInRtlLanguage(2));
        Chapter chapter = new Chapter("ORIGINAL", "בראשית", 1, verses, "", new HashMap<>());
        chapter.setBookLabel(BookLabel.GENESIS);
        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));
        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(false));

        underTest.setText(new CurrentChapterChangingEvent(chapter));

        assertEquals(chapter, underTest.getCurrentChapter());
        assertEquals(getRtlLanguageTextWithNumberInHtmlFormat(), underTest.getText());
        verify(messageBus).publish(new TextChangedEvent(getRtlLanguageTextWithNumberInHtmlFormat()));
    }

    @Test
    void setVerseNumberHiddenState_shouldShowAndHideVerseNumbers() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerse(1));
        verses.add(createVerse(2));
        Chapter chapter = new Chapter("NIV", "Matthew", 1, verses, "", new HashMap<>());
        underTest.setText(new CurrentChapterChangingEvent(chapter));

        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));
        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(true));
        assertEquals(getTextOfTwoVersesWithoutAnythingInHtmlFormat(), underTest.getText());

        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(false));
        assertEquals(getTexWithNumberInHtmlFormat(), underTest.getText());
    }

    @Test
    void setVerseNumberHiddenState_whenLanguageIsRTL_shouldShowAndHideTheVerseNumbers() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerseInRtlLanguage(1));
        verses.add(createVerseInRtlLanguage(2));
        Chapter chapter = new Chapter("ORIGINAL", "בראשית", 1, verses, "", new HashMap<>());
        chapter.setBookLabel(BookLabel.GENESIS);
        underTest.setText(new CurrentChapterChangingEvent(chapter));
        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));

        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(true));
        assertEquals(getRtlLanguageTextWithoutAnythingInHtmlFormat(), underTest.getText());

        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(false));
        assertEquals(getRtlLanguageTextWithNumberInHtmlFormat(), underTest.getText());
    }

    @Test
    void setCrossReferencesHiddenState_shouldShowAndHideCrossReferences() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerse(1));
        verses.add(createVerse(2));
        verses.add(createVerse(3));
        Chapter chapter = new Chapter("NIV", "Matthew", 1, verses, "", new HashMap<>());
        chapter.setBookLabel(BookLabel.MATTHEW);
        when(repository.getCrossReferencesFor("MATTHEW.1.1")).thenReturn(List.of("MATTHEW.2.2"));
        when(repository.getCrossReferencesFor("MATTHEW.1.2")).thenReturn(List.of("MATTHEW.3.3-MATTHEW.4.4"));
        when(repository.getCrossReferencesFor("MATTHEW.1.3")).thenReturn(null);
        underTest.setText(new CurrentChapterChangingEvent(chapter));
        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(true));

        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(false));
        assertEquals(getTextWithCrossReferencesInHtmlFormat(), underTest.getText());

        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));
        assertEquals(getTextOfThreeVersesWithoutAnythingInHtmlFormat(), underTest.getText());
    }

    @Test
    void setCrossReferencesHiddenState_whenLanguageIsRTL_shouldShowAndHideCrossReferences() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerse(1));
        Chapter chapter = new Chapter("NIV", "Matthew", 1, verses, "", new HashMap<>());
        chapter.setBookLabel(BookLabel.MATTHEW);
        when(repository.getCrossReferencesFor("MATTHEW.1.1")).thenReturn(List.of("MATTHEW.2.2"));
        underTest.setText(new CurrentChapterChangingEvent(chapter));

        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(false));
        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(false));

        assertEquals(getTextWithNumberAndCrossReferencesInHtmlFormat(), underTest.getText());
    }

    @Test
    void setCrossReferencesHiddenState_AndSetVerseNumberHiddenState_toFalse_shouldShowBothVerseNumbersAndCrossReferences() {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(createVerseInRtlLanguage(1));
        verses.add(createVerseInRtlLanguage(2));
        Chapter chapter = new Chapter("ORIGINAL", "בראשית", 1, verses, "", new HashMap<>());
        chapter.setBookLabel(BookLabel.GENESIS);
        when(repository.getCrossReferencesFor("GENESIS.1.1")).thenReturn(List.of("MATTHEW.2.2"));
        when(repository.getCrossReferencesFor("GENESIS.1.2")).thenReturn(new ArrayList<>());
        underTest.setText(new CurrentChapterChangingEvent(chapter));
        underTest.setVerseNumberHiddenState(new HiddenVerseNumberStateChangedEvent(true));

        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(false));
        assertEquals(getRtlLanguageTextWithCrossReferencesInHtmlFormat(), underTest.getText());

        underTest.setCrossReferencesHiddenState(new HiddenCrossReferencesStateChangedEvent(true));
        assertEquals(getRtlLanguageTextWithoutAnythingInHtmlFormat(), underTest.getText());
    }

    @Test
    void getTabForegroundColor_shouldComeFromSelectedTheme() {
        Color result = underTest.getTabForegroundColor();

        assertEquals(result, selectedTheme.getTabForegroundColor());
    }

    @Test
    void getMainBackgroundColor_shouldComeFromSelectedTheme() {
        Color result = underTest.getMainBackgroundColor();

        assertEquals(result, selectedTheme.getMainBackgroundColor());
    }

    @Test
    void isDictionaryMenuItemVisible_returnsTrue_inCaseOfOriginalTextAndNewTestament() {
        when(translationService.getCurrentChaptersTranslationName()).thenReturn("ORIGINAL");
        Chapter chapter = new Chapter();
        chapter.setBookLabel(BookLabel.MATTHEW);
        when(translationService.getCurrentChapter()).thenReturn(chapter);

        boolean result = underTest.isDictionaryMenuItemVisible();

        assertTrue(result);
    }

    @Test
    void isDictionaryMenuItemVisible_returnsFalse_inCaseOfOriginalTextAndOldTestament() {
        when(translationService.getCurrentChaptersTranslationName()).thenReturn("ORIGINAL");
        Chapter chapter = new Chapter();
        chapter.setBookLabel(BookLabel.ZECHARIAH);
        when(translationService.getCurrentChapter()).thenReturn(chapter);

        boolean result = underTest.isDictionaryMenuItemVisible();

        assertFalse(result);
    }

    @Test
    void isDictionaryMenuItemVisible_returnsFalse_inCaseOfNonOriginalText() {
        when(translationService.getCurrentChaptersTranslationName()).thenReturn("ASV");

        boolean result = underTest.isDictionaryMenuItemVisible();

        assertFalse(result);
    }

    private MouseEvent createMouseEvent() {
        return new MouseEvent(new JPanel(), 0, 0, 0, 0, 0, 0, false);
    }

    private Verse createVerse(int number) {
        return new Verse(number, number + "text", "", "NIV", BookLabel.MATTHEW.name(), BookLabel.MATTHEW, 1, new ArrayList<>());
    }

    private Verse createVerseInRtlLanguage(int number) {
        return new Verse(number, "בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃", "", "ORIGINAL", "בראשית", BookLabel.GENESIS, 1, new ArrayList<>());
    }

    private String getTexWithNumberInHtmlFormat() {
        return "<div align=left id=1><span disabled class=number>1</span><span class=text>1text</span></div>"
                + "<div align=left id=2><span disabled class=number>2</span><span class=text>2text</span></div>";
    }

    private String getTextWithCrossReferencesInHtmlFormat() {
        return "<div align=left id=1><span class=text>1text</span><div disabled class=crossref><a href='Mt 2'>Mt.2.2</a></div></div>"
                + "<div align=left id=2><span class=text>2text</span><div disabled class=crossref><a href='Mt 3'>Mt.3.3-Mt.4.4</a></div></div>"
                + "<div align=left id=3><span class=text>3text</span></div>";
    }

    private String getTextWithNumberAndCrossReferencesInHtmlFormat() {
        return "<div align=left id=1><span disabled class=number>1</span><span class=text>1text</span>" +
                "<div disabled class=crossref><a href='Mt 2'>Mt.2.2</a></div></div>";
    }

    private String getTextOfTwoVersesWithoutAnythingInHtmlFormat() {
        return "<div align=left id=1><span class=text>1text</span></div>"
                + "<div align=left id=2><span class=text>2text</span></div>";
    }

    private String getTextOfThreeVersesWithoutAnythingInHtmlFormat() {
        return "<div align=left id=1><span class=text>1text</span></div>"
                + "<div align=left id=2><span class=text>2text</span></div>"
                + "<div align=left id=3><span class=text>3text</span></div>";
    }

    private String getRtlLanguageTextWithNumberInHtmlFormat() {
        return "<div align=right id=1><span disabled class=number>\u202B1\u202C</span>"
                + "<span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span></div>"
                + "<div align=right id=2><span disabled class=number>\u202B2\u202C</span>"
                + "<span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span></div>";
    }

    private String getRtlLanguageTextWithCrossReferencesInHtmlFormat() {
        return "<div align=right id=1><span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span>"
                + "\u202B<div disabled class=crossref><a href='Mt 2'>Mt.2.2</a></div>\u202C</div>"
                + "<div align=right id=2><span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span></div>";
    }

    private String getRtlLanguageTextWithoutAnythingInHtmlFormat() {
        return "<div align=right id=1><span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span></div>"
                + "<div align=right id=2><span class=text>בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת הָאָֽרֶץ׃</span></div>";
    }
}
