/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.service.LocaleService;
import org.bitbucket.lorandkutvolgyi.service.RestartService;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class VerseComparingPopupPresenterTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private TranslationService translationService;
    private VerseComparingPopupPresenter underTest;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        underTest = new VerseComparingPopupPresenter(translationService, messagesBundle);
    }

    @Test
    void getText_shouldReturnJoinedAndHtmlFormattedAndSortedVerses() {
        List<Verse> verses = new ArrayList<>();
        Verse verse1 = new Verse(1, "This is it.", "title", "translation1", BookLabel.RUTH.name(), BookLabel.RUTH, 1, new ArrayList<>());
        Verse verse2 = new Verse(1, "Ez az.", "title", "translation2", BookLabel.RUTH.name(), BookLabel.RUTH, 1, new ArrayList<>());
        Verse verse3 = new Verse(1, "Ez is az.", "title", "ORIGINAL", BookLabel.RUTH.name(), BookLabel.RUTH, 1, new ArrayList<>());
        Verse verse4 = new Verse(1, "Ez nem az.", "title", "translation3", BookLabel.RUTH.name(), BookLabel.RUTH, 1, new ArrayList<>());
        verses.add(verse1);
        verses.add(verse2);
        verses.add(verse3);
        verses.add(verse4);
        when(translationService.getCurrentChaptersTranslationName()).thenReturn("translation2");

        String text = underTest.getText(verses);

        assertEquals(getJoinedAndFormattedText(), text);
    }

    @Test
    void getTitle_shouldReturnWithCurrentChaptersBookName() {
        List<Verse> verses = new ArrayList<>();
        Verse verse1 = new Verse(1, "This is it.", "", "translation1", "Józsué könyve", BookLabel.JOSHUA, 1, new ArrayList<>());
        Verse verse2 = new Verse(1, "Ez az.", "", "translation2", BookLabel.JOSHUA.name(), BookLabel.JOSHUA, 1, new ArrayList<>());
        verses.add(verse1);
        verses.add(verse2);
        when(translationService.getCurrentChaptersTranslationName()).thenReturn("translation1");

        String title = underTest.getTitle(verses);

        assertEquals(verse1.getBookName() + " " + verse1.getChapterNumber() + ": " + verse1.getNumber(), title);
    }

    private String getJoinedAndFormattedText() {
        return "<p><b>translation2</b><br><p>Ez az.<br><p><b>Eredeti</b><br><p>Ez is az.<br><p><b>translation1</b><br><p>This is it.<br><p><b>translation3</b><br><p>Ez nem az.";
    }
}
