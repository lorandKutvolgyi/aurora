/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.DictionaryPopupContentReadyEvent;
import org.bitbucket.lorandkutvolgyi.model.DictionaryShowingEvent;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.DictionaryRepository;
import org.bitbucket.lorandkutvolgyi.service.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DictionaryPopupPresenterTest {
    private static final File home =
            new File(Objects.requireNonNull(DictionaryServiceTest.class.getClassLoader().getResource("home")).getFile());
    private static LocaleService localeService;

    @BeforeAll
    static void init() {
        localeService = new LocaleService(mock(RestartService.class));
        BookLabel.StaticFieldHolder.setAttributes(localeService);

    }

    @Test
    void setContent_shouldSetContentAndTitleAndVerseNumber_andPublishMessageForUI() {
        MessageBus messageBus = mock(MessageBus.class);
        DictionaryPopupPresenter underTest = new DictionaryPopupPresenter(messageBus, mock(DictionaryService.class));

        underTest.setContent(new DictionaryShowingEvent(getVerse(), List.of("word1 5 Abba ab-bah'\tof Chaldee origin (2); father as a vocative:--Abba.\tsee SH2")));

        assertEquals("<b>word1</b> <i>5</i> Abba ab-bah'<br>of Chaldee origin (2); father as a vocative:--Abba.<br>see SH2", underTest.getContent());
        assertEquals("1", underTest.getVerseNumber());
        assertEquals("Matthew 1:1", underTest.getTitle());
        verify(messageBus).publish(new DictionaryPopupContentReadyEvent());
    }

    @Test
    void testHasAbbreviations_whenUsingHungarianLanguage_shouldReturnTrue() {
        localeService.setSelectedLanguageWithRestart("hu");
        MessageBus messageBus = mock(MessageBus.class);
        DictionaryRepository repository = new DictionaryRepository(home.getAbsolutePath(), localeService);
        DictionaryService dictionaryService = new DictionaryService(mock(TranslationService.class), repository, mock(MessageBus.class));
        DictionaryPopupPresenter underTest = new DictionaryPopupPresenter(messageBus, dictionaryService);

        boolean result = underTest.hasAbbreviations();

        assertTrue(result);
    }

    @Test
    void testHasAbbreviations_whenUsingEnglishLanguage_shouldReturnFalse() {
        localeService.setSelectedLanguageWithRestart(Locale.ENGLISH.getLanguage());
        MessageBus messageBus = mock(MessageBus.class);
        DictionaryRepository repository = new DictionaryRepository(home.getAbsolutePath(), localeService);
        DictionaryService dictionaryService = new DictionaryService(mock(TranslationService.class), repository, mock(MessageBus.class));
        DictionaryPopupPresenter underTest = new DictionaryPopupPresenter(messageBus, dictionaryService);

        boolean result = underTest.hasAbbreviations();

        assertFalse(result);
    }

    @Test
    void testGetAbbreviations_whenUsingHungarianLanguage_shouldReturnAbbreviations() {
        localeService.setSelectedLanguageWithRestart("hu");
        MessageBus messageBus = mock(MessageBus.class);
        DictionaryRepository repository = new DictionaryRepository(home.getAbsolutePath(), localeService);
        DictionaryService dictionaryService = new DictionaryService(mock(TranslationService.class), repository, mock(MessageBus.class));
        DictionaryPopupPresenter underTest = new DictionaryPopupPresenter(messageBus, dictionaryService);

        String result = underTest.getAbbreviations();

        assertNotNull(result);
    }

    @Test
    void testGetAbbreviations_whenUsingEnglishLanguage_shouldReturnNull() {
        localeService.setSelectedLanguageWithRestart(Locale.ENGLISH.getLanguage());
        MessageBus messageBus = mock(MessageBus.class);
        DictionaryRepository repository = new DictionaryRepository(home.getAbsolutePath(), localeService);
        DictionaryService dictionaryService = new DictionaryService(mock(TranslationService.class), repository, mock(MessageBus.class));
        DictionaryPopupPresenter underTest = new DictionaryPopupPresenter(messageBus, dictionaryService);

        String result = underTest.getAbbreviations();

        assertNull(result);
    }

    private Verse getVerse() {
        Verse verse = new Verse();
        verse.setTranslationName("ORIGINAL");
        verse.setChapterNumber(1);
        verse.setBookLabel(BookLabel.MATTHEW);
        verse.setBookName("Matthew");
        verse.setNumber(1);
        verse.setText("<span class='strong' num='" + 5 + "'>word" + 1 + "</span>");
        return verse;
    }
}
