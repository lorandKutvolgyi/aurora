/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.TextPanelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SearchInputDocumentListenerTest {

    private TextPanelService textPanelService;
    private DocumentEvent documentEvent;
    private Document document;
    private SearchInputDocumentListener underTest;
    private static final int LENGTH = 4;
    private static final String TEXT = "text";

    @BeforeEach
    void setUp() {
        textPanelService = mock(TextPanelService.class);
        documentEvent = mock(DocumentEvent.class);
        document = mock(Document.class);
        underTest = new SearchInputDocumentListener(textPanelService);
    }

    @Test
    void insertUpdate_shouldGetTextFromEventAndCallTextPanelServiceWithIt() throws BadLocationException {
        when(documentEvent.getDocument()).thenReturn(document);
        when(document.getLength()).thenReturn(LENGTH);
        when(document.getText(0, LENGTH)).thenReturn(TEXT);

        underTest.insertUpdate(documentEvent);

        verify(textPanelService).searchText(TEXT);
    }

    @Test
    void insertUpdate_shouldThrowIllegalArgumentException_whenDocumentThrowBadLocationException() throws BadLocationException {
        when(documentEvent.getDocument()).thenReturn(document);
        when(document.getLength()).thenReturn(LENGTH);
        when(document.getText(0, LENGTH)).thenThrow(BadLocationException.class);

        assertThrows(IllegalArgumentException.class, () ->
                underTest.insertUpdate(documentEvent)
        );
    }

    @Test
    void removeUpdate_shouldGetTextFromEventAndCallTextPanelServiceWithIt() throws BadLocationException {
        when(documentEvent.getDocument()).thenReturn(document);
        when(document.getLength()).thenReturn(LENGTH);
        when(document.getText(0, LENGTH)).thenReturn(TEXT);

        underTest.removeUpdate(documentEvent);

        verify(textPanelService).searchText(TEXT);
    }

    @Test
    void changeUpdate_shouldDoNothing() {
        underTest.changedUpdate(documentEvent);

        verify(textPanelService, never()).searchText(any());
    }
}
