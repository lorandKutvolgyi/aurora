/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.NewChapterEvent;
import org.bitbucket.lorandkutvolgyi.model.Book;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.Translation;
import org.bitbucket.lorandkutvolgyi.repository.TranslationRepository;
import org.bitbucket.lorandkutvolgyi.service.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.mockito.Mockito.*;

public class CrossReferencesControllerTest {

    private final ResourceBundle messagesBundle = ResourceBundle.getBundle("MainMessagesBundle", new Locale("hu", "HU"));
    private CrossReferencesController underTest;
    private MessageBus messageBus;
    private TextPanelService textPanelService;
    private Chapter mt1;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        TranslationRepository translationRepository = mock(TranslationRepository.class);
        messageBus = mock(MessageBus.class);

        Book book = getBook();
        mt1 = getChapter(book);
        Translation translation = getTranslation(book);

        when(translationRepository.getDownloadedTranslations()).thenReturn(List.of(translation));
        TranslationService translationService = new TranslationService(translationRepository, messageBus);
        translationService.setTextTabManager(mock(TextTabManager.class));

        textPanelService = mock(TextPanelService.class);
        underTest = new CrossReferencesController(translationService, messagesBundle, textPanelService);
    }

    @Test
    void testClicked_shouldOpenNewChapter_inTheSameTab() {
        underTest.clicked("Mt 1");

        verify(messageBus).publish(new CurrentChapterChangingEvent(mt1));
        verify(messageBus).publish(new NewChapterEvent(mt1));
        verify(textPanelService, never()).openNewTab();
    }

    @Test
    void testClickedWithCtrl_shouldOpenNewChapter_inNewTab() {
        underTest.clickedWithCtrl("Mt 1");

        verify(textPanelService).openNewTab();
        verify(messageBus).publish(new CurrentChapterChangingEvent(mt1));
        verify(messageBus).publish(new NewChapterEvent(mt1));
    }

    private ArrayList<Book> getBooks(Book mt) {
        ArrayList<Book> books = new ArrayList<>();
        books.add(mt);
        return books;
    }

    private Translation getTranslation(Book book) {
        Translation translation = new Translation();
        translation.setName("HUNKAR");
        translation.setNewTestament(getBooks(book));
        return translation;
    }

    private Book getBook() {
        Book mt = new Book();
        mt.setLabel(BookLabel.MATTHEW);
        mt.setName("Máté Evangéliuma");
        return mt;
    }

    private Chapter getChapter(Book mt) {
        Chapter mt1 = new Chapter();
        List<Chapter> chapters = new ArrayList<>();
        mt1.setNumber(1);
        chapters.add(mt1);
        mt.setChapters(chapters);
        return mt1;
    }
}
