/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.service.VerseComparingService;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class VerseComparingListenerTest {

    @Test
    void actionPerformed_shouldDelegateTheCall() {
        VerseComparingService verseComparingService = mock(VerseComparingService.class);
        VerseComparingListener underTest = new VerseComparingListener(verseComparingService);
        Object anySource = new Object();
        int notConsumed = 0;
        String actionCommand = "anything";

        underTest.actionPerformed(new ActionEvent(anySource, notConsumed, actionCommand));

        verify(verseComparingService).setVersesToCompare(actionCommand);
    }
}
