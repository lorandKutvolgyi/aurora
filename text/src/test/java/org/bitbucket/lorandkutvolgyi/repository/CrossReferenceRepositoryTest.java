/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.repository;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CrossReferenceRepositoryTest {

    private static final File home = new File(Objects.requireNonNull(CrossReferenceRepositoryTest.class.getClassLoader().getResource("home")).getFile());

    @Test
    void testGetCrossReferencesFor_shouldReturnReferencesInCorrectOrder() {
        CrossReferenceRepository underTest = new CrossReferenceRepository(home.getAbsolutePath());

        List<String> result = underTest.getCrossReferencesFor("REVELATION.22.21");

        assertEquals(7, result.size());
        assertEquals(result.get(0), "II_THESSALONIANS.3.18");
        assertEquals(result.get(1), "ROMANS.16.20");
        assertEquals(result.get(2), "II_CORINTHIANS.13.14");
        assertEquals(result.get(3), "EPHESIANS.6.23-EPHESIANS.6.24");
        assertEquals(result.get(4), "REVELATION.1.4");
        assertEquals(result.get(5), "ROMANS.1.7");
        assertEquals(result.get(6), "ROMANS.16.23");
    }

    @Test
    void testGetCrossReferencesFor_shouldReturnEmptyString_whenThereIsNoCrossReference() {
        CrossReferenceRepository underTest = new CrossReferenceRepository(home.getAbsolutePath());

        List<String> result = underTest.getCrossReferencesFor("THERE_IS_NO_REFERENCE");

        assertNull(result);
    }
}
