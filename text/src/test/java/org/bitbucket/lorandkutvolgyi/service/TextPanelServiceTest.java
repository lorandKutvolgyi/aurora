/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import org.bitbucket.lorandkutvolgyi.controller.SearchInputKeyController;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.presenter.TextPanelPresenter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.*;

class TextPanelServiceTest {

    private TextPanelPresenter textPanelPresenter;
    private TextPanelService service;
    private SearchInputKeyController controller;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void setUp() {
        textPanelPresenter = mock(TextPanelPresenter.class);
        service = new TextPanelService(textPanelPresenter);
        controller = new SearchInputKeyController(service);
    }

    @Test
    void openNewTab_shouldDelegateTheCallToTextPanelPresenter() {
        service.openNewTab();

        verify(textPanelPresenter).openNewTab();
    }

    @Test
    void searchText_shouldCallSetSelectionPositionOnTextPanelPresenterWithEmptyList_whenTheTextDoesNotContainSearchedText() {
        when(textPanelPresenter.getText()).thenReturn("<b>In the beginning God created the heavens and the earth</b>");

        controller.search("try");

        verify(textPanelPresenter).setSelectionPosition(new ArrayList<>(), 3);
    }

    @Test
    void searchText_shouldCallSetSelectionPositionOnTextPanelPresenterWithStartPositions_whenTheTextContainsSearchedText() {
        when(textPanelPresenter.getText()).thenReturn("<b>In the beginning God created the heavens and the earth</b>");

        controller.search("ea");

        verify(textPanelPresenter).setSelectionPosition(Arrays.asList(23, 34, 49), 2);
    }
}
