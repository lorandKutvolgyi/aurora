/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MARK;
import static org.bitbucket.lorandkutvolgyi.model.BookLabel.MATTHEW;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class VerseComparingServiceTest {

    private TranslationService translationService;
    private VerseComparingService underTest;
    private MessageBus messageBus;

    @BeforeAll
    static void init() {
        BookLabel.StaticFieldHolder.setAttributes(new LocaleService(mock(RestartService.class)));
    }

    @BeforeEach
    void test() {
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
        underTest = new VerseComparingService(translationService, messageBus);
    }

    @Test
    void setVersesToCompare_shouldPublishVersesToCompareChangedEventToMessageBusTakenFromParameterInCurrentChapter() {
        Chapter currentChapter = createChapter("NIV", MATTHEW, 1);
        Translation niv = createTranslation("NIV");
        Translation ruf = createTranslation("RÚF");
        ArrayList<Translation> translations = new ArrayList<>();
        translations.add(niv);
        translations.add(ruf);
        when(translationService.getCurrentChapter()).thenReturn(currentChapter);
        when(translationService.getBook(currentChapter.getTranslationName(), currentChapter.getBookName())).thenReturn(createMatthewBook());
        when(translationService.getDownloadedTranslations()).thenReturn(translations);

        underTest.setVersesToCompare("1");
        underTest.setNextVersesToCompare();
        underTest.setPreviousVersesToCompare();

        verify(messageBus, times(2)).publish(new VerseComparingEvent(createVersesToCompare(1)));
        verify(messageBus).publish(new VerseComparingEvent(createVersesToCompare(2)));

    }

    @Test
    void setVersesToCompare_shouldNotPublishAnythingToMessageBus_whenParameterIsEmpty() {
        underTest.setVersesToCompare("");

        verify(messageBus, never()).publish(any());
    }

    private Book createMatthewBook() {
        Chapter chapter1 = createChapter("NIV", MATTHEW, 1);
        Chapter chapter2 = createChapter("NIV", MATTHEW, 1);
        return new Book(MATTHEW.name(), MATTHEW, Arrays.asList(chapter1, chapter2));
    }

    private Chapter createChapter(String translation, BookLabel bookLabel, int number) {
        return new Chapter(translation, bookLabel.name(), number, createVerses(translation), "", new HashMap<>());
    }

    private ArrayList<Verse> createVerses(String translation) {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(new Verse(1, "", "", translation, MATTHEW.name(), MATTHEW, 1, new ArrayList<>()));
        verses.add(new Verse(2, "", "", translation, MATTHEW.name(), MATTHEW, 1, new ArrayList<>()));
        return verses;
    }

    private ArrayList<Verse> createVersesToCompare(int number) {
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(new Verse(number, "", "", "NIV", MATTHEW.name(), MATTHEW, 1, new ArrayList<>()));
        verses.add(new Verse(number, "", "", "RÚF", MATTHEW.name(), MATTHEW, 1, new ArrayList<>()));
        return verses;
    }

    private Translation createTranslation(String name) {
        Translation result = new Translation();
        result.setOldTestament(new ArrayList<>());
        ArrayList<Book> newTestament = new ArrayList<>();
        ArrayList<Chapter> chaptersOfMatthew = new ArrayList<>();
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 1));
        chaptersOfMatthew.add(createChapter(name, MATTHEW, 2));
        ArrayList<Chapter> chaptersOfMark = new ArrayList<>();
        chaptersOfMark.add(createChapter(name, MARK, 1));
        newTestament.add(new Book(MATTHEW.name(), MATTHEW, chaptersOfMatthew));
        newTestament.add(new Book(MARK.name(), MARK, chaptersOfMark));
        result.setNewTestament(newTestament);
        result.setName(name);
        result.setInfo(name + " info text");
        return result;
    }
}
