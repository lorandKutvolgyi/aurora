/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.service;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.model.BookLabel;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.bitbucket.lorandkutvolgyi.model.DictionaryShowingEvent;
import org.bitbucket.lorandkutvolgyi.model.Verse;
import org.bitbucket.lorandkutvolgyi.repository.DictionaryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

public class DictionaryServiceTest {

    private static File home;
    private static LocaleService localeService;
    private DictionaryService underTest;
    private TranslationService translationService;
    private MessageBus messageBus;
    private Chapter chapter;

    @BeforeAll
    static void init() {
        localeService = mock(LocaleService.class);
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        BookLabel.StaticFieldHolder.setAttributes(localeService);
        home = new File(Objects.requireNonNull(DictionaryServiceTest.class.getClassLoader().getResource("home")).getFile());
    }

    @BeforeEach
    void setUp() {
        chapter = createChapter();
        translationService = mock(TranslationService.class);
        messageBus = mock(MessageBus.class);
    }

    @Test
    void showEntries_shouldShowDictionaryEntriesForTheSelectedVerseOfCurrentChapter() {
        String dictionaryEntry = "word1 5 Ἀββᾶ Prop.n. Father";
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showEntries("1");

        verify(messageBus).publish(eq(new DictionaryShowingEvent(getVerse(1, 5), List.of(dictionaryEntry))));
    }

    @Test
    void showEntries_shouldShowDictionaryEntriesInHungarian_whenHungarianIsSetAsLocale() {
        String dictionaryEntry = "word1 5 &#7936;&#946;&#946;&#8118;    Abba, Atya, édesapa     nm.pers.        m.indecl.";
        when(localeService.getSelectedLanguage()).thenReturn(new Locale("hu", "HU").getLanguage());
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showEntries("1");

        verify(messageBus).publish(eq(new DictionaryShowingEvent(getVerse(1, 5), List.of(dictionaryEntry))));
    }

    @Test
    void showPreviousEntries_shouldShowDictionaryEntriesForTheVerseOneBeforeTheSelectedOneOfCurrentChapter() {
        String dictionaryEntry = "word1 5 Ἀββᾶ Prop.n. Father";
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showPreviousEntriesIfExist("2");

        verify(messageBus).publish(eq(new DictionaryShowingEvent(getVerse(1, 5), List.of(dictionaryEntry))));
    }

    @Test
    void showPreviousEntries_shouldDoNothingForVerseNumberOne() {
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showPreviousEntriesIfExist("1");

        verify(messageBus, never()).publish(any());
    }

    @Test
    void showNextEntries_shouldShowDictionaryEntriesForTheVerseOneAfterTheSelectedOneOfCurrentChapter() {
        String dictionaryEntry = "word2 4 ἀβαρής, ες Adj not burdensome";
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showNextEntriesIfExist("1");

        verify(messageBus).publish(eq(new DictionaryShowingEvent(getVerse(2, 4), List.of(dictionaryEntry))));
    }

    @Test
    void showNextEntries_shouldDoNothingForVerseNumberAboveLastVerseOfCurrentChapter() {
        when(translationService.getCurrentChapter()).thenReturn(chapter);
        when(localeService.getSelectedLanguage()).thenReturn(Locale.ENGLISH.getLanguage());
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        underTest.showNextEntriesIfExist("2");

        verify(messageBus, never()).publish(any());
    }

    @Test
    void testGetAbbreviations_whenUsingHungarianLanguage_shouldReturnAbbreviations() {
        when(localeService.getSelectedLanguage()).thenReturn("hu");
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        String result = underTest.getAbbreviations();

        assertNotNull(result);
    }

    @Test
    void testGetAbbreviations_whenUsingEnglishLanguage_shouldReturnNull() {
        when(localeService.getSelectedLanguage()).thenReturn("en");
        underTest = new DictionaryService(translationService, new DictionaryRepository(home.getAbsolutePath(), localeService), messageBus);

        String result = underTest.getAbbreviations();

        assertNull(result);
    }

    private Chapter createChapter() {
        Chapter chapter = new Chapter();
        chapter.setTranslationName("ORIGINAL");
        chapter.setBookLabel(BookLabel.MATTHEW);
        chapter.setBookName("Matthew");
        chapter.setNumber(1);
        ArrayList<Verse> verses = new ArrayList<>();
        verses.add(getVerse(1, 5));
        verses.add(getVerse(2, 4));
        chapter.setVerses(verses);
        return chapter;
    }

    private Verse getVerse(int number, int strong) {
        Verse verse = new Verse();
        verse.setTranslationName("ORIGINAL");
        verse.setChapterNumber(1);
        verse.setBookLabel(BookLabel.MATTHEW);
        verse.setBookName("Matthew");
        verse.setNumber(number);
        verse.setText("<span class='strong' num='" + strong + "'>word" + number + "</span>");
        return verse;
    }
}
