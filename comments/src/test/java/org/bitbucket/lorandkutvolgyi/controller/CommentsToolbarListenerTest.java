/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.presenter.CommentsPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;

import static org.bitbucket.lorandkutvolgyi.controller.CommentsPanelCommand.*;
import static org.mockito.Mockito.*;

class CommentsToolbarListenerTest {

    private static final String TEXT = "<body>text</body>";
    private static final String CURRENT_TEXT = "currentText";
    private TranslationService translationService;
    private PanelTextReceiver panelTextReceiver;
    private CommentsToolbarListener underTest;
    private CommentsPanelPresenter presenter;

    @BeforeEach
    void setUp() {
        translationService = mock(TranslationService.class);
        presenter = mock(CommentsPanelPresenter.class);
        panelTextReceiver = spy(PanelTextReceiver.class);
        underTest = new CommentsToolbarListener(presenter, translationService, panelTextReceiver);
    }

    @Test
    void actionPerformed_shouldDelegateTheCallToTranslationServiceAndSetEditableInPresenterFalse_whenClickingOnSave() {
        when(panelTextReceiver.getCurrentText()).thenReturn(TEXT);

        underTest.actionPerformed(new ActionEvent(new Object(), 0, SAVE.name()));

        verify(translationService).saveNewComments(TEXT);
        verify(presenter).setEditable(false);
    }

    @Test
    void actionPerformed_shouldCallTranslationServiceWithCurrentCommentsAndSetEditableInPresenterFalse_whenClickingOnCancel() {
        when(translationService.getCurrentChaptersComments()).thenReturn(CURRENT_TEXT);

        underTest.actionPerformed(new ActionEvent(new Object(), 0, CANCEL.name()));

        verify(translationService).saveNewComments(CURRENT_TEXT);
        verify(presenter).setEditable(false);
    }

    @Test
    void actionPerformed_shouldSetEditableInPresenterTrue_whenClickingOnEdit() {
        underTest.actionPerformed(new ActionEvent(new Object(), 0, EDIT.name()));

        verify(presenter).setEditable(true);
    }
}
