/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import org.bitbucket.lorandkutvolgyi.event.CommentsChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.EditableChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CommentsPanelPresenterTest {

    private CommentsPanelPresenter underTest;
    private MessageBus messageBus;

    @BeforeEach
    void setUp() {
        messageBus = mock(MessageBus.class);
        underTest = new CommentsPanelPresenter(messageBus);
    }

    @Test
    void setComments_shouldPublishCommentsChangedAndEditableChangedEventsToMessageBusAfterSettingTheNewComments() {
        String comments = "comments";
        Chapter currentChapter = new Chapter("RÚF", "name", 1, new ArrayList<>(), comments,
                new HashMap<>());

        underTest.setComments(new CurrentChapterChangingEvent(currentChapter));

        verify(messageBus).publish(new CommentsChangedEvent(comments));
        verify(messageBus).publish(new EditableChangedEvent(false));
        assertEquals("comments", underTest.getComments());
    }

    @Test
    void setEditable_shouldPublishEditableChangedEventsToMessageBus() {
        underTest.setEditable(true);

        verify(messageBus).publish(new EditableChangedEvent(true));
    }
}
