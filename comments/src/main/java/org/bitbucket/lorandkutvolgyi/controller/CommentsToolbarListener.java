/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.controller;

import org.bitbucket.lorandkutvolgyi.Loggable;
import org.bitbucket.lorandkutvolgyi.presenter.CommentsPanelPresenter;
import org.bitbucket.lorandkutvolgyi.service.TranslationService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.bitbucket.lorandkutvolgyi.controller.CommentsPanelCommand.*;

public class CommentsToolbarListener implements ActionListener {

    private final CommentsPanelPresenter presenter;
    private final TranslationService translationService;
    private final PanelTextReceiver panelTextReceiver;

    public CommentsToolbarListener(CommentsPanelPresenter presenter, TranslationService translationService, PanelTextReceiver panelTextReceiver) {
        this.presenter = presenter;
        this.translationService = translationService;
        this.panelTextReceiver = panelTextReceiver;
    }

    @Loggable
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actionCommand = actionEvent.getActionCommand();
        if (actionCommand.equals(CANCEL.name())) {
            translationService.saveNewComments(translationService.getCurrentChaptersComments());
            presenter.setEditable(false);
        }
        if (actionCommand.equals(SAVE.name())) {
            String currentText = panelTextReceiver.getCurrentText();
            translationService.saveNewComments(currentText);
            presenter.setEditable(false);
        }
        if (actionCommand.equals(EDIT.name())) {
            presenter.setEditable(true);
        }
    }
}
