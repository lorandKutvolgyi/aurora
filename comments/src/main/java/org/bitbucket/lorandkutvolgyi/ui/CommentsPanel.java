/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.ui;

import dorkbox.messageBus.annotations.Subscribe;
import lombok.SneakyThrows;
import org.bitbucket.lorandkutvolgyi.controller.CommentsToolbarListener;
import org.bitbucket.lorandkutvolgyi.event.CommentsChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.EditableChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Theme;
import org.bitbucket.lorandkutvolgyi.presenter.CommentsPanelPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Objects;
import java.util.ResourceBundle;

import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static org.bitbucket.lorandkutvolgyi.controller.CommentsPanelCommand.*;

public class CommentsPanel extends JPanel {

    private final transient CommentsToolbarListener commentsToolbarListener;
    private final transient Theme selectedTheme;
    private final transient ResourceBundle messagesBundle;
    private JEditorPane panel;
    private JButton edit;
    private JButton save;
    private JButton cancel;

    public CommentsPanel(CommentsPanelPresenter presenter, CommentsToolbarListener commentsToolbarListener, Theme selectedTheme, ResourceBundle messagesBundle) {
        this.commentsToolbarListener = commentsToolbarListener;
        this.selectedTheme = selectedTheme;
        this.messagesBundle = messagesBundle;

        setLayout(new BorderLayout());
        createPanel(presenter);
        createToolbar();
    }

    @Subscribe
    public void setText(CommentsChangedEvent event) {
        panel.setText(event.getComments());
    }

    @Subscribe
    public void setEditable(EditableChangedEvent event) {
        boolean editable = event.isEditable();
        panel.setEditable(editable);
        panel.getCaret().setVisible(editable);
        edit.setEnabled(!editable);
        cancel.setEnabled(editable);
        save.setEnabled(editable);
        repaint();
    }

    String getPanelText() {
        return panel.getText();
    }


    private void createPanel(CommentsPanelPresenter presenter) {
        panel = new JEditorPane();
        panel.setContentType("text/html");
        panel.setText(presenter.getComments());
        panel.setEditable(false);
        addPasteFromClipboardListener(panel);
        add(new JScrollPane(panel), BorderLayout.CENTER);
    }

    private void createToolbar() {
        JToolBar toolBar = new JToolBar();
        toolBar.setBackground(selectedTheme.getToolBarBackgroundColor());
        toolBar.setFloatable(false);
        toolBar.setMargin(new Insets(2, 4, 2, 0));

        toolBar.add(createEditButton());
        toolBar.addSeparator(new Dimension(2, 1));
        toolBar.add(createSaveButton());
        toolBar.addSeparator(new Dimension(2, 1));
        toolBar.add(createCancelButton());
        toolBar.addSeparator(new Dimension(30, 1));
        add(toolBar, BorderLayout.NORTH);
    }

    private void addPasteFromClipboardListener(JEditorPane panel) {
        Action oldAction = panel.getActionMap().get("paste-from-clipboard");
        AbstractAction newAction = createNewAction(oldAction);
        panel.getActionMap().put("paste-from-clipboard", newAction);
    }

    private JButton createEditButton() {
        String actionCommand = EDIT.name();
        KeyStroke eWithCtrl = KeyStroke.getKeyStroke(KeyEvent.VK_E, CTRL_DOWN_MASK);
        edit = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("edit.png"))));
        edit.setToolTipText(messagesBundle.getString(actionCommand));
        edit.setActionCommand(actionCommand);
        edit.setMargin(new Insets(0, 0, 0, 0));
        edit.addActionListener(commentsToolbarListener);
        edit.registerKeyboardAction(commentsToolbarListener, actionCommand, eWithCtrl, JComponent.WHEN_IN_FOCUSED_WINDOW);
        edit.setEnabled(false);
        return edit;
    }

    private JButton createSaveButton() {
        String actionCommand = SAVE.name();
        KeyStroke sWithCtrl = KeyStroke.getKeyStroke(KeyEvent.VK_S, CTRL_DOWN_MASK);
        save = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("save.png"))));
        save.setToolTipText(messagesBundle.getString(actionCommand));
        save.setActionCommand(actionCommand);
        save.setMargin(new Insets(0, 0, 0, 0));
        save.addActionListener(commentsToolbarListener);
        save.registerKeyboardAction(commentsToolbarListener, actionCommand, sWithCtrl, JComponent.WHEN_IN_FOCUSED_WINDOW);
        save.setEnabled(false);
        return save;
    }

    private JButton createCancelButton() {
        String actionCommand = CANCEL.name();
        KeyStroke zWithCtrl = KeyStroke.getKeyStroke(KeyEvent.VK_Z, CTRL_DOWN_MASK);
        cancel = new JButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("cancel.png"))));
        cancel.setToolTipText(messagesBundle.getString(actionCommand));
        cancel.setActionCommand(actionCommand);
        cancel.setMargin(new Insets(0, 0, 0, 0));
        cancel.addActionListener(commentsToolbarListener);
        cancel.registerKeyboardAction(commentsToolbarListener, actionCommand, zWithCtrl, JComponent.WHEN_IN_FOCUSED_WINDOW);
        cancel.setEnabled(false);
        return cancel;
    }

    private AbstractAction createNewAction(Action action) {
        return new AbstractAction() {
            @Override
            @SneakyThrows
            public void actionPerformed(ActionEvent event) {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                String plainCopiedText = (String) clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor);
                clipboard.setContents(new StringSelection(plainCopiedText), null);
                action.actionPerformed(event);
            }
        };
    }
}
