/*
 *  Copyright (c) 2021-2024 Lóránd Kútvölgyi. This file is part of Aurora.
 *
 *                                 Aurora is free software: you can redistribute it and/or modify
 *                                 it under the terms of the GNU General Public License as published by
 *                                 the Free Software Foundation, either version 3 of the License, or
 *                                 (at your option) any later version.
 *
 *                                 Aurora is distributed in the hope that it will be useful,
 *                                 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                                 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                                 GNU General Public License for more details.
 *
 *                                 You should have received a copy of the GNU General Public License
 *                                 along with Aurora.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.bitbucket.lorandkutvolgyi.presenter;

import dorkbox.messageBus.MessageBus;
import dorkbox.messageBus.annotations.Subscribe;
import org.bitbucket.lorandkutvolgyi.event.CommentsChangedEvent;
import org.bitbucket.lorandkutvolgyi.event.CurrentChapterChangingEvent;
import org.bitbucket.lorandkutvolgyi.event.EditableChangedEvent;
import org.bitbucket.lorandkutvolgyi.model.Chapter;

public class CommentsPanelPresenter {

    private final MessageBus messageBus;
    private String comments;

    public CommentsPanelPresenter(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public void setEditable(boolean editable) {
        messageBus.publish(new EditableChangedEvent(editable));
    }

    public String getComments() {
        return comments;
    }

    @Subscribe
    public void setComments(CurrentChapterChangingEvent event) {
        Chapter chapter = event.getChapter();
        if (chapter != null) {
            this.comments = chapter.getComments();
            messageBus.publish(new CommentsChangedEvent(comments));
            messageBus.publish(new EditableChangedEvent(false));
        }
    }
}
